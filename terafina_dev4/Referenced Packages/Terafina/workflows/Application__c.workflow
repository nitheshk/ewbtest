<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Certificate_Support_Phone</fullName>
        <description>Field update to set support phone number for Certificate product</description>
        <field>Support_Phone_Number__c</field>
        <literalValue>CALL: 800.111.3333</literalValue>
        <name>Certificate Support Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Checking_Support_Phone</fullName>
        <description>Field update used to set phone number for checking product.</description>
        <field>Support_Phone_Number__c</field>
        <literalValue>CALL: 800.111.3333</literalValue>
        <name>Checking Support Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Credit_Card_Support_Phone</fullName>
        <description>Field update to set the support phone number for credit cards</description>
        <field>Support_Phone_Number__c</field>
        <literalValue>CALL: 800.111.3333</literalValue>
        <name>Credit Card Support Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Investment_Support_Phone</fullName>
        <description>Field update to set support phone number for investment Products</description>
        <field>Support_Phone_Number__c</field>
        <literalValue>CALL: 800.111.3333</literalValue>
        <name>Investment Support Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mortgage_Short_Form_Support_Phone</fullName>
        <description>Field Update used to set support phone number for mortgage product</description>
        <field>Support_Phone_Number__c</field>
        <literalValue>CALL: 800.111.3333</literalValue>
        <name>Mortgage Short Form Support Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mortgage_and_Other_update</fullName>
        <field>Flow_Control__c</field>
        <literalValue>Identity to AccountDetails to PurchaseDetails</literalValue>
        <name>Mortgage and Other update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Personal_Loan_Support_Phone</fullName>
        <description>Field update used to set the support phone number for Personal loans</description>
        <field>Support_Phone_Number__c</field>
        <literalValue>CALL: 800.111.3333</literalValue>
        <name>Personal Loan Support Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Saving_Support_Phone</fullName>
        <description>Field update to set the support phone number for saving product</description>
        <field>Support_Phone_Number__c</field>
        <literalValue>CALL: 800.111.2222</literalValue>
        <name>Saving Support Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Home_Equity_Phone_Number</fullName>
        <description>Field update used to set phone number for Home equity product</description>
        <field>Support_Phone_Number__c</field>
        <literalValue>CALL: 800.111.2222</literalValue>
        <name>Set Home Equity Phone Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Mortgage_Phone_Number</fullName>
        <field>Support_Phone_Number__c</field>
        <literalValue>CALL: 800.111.2222</literalValue>
        <name>Set Mortgage Phone Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Small_Business_Support_Phone</fullName>
        <description>Field Update for Small Business Support phone number</description>
        <field>Support_Phone_Number__c</field>
        <literalValue>CALL: 800.111.3333</literalValue>
        <name>Small Business Support Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_to_No_Mortgage</fullName>
        <field>Flow_Control__c</field>
        <literalValue>Identity to AccountDetails to ReviewSubmit</literalValue>
        <name>Update to No Mortgage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_to_Only_Mortgage</fullName>
        <field>Flow_Control__c</field>
        <literalValue>Identity to PurchaseDetails</literalValue>
        <name>Update to Only Mortgage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Vehicle_Loan_Support_Phone</fullName>
        <description>Field update used to set the support phone number for Vehicle loans</description>
        <field>Support_Phone_Number__c</field>
        <literalValue>CALL: 800.111.3333</literalValue>
        <name>Vehicle Loan Support Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Certificate Support Phone</fullName>
        <actions>
            <name>Certificate_Support_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow is used to set the phone number for support when Certificate Product is selected</description>
        <formula>Type_of_Certificates__c = Sub_Product__c</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Checking Support Phone</fullName>
        <actions>
            <name>Checking_Support_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow is used to set the phone number for support when Checking Product is selected</description>
        <formula>Type_of_Checking__c = Sub_Product__c</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Credit Card Support Phone</fullName>
        <actions>
            <name>Credit_Card_Support_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow is used to set the phone number for support when Credit Card Product is selected</description>
        <formula>Type_of_Credit_Cards__c = Sub_Product__c</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Home Equity Support Phone</fullName>
        <actions>
            <name>Set_Home_Equity_Phone_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow is used to set the phone number for support when Home Equity Product is selected</description>
        <formula>Type_Of_Home_Equity__c = Sub_Product__c</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Investment Support Phone</fullName>
        <actions>
            <name>Investment_Support_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow is used to set the phone number for support when Investment is selected</description>
        <formula>Type_of_Investments__c = Sub_Product__c</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Mortgage Short Form Support Phone</fullName>
        <actions>
            <name>Mortgage_Short_Form_Support_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow rule to set support phone number for Mortgage Short form</description>
        <formula>Type_of_Mortgage_Short_Application__c = Sub_Product__c</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Mortgage Support Phone</fullName>
        <actions>
            <name>Set_Mortgage_Phone_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow is used to set the phone number for support when Mortgage or Home Equity Product is selected</description>
        <formula>Type_of_Mortgage_Loan__c = Sub_Product__c</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Mortgage and Other Page Flow Control</fullName>
        <actions>
            <name>Mortgage_and_Other_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 9) AND (2 OR 3 OR 4 OR 5 OR 6 OR 7 OR 8)</booleanFilter>
        <criteriaItems>
            <field>Application__c.Type_of_Mortgage_Loan__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Application__c.Type_of_Checking__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Application__c.Type_of_Savings__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Application__c.Type_of_Certificates__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Application__c.Type_of_Credit_Cards__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Application__c.Type_of_Vehicle_Loans__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Application__c.Type_of_Personal_Loans__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Application__c.Type_of_Investments__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Application__c.Type_Of_Home_Equity__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>No Mortgage Page Flow Control</fullName>
        <actions>
            <name>Update_to_No_Mortgage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Application__c.Type_of_Mortgage_Loan__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Application__c.Type_Of_Home_Equity__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Only Mortgage Page Flow Control</fullName>
        <actions>
            <name>Update_to_Only_Mortgage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 9) AND 2 AND 3 AND 4 AND 5 AND 6 AND 7 AND 8</booleanFilter>
        <criteriaItems>
            <field>Application__c.Type_of_Mortgage_Loan__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Application__c.Type_of_Checking__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Application__c.Type_of_Savings__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Application__c.Type_of_Certificates__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Application__c.Type_of_Credit_Cards__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Application__c.Type_of_Vehicle_Loans__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Application__c.Type_of_Personal_Loans__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Application__c.Type_of_Investments__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Application__c.Type_Of_Home_Equity__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Personal Loan Support Phone</fullName>
        <actions>
            <name>Personal_Loan_Support_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow is used to set the phone number for support when Personal Loan is selected</description>
        <formula>Type_of_Personal_Loans__c = Sub_Product__c</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Saving Support Phone</fullName>
        <actions>
            <name>Saving_Support_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow is used to set the phone number for support when Saving Product is selected</description>
        <formula>Type_of_Savings__c = Sub_Product__c</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Small Business Support Phone</fullName>
        <actions>
            <name>Small_Business_Support_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow is used to set the phone number for support when Investment is selected</description>
        <formula>OR(Type_Of_Business_CDs__c =  Sub_Product__c, Type_Of_Business_Checking__c =  Sub_Product__c, Type_Of_Business_Credit_Cards__c =  Sub_Product__c, Type_Of_Business_Loans__c =  Sub_Product__c, Type_Of_Business_Savings__c =  Sub_Product__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Vehicle Loan Support Phone</fullName>
        <actions>
            <name>Vehicle_Loan_Support_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow is used to set the phone number for support when Vehicle Loan is selected</description>
        <formula>Type_of_Vehicle_Loans__c = Sub_Product__c</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
