/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class Utility {
    global static List<TF4SF__Field_Logic_New__c> fieldsAvailable;
    global static Set<String> selectedFields;
    global Utility() {

    }
    global void fieldsAvaliableList(String Product, String SubProduct) {

    }
    global Set<String> fieldsToRender(Id newAppId) {
        return null;
    }
    global void selectedFieldsSet(List<TF4SF__Field_Logic_New__c> fla) {

    }
}
