/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class Index {
    global static String ABOUT_ACCOUNT_OBJECT;
    global static String ACCOUNT_DETAILS_PAGE;
    global static String APP_RETRIEVAL_PAGE;
    global static String APP_STATUS_PAGE;
    global String appFieldsUrl {
        get;
        set;
    }
    global static String APPLICATION2_OBJECT;
    global static String APPLICATION_ACTIVITY_OBJECT;
    global static String APPLICATION_OBJECT;
    global String ApplicationCode {
        get;
        set;
    }
    global String appResourceJSON {
        get;
        set;
    }
    global static String CONFIRMATION_PAGE;
    global static String CROSS_SELL_LOGIC_OBJECT;
    global static String CROSS_SELL_PAGE;
    global static String CUSTOMER_OBJECT;
    global static String DECLARATIONS_PAGE;
    global static String DOCUSIGN_CONFIG_OBJECT;
    global static String EMPLOYMENT_OBJECT;
    global static String EMPLOYMENT_PAGE;
    global static String GET_STARTED_PAGE;
    global static String IDENTITY_OBJECT;
    global static String IDENTITY_PAGE;
    global static String INDEX_PAGE;
    global static String IP_LOOKUP_URL;
    global static String NAMESPACE {
        get;
        set;
    }
    global static String PERSONAL_INFO_PAGE;
    global static String PROPERTY_DETAILS_PAGE;
    global static String PURCHASE_DETAILS_PAGE;
    global static String REVIEW_SUBMIT_PAGE;
    global static String SAVE_FOR_LATER_PAGE;
    global static String SCHEDULE_PAGE;
    global static String SESSION_EXPIRED_PAGE;
    global static String STATUS_PORTAL_PAGE;
    global static String THANK_YOU_PAGE;
    global String Theme {
        get;
        set;
    }
    global static String UPDATE_PAGE;
    global static String VERIFY_IDENTITY_PAGE;
    @RemoteAction
    global static List<String> bringVehicleMakes(String vehicleYearSelected) {
        return null;
    }
    @RemoteAction
    global static List<String> bringVehicleModels(String vehicleYear, String vehicleMakeSelected) {
        return null;
    }
    @RemoteAction
    global static List<String> bringVehicleSubModels(String vehicleYear, String vehicleMakeSelected, String vehicleModelSelected) {
        return null;
    }
    @Deprecated
    global static Map<String,String> callDocusign(String appid) {
        return null;
    }
    @RemoteAction
    global static Map<String,String> callDocusign(String appid, String currentChannel) {
        return null;
    }
    @RemoteAction
    global static Boolean expireSession(Map<String,String> data) {
        return null;
    }
    global String findLocation() {
        return null;
    }
    @RemoteAction
    global static Map<String,String> getTerms(Map<String,String> tdata) {
        return null;
    }
    @RemoteAction
    global static Map<String,String> handleRequest(Map<String,String> tdata, String pageName) {
        return null;
    }
    @RemoteAction
    global static List<String> productList(Id appId) {
        return null;
    }
    @RemoteAction
    global static Boolean saveCardValue(Map<String,String> data) {
        return null;
    }
    global String setAppResource(Id id) {
        return null;
    }
    @RemoteAction
    global static List<String> subProductList(Id appId) {
        return null;
    }
    @RemoteAction
    global static Map<String,String> userLogin(Map<String,String> data) {
        return null;
    }
    @RemoteAction
    global static Map<String,String> validateDollarAmount(Map<String,String> tdata) {
        return null;
    }
}
