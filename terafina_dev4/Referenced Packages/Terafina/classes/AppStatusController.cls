/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class AppStatusController {
    global TF4SF__Application__c app {
        get;
        set;
    }
    global TF4SF__Application_Configuration__c appConfig {
        get;
        set;
    }
    global String applicationId {
        get;
        set;
    }
    global List<TF4SF__Application__c> applicationRecord;
    global String dob {
        get;
        set;
    }
    global String DSPTheme {
        get;
        set;
    }
    global String email {
        get;
        set;
    }
    global List<TF4SF__Identity_Information__c> identityRecord {
        get;
        set;
    }
    global String lastName {
        get;
        set;
    }
    global String ssn {
        get;
        set;
    }
    global String SupportPhoneNumber {
        get;
        set;
    }
    global String Theme {
        get;
        set;
    }
    global AppStatusController(ApexPages.StandardController controller) {

    }
    global System.PageReference StatusRet() {
        return null;
    }
}
