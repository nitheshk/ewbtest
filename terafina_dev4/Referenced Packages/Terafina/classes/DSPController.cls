/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class DSPController {
    global static String ABOUT_ACCOUNT_OBJECT;
    global String acceptLanguage {
        get;
        set;
    }
    global static String ACCOUNT_DETAILS_PAGE;
    global String allRightsReserved {
        get;
        set;
    }
    global static String APP_RETRIEVAL_PAGE;
    global static String APP_STATUS_PAGE;
    global static TF4SF__Application_Configuration__c appconfig {
        get;
        set;
    }
    global String appFieldsUrl {
        get;
        set;
    }
    global String appJSONData {
        get;
        set;
    }
    global static String APPLICATION2_OBJECT;
    global static String APPLICATION_ACTIVITY_OBJECT;
    global static String APPLICATION_CONFIGURATION_OBJECT;
    global static String APPLICATION_OBJECT;
    global String ApplicationCode {
        get;
        set;
    }
    global static Map<String,SObject> appObjects {
        get;
        set;
    }
    global static Map<String,List<SObject>> appObjectsList {
        get;
        set;
    }
    global String appResourceJSON {
        get;
        set;
    }
    global static String channel {
        get;
        set;
    }
    global static String CONFIRMATION_PAGE;
    global List<String> cookieValues {
        get;
        set;
    }
    global String copyRightText {
        get;
        set;
    }
    global static String CROSS_SELL_LOGIC_OBJECT;
    global static String CROSS_SELL_PAGE;
    global String currentPage {
        get;
        set;
    }
    global static String CUSTOMER_OBJECT;
    global static Boolean debug {
        get;
        set;
    }
    global static String DECLARATIONS_PAGE;
    global static String DOCUSIGN_CONFIG_OBJECT;
    global String DSP_Resource_Version {
        get;
        set;
    }
    global static String DYNAMIC_PAGES_PAGE;
    global static String ELIGIBILITY_PAGE;
    global static String EMPLOYMENT_OBJECT;
    global static String EMPLOYMENT_PAGE;
    global String eSignChannels {
        get;
        set;
    }
    global List<String> eSignChannelsList {
        get;
        set;
    }
    global String extensionClasses {
        get;
        set;
    }
    global static List<TF4SF__Field_Logic_New__c> fieldDetailsList;
    global static List<TF4SF__Field_Logic_New__c> fldDetailsList {
        get;
        set;
    }
    global static Id fldDetailsRecTypeId;
    global static Id fldLogicRecTypeId;
    global static List<TF4SF__Formly_Field_Types__c> formlyTypeList {
        get;
        set;
    }
    global static Map<String,TF4SF__Formly_Field_Types__c> formlyTypeMap {
        get;
        set;
    }
    global static Set<String> formlyTypeSet {
        get;
        set;
    }
    global static String GET_STARTED_PAGE;
    global static String IDENTITY_OBJECT;
    global static String IDENTITY_PAGE;
    global String idleTime {
        get;
        set;
    }
    global static String INDEX_PAGE;
    global static Boolean infoDebug {
        get;
        set;
    }
    global static String IP_LOOKUP_URL;
    global static Boolean isMember {
        get;
        set;
    }
    global static String NAMESPACE {
        get;
        set;
    }
    global String packageVersion {
        get;
        set;
    }
    global static Map<String,String> pageCodesMap {
        get;
        set;
    }
    global static String pageCodeValue {
        get;
        set;
    }
    global static List<TF4SF__Field_Logic_New__c> pageCodeValues;
    global static String pageFieldsVersion {
        get;
        set;
    }
    global static Map<String,String> pageFlowMap {
        get;
        set;
    }
    global static Map<String,Map<String,String>> pageFlowsMap {
        get;
        set;
    }
    global static Id pageFlowsRecTypeId;
    global String pageJSONData {
        get;
        set;
    }
    global static Map<String,TF4SF__Field_Logic_New__c> pageNameCodesMap {
        get;
        set;
    }
    global static Id pageNameCodesRecTypeId;
    global static String PERSONAL_INFO_PAGE;
    global static List<TF4SF__Field_Logic_New__c> pgCodeValues {
        get;
        set;
    }
    global static Id pgLabelsRecTypeId;
    global List<String> preferredLanguages {
        get;
        set;
    }
    global static String PRODUCTS_OBJECT;
    global static String PRODUCTS_PAGE;
    global static List<TF4SF__Products__c> productsList {
        get;
        set;
    }
    global String productTheme {
        get;
        set;
    }
    global static String PROPERTY_DETAILS_PAGE;
    global static String PURCHASE_DETAILS_PAGE;
    global static String REVIEW_SUBMIT_PAGE;
    global static String SAVE_FOR_LATER_PAGE;
    global static String SCHEDULE_PAGE;
    global static Id secLabelsRecTypeId;
    global static Id secOrderRecTypeId;
    global static List<TF4SF__Field_Logic_New__c> secTypeLabels {
        get;
        set;
    }
    global static String SESSION_EXPIRED_PAGE;
    global static String STATUS_PORTAL_PAGE;
    global static String THANK_YOU_PAGE;
    global String Theme {
        get;
        set;
    }
    global String Translations {
        get;
        set;
    }
    global static String UPDATE_PAGE;
    global static String VERIFY_IDENTITY_PAGE;
    global String warningTime {
        get;
        set;
    }
    @RemoteAction
    global static String bringVehicleMakes(Map<String,String> DataMap, String pageName) {
        return null;
    }
    @RemoteAction
    global static String bringVehicleModels(Map<String,String> DataMap, String pageName) {
        return null;
    }
    @RemoteAction
    global static String bringVehicleSubModels(Map<String,String> DataMap, String pageName) {
        return null;
    }
    @RemoteAction
    global static Map<String,SObject> callDocusign(Map<String,String> DataMap, String pageName) {
        return null;
    }
    global static Map<String,String> callExtensionClass(Map<String,String> dataMap, String extensionClass) {
        return null;
    }
    @RemoteAction
    global static Map<String,SObject> callExternalMethod(Map<String,String> dataMap) {
        return null;
    }
    @RemoteAction
    global static String cancelApplication(String appId, String pageName) {
        return null;
    }
    @RemoteAction
    global static Boolean expireSession(Map<String,String> data) {
        return null;
    }
    @RemoteAction
    global static String extendExpiration(String appId, String userToken) {
        return null;
    }
    global String findLocation() {
        return null;
    }
    global static String formatCurrency(Decimal i) {
        return null;
    }
    global static void genFieldGroupWrapper(System.JSONGenerator gen, String pageName, String pageCode, List<TF4SF.DSPController.DataWrapper> pageFieldsList, String expProp_section, String hideExp_sec, String template_sec, String className_fieldGroup, String focus_field) {

    }
    global static void genFieldGroup_v4(System.JSONGenerator gen, String pageName, String pageCode, List<TF4SF.DSPController.DataWrapper> pageFieldsList, String expProp_section, String hideExp_sec, String template_sec, String className_fieldGroup, String focus_field) {

    }
    global static String genJSONString_v4_1(String pageName, String pageCode, List<TF4SF__Field_Logic_New__c> pageLabels, Map<String,List<String>> fieldSetSections, Map<String,TF4SF__Field_Logic_New__c> sectionValueMap, Map<String,List<TF4SF.DSPController.DataWrapper>> pageFieldsMap) {
        return null;
    }
    global static String genJSONString_v4(String pageName, String pageCode, List<TF4SF__Field_Logic_New__c> pageLabels, List<String> sectionNames, Map<String,TF4SF__Field_Logic_New__c> sectionValueMap, Map<String,List<TF4SF.DSPController.DataWrapper>> pageFieldsMap) {
        return null;
    }
    @RemoteAction
    global static Map<String,SObject> getAppFields(Map<String,String> data, String pageName) {
        return null;
    }
    @RemoteAction
    global static List<TF4SF__State_Details__c> getCountiesByState(String state) {
        return null;
    }
    @RemoteAction
    global static Map<String,SObject> getExtensionClassList(Map<String,String> dataMap, String pageName) {
        return null;
    }
    global static Map<String,SObject> getFieldDataMap(Map<String,String> data, String pageName, Id id, Map<String,SObject> appData, Boolean schedule) {
        return null;
    }
    @RemoteAction
    global static Map<String,SObject> getFieldData(Map<String,String> data, String pageName, Id id, Map<String,SObject> appData, Boolean schedule) {
        return null;
    }
    global static String getFieldLogicQueryString(String pageName, String currentChannel, Set<String> products, Set<String> subProducts) {
        return null;
    }
    global static void getFormlyTypes() {

    }
    global static String getPageFields(String id, String pageName) {
        return null;
    }
    @RemoteAction
    global static String getPageFieldsJSON(Map<String,String> data, String pageName) {
        return null;
    }
    global static String getPageFields(String pageName, String currentChannel, Set<String> products, Set<String> subProducts, Map<String,Boolean> jointApplicants) {
        return null;
    }
    global static String getPageFields(String pageName, String currentChannel, String productsStr, Set<String> subProducts, Map<String,Boolean> jointApplicants) {
        return null;
    }
    @RemoteAction
    global static List<TF4SF__Product_Codes__c> getProductCodesList(Id appId) {
        return null;
    }
    global static List<TF4SF__Products__c> getProductsList() {
        return null;
    }
    global static List<TF4SF__Products__c> getProductsList(Id id) {
        return null;
    }
    @RemoteAction
    global static List<System.SelectOption> getProducts() {
        return null;
    }
    @RemoteAction
    global static List<TF4SF__Seg_Employer_Codes__c> getSegEmployerCodesList(Map<String,String> DataMap, String pageName) {
        return null;
    }
    @RemoteAction
    global static String getSegEmployerCodes(Map<String,String> DataMap, String pageName) {
        return null;
    }
    @RemoteAction
    global static Map<String,String> getSubProducts(String selectedProduct) {
        return null;
    }
    @RemoteAction
    global static String getTermLimits(Map<String,String> DataMap, String pageName) {
        return null;
    }
    @RemoteAction
    global static String getTerms(Map<String,String> DataMap, String pageName) {
        return null;
    }
    global static void parse(String jsonToParse) {

    }
    @RemoteAction
    global static Map<String,SObject> postData(Map<String,String> tdata, String pageName) {
        return null;
    }
    global static Map<String,SObject> processExternalResponse(Map<String,String> response, Map<String,SObject> appData) {
        return null;
    }
    @RemoteAction
    global static List<String> productList(Id appId) {
        return null;
    }
    @RemoteAction
    global static Boolean saveCardValue(Map<String,String> data) {
        return null;
    }
    @Deprecated
    global static TF4SF.DSPController.ObjectInfo selectStar(String objectName) {
        return null;
    }
    global static TF4SF.DSPController.ObjectInfo selectStar(String objectName, String fieldSetName) {
        return null;
    }
    global static void setAppErrors(Map<String,SObject> appData) {

    }
    global static void setAppObjects(Id id) {

    }
    global static void setAppObjects(Id id, String pageName) {

    }
    global static void setNameSpace() {

    }
    global static String setPageFlow(String pageName, String currentChannel, Set<String> products, Set<String> subProducts) {
        return null;
    }
    global static String setPageFlow(String pageName, String currentChannel, String productsStr, Set<String> subProducts) {
        return null;
    }
    global static void setProductsList(List<TF4SF__Products__c> newProductsList) {

    }
    @RemoteAction
    global static List<String> subProductList(Id appId) {
        return null;
    }
    @RemoteAction
    global static Boolean updateCardValue(Map<String,String> data, String pageName) {
        return null;
    }
    global static void updateDataWrapperList(String reqField, String pageName, String pageCode, String currentChannel, List<TF4SF__Field_Logic_New__c> fieldDetailsList, Map<String,String> pageLabelMap, List<TF4SF.DSPController.DataWrapper> dataWrapperList, Boolean required, Map<String,TF4SF__Field_Logic_New__c> productSpecificPicklist) {

    }
    global static void updateDataWrapperList(String reqField, String pageName, String pageCode, TF4SF__Application__c app, String objName, List<TF4SF__Field_Logic_New__c> fieldDetailsList, Map<String,String> pageLabelMap, List<TF4SF.DSPController.DataWrapper> dataWrapperList, Boolean required) {

    }
    global static void updateDataWrapperList(String reqField, String pageName, String pageCode, TF4SF__Application__c app, String objName, List<TF4SF__Field_Logic_New__c> fieldDetailsList, Map<String,String> pageLabelMap, List<TF4SF.DSPController.DataWrapper> dataWrapperList, Boolean required, Map<String,TF4SF__Field_Logic_New__c> productSpecificPicklist) {

    }
    @RemoteAction
    global static Map<String,SObject> userLogin(Map<String,String> data) {
        return null;
    }
    @RemoteAction
    global static Map<String,SObject> validateDollarAmount(Map<String,String> tdata) {
        return null;
    }
global class DataWrapper implements System.Comparable {
    global DataWrapper(String fieldKey, Boolean isReq, String type_form, String type_html, String label, List<String> pklstValues, Integer ordr, String expProp_flds, String hideExp_flds, String optTypes, String optionsTypesData, String class_name, String templ) {

    }
    global DataWrapper(String fieldKey, Boolean isReq, String type_form, String type_html, String label, List<String> pklstValues, Integer ordr, String expProp_flds, String hideExp_flds, String optTypes, String optionsTypesData, String class_name, String templ, Integer lngth) {

    }
    global DataWrapper(String fieldKey, Boolean isReq, String type_form, String type_html, String label, List<String> pklstValues, Integer ordr, String expProp_flds, String hideExp_flds, String optTypes, String optionsTypesData, String class_name, String templ, Integer lngth, String trans) {

    }
    global DataWrapper(String fieldKey, Boolean isReq, String type_form, String type_html, String label, List<String> pklstValues, Integer ordr, String expProp_flds, String hideExp_flds, String optTypes, String optionsTypesData, String class_name, String templ, Integer lngth, String trans, Id recId) {

    }
    global Integer compareTo(Object compareTo) {
        return null;
    }
}
global class ObjectInfo {
}
}
