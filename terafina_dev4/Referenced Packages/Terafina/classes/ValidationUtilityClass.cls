/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ValidationUtilityClass {
    global ValidationUtilityClass() {

    }
    global ValidationUtilityClass(ApexPages.StandardController controller) {

    }
    global Boolean validateAccountNumber(String accountNumber, String errorFieldName) {
        return null;
    }
    global Boolean validateCheckBoxRequiredFields(Boolean fieldValue, String errorFieldName) {
        return null;
    }
    global Boolean validateCurrencyRequiredFields(Decimal dec, String errorFieldName) {
        return null;
    }
    global Boolean validateEmail(String em) {
        return null;
    }
    global Boolean validateNumberRequiredFields(Decimal num, String errorFieldName) {
        return null;
    }
    global Boolean validatePhone(String ph) {
        return null;
    }
    global Boolean validateRequiredFields(String fieldValue, String errorFieldName) {
        return null;
    }
    global Boolean validateRoutingNumber(String routingNumber, String errorFieldName) {
        return null;
    }
    global Boolean validateZip(String zip) {
        return null;
    }
}
