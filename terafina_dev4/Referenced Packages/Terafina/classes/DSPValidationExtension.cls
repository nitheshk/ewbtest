/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class DSPValidationExtension {
    global DSPValidationExtension() {

    }
    global static Boolean isDataValid(TF4SF__Application__c app, String pageName, TF4SF__Application2__c app2, TF4SF__Employment_Information__c emp, TF4SF__Identity_Information__c iden, TF4SF__About_Account__c acc) {
        return null;
    }
    global static Boolean isDataValid(TF4SF__Application__c app, String pageName, TF4SF__Application2__c app2, TF4SF__Employment_Information__c emp, TF4SF__Identity_Information__c iden, TF4SF__About_Account__c acc, Map<String,String> data) {
        return null;
    }
    global static Boolean validateVehiclePurposeVechicleCollateral(TF4SF__About_Account__c acc, Set<String> fieldsRequired, Set<String> fieldsToDisplay) {
        return null;
    }
}
