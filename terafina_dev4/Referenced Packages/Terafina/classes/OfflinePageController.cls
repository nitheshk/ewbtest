/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/OfflinePage/*')
global class OfflinePageController {
    @HttpPost
    global static String generateApp() {
        return null;
    }
global class ApplicationData {
    global String applicationVersion;
    global String cellPhoneNumber;
    global String createdByBranch;
    global String createdByChannel;
    global String createdByUserId;
    global String createdEmailAddress;
    global String currentBranch;
    global String currentChannel;
    global String currentEmailAddress;
    global String currentPerson;
    global String customersCity;
    global String customersID;
    global String customersState;
    global String customersStreetAddress1;
    global String customersStreetAddress2;
    global String customersZipCode;
    global String dob;
    global String emailAddress;
    global String firstName;
    global String lastName;
    global String personId;
    global String prospectID;
    global String ssn;
    global ApplicationData() {

    }
}
}
