/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class DSPResumeApplication {
    global TF4SF__Application__c app {
        get;
        set;
    }
    global String id {
        get;
        set;
    }
    global User loggedInUser {
        get;
        set;
    }
    global static String NAMESPACE {
        get;
        set;
    }
    global String newApplicationId {
        get;
        set;
    }
    global String singlePageName {
        get;
        set;
    }
    global String usr {
        get;
        set;
    }
    global String ut {
        get;
        set;
    }
    global DSPResumeApplication(ApexPages.StandardController controller) {

    }
    global System.PageReference Resumeapp() {
        return null;
    }
}
