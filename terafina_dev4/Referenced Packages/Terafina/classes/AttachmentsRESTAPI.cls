/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/Attachments/v1/*')
global class AttachmentsRESTAPI {
    global static List<String> allowedContentTypes;
    global AttachmentsRESTAPI() {

    }
    @HttpPost
    global static String attachDoc() {
        return null;
    }
    @HttpDelete
    global static String deleteAttachment() {
        return null;
    }
    @HttpGet
    global static String getAttachment() {
        return null;
    }
}
