/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/LandingPage/v1/*')
global class LandingPageController {
    @HttpPost
    global static void beginApp2() {

    }
    global static TF4SF__Application__c setAppValues(TF4SF__Application__c app, String subProductCode, String ipaddress, User appUser) {
        return null;
    }
    global static TF4SF__Application__c setAppValues(TF4SF__Application__c app, String subProductCode, String ipaddress, User appUser, String promoCode) {
        return null;
    }
    global static String setPageUrl(TF4SF__Application__c app, Integer pgIdx) {
        return null;
    }
global class UserData {
}
}
