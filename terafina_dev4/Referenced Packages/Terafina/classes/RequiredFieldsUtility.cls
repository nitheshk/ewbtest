/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class RequiredFieldsUtility {
    global static List<TF4SF__Field_Logic_New__c> displayedFieldList;
    global static Set<String> displayedFields;
    global static List<TF4SF__Field_Logic_New__c> requiredFieldList;
    global static Set<String> requiredFields;
    global RequiredFieldsUtility() {

    }
    global void displayedFieldsAvaliable(String Product, String SubProduct) {

    }
    global void displayedFieldsSet(List<TF4SF__Field_Logic_New__c> fLogic) {

    }
    global Set<String> fieldsDisplayedAre(String appId) {
        return null;
    }
    global Set<String> fieldsRequiredAre(String appId) {
        return null;
    }
    global void requiredFieldsAvaliable(String Product, String SubProduct) {

    }
    global void requiredFieldsSet(List<TF4SF__Field_Logic_New__c> fLogic) {

    }
}
