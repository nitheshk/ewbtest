/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class Logger {
    global static String appId;
    global static List<TF4SF__Debug_Logs__c> logs;
    global static String source;
    global Logger() {

    }
    global static void addMessage(String message, String timestamp) {

    }
    global static void inputSource(String writeSource, String writeAppId) {

    }
    global static void writeAllLogs() {

    }
}
