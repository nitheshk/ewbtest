@isTest
public class AssetDeleteTest{

@isTest 
public static void method_one(){

String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
    Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
    User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
      EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US', ProfileId = p.Id,
      TimeZoneSidKey = 'America/Los_Angeles',
      UserName = uniqueUserName);

    System.runAs(u) {
    
     Map<String,String> appJointData=new Map<String,String>();

    TF4SF__Application__c app1 = new TF4SF__Application__c(TF4SF__First_Name__c='firstname',TF4SF__Product__c = 'Checking', 
    TF4SF__Sub_Product__c = 'Checking - Checking');
    insert app1;
    
    List<Asset_Details__c> lstDelete= new List<Asset_Details__c>();

    Asset_Details__c ld=new Asset_Details__c ();
    ld.Applicant_Type__c='J1';
    appJointData.put('J1','false');    
    ld.Application__c =app1.id;
    insert ld;
    
    test.startTest();
    AssetDelete ldelete=new AssetDelete ();
    AssetDelete.deleteJointAssets(app1.id,lstDelete,appJointData);
    test.stopTest();

}
}
   @isTest public static void method_two(){

String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
    Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
    User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
      EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US', ProfileId = p.Id,
      TimeZoneSidKey = 'America/Los_Angeles',
      UserName = uniqueUserName);

    System.runAs(u) {
    
     Map<String,String> appJointData=new Map<String,String>();

    TF4SF__Application__c app1 = new TF4SF__Application__c(TF4SF__First_Name__c='firstname',TF4SF__Product__c = 'Checking', 
    TF4SF__Sub_Product__c = 'Checking - Checking');
    insert app1;
    
    List<Asset_Details__c> lstDelete= new List<Asset_Details__c>();   

    Asset_Details__c ld=new Asset_Details__c ();
    ld.Applicant_Type__c='J2';
    appJointData.put('J2','false');    
    ld.Application__c =app1.id;
    insert ld;
   
    
    test.startTest();
    AssetDelete ldelete=new AssetDelete ();
    AssetDelete.deleteJointAssets(app1.id,lstDelete,appJointData);
    test.stopTest();

}
}
    @isTest public static void method_three(){

String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
    Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
    User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
      EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US', ProfileId = p.Id,
      TimeZoneSidKey = 'America/Los_Angeles',
      UserName = uniqueUserName);

    System.runAs(u) {
    
     Map<String,String> appJointData=new Map<String,String>();
 

    TF4SF__Application__c app1 = new TF4SF__Application__c(TF4SF__First_Name__c='firstname',TF4SF__Product__c = 'Checking', 
    TF4SF__Sub_Product__c = 'Checking - Checking');
    insert app1;
    
    List<Asset_Details__c> lstDelete= new List<Asset_Details__c>();

    Asset_Details__c ld=new Asset_Details__c ();
    ld.Applicant_Type__c='J3';
    ld.Application__c =app1.id;
          appJointData.put('J3','false');    
    lstDelete.add(ld);    
    insert lstDelete;
    
    test.startTest();
    AssetDelete ldelete=new AssetDelete ();
    AssetDelete.deleteJointAssets(app1.id,lstDelete,appJointData);
    test.stopTest();

}
}
}