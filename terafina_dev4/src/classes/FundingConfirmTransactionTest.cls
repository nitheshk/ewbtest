@isTest
public class FundingConfirmTransactionTest{

@isTest static void Test_one(){

 Map<String, String> tdata =new Map<String, String>();
 
 VELOSETTINGS__C vo = new VELOSETTINGS__C();
  vo.Name='test';
  vo.TokenEndPoint__c = 'rsgsdgsdf';
  vo.Request_GUID_Name__c = 'RequestUUID';
  vo.ProcessAPI_Timeout__c = 120000;
  insert vo;
  
  
      TF4SF__Application__c app = new TF4SF__Application__c();
        app.TF4SF__Application_Status__c = 'Open';
        app.TF4SF__Product__c='checking';
        app.TF4SF__User_Token__c='AnxzH/+Kmj85t2Wzkj5qETFWtwltJXsC5dcjGEgtGa89TTX4OIYGRky9UDclje65';
        app.Primary_Product_Sub_Status__c='Funding Initiated';
        insert app;
        
        TF4SF__Documentation_Request__c  docObj = new TF4SF__Documentation_Request__c ();
        docObj.TF4SF__Application__c = app.id;
        docObj.TF4SF__Type__c = 'Sample Doc';
        insert docObj;
        
      TF4SF__Application2__c app2=new TF4SF__Application2__c();
      app2.TF4SF__Application__c =app.id;
       insert app2;
      
      TF4SF__Employment_Information__c emp=new TF4SF__Employment_Information__c();
      emp.TF4SF__Application__c=app.id;
      insert emp;
      
      TF4SF__Identity_Information__c iden=new TF4SF__Identity_Information__c();
      iden.TF4SF__Application__c =app.id;
      insert iden;
      
      TF4SF__About_Account__c acc=new TF4SF__About_Account__c();
      acc.TF4SF__Application__c=app.id;
      insert acc;
    
       tdata.put('id',app.id);
      
      test.startTest();
      Test.setMock(HttpCalloutMock.class,new FundingConfirmTransactionMock());                          
      FundingConfirmTransaction fc=new FundingConfirmTransaction();
      FundingConfirmTransaction.main(tdata);
      //FundingConfirmTransaction.updateApp(app.id);
      //FundingConfirmTransaction.ConfirmTransaction(app.id,'Token');
      test.stopTest();
   

}
}