@isTest
public class SubmitApplicationTest {
    
    @isTest static void test_method_one() {
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
                          EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
                          LocaleSidKey = 'en_US', ProfileId = p.Id,
                          TimeZoneSidKey = 'America/Los_Angeles',
                          UserName = uniqueUserName);
        
        System.runAs(u) {
            
            Map<String, String> tdata = new Map<String, String>();
            
            TF4SF__Application_Configuration__c appConfig = new TF4SF__Application_Configuration__c();
            appConfig.TF4SF__key__c ='YMmKJt5QxS7KlAEASMsj4Q==';
            appConfig.TF4SF__Timeout_Seconds__c=900;
            appConfig.Name = 'key';
            appConfig.TF4SF__Application_Code__c='DS';
            appConfig.TF4SF__Theme__c='DSP';
            insert appConfig;
            Contact con = new Contact();
            con.lastName = 'test';
            insert con;
            TF4SF__Application__c app = new TF4SF__Application__c ();
            
            app.TF4SF__Application_Status__c = 'Open';
            app.TF4SF__Product__c='checking';
            app.TF4SF__Sub_Product__c='checking-Home Loan';
            app.TF4SF__User_Token__c='AnxzH/+Kmj85t2Wzkj5qETFWtwltJXsC5dcjGEgtGa89TTX4OIYGRky9UDclje65';  
            app.TF4SF__First_Name__c='TestFirst'; 
            app.TF4SF__Middle_Name__c='TestMiddle'; 
            app.TF4SF__Last_Name__c ='TestLast';
            app.TF4SF__Suffix__c ='TestSuffix';
            app.TF4SF__Street_Address_1__c = 'TestStreet1';
            app.TF4SF__Street_Address_2__c = 'TestStreet2';
            app.TF4SF__City__c = 'TestCity';
            app.TF4SF__State__c = 'TestState';
            app.TF4SF__Zip_Code__c = '44783';
            app.Ownerid=u.id;
            app.TF4SF__Primary_Phone_Number__c ='4848894344';
            app.TF4SF__Secondary_Phone_Number__c ='4443557847';
            app.TF4SF__Email_Address__c ='test@gmail.com';
            app.TF4SF__IP_Address__c = 'ipaddress';
            app.TF4SF__Current_User_Email_Address__c='standarduser@testorg.com';
            app.TF4SF__Current_Channel__c ='Online';
            app.TF4SF__Promo_Code__c='4355435';
            app.TF4SF__Current_Person__c=u.Id;
            app.TF4SF__External_App_Stage__c='455';
            app.TF4SF__IP_Address__c='test';
            app.Primary_Product_Case_Id__c='3244234';
            app.Primary_Product_Case_Status__c='open';
            app.TF4SF__User_Agent__c=u.id;
            app.TF4SF__Created_Person__c=u.id;
            app.TF4SF__Current_Branch_Name__c ='Home';
            app.TF4SF__User_Token_Expires__c=System.now();
            app.TF4SF__Current_timestamp__c=System.now(); 
            app.TF4SF__Country_Code__c = '+1';
            insert app;
            
            List<TF4SF__Debug_Logs__c> debuglst=new List<TF4SF__Debug_Logs__c>();
            TF4SF__Debug_Logs__c debug=new TF4SF__Debug_Logs__c();
            debug.TF4SF__Application__c=app.id;
            debug.TF4SF__Debug_Message__c='{"data":{"ApplicationId":"APP-M-01689","ApplicationStatus":"Pending","InstantIdResponse":{"Status":{"ConversationId":"31000105581345","RequestId":"300826705","TransactionStatus":"pending","ActionType":"answers","Reference":"Reference1"},"Products":[{"ProductType":"InstantID","ExecutedStepName":"InstantID Step","ProductConfigurationName":"ewb.onlineaccountopening_instantid","ProductStatus":"pass"},{"ProductType":"Velocity","ExecutedStepName":"PostID Step","ProductConfigurationName":"ewb.testing.onlineaccountopening_velocity_postid","ProductStatus":"pass","Items":[{"ItemName":"FREQUENCY","ItemStatus":"pass"},{"ItemName":"QUIZ","ItemStatus":"pass"}]},{"ProductType":"IdentityEvent","ExecutedStepName":"Identity_Events Step","ProductConfigurationName":"ewb.onlineaccountopening_identity_events","ProductStatus":"pass"},{"ProductType":"IIDQA","ExecutedStepName":"Authentication Step","ProductConfigurationName":"ewb.testing.redherrings_authentication_open","ProductStatus":"pending","QuestionSet":{"QuestionSetId":96387975,"Questions":[{"QuestionId":331938045,"Key":30141,"Type":"singlechoice","Text":{"Statement":"Which of the following people have you known?"},"HelpText":{"Statement":"Names may be listed as last-name first-name, include maiden names or contain slight misspellings."},"Choices":[{"ChoiceId":1633294845,"Text":{"Statement":"Aaron Loeser"}},{"ChoiceId":1633294855,"Text":{"Statement":"Jason Sharf"}},{"ChoiceId":1633294865,"Text":{"Statement":"Joshua Neuman"}},{"ChoiceId":1633294875,"Text":{"Statement":"Ruby Hahn"}},{"ChoiceId":1633294885,"Text":{"Statement":"I do not know ANY of the people listed"}}]},{"QuestionId":331938055,"Key":5551,"Type":"singlechoice","Text":{"Statement":"In which of the following cities have you attended college?"},"HelpText":{"Statement":"Select the city of the college you have attended."},"Choices":[{"ChoiceId":1633294895,"Text":{"Statement":"Bartlesville"}},{"ChoiceId":1633294905,"Text":{"Statement":"Boston"}},{"ChoiceId":1633294915,"Text":{"Statement":"Dillon"}},{"ChoiceId":1633294925,"Text":{"Statement":"Rochester"}},{"ChoiceId":1633294935,"Text":{"Statement":"None of the above"}}]},{"QuestionId":331938065,"Key":1011,"Type":"singlechoice","Text":{"Statement":"Which of the following addresses have you ever been associated with?"},"HelpText":{"Statement":"The addresses listed may be partial, misspelled or contain minor numbering variations from your actual address"},"Choices":[{"ChoiceId":1633294945,"Text":{"Statement":"12540 Scully Avenue"}},{"ChoiceId":1633294955,"Text":{"Statement":"139 South Allen Avenue"}},{"ChoiceId":1633294965,"Text":{"Statement":"477 Staunton Street"}},{"ChoiceId":1633294975,"Text":{"Statement":"618 Sonoma Street"}},{"ChoiceId":1633294985,"Text":{"Statement":"I have never been associated with any of these addresses"}}]}]}}],"PassThroughs":[{"Type":"INSTANT_ID","Data":""}]},"PinpointResponse":{"status":"ok","message":{"infection_data":{"malware":{"malware_ids":{},"info":{},"malware":{},"timestamp":"2018-07-08 17:57:07","risk_score":0,"customer_session_id":"GDn6SFI1UeCu3NN45tffGX7kE"}},"device_data":{"device_key":"N/A","is_new":null,"v3":{"screen_width":"1366","activeX":"0","dma_code":0,"ip_class":"A","continent_code":"NA","remote_addr":"63.157.54.31","city":"Rosemead","plugins":"1147357978","country_code":"US","counter":"1","last_access":"2018-07-08 19:01:47","screen_touch":"0","agent_key":"N/A","accept":"*/*","screen_dpi":"24","client_language":"en-US","post_code":"91770","screen_height":"768","mimes":"41182375","longitude":-118.0818,"latitude":34.0616,"first_time":"2018-07-08 19:01:47","cpu":"Win32","browser":"Chrome","machine_id":"N/A","digest":"e6bb8043f4acc7cd4f6fe2398547ee6d9c63b4e5","js":"1","country_name":"United States","file_upload_indicator":"0","platform":"Win32","history_length":"31","file_upload":"1","client_time_zone":"420","navigator_props":"3695045678","doc_location":"https%3A//uat-eastwestbank.cs95.force.com/","browser_version":"58.0.3029","client_charset":"UTF-8","isp":"CenturyLink","x_forwarded_for":"63.157.54.31, 10.45.214.93","accept_language":"en-US,en;q=0.8","user_agent":"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36","org":"CenturyLink","region":"CA","first_access":"2018-07-08 19:01:47","charset":1,"country":"USA","id":2536920884,"pinpoint_session":"00003005838298976559","cookie":"0.3690909790571","hashed_user_id":"N/A","application":"222916","os":"Windows 7","business":"eastwestbank","fonts":"00070C12405476E69598BA40A6E00040","area_code":"0","ip_time_zone":"-480","accept_encoding":"gzip, deflate, br","country_code3":"USA"}},"recommendation":{"risk_score":0,"reason_id":0,"recommendation":"allow","reason":""},"customer_session_id":"GDn6SFI1UeCu3NN45tffGX7kE"}}},"result":{"success":true,"code":"200","message":"Success"}}';
            debug.TF4SF__Source__c='DepositPreSubmit Response';
            insert debug;
            //debuglst.add(debug);
            
            TF4SF__Application2__c app2=new TF4SF__Application2__c();
            app2.TF4SF__Application__c=app.id;
            insert app2;
            
            TF4SF__About_Account__c acc=new TF4SF__About_Account__c();
            acc.TF4SF__Application__c=app.id;
            insert acc;
            
            TF4SF__Application_Activity__c appact=new TF4SF__Application_Activity__c();
            appact.TF4SF__Application__c=app.Id;
            appact.TF4SF__Action__c='Resumed the Application';
            appact.TF4SF__Activity_Time__c = System.now();
            appact.TF4SF__Channel__c='online';
            appact.TF4SF__Branch__c='US';  
            appact.TF4SF__Name__c =u.id;
            insert appact;
            
            TF4SF__Identity_Information__c iden = new TF4SF__Identity_Information__c ();
            iden.TF4SF__Application__c = app.id;
            iden.TF4SF__SSN_Prime__c='5656576786';
            iden.Custom_Picklist12__c='Yes';
            iden.TF4SF__Custom_Text17__c='Yes';
            iden.TF4SF__Date_of_Birth__c='07/26/1973';
            iden.TF4SF__Issue_Date__c='07/26/2002';
            iden.TF4SF__Expiry_Date__c='07/26/2020';
            iden.dual_citizenship_country__c = 'India';
            insert iden;
            
            TF4SF__Employment_Information__c em = new TF4SF__Employment_Information__c();
            em.TF4SF__Application__c = app.id;
            em.TF4SF__Occupation__c='test';
            insert em;
            
            TF4SF__Debug_Logs__c db = new TF4SF__Debug_Logs__c();
            db.TF4SF__Application__c = app.id;
            insert db;
            
            List<TF4SF__Product_Codes__c> pclst=new List<TF4SF__Product_Codes__c>();
            TF4SF__Product_Codes__c pc=new TF4SF__Product_Codes__c();
            pc.TF4SF__Sub_Product_Code__c='Checking';
            pc.TF4SF__Sub_Product__c='checking';
            pc.TF4SF__Product__c='Homeloan';
            pc.Product_Code_Value__c='test';
            pc.Name='Test';
            insert pc;
            pclst.add(pc);
            
            VELOSETTINGS__C vo = new VELOSETTINGS__C();
            vo.Name='test';
            vo.Request_GUID_Name__c = 'RequestUUID';
            vo.ProcessAPI_Timeout__c = 120000;
            vo.ClientId__c = '8er2cx5d37h96GtJk9b9';
            vo.ClientSecret__c = '8er2cx5d37h96GtJk9b9';
            vo.EndPoint__c = 'https://devopenapi.velobank.com/';
            vo.FundingEndpoint__c ='https://devopenapi.velobank.com/';
            vo.TokenEndPoint__c = 'https://openapiservices.eastwestbank.com/AuthorizationServer/api/token/client/jwt';
            insert vo;
            
            tdata.put('id',(String)app.id);
            test.startTest();
            Test.setMock(HttpCalloutMock.class, new SubmitApplication_Mock()); 
            SubmitApplication.main(tdata);
            test.stopTest();
        }
    }
}