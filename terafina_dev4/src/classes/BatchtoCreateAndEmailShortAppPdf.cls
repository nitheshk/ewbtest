global class BatchtoCreateAndEmailShortAppPdf implements
Database.Batchable <sObject> , Database.Stateful, Database.AllowsCallouts { 


  global Database.QueryLocator start(Database.BatchableContext bc) {
   
    String  query =  'SELECT Id, TF4SF__Purchase_P__c, TF4SF__Total_Loan_Amount__c,TF4SF__Application__c,'; 
            query +=  ' TF4SF__Application__r.TF4SF__Product__c, TF4SF__Application__r.TF4SF__Sub_Product__c ,';  
            query +=  ' TF4SF__Application__r.TF4SF__First_Name__c ,TF4SF__Application__r.TF4SF__Last_Name__c,';
            query +=  ' TF4SF__Application__r.TF4SF__Email_Address__c';
            query +=  ' FROM TF4SF__About_Account__c';          
            query += ' WHERE TF4SF__Application__r.TF4SF__Product__c = \'Home Loan\'';
            query += ' AND TF4SF__Application__r.TF4SF__Sub_Product__c = \'Home Loan - Short App\'';
            query += ' AND TF4SF__Application__r.Pre_Qual_PDF_Created__c = False';

        return Database.getQueryLocator(query);      
  }

  global void execute(Database.BatchableContext bc, List<TF4SF__About_Account__c> scope) {
    List<Attachment> lstAttachment = new List<Attachment>();
    //process each batch of records
    try {

        //Add all the Application ID's to a list
          List<Id> lstId = new List<Id>();
          for(TF4SF__About_Account__c obj : scope){
            lstId.add(obj.TF4SF__Application__c);
          }
          
          //Call PreQualificationLetter to generate PDF and email it to application
          PreQualificationLetterController.PreQualificationLetter(scope);


          //Query all the applications to update Pre_Qual_PDF_Created__c flag
          List<TF4SF__Application__c> lstApp = [SELECT Id,Pre_Qual_PDF_Created__c 
                                                FROM TF4SF__Application__c 
                                                WHERE Id IN : lstId ];

          for(TF4SF__Application__c obj : lstApp){
            obj.Pre_Qual_PDF_Created__c = True;
          }

          update lstApp;
        
        if(Test.isRunningTest()){integer a = 10/0;}
          
          
        } catch (exception e) {
            System.debug('Exception ==>' + e.getMessage());
        } finally {
         
        }       
  }

  global void finish(Database.BatchableContext bc) {
  } 
   
  }