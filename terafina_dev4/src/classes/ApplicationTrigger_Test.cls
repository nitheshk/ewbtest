@isTest
public class ApplicationTrigger_Test{
@isTest
public static void test1(){

 String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
 Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
 User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
      EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US', ProfileId = p.Id,
      TimeZoneSidKey = 'America/Los_Angeles',
      UserName = uniqueUserName);

    System.runAs(u) {
        
     TF4SF__Application__c app = new TF4SF__Application__c(
        TF4SF__External_App_Stage__c = '1', TF4SF__External_AppStage_CrossSell1__c = 'Pending',
        TF4SF__External_AppStage_CrossSell2__c = 'Pending', TF4SF__External_AppStage_CrossSell3__c = 'Pending',
        TF4SF__Primary_Offer__c = 'Credit Cards - Visa Platinum', 
        TF4SF__Second_Offer__c = 'Personal Loans - Certificate Secured',TF4SF__Third_Offer__c = 'Vehicle Loans - Auto',
        TF4SF__Current_Person__c = u.Id, TF4SF__Custom_Checkbox4__c = true, TF4SF__Current_Branch_Name__c = 'Personal-Loans',
        TF4SF__Mailing_Zip_Code__c='57423', TF4SF__Zip_Code_Prev__c = '12345', TF4SF__Preferred_Contact_Method__c = 'Work',
        TF4SF__Membership_Qualification_Type__c = 'Qualifying Member or Roommate', TF4SF__Custom_Text5__c = 'test',TF4SF__Last_Name_J__c='\\',TF4SF__Middle_Name_J__c='\\',
        TF4SF__First_Name_J__c='\\',TF4SF__Tertiary_Phone_Number__c = '', OwnerId = u.Id,  TF4SF__First_Name__c = '\\', 
        TF4SF__Last_Name__c = '\\', TF4SF__Email_Address__c = 'test@test.com',TF4SF__First_Name_J2__c='\\',TF4SF__Middle_Name_J2__c='\\',
        TF4SF__Last_Name_J2__c='\\',TF4SF__First_Name_J3__c='\\',TF4SF__Middle_Name_J3__c='\\',TF4SF__Last_Name_J3__c='\\',TF4SF__Product__c = 'ALL', TF4SF__Sub_Product__c = 'Checking - Checking',TF4SF__City__c = 'There',
        TF4SF__Primary_Phone_Number__c = '8889557212', TF4SF__State__c = 'AZ', TF4SF__Street_Address_1__c = '123 That Street', 
        TF4SF__Street_Address_2__c = '', TF4SF__Zip_Code__c = '89898',TF4SF__Months__c = '5', TF4SF__Middle_Name__c = '\\',
        TF4SF__Application_Status__c = 'Submitted', TF4SF__Primary_Product_Status__c = 'Pending Review', TF4SF__Years__c = 2.0, 
        TF4SF__Secondary_Phone_Number__c = '8885551313');
        insert app;
        
        List<TF4SF__Application__c> appList = new List<TF4SF__Application__c>();
        appList.add(app);
        
    List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        
    Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
    email.setReplyTo('autobuyingservice@patelco.org');
    email.setTargetObjectId(UserInfo.getUserId());
    email.setWhatId(app.Id);
    email.setSaveAsActivity(false);
    email.setBccSender(false);
    email.setTemplateId(app.id);
    mails.add(email);
    
        
      Blob b = Blob.valueOf('Test Data');
      Attachment attachment = new Attachment();
      attachment.ParentId = app.Id;
      attachment.Name = 'CD Account Agreement.pdf';
      attachment.Body = b;
      insert(attachment);
       
}
} 
    @isTest
public static void test2(){

 String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
 Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
 User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
      EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US', ProfileId = p.Id,
      TimeZoneSidKey = 'America/Los_Angeles',
      UserName = uniqueUserName);

    System.runAs(u) {
        
     TF4SF__Application__c app = new TF4SF__Application__c(
        TF4SF__External_App_Stage__c = '1', TF4SF__External_AppStage_CrossSell1__c = 'Pending',
        TF4SF__External_AppStage_CrossSell2__c = 'Pending', TF4SF__External_AppStage_CrossSell3__c = 'Pending',
        TF4SF__Primary_Offer__c = 'Credit Cards - Visa Platinum', 
        TF4SF__Second_Offer__c = 'Personal Loans - Certificate Secured',TF4SF__Third_Offer__c = 'Vehicle Loans - Auto',
        TF4SF__Current_Person__c = u.Id, TF4SF__Custom_Checkbox4__c = true, TF4SF__Current_Branch_Name__c = 'Personal-Loans',
        TF4SF__Mailing_Zip_Code__c='57423', TF4SF__Zip_Code_Prev__c = '12345', TF4SF__Preferred_Contact_Method__c = 'Work',
        TF4SF__Membership_Qualification_Type__c = 'Qualifying Member or Roommate', TF4SF__Custom_Text5__c = 'test',TF4SF__Last_Name_J__c='\\',TF4SF__Middle_Name_J__c='\\',
        TF4SF__First_Name_J__c='\\',TF4SF__Tertiary_Phone_Number__c = '', OwnerId = u.Id,  TF4SF__First_Name__c = '\\', 
        TF4SF__Last_Name__c = '\\', TF4SF__Email_Address__c = 'test@test.com',TF4SF__First_Name_J2__c='\\',TF4SF__Middle_Name_J2__c='\\',
        TF4SF__Last_Name_J2__c='\\',TF4SF__First_Name_J3__c='\\',TF4SF__Middle_Name_J3__c='\\',TF4SF__Last_Name_J3__c='\\',TF4SF__Product__c = 'Home Loan', TF4SF__Sub_Product__c = 'Home Loan',TF4SF__City__c = 'There',
        TF4SF__Primary_Phone_Number__c = '8889557212', TF4SF__State__c = 'AZ', TF4SF__Street_Address_1__c = '123 That Street', 
        TF4SF__Street_Address_2__c = '', TF4SF__Zip_Code__c = '89898',TF4SF__Months__c = '5', TF4SF__Middle_Name__c = '\\',
        TF4SF__Application_Status__c = 'Submitted', TF4SF__Primary_Product_Status__c = 'Pending Review', TF4SF__Years__c = 2.0, 
        TF4SF__Secondary_Phone_Number__c = '8885551313');
        insert app;
        
        List<TF4SF__Application__c> appList = new List<TF4SF__Application__c>();
        appList.add(app);
        
    List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        
    Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
    email.setReplyTo('autobuyingservice@patelco.org');
    email.setTargetObjectId(UserInfo.getUserId());
    email.setWhatId(app.Id);
    email.setSaveAsActivity(false);
    email.setBccSender(false);
    email.setTemplateId(app.id);
    mails.add(email);
    
        
      Blob b = Blob.valueOf('Test Data');
      Attachment attachment = new Attachment();
      attachment.ParentId = app.Id;
      attachment.Name = 'CD Account Agreement.pdf';
      attachment.Body = b;
      insert(attachment);
       
}
} 
}