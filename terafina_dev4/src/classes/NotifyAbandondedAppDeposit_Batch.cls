//Batch class to notify to the abandoned apps 
global class NotifyAbandondedAppDeposit_Batch implements Database.Batchable<sObject>{
    global Database.QueryLocator start(Database.BatchableContext BC) {
        //**********Get records from object Application 
        return Database.getQueryLocator('SELECT Id, TF4SF__Email_Address__c,Abandoned_Email__c,Time_Diff__c,Date_Elapsed_SFL_Abandoned__c FROM TF4SF__Application__c where  (TF4SF__Product__c=\'Savings\' OR TF4SF__Product__c=\'Checking\') and Abandoned_Email__c =true AND (TF4SF__Application_Status__c =\'Save for Later\' OR TF4SF__Application_Status__c=\'Abandoned\')');}
    
    global void execute(Database.BatchableContext BC, List<TF4SF__Application__c> listApp) {
        
        //*****call documentStipulationRequest_Utilityclass
        EmailNotificationDeposit_Utility utility = new EmailNotificationDeposit_Utility();
        utility.sendEmail(listApp);
    }
    
    global void finish(Database.BatchableContext BC) { 
        //method sends the email
       ScheduleFollowupEmailBatchApexDeposit.SchedulerMethod();
    }
}