public class IncomeDelete {

    //This is to delete the joint applicants Income details if Joints are added to the application and then removed
    public static void deleteJointIncome(String appId,List<Income_Details__c> lstDelete,Map<String,String> appJointData) {

        List<Income_Details__c> lstResult = new List<Income_Details__c>();
        List<Income_Details__c> lstIncomeDeleteJ1 = new List<Income_Details__c>();
        List<Income_Details__c> lstIncomeDeleteJ2 = new List<Income_Details__c>();
        List<Income_Details__c> lstIncomeDeleteJ3 = new  List<Income_Details__c>();

        String jointOne = ( String.isNotBlank(appJointData.get('J1')) ? appJointData.get('J1') : 'True');
        String jointTwo= ( String.isNotBlank(appJointData.get('J2')) ? appJointData.get('J2') : 'True');
        String jointThree = ( String.isNotBlank(appJointData.get('J3')) ? appJointData.get('J3') : 'True');

        System.debug('appId ==>' + appId);
        System.debug('jointOne ==>' + jointOne);
        System.debug('jointTwo ==>' + jointTwo);
        System.debug('jointThree ==>' + jointThree);

        try{
                //Get all the incomes of the application
                if (String.isNotBlank(appId)) {
                    lstResult = [SELECT Id,Applicant_Type__c FROM Income_Details__c WHERE Application__c =: appId LIMIT 5000];                  
                }
                else if (lstDelete.size() > 0)
                {
                    lstResult = lstDelete.clone();
                }

                if (lstResult.size() > 0) {

                    for( Income_Details__c objIncome : lstResult ) {    

                            //Store the J1 income in a list                             
                            if ( objIncome.Applicant_Type__c == 'J1') {
                                lstIncomeDeleteJ1.add(objIncome);
                            }

                            //Store the J2 income in a list                             
                            if ( objIncome.Applicant_Type__c == 'J2') {
                                lstIncomeDeleteJ2.add(objIncome);
                            }

                            //Store the J3 income in a list                             
                            if ( objIncome.Applicant_Type__c == 'J3') {
                                lstIncomeDeleteJ3.add(objIncome);
                            }
                    }

                    System.debug('lstIncomeDeleteJ2 ==>' + lstIncomeDeleteJ2);

                    //**********************************  Joint 1 ***************************************************
                    //If Joint 1 is removed, delete Joint 1 income from Income details objects
                    if (jointOne == 'false' ) { 
                        
                        if (lstIncomeDeleteJ1.size() > 0) { 
                            delete lstIncomeDeleteJ1;
                        }
                                                                    
                    }

                    //**********************************  Joint 1 End ***************************************************


                    //**********************************  Joint 2 ***************************************************
                    //If Joint 2 is removed, delete Joint 2 income from Income details objects              
                    if (jointTwo  == 'false') {
                        System.debug('Inside joint 2 delete ==>' + lstIncomeDeleteJ2);
                        if (lstIncomeDeleteJ2.size() > 0) { 
                            delete lstIncomeDeleteJ2;
                        }
                    }
                    

                    //**********************************  Joint 2 End  ***************************************************


                    //**********************************  Joint 3 ***************************************************

                    //If Joint 3 is removed, delete Joint 3 income from Income details objects  
                    if (jointThree  == 'false') {       
                        
                        if (lstIncomeDeleteJ3.size() > 0) { 
                            delete lstIncomeDeleteJ3;
                        }
                        
                    }

                    //**********************************  Joint 3 End  ***************************************************

                }



            }catch(exception ex) {
                System.debug('Excpetion in deleteJointIncome ' + ex.getMessage());
                System.debug('Excpetion in deleteJointIncome line number' + ex.getLineNumber());                
            }

    }
    
  
}