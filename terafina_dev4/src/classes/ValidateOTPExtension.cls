global with sharing class ValidateOTPExtension implements TF4SF.DSP_Interface {

global static Map<String, String> main(Map<String, String> tdata) {

        Map<String, String> data = new Map<String, String>();
        Map<String, String> verifyEnroll = new Map<String, String>();
        String Options = '';
        String appId = tdata.get('id');
        List<String> appIdList = new List<String>();
        appIdList.add(appId);
        String otpCode= tdata.get('otp');
        String phoneNo='', emailId='',valid= 'false', isOther='0',message='',status='0', factorId='', oktauserId='', smsId='',oktaStatus='';
        String abandonDspId='',userId='', responsesmsId='', responseStatus='';
        String factorResult = '';
        List<OKTA__c> oktaList = new List<OKTA__c>();
         //Based on current DSP Id get the Email & retrive recent Abandon App
         //Current DSP Id Email will have retrieval Email Id(Abandon App)
        try
        {
         List <TF4SF__Application__c>  listCurrentApp = [SELECT Id, TF4SF__Application_Status__c,
         TF4SF__Sub_Product__c ,TF4SF__Email_Address__c,Retrieve_Email__c, TF4SF__Primary_Phone_Number__c, OKTA_UserId__c, OKTA_factorId__c, OKTA_SMSId__c, OKTA_Status__c
         FROM TF4SF__Application__c 
         where Id=:appId];
        
        if (!listCurrentApp .isempty()) {
        
            emailId = listCurrentApp[0].Retrieve_Email__c;
        if (emailId!='') {    
            
            oktaList = [SELECT Id, Email_Address__c, Factor_Id__c, SMS_Id__c, Status__c, User_Id__c FROM OKTA__c WHERE Email_Address__c =: emailId.toLowerCase() LIMIT 1];
            if (!oktaList.isempty()) {
                factorId = oktaList[0].factor_Id__c;
                oktauserId = oktaList[0].User_Id__c;
                smsId = oktaList[0].SMS_Id__c;
                oktaStatus = oktaList[0].Status__c;
            }
            
            //Get recent abandoned app based on email id
            List <TF4SF__Application__c>  listApp = [SELECT Id, 
            TF4SF__Application_Status__c,TF4SF__Sub_Product__c ,TF4SF__Email_Address_J__c, TF4SF__Email_Address__c,TF4SF__Primary_Phone_Number__c, OKTA_UserId__c, OKTA_factorId__c, OKTA_SMSId__c, OKTA_Status__c,
            (select  Id,TF4SF__Date_of_Birth__c,TF4SF__Country_of_Citizenship__c,TF4SF__SSN_Last_Four_PA__c from TF4SF__Identity_Information__r) 
            FROM TF4SF__Application__c where TF4SF__Email_Address__c=: emailId AND 
            (TF4SF__Application_Status__c='Open' OR TF4SF__Application_Status__c='Abandoned' OR TF4SF__Application_Status__c='Save for Later')
            order by createdDate DESC limit 1];
            
            if (!listApp .isempty()) {            
                
                if (String.isNotBlank(oktaStatus)) {
                    if (oktaStatus == 'ACTIVE') {
                        if (String.isNotBlank(smsId)) {
                            factorResult = verify(listApp[0].id, AuthToken.Auth(), oktauserId, otpCode, smsId);
                            responsesmsId = oktaList[0].SMS_Id__c;
                            responseStatus = oktaList[0].Status__c;
                            if (factorResult == 'SUCCESS') {
                                valid='true';
                                status='200';    
                            }
                            
                        }
                    } else {
                       verifyEnroll = verifySMSRegisteration(listApp[0].id, AuthToken.Auth(), oktauserId, otpCode, factorId);
                       responsesmsId = verifyEnroll.get('smsId');
                       responseStatus = verifyEnroll.get('oktaStatus');
                       if(String.isNotBlank(responsesmsId)) {
                        valid='true';
                        status='200';
                       }
                    }
                }

                phoneNo=listApp[0].TF4SF__Primary_Phone_Number__c;                
                abandonDspId=listApp[0].Id;  
                        
                } 
            }
        }   
              
        }
        catch(Exception ex)
        {
            message=ex.getMessage();
        }
        message=emailId;
        userId=UserInfo.getUserId(); 

        Options += '{';
        Options += '"status": "'+status+'",';        
        Options += '"statusMessage": "'+valid+'",'; 
        Options += '"AbandonDspId": "'+abandonDspId+'",'; 
        Options += '"UserId": "'+userId+'",';
        Options += '"message": "'+message+'"'; 
        Options += '}';

        data.put('status',Options);
        if (status == '200'){
            EmailValidationExtension.updateOkta(abandonDspId, emailId, oktauserId, factorId, responseStatus, responsesmsId);
        }
        return data;
}



public static Map<String, String> verifySMSRegisteration(String appId, String Token, String userId, String PhoneCode, String factorId) {
    //Phone = Phone.replaceAll('(', '').replaceAll(') ','').replaceAll('-','').trim();
    Map<String, String> verifyMap = new Map<String, String>();
    String smsId = '';
    String oktaStatus = '';
    Http h = new Http();
    HttpRequest req = new HttpRequest();
    VELOSETTINGS__C velo = VELOSETTINGS__C.getOrgDefaults();
    Integer timeOut = Integer.ValueOf(velo.ProcessAPI_Timeout__c);
    String GUID = GUIDGenerator.getGUID();
    req.setHeader(velo.Request_GUID_Name__c,GUID);
    req.setTimeout(timeOut);
    req.setEndpoint(velo.Endpoint__c+'onb/api/process/iam/factors/sms/verify');
    req.setHeader('Authorization','Bearer '+Token);
    req.setHeader('Accept','application/json');
    req.setHeader('Content-Type', 'application/json');
    req.setMethod('POST');
    String body = '{';
    body += '"userId" : "'+userId+'",';
    body += '"factorId": "'+factorId+'",';
    body += '"passCode": "'+PhoneCode+'"';
    body += '}';

           
    req.setBody(body);
    HttpResponse res = h.send(req);
    system.debug('Res is '+res.getbody());

    //List<TF4SF__Application__c> app = [SELECT Id, OKTA_User_Id__c FROM TF4SF__Application__c WHERE Id =: appId LIMIT 1];
    //Map<String, Object> OktaMap = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
    //app[0].OKTA_User_Id__c = String.valueOf(OktaMap.get('UserUID'));
    //update app;
    Map<String, Object> OktaMap = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
    smsId = String.valueOf(OktaMap.get('id'));
    oktaStatus = String.valueOf(OktaMap.get('status'));
    verifyMap.put('smsId', smsId);
    verifyMap.put('oktaStatus', oktaStatus);
    try {
        DebugLogger.InsertDebugLog(appId, GUID, 'ValidateOTPExtension verifySMSRegisteration Request GUID');
        DebugLogger.InsertDebugLog(appId, req.getBody(), 'ValidateOTPExtension verifySMSRegisteration Request');
        DebugLogger.InsertDebugLog(appId, res.getBody(), 'ValidateOTPExtension verifySMSRegisteration Response');
    } catch (Exception e) {
        system.debug('exception '+e.getMessage());
    }
    return verifyMap;
}

public static string verify(String appId, String Token, String userId, String PhoneCode, String factorId) {
    
    String result = '';
    Http h = new Http();
    HttpRequest req = new HttpRequest();
    VELOSETTINGS__C velo = VELOSETTINGS__C.getOrgDefaults();
    Integer timeOut = Integer.ValueOf(velo.ProcessAPI_Timeout__c);
    String GUID = GUIDGenerator.getGUID();
    req.setHeader(velo.Request_GUID_Name__c,GUID);
    req.setTimeout(timeOut);
    req.setEndpoint(velo.Endpoint__c+'onb/api/process/iam/verify');
    req.setHeader('Authorization','Bearer '+Token);
    req.setHeader('Accept','application/json');
    req.setHeader('Content-Type', 'application/json');
    req.setMethod('POST');
    String body = '{';
    body += '"userId" : "'+userId+'",';
    body += '"factorId": "'+factorId+'",';
    body += '"passCode": "'+PhoneCode+'"';
    body += '}';

           
    req.setBody(body);
    HttpResponse res = h.send(req);
    //UpdatePromo = res.getBody();
    system.debug('Res is '+res.getbody());
    try {
        DebugLogger.InsertDebugLog(appId, GUID, 'ValidateOTPExtension Challenge Request GUID');
        DebugLogger.InsertDebugLog(appId, req.getBody(), 'ValidateOTPExtension Challenge Request');
        DebugLogger.InsertDebugLog(appId, res.getBody(), 'ValidateOTPExtension Challenge Response');
    } catch (Exception e) {
        system.debug('exception '+e.getMessage());
    }
    //List<TF4SF__Application__c> app = [SELECT Id, OKTA_User_Id__c FROM TF4SF__Application__c WHERE Id =: appId LIMIT 1];
    Map<String, Object> OktaMap = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
    result = String.valueOf(OktaMap.get('factorResult'));
    return result;
}

}