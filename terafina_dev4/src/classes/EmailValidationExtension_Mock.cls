@isTest
global class EmailValidationExtension_Mock implements HttpCalloutMock {
// Implement this interface method
  global HTTPResponse respond(HTTPRequest req) {
    // Optionally, only send a mock response for a specific endpoint
    // and method.
    // Create a fake response
    HttpResponse res = new HttpResponse();
    res.setHeader('Content-Type', 'application/json');
    res.setBody('{"FirstName":"TestFirst","PhoneNumber":"Phone","LastName":"TestLast","Email":"test@gmail.com","Status":"InProgress","RequestUUID":"Testuid","Token":"is invalid","oktaStatus":" ","userId":"userId","passCode":"Testpasscode","factorId":"factorId"}');
    res.setStatusCode(200);
    return res;
}
}