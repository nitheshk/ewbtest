global class BatchToSendDocuments implements Database.Batchable<sObject>, Database.AllowsCallouts {
  
    String query;
    List<Attachment> newAttList = new List<Attachment>(); 
    Set<Id> discIdSet = new Set<Id>();
      
    global BatchToSendDocuments() { }
  
    global Database.QueryLocator start(Database.BatchableContext BC) {
        query = 'SELECT Id, Name, TF4SF__Product__c, TF4SF__Sub_Product__c, Documents_Posted__c, Documents_Posted_Timestamp__c, TF4SF__Primary_Product_Status__c, TF4SF__External_App_Stage__c, TF4SF__External_App_ID__c FROM TF4SF__Application__c WHERE Documents_Posted__c = false ORDER BY CreatedDate DESC';
        return Database.getQueryLocator(query);
    }

        global void execute(Database.BatchableContext BC, List<TF4SF__Application__c> AppList) {
        try {
            system.debug('AppList :' + AppList);
            Set<Id> appIdSet = new Set<Id>();
            List<String> disclosureNames = new List<String>();
            List<Attachment> attList = new List<Attachment>();
            Map<String, TF4SF__Application__c> appsMap = new Map<String, TF4SF__Application__c>();
            List<TF4SF__Application__c> appsUpdateList = new List<TF4SF__Application__c>();
            for (TF4SF__Application__c app : AppList) {
                
                if (app.TF4SF__Product__c == 'Checking') {
                    disclosureNames.add('Premier Checking');
                } else {
                    disclosureNames.add('Premier Saving');
                }
                
                appIdSet.add(app.Id);
                appsMap.put(app.Id, app);
            }
            
            disclosureNames.add('Deposit Agreement');
            disclosureNames.add('Premier Saving');
            disclosureNames.add('Fee Schedule');
            
            List<TF4SF__Disclosure__c> disList = [SELECT Id, Name FROM TF4SF__Disclosure__c WHERE Name IN: disclosureNames];
            for (TF4SF__Disclosure__c disc : disList) {
                discIdSet.add(disc.Id);
                appIdSet.add(disc.Id);
            }
            String fileNames = Label.Synergy_Job_File_Names;
            Set<String> fileNamesList =  new Set<String>(fileNames.split(','));
            attList = [SELECT Id, Body, Name, ContentType, ParentId FROM Attachment WHERE ParentId IN: appIdSet AND Name =: fileNamesList];
            for (Attachment att : attList) {
                TF4SF__Application__c appToUpdate = appsMap.get(att.ParentId);
                //Reset previously set flag if one upload success but other fails
                appToUpdate.Documents_Posted__c = false;

                String depositProds = Label.Deposti_Product_Types ;
                String product = appToUpdate.TF4SF__Product__c;
                system.debug('depositProds' + depositProds);
                Boolean uploadSuccess;
                if(depositProds.containsIgnoreCase(product)){
                    //Deposits must be approved with account and CIS number
                    if( appToUpdate.TF4SF__Primary_Product_Status__c == 'Approved'){
                        sendDocument(att,appToUpdate);
                    }
                }else{
                    //Loan products
                    sendDocument(att,appToUpdate);
                }        
                if(uploadSuccess){
                    appToUpdate.Documents_Posted__c = true; 
                    appsUpdateList.add(appToUpdate);
                }
            }
            if(appsUpdateList != null ){
                update appsUpdateList;
            }
        } catch (Exception ex) {
            system.debug('ex ########### :' + ex);
        }
        
        
        
    }
  
    global void finish(Database.BatchableContext BC) {
        
    }

    public static Boolean sendDocument(Attachment att, TF4SF__Application__c app) {
        Boolean serviceStatus = false;
        try{
            VELOSETTINGS__C velo = VELOSETTINGS__C.getOrgDefaults();
            String Token = AuthToken.Auth();
            Integer timeOut = Integer.ValueOf(velo.ProcessAPI_Timeout__c);
            String GUID = GUIDGenerator.getGUID();

            Http h2 = new Http();
            HttpRequest req2 = new HttpRequest();
            req2.setHeader(velo.Request_GUID_Name__c,GUID);
            req2.setTimeout(timeOut);
            req2.setEndpoint(velo.EndPoint__c+'api/process/synergy');
            req2.setHeader('Authorization','Bearer '+Token);
            req2.setHeader('Accept','application/json');
            req2.setHeader('Content-Type', 'application/json');
            req2.setMethod('POST');
            String idBody = '';
            idBody += '{';
                idBody += '"DocumentExtension":"'+getAttachmentExtn(att.Name)+'",';
                idBody += '"DocumentSource": "'+getDocumentSource(app)+'",';
                //idBody += '"DocumentType": "LegalDocument",';
                idBody += '"DocumentType": "'+getDocumentType(att.Name)+'",'; // If we pass W8DEPOSIT, W9DEPOSIT APIs failing even it is valid ENUM Key. We can change CustomMetadata
                idBody += '"SourceIdentifierId": "'+app.Name+'",';
                idBody += '"RequestUUID": "'+GUID+'",';
                idBody += '"AccountNumber": "'+app.TF4SF__External_App_ID__c+'",';
                idBody += '"CustomerNumber": "'+app.TF4SF__External_App_Stage__c+'",';
                idBody += '"DocumentDate": "'+String.valueOf(system.now().format('yyyy-MM-dd\'T\'HH:mm:ss.SSSZ'))+'",';
                idBody += '"DocumentContent": "'+EncodingUtil.base64Encode(att.body)+'"';
            idBody += '}';

            req2.setBody(idBody);
            HttpResponse res2;
            res2 = h2.send(req2);
            system.debug('Req is '+req2.getbody()); 
            system.debug('Res is '+res2); 
            //responseJson = res2.getBody();
            system.debug('Res is '+res2.getbody()); 
            if(res2 != null && res2.getStatusCode() == 200){
                serviceStatus = true;
            }
        }catch(Exception ex){
            serviceStatus = false;
        }
        return serviceStatus;
    }

    private static String getAttachmentExtn(String fileName){
        String fileExtn ='';
        try{
            String extn = fileName != null ? fileName.substring(fileName.indexOf('.')+1) : 'JPEG';
            fileExtn = DepositSubmit.getEnumKeyByFieldAndUIValue(Label.Synergy_Doc_Extn_Field,extn);
        }catch(Exception ex){
            system.debug('ex %%%%%:' + ex);
        }
        system.debug('fileExtn $$$$$$$$$:' + fileExtn);
        return fileExtn;
    }
    private static String getDocumentType(String fileName){
        String docType ='';
        try{
            //Default to LegalDocument
            String filePart = fileName != null ? fileName.substring(0,fileName.indexOf('.')) : 'LegalDocument';
            docType = DepositSubmit.getEnumKeyByFieldAndUIValue(Label.Synergy_Doc_Type_Field,filePart);
        }catch(Exception ex){
            system.debug('ex %%%%%:' + ex);
        }
        system.debug('docType $$$$$$$$$:' + docType);
        return docType;
    }
    
    private static String getDocumentSource(TF4SF__Application__c app){
        String finalDocSource ;
        String depositProds = Label.Deposti_Product_Types ;
        String product = app.TF4SF__Product__c;
        system.debug('depositProds' + depositProds);
        if(depositProds.containsIgnoreCase(product)){
            finalDocSource = Label.Batch_To_Send_DocSource_Deposit_Value;
        }else{
            finalDocSource = Label.Batch_To_Send_DocSource_Loan_Value;
        }
        return finalDocSource;
    }
}