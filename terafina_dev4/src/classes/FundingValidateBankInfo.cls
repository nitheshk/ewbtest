global with sharing class FundingValidateBankInfo implements TF4SF.DSP_Interface {

    global static Map<String, String> main (Map<String, String> tdata) {
        Map<String, String> data = new Map<String, String>();
        String appId = tdata.get('id');
        String Token = '',StatusCode='0';
        String RtNumber = '';
        String RtNumberCHK = tdata.get('routingNumChk');
        String RtNumberSAV = tdata.get('routingNumSav');
        if (String.isNotBlank(RtNumberCHK)) {
            RtNumber = RtNumberCHK;
        } 
        if (String.isNotBlank(RtNumberSAV)) {
            RtNumber = RtNumberSAV;
        }
        

        try {
        VELOSETTINGS__C velo = VELOSETTINGS__C.getOrgDefaults();
        Token = AuthToken.Auth();    
                
        String response = ValidateBankInformation(appId, Token, RtNumber);
        StatusCode='200';
        data.put('ValidateBankInfo',response);

        //JSON2ApexValidateBankInfoRes resDetails=JSON2ApexValidateBankInfoRes.parse(response);
        //String CEUserID = resDetails.data.CreateUserResponse.body.CreateUserResponse.return_Z.CEUserID;
        //String FIID = resDetails.data.GetFISeedResponse.body.GetFISeedDataResponse.return_Z.FIInfo.FIID;
        //String userparam = resDetails.data.GetFISeedResponse.body.GetFISeedDataResponse.return_Z.FIInfo.FICredentials.FILoginParametersList.FILoginParametersInfo[0].CredentialsParamId;
        //String passparam = resDetails.data.GetFISeedResponse.body.GetFISeedDataResponse.return_Z.FIInfo.FICredentials.FILoginParametersList.FILoginParametersInfo[1].CredentialsParamId;
        } catch (Exception e) {
            data.put('server-errors', 'Error encountered in FundingValidateBankInfo class: ' + e.getMessage() + '; line: ' + e.getLineNumber() + '; type: ' + e.getTypeName() + '; stack trace: ' + e.getStackTraceString());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error encountered in FundingValidateBankInfo class: ' + e.getMessage() + '; line: ' + e.getLineNumber() + '; type: ' + e.getTypeName() + '; stack trace: ' + e.getStackTraceString()));
            System.debug('exception' + e + ' ' + e.getLineNumber());
        }
        data.put('StatusCode',StatusCode);  
        return data;
    }

    public static String ValidateBankInformation(String appId, String Token, String RtNumber) {
        String result = '';

        String applicationId = '\'' + appId + '\'';
        //System.debug('applicationId//productnumber:'+applicationId+'--'+productnumber);
        //select all fields in Application
        queryHelper.ObjectInfo appInfo = queryHelper.selectStar('TF4SF__Application__c');
        String queryString =  appInfo.query;
        queryString += 'WHERE ID = ' + applicationId ;
        TF4SF__Application__c app = Database.query(queryString);

        //this.Application = app;
        queryHelper.ObjectInfo app2Info = queryHelper.selectStar('TF4SF__Application2__c');
        String app2String =  app2Info.query;
        app2String += 'WHERE TF4SF__Application__c = ' + applicationId ;
        TF4SF__Application2__c app2 = Database.query(app2String);

        queryHelper.ObjectInfo empInfo = queryHelper.selectStar('TF4SF__Employment_Information__c');
        String employmentQuery =  empInfo.query;
        employmentQuery += 'WHERE TF4SF__Application__c = ' + applicationId;
        TF4SF__Employment_Information__c employment = Database.query(employmentQuery);
        //System.debug('employment:' + employment);

        queryHelper.ObjectInfo identityInfo = queryHelper.selectStar('TF4SF__Identity_Information__c');
        String identityQuery = identityInfo.query;
        identityQuery += 'WHERE TF4SF__Application__c = ' + applicationId;
        TF4SF__Identity_Information__c identity = Database.query(identityQuery);
        System.debug('identity:' + identity);

        queryHelper.ObjectInfo aboutAccountInfo = queryHelper.selectStar('TF4SF__About_Account__c');
        String aboutAccountQuery = aboutAccountInfo.query;
        aboutAccountQuery += 'WHERE TF4SF__Application__c = ' + applicationId;
        //System.debug('abtaccountquery:'+aboutAccountQuery);
        TF4SF__About_Account__c about = Database.query(aboutAccountQuery);
        //System.debug('about account:' + about); 

        DepositSubmit.Application application = new DepositSubmit.Application(app, app2, employment, identity, about);

        String RoutingNumber = '';
        RoutingNumber = RtNumber;
        if (String.isBlank(RoutingNumber)) {
            if (application.app.TF4SF__Product__c == 'Checking') {
                RoutingNumber = application.about.TF4SF__Routing_Number_CHK__c;
            } else {
                RoutingNumber = application.about.TF4SF__Routing_Number_SAV__c;
            }
        }

        result += '{';
            result += '"CreateUser": {';
                result += '"RqUID": "'+ValidatePromoCodeExtension.getRandomString(25)+'",';
                result += '"AgreementAccepted": "true",';
                result += '"UserInfo": {';
                    result += '"LastName": "'+application.app.TF4SF__Last_Name__c+'",';
                    result += '"FirstName": "'+application.app.TF4SF__First_Name__c+'",';
                    result += '"DateOfBirth": "'+formatDate(application.identity.TF4SF__Date_Of_Birth__c)+'",';
                    result += '"SSN": "'+application.identity.TF4SF__SSN_Prime__c.replaceAll('-','')+'",';
                    result += '"DrvLicNumber": "",';
                    result += '"DrvLicState": "",';
                    result += '"DrvLicAddressType": ""';
                result += '},';
                result += '"ContactInfo": {';
                    result += '"PhoneType": "DAYTIMEPHONE",';
                    result += '"PhoneNumber": "'+application.app.TF4SF__Primary_Phone_Number__c+'",';
                    result += '"AddressType": "CURRENT-ADDRESS",';
                    result += '"Line1": "'+application.app.TF4SF__Street_Address_1__c+'",';
                    result += '"City": "'+application.app.TF4SF__City__c+'",';
                    result += '"State": "'+application.app.TF4SF__State__c+'",';
                    if (application.app.TF4SF__Zip_Code__c.contains('-')) {
                        result += '"Zip": "'+application.app.TF4SF__Zip_Code__c.subString(0,5)+'",';
                    } else {
                        result += '"Zip": "'+application.app.TF4SF__Zip_Code__c+'",';
                    }
                    result += '"Email": "'+application.app.TF4SF__Email_Address__c+'",';
                    result += '"EmailType": "1"';
                result += '},';
                result += '"FundingAccountInfo": {';
                    result += '"RoutingNumber": "'+RoutingNumber+'"';
                result += '}';
            result += '}';
        result += '}';
        return execute(appId,result, Token);

    }

    private static String execute(String appId, String json, String Token) {
        System.debug('appId, JSON: ' + appId  + '---' + json);
        String responseJson = '';
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();

        
        VELOSETTINGS__C velo = VELOSETTINGS__C.getOrgDefaults();
        Integer timeOut = Integer.ValueOf(velo.ProcessAPI_Timeout__c);
        String GUID = GUIDGenerator.getGUID();
        req.setTimeout(timeOut);
        req.setHeader(velo.Request_GUID_Name__c,GUID);
        req.setEndpoint(velo.FundingEndpoint__c+'api/process/validateBankInfo');
        req.setHeader('Authorization','Bearer '+Token);
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Accept','application/json');
        req.setMethod('POST');
        
        req.setBody(json);
        HttpResponse res = h.send(req);
        responseJson = res.getBody();
        system.debug('Res is '+res.getbody()); 

        DebugLogger.InsertDebugLog(appId, GUID, 'Validate BankInfo Request GUID');
        DebugLogger.InsertDebugLog(appId, req.getBody(), 'Validate BankInfo Request');
        DebugLogger.InsertDebugLog(appId, res.getBody(), 'Validate BankInfo  Response'); 
        return responseJSON;
    }

    public static string formatDate (String DateOfBirth) {
        string properDate = '';
        properDate = DateOfBirth.replaceAll('/','-');
        return properDate;
    }

}