/**********************************************************
* Name : AppDetailsViewer
* Desc : Customized page to display the application details
* Date : 12 Sep 2018
* Author : Nithesh K
* *******************************************************/
public class AppDetailsViewer{
    /**
     * Variable Declaration  
     */
    public TF4SF__Application__c applicationObj { get; set; }
    public TF4SF__Identity_Information__c identityObj{get;set;}
    public TF4SF__About_Account__c accountObj{get;set;}
    public TF4SF__Employment_Information__c employmentObj{get;set;}
    public TF4SF__Application2__c application2Obj{get;set;}
    
    public Boolean isEditMode       { get; set; }
    private List<FieldSetDetail__mdt> FieldSetDetailMDTObj;    
    private Map<String,List<FieldSetDetail__mdt>> objectVsMetaDataList;
    
    /**
      *  @Desc: Intialization of the data
      *  @name: AppDetailsViewer - Class Constructor 
      */
    public AppDetailsViewer() {
        applicationObj = new TF4SF__Application__c();
    }
    
    /**
      *  @Desc: Intialization of the Meta data and Application data
      *  @name: AppDetailsViewer - Class StandardController
      *  @param: ApexPages.StandardController -  Targeted Application object need to display
      */
    public AppDetailsViewer(ApexPages.StandardController stdController){
        applicationObj = new TF4SF__Application__c();
        applicationObj=[select id,TF4SF__Product__c from TF4SF__Application__c where id=:stdController.getRecord().id];
        
        //Get field set details from custom metadata 
        generateMetadata();
    
        //Get data from Application,Employee,About accout, Identity and Application2 object based on the field set information     
        fetchDetails(); 
        
        // On Page load, checking for Read mode or Edit mode.
        if(String.isNotEmpty(ApexPages.currentPage().getParameters().get('editmode')) && TF4SF__Application__c.sObjectType.getDescribe().Updateable){
            isEditMode =Boolean.valueOf(ApexPages.currentPage().getParameters().get( 'editmode' ));
        } else{
            isEditMode =False;
        } 
    }
    
    /**
      *  @Desc: Get metadata information from FieldSetDetail__mdt and map into objectVsMetaDataList.
      *  @name: generateMetadata - Private Method 
      */
    Private void generateMetadata(){
        FieldSetDetailMDTObj=[SELECT FieldSetOrder__c,FieldSetName__c,ProductType__c,SectionName__c,SObjectName__c FROM FieldSetDetail__mdt where MasterLabel= 'FieldSetDetail' ORDER BY FieldSetOrder__c ASC NULLS LAST LIMIT 50000];
        objectVsMetaDataList=new  Map<String,List<FieldSetDetail__mdt>> ();
        List<FieldSetDetail__mdt> metaDataList = new List<FieldSetDetail__mdt> ();
        
         for(FieldSetDetail__mdt mdt:FieldSetDetailMDTObj ){             
            Set<String> productList= New Set<String>(mdt.ProductType__c.split(';')) ;
                if(productList.contains(applicationObj.TF4SF__Product__c)){
                    if(objectVsMetaDataList.containsKey(mdt.SObjectName__c)){
                        metaDataList=objectVsMetaDataList.get(mdt.SObjectName__c);
                        metaDataList.add(mdt);
                        objectVsMetaDataList.put(mdt.SObjectName__c,metaDataList);
                    }else{
                        objectVsMetaDataList.put(mdt.SObjectName__c,new List<FieldSetDetail__mdt>{mdt});
                    }
                }
         }//End of For Loop
    }
    
    
    
    /**
      *  @Desc: Generates a query with all the fields in the Field Set 
      *         inorder to query the records values from the application.
      *  @name: fetchDetails - Public Method 
      */
    public void fetchDetails () {
        applicationObj.Id = String.valueOf(ApexPages.currentPage().getParameters().get( 'id' ));
        set<Schema.FieldSetMember> fieldsList;
        String query;
        if( applicationObj.Id != NULL && String.isNotEmpty(applicationObj.Id) ) {
            
            if(objectVsMetaDataList.containsKey('TF4SF__Application__c')){
            fieldsList= new set<Schema.FieldSetMember> (); 
            For(FieldSetDetail__mdt mdt:objectVsMetaDataList.get('TF4SF__Application__c')){
                fieldsList.addAll(Schema.SObjectType.TF4SF__Application__c.fieldSets.getMap().get(mdt.FieldSetName__c).getFields());
            }
            
            query = 'SELECT '; 
            for( Schema.FieldSetMember field : fieldsList ) {
                query += field.getFieldPath() + ', ';
            }
            query += 'Id FROM TF4SF__Application__c WHERE Id=\'' + applicationObj.Id + '\'';
            system.debug('TF4SF__Application__c: query *** ' + query);
            applicationObj = Database.query( String.valueOf(query) );
            } else{
                applicationObj=new TF4SF__Application__c();
            }
            
           
             //fetch Employment_Information__c object data
            if(objectVsMetaDataList.containsKey('TF4SF__Employment_Information__c')){
            fieldsList= new set<Schema.FieldSetMember> (); 
            For(FieldSetDetail__mdt mdt:objectVsMetaDataList.get('TF4SF__Employment_Information__c')){
                fieldsList.addAll(Schema.SObjectType.TF4SF__Employment_Information__c.fieldSets.getMap().get(mdt.FieldSetName__c).getFields());
            }
            
            query = 'SELECT '; 
            for( Schema.FieldSetMember field : fieldsList ) {
                query += field.getFieldPath() + ', ';
            }
            query += 'Id FROM TF4SF__Employment_Information__c WHERE TF4SF__Application__c =\'' + applicationObj.Id + '\'';
            system.debug('TF4SF__Employment_Information__c: query *** ' + query);
            employmentObj = Database.query( String.valueOf(query) );
            } else{
                employmentObj=new TF4SF__Employment_Information__c();
            }
            
             
             //fetch Identity_Information__c object data
            if(objectVsMetaDataList.containsKey('TF4SF__Identity_Information__c')){
            fieldsList= new set<Schema.FieldSetMember> (); 
            For(FieldSetDetail__mdt mdt:objectVsMetaDataList.get('TF4SF__Identity_Information__c')){
                fieldsList.addAll(Schema.SObjectType.TF4SF__Identity_Information__c.fieldSets.getMap().get(mdt.FieldSetName__c).getFields());
            }
            query = 'SELECT '; 
            for( Schema.FieldSetMember field : fieldsList ) {
                query += field.getFieldPath() + ', ';
            }
            query += 'Id FROM TF4SF__Identity_Information__c WHERE TF4SF__Application__c =\'' + applicationObj.Id + '\'';
            system.debug('TF4SF__Identity_Information__c: query *** ' + query);
            identityObj = Database.query( String.valueOf(query) );
            } else{
                identityObj=new TF4SF__Identity_Information__c();
            }
            
             //fetch About_Account__c object data
            if(objectVsMetaDataList.containsKey('TF4SF__About_Account__c')){
            fieldsList= new set<Schema.FieldSetMember> (); 
            For(FieldSetDetail__mdt mdt:objectVsMetaDataList.get('TF4SF__About_Account__c')){
                fieldsList.addAll(Schema.SObjectType.TF4SF__About_Account__c.fieldSets.getMap().get(mdt.FieldSetName__c).getFields());
            }
            
            query = 'SELECT '; 
            for( Schema.FieldSetMember field : fieldsList ) {
                query += field.getFieldPath() + ', ';
            }
            query += 'Id FROM TF4SF__About_Account__c WHERE TF4SF__Application__c =\'' + applicationObj.Id + '\'';
            system.debug('TF4SF__About_Account__c: query *** ' + query);
            accountObj = Database.query( String.valueOf(query) );
            } else{
                accountObj=new TF4SF__About_Account__c();
            }
            
            
             //fetch TF4SF__Application2__c object data
            if(objectVsMetaDataList.containsKey('TF4SF__Application2__c')){
            fieldsList= new set<Schema.FieldSetMember> (); 
            For(FieldSetDetail__mdt mdt:objectVsMetaDataList.get('TF4SF__Application2__c')){
                fieldsList.addAll(Schema.SObjectType.TF4SF__Application2__c.fieldSets.getMap().get(mdt.FieldSetName__c).getFields());
            }
            
            query = 'SELECT '; 
            for( Schema.FieldSetMember field : fieldsList ) {
                query += field.getFieldPath() + ', ';
            }
            query += 'Id FROM TF4SF__Application2__c WHERE TF4SF__Application__c =\'' + applicationObj.Id + '\'';
            system.debug('TF4SF__Application2__c: query *** ' + query);
            application2Obj = Database.query( String.valueOf(query) );
            } else{
                application2Obj=new TF4SF__Application2__c();
            }
        }
        isEditMode =False;
 
    }
    

    // @return: List<FieldSetDetail__mdt>  - Return Application field set details
    public List<FieldSetDetail__mdt> getApplicationFieldSet(){ 
        return objectVsMetaDataList.get('TF4SF__Application__c');
    }
    
    //  @return: List<FieldSetDetail__mdt>  - Return Employee field set details
    public List<FieldSetDetail__mdt> getEmployeeFieldSet(){ 
        return objectVsMetaDataList.get('TF4SF__Employment_Information__c');
    }
    
    //  @return: List<FieldSetDetail__mdt>  - Return Identity field set details
    public List<FieldSetDetail__mdt> getIdentityFieldSet(){ 
        return objectVsMetaDataList.get('TF4SF__Identity_Information__c');
    }
    
    // @return: List<FieldSetDetail__mdt>  - Return Account field set details
    public List<FieldSetDetail__mdt> getAccountFieldSet(){ 
        return objectVsMetaDataList.get('TF4SF__About_Account__c');
    }
    // @return: List<FieldSetDetail__mdt>  - Return Application2 field set details
    public List<FieldSetDetail__mdt> getApplication2FieldSet(){ 
        return objectVsMetaDataList.get('TF4SF__Application2__c');
    }
    
    
    /**
      *  @Desc: Enable edit mode in page layout based on FLS
      *  @name: enableEditMode - public Method 
      */
    public void enableEditMode(){
        isEditMode =true;
    }

    /**
      *  @Desc: Save Application details
      *  @name: saveApplication - public Method 
      */
    public void saveApplication() {      
        try{
        List<SObject> allApplicationObject=new List<SObject>();
        if(String.isNotEmpty(applicationObj.id)){
            allApplicationObject.add(applicationObj);
        }
        if(String.isNotEmpty(employmentObj.id)){
            allApplicationObject.add(employmentObj);
        }
        if(String.isNotEmpty(identityObj.id)){
            allApplicationObject.add(identityObj);
        }
        if(String.isNotEmpty(accountObj.id)){
            allApplicationObject.add(accountObj);
        }
        if(String.isNotEmpty(application2Obj.id)){
            allApplicationObject.add(application2Obj);
        }
        update allApplicationObject;
        isEditMode =false;
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Application saved successfully'));
        }
        Catch(exception ex){
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Application has not saved because of internal issue'));
        DebugLogger.InsertDebugLog(applicationObj.id, ex.getMessage(), 'DepositAppDetails saveApplication()');               
        }
    }    
}