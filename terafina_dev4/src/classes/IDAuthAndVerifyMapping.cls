/*************************************************
@Author : Nag
@Date : 8/4/2018
@Description : This Object holds Field mapping JSON for custom metadata field
****************************************************/
global class IDAuthAndVerifyMapping{
    global boolean NoValidReponseFillFromUI;
    global boolean FillFromResposneOnly;
    global boolean NotValidResponseFillUI;
    global MappingDetails[] MappingDetails;
    public class MappingDetails {
        public String ResFieldName; //FullName
        public String PrefillFieldUI; //Application__c.Full_Name_PA__c
        public String PrefillFieldRes;
    }
    public static IDAuthAndVerifyMapping parse(String json){
        return (IDAuthAndVerifyMapping) System.JSON.deserialize(json, IDAuthAndVerifyMapping.class);
    }
    
}