global with sharing  class ReviewSubmitListObject implements TF4SF.DSP_Interface { 

    global static Map<String, String> main(Map<String, String> tdata) {

        Map<String, String> dataClone = tdata.clone();
        Map<String, String> returnData= new Map<String, String>();
        System.debug('ReviewSubmitListObject main called');
        String appId = dataClone.get('Id');
        DebugLogger.InsertDebugLog(appId, appId,'ReviewSubmitListObject main called');
        String objectList = dataClone.get('objectList');

        List<TF4SF__Application__c> lstApp ;
        String result ='';
        

        if (String.isNotBlank(appId) && String.isNotBlank(objectList)) {
            //Query Application object to check if there are joint appllicants                                                  
            lstApp = [SELECT Id, TF4SF__First_Joint_Applicant__c, TF4SF__Second_Joint_Applicant__c, TF4SF__Third_Joint_Applicant__c FROM  TF4SF__Application__c WHERE Id =:appId LIMIT 1];
            
            //Split the string to get the objects to be queried
            List<String> lstObjects = objectList.split(',') ;

            result += '{';

            for(String objName: lstObjects) {
                result += getData(appId,lstApp,objName);
            }
              result = result.removeEnd(',');

            result += '}';
        }

        returnData.put('listobjectresult' ,result);
        return returnData;
    }

    public static  String getData(String appID,List<TF4SF__Application__c> lstApp,String objectName) {

        String strFields = '';          
        List<string> lstFieldName = new List<string>();   
        Map<String,String> result = new Map<String,String>();
        TF4SF__Application__c objApp;
        String strDataJson = '';        

        try{
            System.debug('ReviewSubmitListObject get Data called');
       // DebugLogger.InsertDebugLog(appId, json,'ReviewSubmitListObject get data called');
            //Get the fields from field set
            Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
            Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(objectName);
            Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();           
            Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get('All_Fields');
			system.debug('lstapp size' + lstApp.size());
            //Get the Field names and store it in a list
            for(Schema.FieldSetMember fieldSetMemberObj : fieldSetObj.getFields()) {
                 
                    lstFieldName.add(fieldSetMemberObj.getFieldPath());                 
                    strFields =  strFields + fieldSetMemberObj.getFieldPath() + ',';
            }
            strFields = strFields.removeEnd(',');

            if (lstApp.size() > 0) {
                objApp = lstApp[0];
            }
            
            if (String.isNotBlank(appId) && objApp != null) {

                //Build query string
                appId = '\'' +  appId + '\''  ;
                String strQuery = 'SELECT ' + strFields + ' FROM ' + objectName + ' WHERE Application__c =' + appId +' ORDER BY CreatedDate  LIMIT 5000' ;

                //Get Assets 
                List<sObject> lstResult = (List<sObject>) Database.query(strQuery);                

                objectName = objectName.replace('__c', '');
                //Start building data json               
                strDataJson+=   '\"' + objectName + '_PA__c\":';   //Data Json Start
                strDataJson+=   '[';
               // strDataJson+=       '\"PA\":';
              //  strDataJson+=       '[';
                //Loop through the result and buld Data Json
                for( sObject obj : lstResult ) {
                    
                    //Primary Applicant
                    if ( obj.get('Applicant_Type__c') == 'PA') {                                              
                        strDataJson+=       '{';
                        strDataJson+=       '\"PA\":';  
                        strDataJson+=       '{';

                        //Loop through all the fields of the object and add to Data Json
                        for (String strField : lstFieldName) {                                  
                                strDataJson+=   '\"' + strField + '\":' + ' \"' + obj.get(strField) + '\",' ;                                 
                        }
                        strDataJson = strDataJson.removeEnd(',');
                        strDataJson+=       '}';
                        strDataJson+=       '},';                       
                    }
                                         
                }
                strDataJson = strDataJson.removeEnd(',');
               // strDataJson +=      ']';
                strDataJson +=  '],';          //Data Json End
                                                   


               

                
                //**********************************  Joint 1 ***************************************************
                if (objApp.TF4SF__First_Joint_Applicant__c == True) {      

                    strDataJson+=   '\"' + objectName + '_J1__c\":';   //Data Json Start
                    strDataJson+=   '[';
                   // strDataJson+=       '\"J1\":';
                   // strDataJson+=       '[';                             
                    //Loop through the result and buld Data Json
                     for( sObject obj : lstResult ) {
                                                
                        //Joint 1                           
                        if ( obj.get('Applicant_Type__c') == 'J1') {
                            strDataJson+=       '{';
                            strDataJson+=       '\"J1\":';  
                            strDataJson+=       '{';

                            //Loop through all the fields of the object and add to Data Json
                            for (String strField : lstFieldName) {                                  
                                    strDataJson+= '\"' + strField + '\":' + ' \"' + obj.get(strField) + '\",' ;                                 
                            }

                            strDataJson = strDataJson.removeEnd(',');
                            strDataJson+=       '}';
                            strDataJson+=       '},';    
                        }                               
                        
                    }
                    
                    strDataJson = strDataJson.removeEnd(',');
                   // strDataJson +=      ']';
                    strDataJson +=  '],';          //Data Json End
                }

                

                //**********************************  Joint 1 end ***************************************************

                
                //**********************************  Joint 2 ***************************************************
                if (objApp.TF4SF__Second_Joint_Applicant__c == True) {
                    
                    strDataJson+=   '\"' + objectName + '_J2__c\":';   //Data Json Start
                    strDataJson+=   '[';
                    //strDataJson+=       '\"J2\":';
                   // strDataJson+=       '[';                             
                    //Loop through the result and buld Data Json
                    for( sObject obj : lstResult ) {
                                                
                        //Joint 1                           
                        if (obj.get('Applicant_Type__c') == 'J2') {
                            strDataJson+=       '{';
                            strDataJson+=       '\"J2\":';  
                            strDataJson+=       '{';
                            //Loop through all the fields of the object and add to Data Json
                            for (String strField : lstFieldName) {                                  
                                    strDataJson+= '\"' + strField + '\":' + ' \"' + obj.get(strField) + '\",' ;                                 
                            }

                            strDataJson = strDataJson.removeEnd(',');
                            strDataJson+=       '}';
                            strDataJson+=       '},';    
                        }                               
                        
                    }
                    
                    strDataJson = strDataJson.removeEnd(',');
                   // strDataJson +=      ']';
                    strDataJson +=  '],';          //Data Json End
                }

                

                //**********************************  Joint 2 End ***************************************************

                
                //**********************************  Joint 3 ***************************************************
                if (objApp.TF4SF__Third_Joint_Applicant__c == True) {

                    strDataJson+=   '\"' + objectName + '_J3__c\":';   //Data Json Start
                    strDataJson+=   '[';
                   
                    //strDataJson+=       '[';                             
                    //Loop through the result and buld Data Json
                    for( sObject obj : lstResult ) {
                                                
                        //Joint 1                           
                        if ( obj.get('Applicant_Type__c') == 'J3') {
                           
                            strDataJson+=       '{';
                             strDataJson+=       '\"J3\":';  
                               strDataJson+=       '{';
                            //Loop through all the fields of the object and add to Data Json
                            for (String strField : lstFieldName) {                                  
                                    strDataJson+= '\"' + strField + '\":' + ' \"' + obj.get(strField) + '\",' ;                                 
                            }

                            strDataJson = strDataJson.removeEnd(',');
                            strDataJson+=       '}';
                            strDataJson+=       '},';    
                        }                               
                        
                    }
                    
                    strDataJson = strDataJson.removeEnd(',');
                   // strDataJson +=      ']';
                    strDataJson +=  '],';          //Data Json End
                }
                
                //**********************************  Joint 3 end ***************************************************

                
                strDataJson = strDataJson.replace('null',''); 
            }


            }catch(exception ex){
                System.debug('Exception in getAssets ' + ex.getMessage());
                System.debug('Exception in getAssets line numbmer ' + ex.getLineNumber());
        //DebugLogger.InsertDebugLog('ReviewSubmitListObject exception called');

            }
            
            return strDataJson;
    }

}