@isTest
global class BatchtoUpdateCompletedTrusGuarenAppsTest{ 

@isTest static void method_one(){

        TF4SF__Application__c app = new TF4SF__Application__c ();
        app.TF4SF__Application_Status__c = 'Submitted';
        app.TF4SF__Product__c = 'Home Loan';
        app.TF4SF__Sub_Product__c =  'Home Loan - Short App';
        
        insert app;
         List<Trustee_Guarantor__c> trustg=new  List<Trustee_Guarantor__c> ();
        Trustee_Guarantor__c tg=new Trustee_Guarantor__c();
		tg.Child_Application_Completed__c=true;
        //tg.Child_Application__c=
        tg.Application__c=app.id;
        trustg.add(tg); 
        insert tg;
        
        Database.QueryLocator QL;
        Database.BatchableContext BC;
        BatchtoUpdateCompletedTrusGuarenApps instance1 = new BatchtoUpdateCompletedTrusGuarenApps();
        instance1.start(BC);
        instance1.execute(BC,trustg);
        instance1.finish(BC);

}
}