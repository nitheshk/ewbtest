@isTest
global with sharing class UpdateCaseStatusTest {

    @isTest static void test_method_one() {

        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
        EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
        LocaleSidKey = 'en_US', ProfileId = p.Id,
        TimeZoneSidKey = 'America/Los_Angeles',
        UserName = uniqueUserName);

        System.runAs(u) {
            
         

            TF4SF__Application__c app1 = new TF4SF__Application__c();
            app1.Primary_Product_Case_Id__c ='CaseNumber';
            app1.TF4SF__Product__c = 'Checking';
            app1.Primary_Product_Case_Status__c='Close & Proceed';
            app1.TF4SF__Primary_Product_Status__c='Approved';
            app1.TF4SF__Sub_Product__c = 'Checking - Checking';
            app1.TF4SF__Application_Status__c = 'open';
            app1.TF4SF__Current_Channel__c = 'Online';
            app1.TF4SF__Email_Address__c = 'abc@gmail.com';
            app1.TF4SF__External_App_ID__c='AccNumber';
            app1.TF4SF__External_App_Stage__c='CustomerNumber';
            app1.TF4SF__Application_Page__c='StartFundingPage';
            app1.TF4SF__First_Name__c = 'Test';
            app1.TF4SF__Last_Name__c = 'LastTest';
            app1.TF4SF__Country_Code__c = '+1';
            insert app1;
            
            TF4SF__Application2__c app2 = new TF4SF__Application2__c ();
            app2.TF4SF__Application__c = app1.id;
            insert app2;
            
            TF4SF__Identity_Information__c iden = new TF4SF__Identity_Information__c ();
            iden.TF4SF__Application__c = app1.id;
            iden.TF4SF__SSN_Prime__c='Testssn';
            iden.TF4SF__Date_of_Birth__c='01/06/1993';
            iden.dual_citizenship_country__c = 'India';
            iden.TF4SF__Issue_Date__c='07/26/2002';
            iden.TF4SF__Expiry_Date__c='07/26/2020';
            insert iden;
            
            TF4SF__Employment_Information__c em = new TF4SF__Employment_Information__c();
            em.TF4SF__Application__c = app1.id;
            insert em;
            
            TF4SF__Debug_Logs__c db = new TF4SF__Debug_Logs__c();
            db.TF4SF__Application__c = app1.id;
            insert db;    
            
            TF4SF__About_Account__c ab = new TF4SF__About_Account__c();
            ab.TF4SF__Application__c = app1.id;    
            insert ab;
             
            test.startTest();
            //UpdateCaseStatus.CaseSubmitApp(app1.id, 'CaseNumber','false','false');
            UpdateCaseStatus.finalAppUpdate(app1.id, 'CustomerNumber', 'AccNumber', 'Approved','false','false');
            UpdateCaseStatus.AppUpdateDeclined(app1.id, 'Close & Proceed', 'false','false');
            UpdateCaseStatus.pendingCaseUpdate(app1.id, 'CustomerNumber', 'Close & Proceed', 'false', 'false');
            
            
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/CaseUpdate/v1/';  
            req.httpMethod = 'POST';
            RestContext.request = req;
            RestContext.response = res;
            string JsonMsg='{"data":{"CaseNumber":"34567","ApplicationId": "APP-M-00191","CaseStatus":"Close & Proceed","PoliticalExposedPerson":"false","DoNotBank":"false"}}';
            req.requestBody = Blob.valueof(JsonMsg);

            UpdateCaseStatus obj=new UpdateCaseStatus();
            Test.setMock(HttpCalloutMock.class, new MockUpdateCaseStudy());
            UpdateCaseStatus.UpdateStatus();
            test.stopTest();
        }
    }
}