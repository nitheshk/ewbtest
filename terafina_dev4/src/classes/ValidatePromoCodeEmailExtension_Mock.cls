@isTest
global class ValidatePromoCodeEmailExtension_Mock implements HttpCalloutMock {

  // Implement this interface method
  global HTTPResponse respond(HTTPRequest req) {
    // Optionally, only send a mock response for a specific endpoint
    // and method.
    // Create a fake response
    HttpResponse res = new HttpResponse();
    res.setHeader('Content-Type', 'application/json');
    res.setBody('{"Email":"test@gmail.com","Token":"is invalid","Phone":"12321444"}');
    res.setStatusCode(200);
    return res;
}
}