public class ApplicationTriggerHandler {  
    //Send email on abandoned of the application to the ML and applicant
    
    public void sendAbandonedEmail(List<TF4SF__Application__c> listApp) {
        try{
            Boolean count = true;
            List<Contact> conList = new List<Contact>();
            List<TF4SF__Application__c> lstApp = new List<TF4SF__Application__c>();
            set<id> appIDSet = new set<id>();
            
            List<Messaging.SingleEmailMessage> mailsList =  new List<Messaging.SingleEmailMessage>();
            EmailTemplate templateId = [SELECT Id, Body, Subject, HtmlValue FROM EmailTemplate WHERE DeveloperName = 'Application_Abandoned_Email'];
            List<Contact> con = [SELECT Id, LastName FROM Contact LIMIT 1];
            OrgWideEmailAddress owa = [SELECT Id, Address, DisplayName FROM OrgWideEmailAddress where DisplayName ='talk2us@eastwestbank.com' limit 1];
                 
            for (TF4SF__Application__c app : listApp) {
                if(app.Abandoned_Email__c==false && app.TF4SF__Email_Address__c != null && app.TF4SF__Product__c.contains('Home Loan') && app.TF4SF__Application_Status__c=='Abandoned') {
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage();
                    
                    mail.setTemplateId(templateId.id);
                    //To send FROM address
                    if(owa != null) {
                        mail.setOrgWideEmailAddressId(owa.id); 
                    }
                    
                    mail.toAddresses = new string[]{app.TF4SF__Email_Address__c} ;
                    if (con.size() > 0) {
                        mail.setTargetObjectId(con[0].id); 
                    }
                    else
                    {
                        count =false;
                        Contact conRec = new Contact();
                        if (Schema.sObjectType.Contact.fields.LastName.isCreateable()) { conRec.LastName = app.Name; }
                        conList.add(conRec);
                        if(Contact.sObjectType.getDescribe().isCreateable()) {insert conList;}
                        mail.setTargetObjectId(conList[0].id); 
                    }
                    
                    mail.setWhatId(app.id);
                    // mail.setTargetObjectId(UserInfo.getUserId());
                    mail.setTreatTargetObjectAsRecipient(false);
                    /*--------Send email to the MLO-----------------------*/
                    
                    
                    mailsList.add(mail);
                    
                    app.Abandoned_Email__c=true;
                    
                    appIDSet.add(app.id);
                }
            }
            if (mailsList.size () > 0) {
                Messaging.sendEmail(mailsList );
                if(conList.size () > 0) {
                    if(Contact.sObjectType.getDescribe().isDeletable()) { delete conList; }
                }
                if (appIDSet.size() > 0) {
                    //lstApp = [select id,Abandoned_Email__c from TF4SF__Application__c  where id IN :appIDSet ];
                    //for (TF4SF__Application__c   app1 : lstApp ) {
                       // app1.Abandoned_Email__c = true;
                        
                    //}
                    
                }
            }
            
        }
        catch(Exception w){}
    }
     public void sendSubmissionEmail(List<TF4SF__Application__c> listApp) {
        try{
            Boolean count = true;
            List<Contact> conList = new List<Contact>();
            List<TF4SF__Application__c> lstApp = new List<TF4SF__Application__c>();
            set<id> appIDSet = new set<id>();
            
            List<Messaging.SingleEmailMessage> mailsList =  new List<Messaging.SingleEmailMessage>();
            EmailTemplate templateId = [SELECT Id, Body, Subject, HtmlValue FROM EmailTemplate WHERE DeveloperName = 'Mortgage_Application_Submitted_Email'];
          OrgWideEmailAddress owa = [SELECT Id, Address, DisplayName FROM OrgWideEmailAddress where DisplayName ='talk2us@eastwestbank.com' limit 1];
             
            
            List<Contact> con = [SELECT Id, LastName FROM Contact LIMIT 1];
            for (TF4SF__Application__c app : listApp) {
                if( app.TF4SF__Product__c.contains('Home Loan') && app.TF4SF__Application_Status__c=='Submitted' && app.is_Processed_Abandoned_Mail_3__c == false) {
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage();
                    
                    mail.setTemplateId(templateId.id);
                    //To send FROM address
                    if(owa != null) {
                        mail.setOrgWideEmailAddressId(owa.id); 
                    }
                    //mail.toAddresses = new string[]{app.TF4SF__Email_Address__c,'mortgage-submissions@eastwestbank.com'} ;
                    mail.toAddresses = new string[]{app.TF4SF__Email_Address__c,'mohit.patil@terafinainc.com'} ;
                        if (con.size() > 0) {
                            mail.setTargetObjectId(con[0].id); 
                        }
                    else
                    {
                        count =false;
                        Contact conRec = new Contact();
                        if (Schema.sObjectType.Contact.fields.LastName.isCreateable()) { conRec.LastName = app.Name; }
                        conList.add(conRec);
                        if(Contact.sObjectType.getDescribe().isCreateable()) {insert conList;}
                        mail.setTargetObjectId(conList[0].id); 
                    }
                    if(owa != null) {
                        mail.setOrgWideEmailAddressId(owa.id); 
                    }
                    
                    mail.setWhatId(app.id);
                    // mail.setTargetObjectId(UserInfo.getUserId());
                    mail.setTreatTargetObjectAsRecipient(false);
                    /*--------Send email to the MLO-----------------------*/
                    
                    if (Schema.sObjectType.TF4SF__Application__c.fields.is_Processed_Abandoned_Mail_3__c.isCreateable() || Schema.sObjectType.TF4SF__Application__c.fields.is_Processed_Abandoned_Mail_3__c.isUpdateable()) { app.is_Processed_Abandoned_Mail_3__c = true; }
                    mailsList.add(mail);
                    lstApp.add(app);
                  
                }
            }
            if (mailsList.size () > 0) {
                Messaging.sendEmail(mailsList );
                if (TF4SF__Application__c.SObjectType.getDescribe().isUpdateable()) { update lstApp; }
                if(conList.size () > 0) {
                    if(Contact.sObjectType.getDescribe().isDeletable()) { delete conList; }
                }
            }
            
        }
        catch(Exception w){}
    }
    //Send email on submit of the application to the ML and applicant
    public void confirmationEmailMLO(List<TF4SF__Application__c> listApp) {
        
    }
   
    public void changeUserNotification(List<TF4SF__Application__c> appList,Map<string,string> newMap) {
        try{
            Boolean count = true;
            List<Contact> conList = new List<Contact>();
            List<TF4SF__Application__c> lstApp = new List<TF4SF__Application__c>();
            set<id> appIDSet = new set<id>();
            
            List<Messaging.SingleEmailMessage> mailsList =  new List<Messaging.SingleEmailMessage>();
            EmailTemplate templateId = [SELECT Id, Body, Subject, HtmlValue FROM EmailTemplate WHERE DeveloperName = 'Mortgage_Application_Owner_Change'];
            
            List<Contact> con = [SELECT Id, LastName FROM Contact LIMIT 1];
            for (TF4SF__Application__c app : appList) {
                
                if(newMap.containsKey(app.OwnerId) ) {
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setTemplateId(templateId.id);
                    mail.toAddresses = new string[]{newMap.get(app.OwnerId)} ;
                        if (con.size() > 0) {
                            mail.setTargetObjectId(con[0].id); 
                        }
                    else
                    {
                        count =false;
                        Contact conRec = new Contact();
                        if (Schema.sObjectType.Contact.fields.LastName.isCreateable()) { conRec.LastName = app.Name; }
                        conList.add(conRec);
                        if(Contact.sObjectType.getDescribe().isCreateable()) {insert conList;}
                        mail.setTargetObjectId(conList[0].id); 
                    }
                    
                    mail.setWhatId(app.id);
                    
                    mail.setTreatTargetObjectAsRecipient(false);
                    
                    mailsList.add(mail);
                    
                }
            }
            if (mailsList.size () > 0) {
                Messaging.sendEmail(mailsList );
                if(conList.size () > 0) { 
                    if(Contact.sObjectType.getDescribe().isDeletable()) { delete conList; }
                }
            }
        }
        catch(Exception w){}       
        
    }  
    
    
    public void sendConfirmationEmailForDeposit(List<TF4SF__Application__c> listApp) {

         
            Boolean count = true;
            List<Contact> conList = new List<Contact>();
            List<TF4SF__Application__c> lstApp = new List<TF4SF__Application__c>();
           
            try{
            List<Messaging.SingleEmailMessage> mailsList =  new List<Messaging.SingleEmailMessage>();
            EmailTemplate templateId = [SELECT Id, Body, Subject, HtmlValue FROM EmailTemplate WHERE DeveloperName = 'ConfirmationEmail'];
            OrgWideEmailAddress owa = [SELECT Id, Address, DisplayName FROM OrgWideEmailAddress where DisplayName ='talk2us@velobank.com' limit 1];
            List<Contact> con = [SELECT Id, LastName FROM Contact LIMIT 1];
            System.debug(listApp);
            for (TF4SF__Application__c app : listApp) {
                if(app.TF4SF__Email_Address__c != null)  {
                    
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();               
                    
                    mail.setTemplateId(templateId.id);
                    //To send FROM address
                    if(owa != null) {
                        mail.setOrgWideEmailAddressId(owa.id); 
                    }
                    mail.toAddresses = new string[]{app.TF4SF__Email_Address__c} ;
                        if (con.size() > 0) {
                            mail.setTargetObjectId(con[0].id); 
                        }
                    else
                    {
                        count =false;
                        Contact conRec = new Contact();
                        if (Schema.sObjectType.Contact.fields.LastName.isCreateable()) { conRec.LastName = app.Name; }
                        conList.add(conRec);
                        if(Contact.sObjectType.getDescribe().isCreateable()) {insert conList;}
                        mail.setTargetObjectId(conList[0].id); 
                    }
                    
                    mail.setWhatId(app.id);              
                    mail.setTreatTargetObjectAsRecipient(false);              
                    mailsList.add(mail);               
                }
            }
            if (mailsList.size () > 0) {
                Messaging.sendEmail(mailsList );
                if(conList.size () > 0) {
                    if(Contact.sObjectType.getDescribe().isDeletable()) { delete conList; }
                }
            }
        }
        catch(Exception ex){
             if(conList.size () > 0) {
                if(Contact.sObjectType.getDescribe().isDeletable()) { delete conList; }
             }
            System.debug('Exception occured in TriggerHandler class sendConfirmationEmailForDeposit method with error msg :::: ' + ex.getMessage() + ' &&&&& at line ##### ' + ex.getLineNumber());
        } 
    }

    public void sendPendingEmailForDeposit(List<TF4SF__Application__c> listApp) {

          try{
            Boolean count = true;
            List<Contact> conList = new List<Contact>();
            List<TF4SF__Application__c> lstApp = new List<TF4SF__Application__c>();
            set<id> appIDSet = new set<id>();
            
            List<Messaging.SingleEmailMessage> mailsList =  new List<Messaging.SingleEmailMessage>();
            EmailTemplate templateId = [SELECT Id, Body, Subject, HtmlValue FROM EmailTemplate WHERE DeveloperName = 'PendingEmail'];
            OrgWideEmailAddress owa = [SELECT Id, Address, DisplayName FROM OrgWideEmailAddress where DisplayName ='talk2us@velobank.com' limit 1];
            List<Contact> con = [SELECT Id, LastName FROM Contact LIMIT 1];
            System.debug(listApp);
            for (TF4SF__Application__c app : listApp) {
                if(app.TF4SF__Email_Address__c != null)  {
                    
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();               
                    
                    mail.setTemplateId(templateId.id);
                    //To send FROM address
                    if(owa != null) {
                        mail.setOrgWideEmailAddressId(owa.id); 
                    }
                    mail.toAddresses = new string[]{app.TF4SF__Email_Address__c} ;
                        if (con.size() > 0) {
                            mail.setTargetObjectId(con[0].id); 
                        }
                    else
                    {
                        count =false;
                        Contact conRec = new Contact();
                        if (Schema.sObjectType.Contact.fields.LastName.isCreateable()) { conRec.LastName = app.Name; }
                        conList.add(conRec);
                        if(Contact.sObjectType.getDescribe().isCreateable()) {insert conList;}
                        mail.setTargetObjectId(conList[0].id); 
                    }
                    
                    mail.setWhatId(app.id);              
                    mail.setTreatTargetObjectAsRecipient(false);              
                    mailsList.add(mail);               
                    //app.Abandoned_Email__c=true;                
                    appIDSet.add(app.id);
                }
            }
            if (mailsList.size () > 0) {
                Messaging.sendEmail(mailsList );
                if(conList.size () > 0) {
                    if(Contact.sObjectType.getDescribe().isDeletable()) { delete conList; }
                }
                // if (appIDSet.size() > 0) {
                //     lstApp = [select id,Abandoned_Email__c from TF4SF__Application__c where id IN :appIDSet ];
                //     for (TF4SF__Application__c   app1 : lstApp ) {
                //         app1.Abandoned_Email__c = true;                    
                //     }                
                // }
            }
        }
        catch(Exception ex){
            System.debug('Exception occured in TriggerHandler class sendPendingEmailForDeposit method with error msg :::: ' + ex.getMessage() + ' &&&&& at line ##### ' + ex.getLineNumber());
        } 
    }
    
    public void sendAbandonedEmailForDeposit(List<TF4SF__Application__c> listApp) {
              
            Boolean count = true;
            List<Contact> conList = new List<Contact>();
            List<TF4SF__Application__c> lstApp = new List<TF4SF__Application__c>();
            set<id> appIDSet = new set<id>();
            
            try{
            List<Messaging.SingleEmailMessage> mailsList =  new List<Messaging.SingleEmailMessage>();
            EmailTemplate templateId = [SELECT Id, Body, Subject, HtmlValue FROM EmailTemplate WHERE DeveloperName = 'AbandonEmail'];
            OrgWideEmailAddress owa = [SELECT Id, Address, DisplayName FROM OrgWideEmailAddress where DisplayName ='talk2us@velobank.com' limit 1];
          
            List<Contact> con = [SELECT Id, LastName FROM Contact LIMIT 1];
            System.debug(listApp);
            for (TF4SF__Application__c app : listApp) {
                if((app.TF4SF__Product__c.contains('All') || app.TF4SF__Product__c.contains('Checking') || app.TF4SF__Product__c.contains('Savings')) && app.TF4SF__Application_Status__c== 'Abandoned') {              
                    if(app.TF4SF__Email_Address__c != null && app.Abandoned_Email__c==false)  {
                        
                        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();               
                        
                        mail.setTemplateId(templateId.id);
                        //To send FROM address
                        if(owa != null) {
                            mail.setOrgWideEmailAddressId(owa.id); 
                        }
                        mail.toAddresses = new string[]{app.TF4SF__Email_Address__c} ;
                            if (con.size() > 0) {
                                mail.setTargetObjectId(con[0].id); 
                            }
                        else
                        {
                            count =false;
                            Contact conRec = new Contact();
                            if (Schema.sObjectType.Contact.fields.LastName.isCreateable()) { conRec.LastName = app.Name; }
                            conList.add(conRec);
                            if(Contact.sObjectType.getDescribe().isCreateable()) {insert conList;}
                            mail.setTargetObjectId(conList[0].id); 
                        }
                        
                        mail.setWhatId(app.id);              
                        mail.setTreatTargetObjectAsRecipient(false);              
                        mailsList.add(mail);    
                        app.Abandoned_Email__c=true;           
                        appIDSet.add(app.id);
                    }
                }
            }
            if (mailsList.size () > 0) {
                Messaging.sendEmail(mailsList );
                if(conList.size () > 0) {
                    if(Contact.sObjectType.getDescribe().isDeletable()) { delete conList; }
                }    

                if (appIDSet.size() > 0) {
                    lstApp = [select id,Abandoned_Email__c from TF4SF__Application__c  where id IN :appIDSet ];
                    for (TF4SF__Application__c   app1 : lstApp ) {
                        app1.Abandoned_Email__c = true;
                    }  
                }
            }
        }
        catch(Exception ex){
            if(conList.size () > 0) {
                if(Contact.sObjectType.getDescribe().isDeletable()) { delete conList; }
            }
            System.debug('Exception occured in TriggerHandler class sendAbandonEmailForDeposit method with error msg :::: ' + ex.getMessage() + ' &&&&& at line ##### ' + ex.getLineNumber());
        }
    }  

    public void sendDeclinedEmailForDeposit(List<TF4SF__Application__c> listApp) {

            Boolean count = true;
            List<Contact> conList = new List<Contact>();
            List<TF4SF__Application__c> lstApp = new List<TF4SF__Application__c>();
           
            try{
            List<Messaging.SingleEmailMessage> mailsList =  new List<Messaging.SingleEmailMessage>();
            EmailTemplate templateId = [SELECT Id, Body, Subject, HtmlValue FROM EmailTemplate WHERE DeveloperName = 'DeclinedEmail'];
            OrgWideEmailAddress owa = [SELECT Id, Address, DisplayName FROM OrgWideEmailAddress where DisplayName ='talk2us@velobank.com' limit 1];

            List<Contact> con = [SELECT Id, LastName FROM Contact LIMIT 1];
            System.debug(listApp);
            for (TF4SF__Application__c app : listApp) {
                if(app.TF4SF__Email_Address__c != null)  {
                    
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();               
                    
                    mail.setTemplateId(templateId.id);
                    //To send FROM address
                    if(owa != null) {
                        mail.setOrgWideEmailAddressId(owa.id); 
                    }
                    mail.toAddresses = new string[]{app.TF4SF__Email_Address__c} ;
                        if (con.size() > 0) {
                            mail.setTargetObjectId(con[0].id); 
                        }
                    else
                    {
                        count =false;
                        Contact conRec = new Contact();
                        if (Schema.sObjectType.Contact.fields.LastName.isCreateable()) { conRec.LastName = app.Name; }
                        conList.add(conRec);
                        if(Contact.sObjectType.getDescribe().isCreateable()) {insert conList;}
                        mail.setTargetObjectId(conList[0].id); 
                    }
                    
                    mail.setWhatId(app.id);              
                    mail.setTreatTargetObjectAsRecipient(false);              
                    mailsList.add(mail);               
                }
            }
            if (mailsList.size () > 0) {
                Messaging.sendEmail(mailsList );
                if(conList.size () > 0) {
                   if(Contact.sObjectType.getDescribe().isDeletable()) { delete conList; }
                }    
             
            }
        }
        catch(Exception ex){
             if(conList.size () > 0) {
                if(Contact.sObjectType.getDescribe().isDeletable()) { delete conList; }
            } 
            System.debug('Exception occured in TriggerHandler class sendDeclinedEmailForDeposit method with error msg :::: ' + ex.getMessage() + ' &&&&& at line ##### ' + ex.getLineNumber());
        }  
    } 
    
    
}