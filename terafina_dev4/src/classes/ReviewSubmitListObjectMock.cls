@isTest
global class ReviewSubmitListObjectMock implements HttpCalloutMock {
  // Implement this interface method
  global HTTPResponse respond(HTTPRequest req) {
    // Optionally, only send a mock response for a specific endpoint
    // and method.
    // Create a fake response
    HttpResponse res = new HttpResponse();
    res.setHeader('Content-Type', 'application/json');
    res.setBody('{"TF4SF__First_Name__c":"firstname","TF4SF__Product__c":"Checking","TF4SF__Sub_Product__c":"Checking - Checking","TF4SF__First_Joint_Applicant__c":true,"TF4SF__Second_Joint_Applicant__c":true,"TF4SF__Third_Joint_Applicant__c":true}');
    res.setStatusCode(200);
    return res;
  }
}