@isTest
public class ApplicationTriggerHandlerTest {
    
    @isTest static void test_method_one() {

        List<TF4SF__Application__c> applst = new List<TF4SF__Application__c>();

        TF4SF__Application__c app = new TF4SF__Application__c();
        app.TF4SF__Custom_Checkbox2__c = false;
        app.TF4SF__Custom_Checkbox3__c = true;
        app.TF4SF__Custom_Checkbox4__c = true;
        app.TF4SF__Custom_Text3__c ='800';
        app.TF4SF__Months__c  ='3';
        app.TF4SF__Custom_Text4__c='800';
        app.TF4SF__Months_J__c = '3';
        app.TF4SF__Custom_Text5__c = '800';
        app.TF4SF__Months_J2__c = '3';
        app.TF4SF__Custom_Text6__c = '800';
        app.TF4SF__Months_J3__c ='3';
        app.Abandoned_Email__c= false;
        app.TF4SF__Email_Address__c ='test@test.com'; 
        app.TF4SF__Product__c='Home Loan';
        app.TF4SF__Application_Status__c='Abandoned';
        app.TF4SF__Sub_Product__c = 'Checking - Checking';
        applst.add(app);
        insert applst;
       
        Contact con1=new Contact();
        con1.lastname='Testing';
        con1.email='test@test.com';
        insert con1;
        
        app.TF4SF__Application_Status__c='Abandoned';
        update app;
        
        Map<string,string> newMap=new Map<string,string>(); 
        newMap.put(app.OwnerId,'');
        
        Test.startTest();
        ApplicationTriggerHandler Objapp=new ApplicationTriggerHandler();
        Objapp.sendAbandonedEmailForDeposit(applst);
        Objapp.sendAbandonedEmail(applst);
        Objapp.confirmationEmailMLO(applst);
        Objapp.sendPendingEmailForDeposit(applst);
        Objapp.sendConfirmationEmailForDeposit(applst);
        Objapp.changeUserNotification(applst,newMap);
        Test.stopTest();
    }
}