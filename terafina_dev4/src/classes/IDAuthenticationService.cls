/*************************************************
@Author : Nag
@Date : 8/2/2018
@Description : This makes outbound service calls
****************************************************/
@RestResource(urlMapping='/idAuthentication')
global class IDAuthenticationService{

    @HttpPost
    global static IDAuthResponse callIdAuthService(IDAuthRequest requestPayload){
        
        IDAuthResponse response = new IDAuthResponse();
        String Token = '';
        String idType = '';
        String NID_CUSTOM_LABEL = Label.IDAuth_NID_Service_Value;
        String PP_CUSTOM_LABEL = Label.IDAuth_PP_Service_Value;
        String GUID = '';
        try{ 
                VELOSETTINGS__C velo = VELOSETTINGS__C.getOrgDefaults();
                Token = AuthToken.Auth();
                ID_Auth_Config__mdt idAuthConfig;
                
                 
                if(requestPayload != null){
                    IDAuthRequest.DataFields DataFields = requestPayload.getDataFields();
                    idType = DataFields.getDocument() != null ? DataFields.getDocument().getDocumentType() : '';
                }
                
                for(ID_Auth_Config__mdt idAuth : [SELECT Configuration_Name__c,DeveloperName,Id,IDV_Fill_Fields__c,ID_Type__c,Label,Language,MasterLabel,Method__c,NamespacePrefix,QualifiedApiName,Service_Value__c,
                        Time_Out__c,UI_Prefill_Fields__c,URL__c,Use_Mock_Service__c,Valid_Criteria__c FROM ID_Auth_Config__mdt where ID_Type__c =: idType]){
                    idAuthConfig = idAuth;
                }
                
                System.debug('idAuthConfig ######:' + idAuthConfig);
                Http h = new Http();
                String method = 'POST';
                String URL = '';
                Integer timeOut = 120000;
                if(idAuthConfig !=null){
                    method = idAuthConfig.Method__c;
                    URL = idAuthConfig.URL__c;
                    timeOut = Integer.valueOf(idAuthConfig.Time_Out__c);
                }
                
                GUID = GUIDGenerator.getGUID();
                HttpRequest req = new HttpRequest();

                req.setHeader(velo.Request_GUID_Name__c,GUID);
                req.setEndpoint(URL);
                req.setHeader('Authorization','Bearer '+Token);
                req.setHeader('Accept','application/json');
                req.setHeader('Content-Type','application/json');
                req.setMethod(method);
                req.setTimeout(timeOut);
                
                String idBody = JSON.serialize(requestPayload);
                
                req.setBody(idBody);
                HttpResponse res = null;
                
                if(!idAuthConfig.Use_Mock_Service__c && !Test.IsRunningTest()){
                    System.debug('INSIDE ACUTAL API CALL @@@@@@@@@@@@@@: ');
                    res = h.send(req);
                }else if(idType == NID_CUSTOM_LABEL){
                    IDAuthRequestMockNID idANID = new IDAuthRequestMockNID();
                    res = idANID.respond(req);
                }else if(idType == PP_CUSTOM_LABEL){
                    IDAuthRequestMockPP idAPP = new IDAuthRequestMockPP();
                    res = idAPP.respond(req);
                } 
                system.debug('req : ' + req.getBody());
                system.debug('res : ' + res.getBody());
                String resBody = res.getBody();
                response = (IDAuthResponse)System.JSON.deserialize(resBody, IDAuthResponse.class);
                system.debug('response obj:' + response);

                DebugLogger.InsertDebugLog(requestPayload.applicationSfdcID, GUID, idType +' Request GUID');

            }catch(Exception ex){
                DebugLogger.InsertDebugLog(requestPayload.applicationSfdcID, GUID, idType +' Request GUID');
                system.debug('Exception ##### :' + ex);
            }
        return response;
    }
    
    
}