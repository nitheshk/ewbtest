global with sharing class DepositResumePageController {

    public TF4SF__Application__c app{get; set;}
	public String appId{get;set;}
	public User loggedInUser{get; set;}
	public String usr{get; set;}
	public String ut{get; set;}

	public DepositResumePageController(ApexPages.StandardController stdController) {
		appId = ApexPages.currentPage().getParameters().get('id');
		usr = ApexPages.currentPage().getParameters().get('usr');
		app = [SELECT Id, TF4SF__Application_Status__c, TF4SF__Application_Page__c, TF4SF__Current_Channel__c, TF4SF__Current_timestamp__c, TF4SF__Current_Person__c, TF4SF__Current_Branch_Name__c, TF4SF__Current_User_Email_Address__c, TF4SF__User_Token__c, TF4SF__User_Token_Expires__c, TF4SF__Application_Version__c FROM TF4SF__Application__c WHERE Id =: appId AND CreatedDate != NULL];
		System.debug('app.TF4SF__Application_Page__c in constructor ==== ' + app.TF4SF__Application_Page__c);
		if (usr != null) { loggedInUser = [SELECT Id, Email, TF4SF__Channel__c, TF4SF__Location__c, Name FROM User WHERE Id = :usr]; } else {
            loggedInUser = [SELECT Id, Email, TF4SF__Channel__c, TF4SF__Location__c, Name FROM User WHERE Id = :UserInfo.getUserId()];
        }
		if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Current_timestamp__c.isCreateable() && Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Current_timestamp__c.isUpdateable()) { app.TF4SF__Current_timestamp__c = System.now(); }
		if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Current_Person__c.isCreateable() && Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Current_Person__c.isUpdateable()) { app.TF4SF__Current_Person__c = loggedInUser.Id; }
		if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Current_Branch_Name__c.isCreateable() && Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Current_Branch_Name__c.isUpdateable()) { app.TF4SF__Current_Branch_Name__c = loggedInUser.TF4SF__Location__c; }
		if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Current_User_Email_Address__c.isCreateable() && Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Current_User_Email_Address__c.isUpdateable()) { app.TF4SF__Current_User_Email_Address__c = loggedInUser.Email; }

		if (loggedInUser != null && loggedInUser.TF4SF__Channel__c != null) {
			if (Schema.sObjectType.TF4SF__Application__c.fields.Ownerid.isCreateable() && Schema.sObjectType.TF4SF__Application__c.fields.Ownerid.isUpdateable()) { app.Ownerid = loggedInUser.Id; }
			if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Current_Channel__c.isCreateable() && Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Current_Channel__c.isUpdateable()) { app.TF4SF__Current_Channel__c = loggedInUser.TF4SF__Channel__c; }
		} else {
			if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Current_Channel__c.isCreateable() && Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Current_Channel__c.isUpdateable()) { app.TF4SF__Current_Channel__c = 'Online'; }
		}
	}

	public PageReference ResumeApp() {

		PageReference pg = null;
		String url = 'tf4sf__dsp';
		System.debug('appId ==== ' + appId);
		System.debug('app.TF4SF__Application_Page__c ==== ' + app.TF4SF__Application_Page__c);
		if (!String.isBlank(appId)) {
			if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Application_Status__c.isUpdateable()) {
				if (app.TF4SF__Application_Status__c != 'Submitted') {
					app.TF4SF__Application_Status__c = 'Open';
				}
			}
			if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Application_Page__c.isCreateable() && Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Application_Page__c.isUpdateable()) { app.TF4SF__Application_Page__c = app.TF4SF__Application_Page__c; }
			if(!test.isRunningTest()) CryptoHelperCustom.setAppToken(app);
			if (TF4SF__Application__c.SObjectType.getDescribe().isUpdateable()) { update app; }
			System.debug('app = ' + app);
			System.debug('app status after update =====' + app.TF4SF__Application_Status__c);
			System.debug('app.TF4SF__Application_Page__c after update =====' + app.TF4SF__Application_Page__c);

			TF4SF__Application__c latestApp = [SELECT Id, TF4SF__User_Token__c, TF4SF__Application_Page__c FROM TF4SF__Application__c WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
			System.debug('latestApp.TF4SF__Application_Page__c ==== ' + latestApp.TF4SF__Application_Page__c);
			System.debug('Encrypted usertoken from app = ' + app.TF4SF__User_Token__c);
			System.debug('Encrypted usertoken from latestApp = ' + latestApp.TF4SF__User_Token__c);
			String userToken = CryptoHelperCustom.decrypt(latestApp.TF4SF__User_Token__c);
			System.debug('Decrypted usertoken from latestApp = '+ userToken);

			TF4SF__Application_Activity__c appact = new TF4SF__Application_Activity__c ();
			if (Schema.sObjectType.TF4SF__Application_Activity__c.fields.TF4SF__Application__c.isCreateable()) { appact.TF4SF__Application__c = app.Id; }
			if (Schema.sObjectType.TF4SF__Application_Activity__c.fields.TF4SF__Branch__c.isCreateable()) { appact.TF4SF__Branch__c = app.TF4SF__Current_Branch_Name__c; }
			if (Schema.sObjectType.TF4SF__Application_Activity__c.fields.TF4SF__Channel__c.isCreateable()) { appact.TF4SF__Channel__c = app.TF4SF__Current_Channel__c; }
			if (Schema.sObjectType.TF4SF__Application_Activity__c.fields.TF4SF__Name__c.isCreateable()) { appact.TF4SF__Name__c = app.TF4SF__Current_Person__c; }
			if (Schema.sObjectType.TF4SF__Application_Activity__c.fields.TF4SF__Action__c.isCreateable()) { appact.TF4SF__Action__c = 'Resumed the Application'; }
			if (Schema.sObjectType.TF4SF__Application_Activity__c.fields.TF4SF__Activity_Time__c.isCreateable()) { appact.TF4SF__Activity_Time__c = System.now(); }
			if (TF4SF__Application_Activity__c.SObjectType.getDescribe().isCreateable()) { insert appact; }

			TF4SF.Logger.inputSource('ResumeApplication - Resumeapp', String.escapeSingleQuotes(appId));
			TF4SF.Logger.addMessage('Redirecting to dsp page', System.now().format());
			pg = new PageReference(TF4SF__SiteUrl__c.getOrgDefaults().TF4SF__Url__c + url + '#resume-email-verification');
			Cookie id = ApexPages.currentPage().getCookies().get('id');
			Cookie ut = ApexPages.currentPage().getCookies().get('ut');
			id = new Cookie('id', app.Id, null, -1, true);
			ut = new Cookie('ut', userToken, null, -1, true);
			// Set the new cookie for the page
			ApexPages.currentPage().setCookies(new Cookie[]{id, ut});
		}
		pg.setRedirect(false);
		TF4SF.Logger.writeAllLogs();
		System.debug('PageReference before return ======== ' + pg);
		return pg;
	}
}