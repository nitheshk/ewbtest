@isTest
public class MLOControllerTest{
@isTest static void Test_one(){

  String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
      Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
      User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
      EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US', ProfileId = p.Id,
      TimeZoneSidKey = 'America/Los_Angeles',
      UserName = uniqueUserName,TF4SF__Location__c='Yakima South 1st St');
      System.runAs(u) {
      
      TF4SF__Application__c app = new TF4SF__Application__c ();
        app.TF4SF__Product__c = 'Checking';
        app.TF4SF__Sub_Product__c = 'Checking - Checking';
        app.TF4SF__First_Name__c='firstname';
        app.TF4SF__Last_Name__c='lastname';
        app.TF4SF__City__c='testcity';
        app.TF4SF__Zip_Code__c='testzip';
        app.TF4SF__State__c='teststate';
        app.TF4SF__Street_Address_1__c='teststreet1';
        app.TF4SF__Street_Address_2__c='teststreet2';
        insert app;
          
       Account acc=new Account(Name='testname', Type='test1', Phone='234234234', BillingLatitude=67.8,
       BillingLongitude=45.5,BillingStreet='teststreet',BillingCity='testcity', BillingState='teststate');
       insert acc;  
          List<Task> taskList=new List<Task>();
          Task tk=new Task();
          tk.ActivityDate=Date.Today().addDays(5);
          tk.Description='Please call the customer at this numbmer';
          tk.Subject='';
          tk.WhatId=app.Id;
          tk.WhoId=u.Id;
          tk.Status='test';
          taskList.add(tk);
          
        test.startTest();
        MLOController mloc=new MLOController();
        MLOController.getMLO('40.5', '36.5',app.id, '8465', 'US', 'searchbyfilter');
           MLOController.getMLO2('40.5', '36.5',app.id, '8465', 'US', 'searchbyfilter');
           MLOController.getMLO3('40.5', '36.5',app.id, '8465', 'US', 'searchbyfilter');
           MLOController.getMLO4('40.5', '36.5',app.id, '8465', 'US', 'searchbyfilter');
           MLOController.getMLO5('40.5', '36.5',app.id, '8465', 'US', 'searchbyfilter');
           MLOController.getMLO6('40.5', '36.5',app.id, '8465', 'US', 'searchbyfilter');
           MLOController.getMLO7('40.5', '36.5',app.id, '8465', 'US', 'searchbyfilter');
           MLOController.getMLO8('40.5', '36.5',app.id, '8465', 'US', 'searchbyfilter');
           MLOController.getMLO9('40.5', '36.5',app.id, '8465', 'US', 'searchbyfilter');
          MLOController.getMLO10('40.5', '36.5',app.id, '8465', 'US', 'searchbyfilter');
          MLOController.getMLO11('40.5', '36.5',app.id, '8465', 'US', 'searchbyfilter');
           
        MLOController.saveAppDetails('phoneNumber','timeSlot', app.Id,'mloId');  
        test.stopTest();
}
} 
    
    @isTest static void Test_two(){

  String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
      Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
      User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
      EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US', ProfileId = p.Id,
      TimeZoneSidKey = 'America/Los_Angeles',
      UserName = uniqueUserName,TF4SF__Location__c='Yakima South 1st St');
      System.runAs(u) {
      
      TF4SF__Application__c app = new TF4SF__Application__c ();
        app.TF4SF__Product__c = 'Checking';
        app.TF4SF__Sub_Product__c = 'Checking - Checking';
        app.TF4SF__First_Name__c='firstname';
        app.TF4SF__Last_Name__c='lastname';
        app.TF4SF__City__c='testcity';
        app.TF4SF__Zip_Code__c='testzip';
        app.TF4SF__State__c='teststate';
        app.TF4SF__Street_Address_1__c='teststreet1';
        app.TF4SF__Street_Address_2__c='teststreet2';
        insert app;
          
       Account acc=new Account(Name='testname', Type='test1', Phone='234234234', BillingLatitude=67.8,
       BillingLongitude=45.5,BillingStreet='teststreet',BillingCity='testcity', BillingState='teststate');
       insert acc;  
          List<Task> taskList=new List<Task>();
          Task tk=new Task();
          tk.ActivityDate=Date.Today().addDays(5);
          tk.Description='Please call the customer at this numbmer';
          tk.Subject='';
          tk.WhatId=app.Id;
          tk.WhoId=u.Id;
          tk.Status='test';
          taskList.add(tk);
          
        test.startTest();
        MLOController mloc=new MLOController();
        MLOController.getMLO('40.5', '36.5',app.id, '8465', '', 'searchbyfilter');
        MLOController.saveAppDetails('phoneNumber','timeSlot', app.Id,'mloId');  
        test.stopTest();
}
} 
     @isTest static void Test_three(){

  String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
      Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
      User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
      EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US', ProfileId = p.Id,
      TimeZoneSidKey = 'America/Los_Angeles',
      UserName = uniqueUserName,TF4SF__Location__c='Yakima South 1st St');
      System.runAs(u) {
      
      TF4SF__Application__c app = new TF4SF__Application__c ();
        app.TF4SF__Product__c = 'Checking';
        app.TF4SF__Sub_Product__c = 'Checking - Checking';
        app.TF4SF__First_Name__c='firstname';
        app.TF4SF__Last_Name__c='lastname';
        app.TF4SF__City__c='testcity';
        app.TF4SF__Zip_Code__c='testzip';
        app.TF4SF__State__c='teststate';
        app.TF4SF__Street_Address_1__c='teststreet1';
        app.TF4SF__Street_Address_2__c='teststreet2';
        insert app;
          
       Account acc=new Account(Name='testname', Type='test1', Phone='234234234', BillingLatitude=67.8,
       BillingLongitude=45.5,BillingStreet='teststreet',BillingCity='testcity', BillingState='teststate');
       insert acc;  
          List<Task> taskList=new List<Task>();
          Task tk=new Task();
          tk.ActivityDate=Date.Today().addDays(5);
          tk.Description='Please call the customer at this numbmer';
          tk.Subject='';
          tk.WhatId=app.Id;
          tk.WhoId=u.Id;
          tk.Status='test';
          taskList.add(tk);
          
        test.startTest();
        MLOController ml=new MLOController();
        MLOController.getMLO('40.5', '36.5',app.id, '', 'US', 'searchbyfilter');
        MLOController.saveAppDetails('phoneNumber','timeSlot', app.Id,'mloId');  
        test.stopTest();
}
} 
    @isTest static void Test_fourth(){

  String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
      Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
      User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
      EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US', ProfileId = p.Id,
      TimeZoneSidKey = 'America/Los_Angeles',
      UserName = uniqueUserName,TF4SF__Location__c='Yakima South 1st St');
      System.runAs(u) {
      
      TF4SF__Application__c app = new TF4SF__Application__c ();
        app.TF4SF__Product__c = 'Checking';
        app.TF4SF__Sub_Product__c = 'Checking - Checking';
        app.TF4SF__First_Name__c='firstname';
        app.TF4SF__Last_Name__c='lastname';
        app.TF4SF__City__c='testcity';
        app.TF4SF__Zip_Code__c='testzip';
        app.TF4SF__State__c='teststate';
        app.TF4SF__Street_Address_1__c='teststreet1';
        app.TF4SF__Street_Address_2__c='teststreet2';
        insert app;
          
          Location__c loc=new Location__c();
          loc.Address__c='TestAddress';
          loc.Name__c='TestName';
          loc.State__c='AK';  
          loc.Zip_Code__c='56456';
          loc.Location__Latitude__s=40.5;
          loc.Location__Longitude__s=36.5;
          insert loc;
          
          Location_Mlo_Map__c objlm=new Location_Mlo_Map__c();
            objlm.Location__c=loc.id;
            objlm.MLO_User__c=u.Id;
            insert objlm;
         
       Account acc=new Account(Name='testname', Type='test1', Phone='234234234', BillingLatitude=67.8,
       BillingLongitude=45.5,BillingStreet='teststreet',BillingCity='testcity', BillingState='teststate');
       insert acc;  
          List<Task> taskList=new List<Task>();
          Task tk=new Task();
          tk.ActivityDate=Date.Today().addDays(5);
          tk.Description='Please call the customer at this numbmer';
          tk.Subject='';
          tk.WhatId=app.Id;
          tk.WhoId=u.Id;
          tk.Status='test';
          taskList.add(tk);
          
        test.startTest();
        MLOController mloc=new MLOController();
        MLOController.getMLO('40.5', '36.5',app.id, '', '', 'searchall');
        MLOController.saveAppDetails('phoneNumber','timeSlot', app.Id,'mloId');  
        test.stopTest();
}
} 
    @isTest static void Test_five(){

  String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
      Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
      User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
      EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US', ProfileId = p.Id,
      TimeZoneSidKey = 'America/Los_Angeles',
      UserName = uniqueUserName,TF4SF__Location__c='Yakima South 1st St');
      System.runAs(u) {
      
      TF4SF__Application__c app = new TF4SF__Application__c ();
        app.TF4SF__Product__c = 'Checking';
        app.TF4SF__Sub_Product__c = 'Checking - Checking';
        app.TF4SF__First_Name__c='firstname';
        app.TF4SF__Last_Name__c='lastname';
        app.TF4SF__City__c='testcity';
        app.TF4SF__Zip_Code__c='testzip';
        app.TF4SF__State__c='teststate';
        app.TF4SF__Street_Address_1__c='teststreet1';
        app.TF4SF__Street_Address_2__c='teststreet2';
        insert app;
          
          
       Account acc=new Account(Name='testname', Type='test1', Phone='234234234', BillingLatitude=67.8,
       BillingLongitude=45.5,BillingStreet='teststreet',BillingCity='testcity', BillingState='teststate');
       insert acc;  
          List<Task> taskList=new List<Task>();
          Task tk=new Task();
          tk.ActivityDate=Date.Today().addDays(5);
          tk.Description='Please call the customer at this numbmer';
          tk.Subject='';
          tk.WhatId=app.Id;
          tk.WhoId=u.Id;
          tk.Status='test';
          taskList.add(tk);
          
        test.startTest();
        MLOController mloc=new MLOController();
        MLOController.getMLO('40.5', '36.5',app.id, '8465', 'US', 'searchbygeolocation');
        MLOController.saveAppDetails('phoneNumber','timeSlot', app.Id,'mloId');  
        test.stopTest();
}
} 
}