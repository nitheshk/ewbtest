@isTest
global class MortgageSubmit_Mock implements HttpCalloutMock {
  // Implement this interface method
  global HTTPResponse respond(HTTPRequest req) {
    // Optionally, only send a mock response for a specific endpoint
    // and method.
    // Create a fake response
    HttpResponse res = new HttpResponse();
    res.setHeader('Content-Type','application/json');
    //res.setBody('{"TF4SF__Product__c":"Home Loan","TF4SF__First_Name__c":"test","TF4SF__Last_Name__c":"Test","TF4SF__Email_Address__c":"test@test.com","TF4SF__Street_Address_1__c":"TestStreet1","TF4SF__Street_Address_2__c":"TestStreet2","Street_Address_1_Prev_1_PA__c":"2","Zip_Code_Prev_1_PA__c":"2","Months_Prev_1_From_PA__c":"2","Months_Prev_1_To_PA__c":"2","Months_Prev_From_PA__c":"2","Months_Prev_To_PA__c":"2","Year_Prev_1_From_PA__c":"test","Year_Prev_1_To_PA__c":"test","Year_Prev_From_PA__c":"test","Year_Prev_To_PA__c":"test","TF4SF__Housing_Status__c":"test","TF4SF__Street_Address_1_Prev__c":"test","TF4SF__Street_Address_2_Prev__c":"test","TF4SF__City_Prev__c":"test","TF4SF__State_Prev__c":"test","Street_Address_1_Prev_2_PA__c":"test","City_Prev_1_PA__c":"test","State_Prev_1_PA__c":"test","TF4SF__Zip_Code_Prev__c":"test","TF4SF__City__c":"USA","TF4SF__State__c":"USA","TF4SF__Zip_Code__c":"43534","TF4SF__Primary_Phone_Number__c":"1321423444","TF4SF__User_Token__c":"Testtoken","TF4SF__Sub_Product__c":"Home Loan - Short Ap"}');
    res.setBody('{"data": {"data": { "id": "3f0f3350-1a09-4298-affc-fad742811980","RequestUUID": "a04m000000BAxJyAAL",  "ErrorMessageList": null        },        "result": {            "success": true,            "code": "200",            "message": null        }    },    "result": {        "success": true,        "code": "200",        "message": "Success"    }}');
    res.setStatusCode(200);
    return res;
  }
}