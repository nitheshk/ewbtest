/*************************************************
@Author : Nag
@Date : 8/3/2018
@Description : This request payload for IDAuth service call
****************************************************/
global class IDAuthRequest
{
    //This can't be deleted or replaced generic varaible
    global transient String applicationSfdcID {get;set;}

    private String ConfigurationName;

    private String CountryCode;

    private boolean AcceptTruliooTermsAndConditions;

    private DataFields DataFields;

    public String getConfigurationName ()
    {
        return ConfigurationName;
    }

    public void setConfigurationName (String ConfigurationName)
    {
        this.ConfigurationName = ConfigurationName;
    }

    public String getCountryCode ()
    {
        return CountryCode;
    }

    public void setCountryCode (String CountryCode)
    {
        this.CountryCode = CountryCode;
    }

    public boolean getAcceptTruliooTermsAndConditions ()
    {
        return AcceptTruliooTermsAndConditions;
    }

    public void setAcceptTruliooTermsAndConditions (boolean AcceptTruliooTermsAndConditions)
    {
        this.AcceptTruliooTermsAndConditions = AcceptTruliooTermsAndConditions;
    }

    public DataFields getDataFields ()
    {
        return DataFields;
    }

    public void setDataFields (DataFields DataFields)
    {
        this.DataFields = DataFields;
    }
    
    global class DataFields
    {
        private Document Document;
    
        public Document getDocument ()
        {
            return Document;
        }
    
        public void setDocument (Document Document)
        {
            this.Document = Document;
        }
    } 
    
    global class Document
    {
        private String DocumentFrontImage;
    
        private String DocumentBackImage;
    
        private String DocumentType;
    
        public String getDocumentFrontImage ()
        {
            return DocumentFrontImage;
        }
    
        public void setDocumentFrontImage (String DocumentFrontImage)
        {
            this.DocumentFrontImage = DocumentFrontImage;
        }
    
        public String getDocumentBackImage ()
        {
            return DocumentBackImage;
        }
    
        public void setDocumentBackImage (String DocumentBackImage)
        {
            this.DocumentBackImage = DocumentBackImage;
        }
    
        public String getDocumentType ()
        {
            return DocumentType;
        }
    
        public void setDocumentType (String DocumentType)
        {
            this.DocumentType = DocumentType;
        }
    }
    
}