@isTest
public class OnlineBankingControllerTest{

    static testMethod void test1() {
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
            EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US', ProfileId = p.Id,
            TimeZoneSidKey = 'America/Los_Angeles',
            UserName = uniqueUserName,TF4SF__Location__c='Yakima South 1st St');
            
        System.runAs(u) {
            
             TF4SF__Application_Configuration__c appConfig = new TF4SF__Application_Configuration__c();
        appConfig.TF4SF__Application_Code__c = 'dsp4_0';
        appConfig.TF4SF__Theme__c = 'dsp4_0';
        appConfig.TF4SF__Key__c = 'YMmKJt5QxS7KlAEASMsj4Q==';
        appConfig.TF4SF__Popup_Seconds__c = 120;
        appConfig.TF4SF__Timeout_Seconds__c = 900;
        insert appConfig;   

            
            
            TF4SF__Application__c app1 = new TF4SF__Application__c(TF4SF__Product__c = 'checking', TF4SF__Sub_Product__c = 'Savings',
                OwnerId = u.Id, TF4SF__Current_Person__c = u.Id, TF4SF__Created_Channel__c = 'Branch', TF4SF__Current_Channel__c = 'Online',
                TF4SF__Created_Branch_Name__c = 'Home', TF4SF__Created_Person__c = u.Id, TF4SF__Current_Branch_Name__c = 'WallaWa',
                TF4SF__Application_Page__c = 'CrossSellPage',TF4SF__Primary_Offer__c='Savings',TF4SF__Type_of_Savings__c='Savings',TF4SF__Second_Offer__c='Savings',TF4SF__Third_Offer__c='Savings', TF4SF__User_Token__c = 'CyRKhdyNzN6eazIJktYCGbx5SUeGnTXRr8vQIi1tx8pDN2nVEm74GvThg5QB9wPZ',TF4SF__Theme_URL__c = 'TestURL',TF4SF__Application_Status__c = 'Submitted',TF4SF__Special_Promo_Code__c = 'zass45a',TF4SF__Application_Version__c = '4.25',
                 TF4SF__Current_timestamp__c=System.now(),TF4SF__Created_Timestamp__c = System.now() 
            );
            insert app1;
			
			TF4SF__Application_Activity__c appact=new TF4SF__Application_Activity__c();
            appact.TF4SF__Application__c=app1.Id;
            appact.TF4SF__Action__c='Created the Application from Online Banking';
            appact.TF4SF__Activity_Time__c = System.now();
            appact.TF4SF__Channel__c='online';
            appact.TF4SF__Branch__c='US';  
            appact.TF4SF__Name__c =u.id;
            insert appact;
            
                TF4SF__Identity_Information__c ident = new TF4SF__Identity_Information__c(
        TF4SF__Date_of_Birth__c = '01/01/1970',TF4SF__SSN_Prime__c = '111222111',TF4SF__SSN_Last_Four_PA__c = '1112',TF4SF__ID_Type__c = 'Drivers License',TF4SF__Identity_Number_Primary__c = '111222111',TF4SF__State_Issued__c = 'CA',
        TF4SF__Issue_Date__c = '01/01/1970',TF4SF__Expiry_Date__c = '01/01/1970',TF4SF__SSN_Last_Four_J1__c = '1111',TF4SF__SSN_Last_Four_J2__c = '1111',
        TF4SF__SSN_Last_Four_J3__c = '1111',TF4SF__SSN_J1__c = '111222111',TF4SF__SSN_J2__c = '111222111',TF4SF__SSN_J3__c = '111222111',
        TF4SF__Date_of_Birth_J1__c = '01/01/1970',TF4SF__ID_Type_J1__c = 'Drivers License',TF4SF__Identity_Number_J1__c = '111222111',TF4SF__State_Issued_J1__c = 'CA',
        TF4SF__Issue_Date_J1__c = '01/01/1970',TF4SF__Expiry_Date_J1__c = '01/01/1970',TF4SF__Date_of_Birth_J2__c = '01/01/1970',TF4SF__ID_Type_J2__c = 'Drivers License',TF4SF__Identity_Number_J2__c = '111222111',
        TF4SF__State_Issued_J2__c = 'CA',TF4SF__Issue_Date_J2__c = '01/01/1970',TF4SF__Expiry_Date_J2__c = '01/01/1970',TF4SF__Date_of_Birth_J3__c = '01/01/1970',
        TF4SF__ID_Type_J3__c = 'Drivers License',TF4SF__Identity_Number_J3__c = '111222111',TF4SF__State_Issued_J3__c = 'CA',TF4SF__Issue_Date_J3__c = '01/01/1970',
        TF4SF__Expiry_Date_J3__c = '01/01/1970',TF4SF__Custom_Text11__c = 'Name',TF4SF__Custom_Text12__c = 'Name',TF4SF__Custom_Text13__c = 'Name',TF4SF__Custom_Text14__c = 'Name',
        TF4SF__Application__c = app1.Id);
        insert ident;
		
	TF4SF__About_Account__c ab = new TF4SF__About_Account__c();
        ab.TF4SF__Application__c = app1.id;
      
        insert ab;
      
      TF4SF__Employment_Information__c emp = new TF4SF__Employment_Information__c(
        TF4SF__Employer__c = 'Employer',TF4SF__Employer_J1__c = 'Employer', TF4SF__Employer_J2__c = 'Employer', 
        TF4SF__Employer_J3__c = 'Employer',TF4SF__Application__c = app1.Id);
      insert emp;

            List<TF4SF__Product_Codes__c> pclist = new  List<TF4SF__Product_Codes__c>();
            TF4SF__Product_Codes__c pc1 = new TF4SF__Product_Codes__c (Name = 'Name', TF4SF__Product__c = 'ALL',
            TF4SF__Sub_Product__c = 'Test Sub Product', Product_Image__c = 'Test Image',TF4SF__Sub_Product_Code__c = 'ALL',
            Landing_Page_Description__c = ' Test Title');
            insert pc1;
            pclist.add(pc1);
            
            OnlineEncryption__c One = new  OnlineEncryption__c();
            one.Key__c = 'H+RYeZyh1kh5ZpWExkQbpw==';
            one.Vector__c = 'H+RYeZyh1kh5ZpWExkQbpw==';
            insert one;
            
           
           
            
            test.StartTest();
           
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            String JsonMsg = '{"FirstName":"TestFirstName","MiddleName":"TestMiddleName","LastName":"TestLastName","FullName":"TestFullName","Suffix":"Mr","Address1":"TestAddres1","Address2":"TestAddres2","City":"NY","State":"NewYork","Zip":"4357","ZipPlusFour":"TestZipPlusFour","PrimaryPhone":"1234567890","WorkPhone":"(509)525-2100","TaxIdentifier":"Gst543","MemberNumber":"807","DOB":"20/12/1990","emailAddress":"test@test.com","IDType":"Bussiness","IDNumber":"6465","IDStateIssued":"NewYork","IDIssueDate":"15/02/1976","IDExpirationDate":"15/02/1989","IsEmployee":"PEmp","'+ u.id +'","' + u.id +'"}'; 
            req.requestBody = Blob.valueof(JsonMsg);
            System.debug('req.requestBody' + req.requestBody);

            req.requestURI = '/authPrefill/v1/';  
            req.httpMethod = 'POST';
            RestContext.request = req;
            RestContext.response = res;
            RestContext.request.params.put('subProductCode', 'CKPC');
            PageReference pageRef = Page.TF4SF__OnlinePage;   
            pageRef.getParameters().put('id', String.valueOf(app1.Id));
            Test.setCurrentPage(pageRef);
            
		
            Test.setMock(HttpCalloutMock.class, new OnlineBankingControllerMock());
            OnlineBankingController onCtrl = new OnlineBankingController();
            OnlineBankingController.startApplication();
            OnlineBankingController.parseProductType(app1);
          //  OnlineBankingController.formatDate('1986/02/02');
            test.stopTest();
        }
    }
      static testMethod void test2() {
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
            EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US', ProfileId = p.Id,
            TimeZoneSidKey = 'America/Los_Angeles',
            UserName = uniqueUserName,TF4SF__Location__c='Yakima South 1st St');
            
        TF4SF__Application_Configuration__c appConfig = new TF4SF__Application_Configuration__c();
        appConfig.TF4SF__Application_Code__c = 'dsp4_0';
        appConfig.TF4SF__Theme__c = 'dsp4_0';
        appConfig.TF4SF__Key__c = 'YMmKJt5QxS7KlAEASMsj4Q==';
        appConfig.TF4SF__Popup_Seconds__c = 120;
        appConfig.TF4SF__Timeout_Seconds__c = 900;
        insert appConfig;   

        System.runAs(u) {
            TF4SF__Application__c app1 = new TF4SF__Application__c(TF4SF__Product__c = 'Savings', TF4SF__Sub_Product__c = 'All',
                OwnerId = u.Id, TF4SF__Current_Person__c = u.Id, TF4SF__Created_Channel__c = 'Branch', TF4SF__Current_Channel__c = 'Online',
                TF4SF__Created_Branch_Name__c = 'Home', TF4SF__Created_Person__c = u.Id, TF4SF__Current_Branch_Name__c = 'WallaWa',
                TF4SF__Application_Page__c = 'CrossSellPage',TF4SF__Type_of_Checking__c='All', TF4SF__User_Token__c = 'CyRKhdyNzN6eazIJktYCGbx5SUeGnTXRr8vQIi1tx8pDN2nVEm74GvThg5QB9wPZ',TF4SF__Theme_URL__c = 'TestURL',TF4SF__Application_Status__c = 'Submitted',TF4SF__Special_Promo_Code__c = 'zass45a',TF4SF__Application_Version__c = '4.25',
                 TF4SF__Current_timestamp__c=System.now(),TF4SF__Created_Timestamp__c = System.now() 
            );
            insert app1;
			
			TF4SF__Application_Activity__c appact=new TF4SF__Application_Activity__c();
            appact.TF4SF__Application__c=app1.Id;
            appact.TF4SF__Action__c='Resumed the Application';
            appact.TF4SF__Activity_Time__c = System.now();
            appact.TF4SF__Channel__c='online';
            appact.TF4SF__Branch__c='US';  
            appact.TF4SF__Name__c =u.id;
            insert appact;
            
                TF4SF__Identity_Information__c ident = new TF4SF__Identity_Information__c(
        TF4SF__Date_of_Birth__c = '01/01/1970',TF4SF__SSN_Prime__c = '111222111',TF4SF__SSN_Last_Four_PA__c = '1112',TF4SF__ID_Type__c = 'Drivers License',TF4SF__Identity_Number_Primary__c = '111222111',TF4SF__State_Issued__c = 'CA',
        TF4SF__Issue_Date__c = '01/01/1970',TF4SF__Expiry_Date__c = '01/01/1970',TF4SF__SSN_Last_Four_J1__c = '1111',TF4SF__SSN_Last_Four_J2__c = '1111',
        TF4SF__SSN_Last_Four_J3__c = '1111',TF4SF__SSN_J1__c = '111222111',TF4SF__SSN_J2__c = '111222111',TF4SF__SSN_J3__c = '111222111',
        TF4SF__Date_of_Birth_J1__c = '01/01/1970',TF4SF__ID_Type_J1__c = 'Drivers License',TF4SF__Identity_Number_J1__c = '111222111',TF4SF__State_Issued_J1__c = 'CA',
        TF4SF__Issue_Date_J1__c = '01/01/1970',TF4SF__Expiry_Date_J1__c = '01/01/1970',TF4SF__Date_of_Birth_J2__c = '01/01/1970',TF4SF__ID_Type_J2__c = 'Drivers License',TF4SF__Identity_Number_J2__c = '111222111',
        TF4SF__State_Issued_J2__c = 'CA',TF4SF__Issue_Date_J2__c = '01/01/1970',TF4SF__Expiry_Date_J2__c = '01/01/1970',TF4SF__Date_of_Birth_J3__c = '01/01/1970',
        TF4SF__ID_Type_J3__c = 'Drivers License',TF4SF__Identity_Number_J3__c = '111222111',TF4SF__State_Issued_J3__c = 'CA',TF4SF__Issue_Date_J3__c = '01/01/1970',
        TF4SF__Expiry_Date_J3__c = '01/01/1970',TF4SF__Custom_Text11__c = 'Name',TF4SF__Custom_Text12__c = 'Name',TF4SF__Custom_Text13__c = 'Name',TF4SF__Custom_Text14__c = 'Name',
        TF4SF__Application__c = app1.Id);
        insert ident;
		
		TF4SF__About_Account__c ab = new TF4SF__About_Account__c();
        ab.TF4SF__Application__c = app1.id;
      
        insert ab;
      
      TF4SF__Employment_Information__c emp = new TF4SF__Employment_Information__c(
        TF4SF__Employer__c = 'Employer',TF4SF__Employer_J1__c = 'Employer', TF4SF__Employer_J2__c = 'Employer', 
        TF4SF__Employer_J3__c = 'Employer',TF4SF__Application__c = app1.Id);
      insert emp;

            List<TF4SF__Product_Codes__c> pclist = new  List<TF4SF__Product_Codes__c>();
            TF4SF__Product_Codes__c pc1 = new TF4SF__Product_Codes__c (Name = 'Name', TF4SF__Product__c = 'ALL',
            TF4SF__Sub_Product__c = 'Test Sub Product', Product_Image__c = 'Test Image',TF4SF__Sub_Product_Code__c = 'ALL',
            Landing_Page_Description__c = ' Test Title');
            insert pc1;
            pclist.add(pc1);
            
            OnlineEncryption__c One = new  OnlineEncryption__c();
            one.Key__c = 'Test';
            one.Vector__c = 'Vector test';
            insert one;
            
            test.StartTest();
           
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            String JsonMsg = '{"FirstName":"TestFirstName","MiddleName":"TestMiddleName","LastName":"TestLastName","FullName":"TestFullName","Suffix":"Mr","Address1":"TestAddres1","Address2":"TestAddres2","City":"NY","State":"NewYork","Zip":"4357","ZipPlusFour":"TestZipPlusFour","PrimaryPhone":"1234567890","WorkPhone":"(509)525-2100","TaxIdentifier":"Gst543","MemberNumber":"807","DOB":"20/12/1990","emailAddress":"test@test.com","IDType":"Bussiness","IDNumber":"6465","IDStateIssued":"NewYork","IDIssueDate":"15/02/1976","IDExpirationDate":"15/02/1989","IsEmployee":"PEmp","'+ u.id +'","' + u.id +'"}'; 
            req.requestBody = Blob.valueof(JsonMsg);
            System.debug('req.requestBody' + req.requestBody);

            req.requestURI = '/authPrefill/v1/';  
           req.httpMethod = 'POST';
            RestContext.request = req;
            RestContext.response = res;
            
            //RestContext.request = new RestRequest();
       //   RestContext.request.params.put('subProductCode', 'CKPC');
		   RestContext.request.params.put('onbId', app1.Id);
            PageReference pageRef = Page.TF4SF__OnlinePage;   
              pageRef.getParameters().put('id', String.valueOf(app1.Id));
            Test.setCurrentPage(pageRef);
           Test.setMock(HttpCalloutMock.class, new OnlineBankingControllerMock());
            OnlineBankingController onCtrl = new OnlineBankingController();
            OnlineBankingController.startApplication();
            OnlineBankingController.parseProductType(app1);
          //  OnlineBankingController.formatDate('1986/02/02');
            test.stopTest();
        }
    }
    
 static testMethod void test3() {
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
            EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US', ProfileId = p.Id,
            TimeZoneSidKey = 'America/Los_Angeles',
            UserName = uniqueUserName,TF4SF__Location__c='Yakima South 1st St');
            
        TF4SF__Application_Configuration__c appConfig = new TF4SF__Application_Configuration__c();
        appConfig.TF4SF__Application_Code__c = 'dsp4_0';
        appConfig.TF4SF__Theme__c = 'dsp4_0';
        appConfig.TF4SF__Key__c = 'YMmKJt5QxS7KlAEASMsj4Q==';
        appConfig.TF4SF__Popup_Seconds__c = 120;
        appConfig.TF4SF__Timeout_Seconds__c = 900;
        insert appConfig;   

        System.runAs(u) {
            TF4SF__Application__c app1 = new TF4SF__Application__c(TF4SF__Product__c = 'Savings', TF4SF__Sub_Product__c = 'Checking',
                OwnerId = u.Id, TF4SF__Current_Person__c = u.Id, TF4SF__Created_Channel__c = 'Branch', TF4SF__Current_Channel__c = 'Online',
                TF4SF__Created_Branch_Name__c = 'Home', TF4SF__Created_Person__c = u.Id, TF4SF__Current_Branch_Name__c = 'WallaWa',TF4SF__Type_of_Checking__c='Checking',
                TF4SF__Application_Page__c = 'CrossSellPage', TF4SF__User_Token__c = 'CyRKhdyNzN6eazIJktYCGbx5SUeGnTXRr8vQIi1tx8pDN2nVEm74GvThg5QB9wPZ',TF4SF__Theme_URL__c = 'TestURL',TF4SF__Application_Status__c = 'Submitted',TF4SF__Special_Promo_Code__c = 'zass45a',TF4SF__Application_Version__c = '4.25',
                 TF4SF__Current_timestamp__c=System.now(),TF4SF__Primary_Offer__c='Checking',TF4SF__Second_Offer__c='Checking',TF4SF__Third_Offer__c='Checking',TF4SF__Created_Timestamp__c = System.now() 
            );
            insert app1;
			
			TF4SF__Application_Activity__c appact=new TF4SF__Application_Activity__c();
            appact.TF4SF__Application__c=app1.Id;
            appact.TF4SF__Action__c='Resumed the Application';
            appact.TF4SF__Activity_Time__c = System.now();
            appact.TF4SF__Channel__c='online';
            appact.TF4SF__Branch__c='US';  
            appact.TF4SF__Name__c =u.id;
            insert appact;
            
                TF4SF__Identity_Information__c ident = new TF4SF__Identity_Information__c(
        TF4SF__Date_of_Birth__c = '01/01/1970',TF4SF__SSN_Prime__c = '111222111',TF4SF__SSN_Last_Four_PA__c = '1112',TF4SF__ID_Type__c = 'Drivers License',TF4SF__Identity_Number_Primary__c = '111222111',TF4SF__State_Issued__c = 'CA',
        TF4SF__Issue_Date__c = '01/01/1970',TF4SF__Expiry_Date__c = '01/01/1970',TF4SF__SSN_Last_Four_J1__c = '1111',TF4SF__SSN_Last_Four_J2__c = '1111',
        TF4SF__SSN_Last_Four_J3__c = '1111',TF4SF__SSN_J1__c = '111222111',TF4SF__SSN_J2__c = '111222111',TF4SF__SSN_J3__c = '111222111',
        TF4SF__Date_of_Birth_J1__c = '01/01/1970',TF4SF__ID_Type_J1__c = 'Drivers License',TF4SF__Identity_Number_J1__c = '111222111',TF4SF__State_Issued_J1__c = 'CA',
        TF4SF__Issue_Date_J1__c = '01/01/1970',TF4SF__Expiry_Date_J1__c = '01/01/1970',TF4SF__Date_of_Birth_J2__c = '01/01/1970',TF4SF__ID_Type_J2__c = 'Drivers License',TF4SF__Identity_Number_J2__c = '111222111',
        TF4SF__State_Issued_J2__c = 'CA',TF4SF__Issue_Date_J2__c = '01/01/1970',TF4SF__Expiry_Date_J2__c = '01/01/1970',TF4SF__Date_of_Birth_J3__c = '01/01/1970',
        TF4SF__ID_Type_J3__c = 'Drivers License',TF4SF__Identity_Number_J3__c = '111222111',TF4SF__State_Issued_J3__c = 'CA',TF4SF__Issue_Date_J3__c = '01/01/1970',
        TF4SF__Expiry_Date_J3__c = '01/01/1970',TF4SF__Custom_Text11__c = 'Name',TF4SF__Custom_Text12__c = 'Name',TF4SF__Custom_Text13__c = 'Name',TF4SF__Custom_Text14__c = 'Name',
        TF4SF__Application__c = app1.Id);
        insert ident;
		
		TF4SF__About_Account__c ab = new TF4SF__About_Account__c();
        ab.TF4SF__Application__c = app1.id;
      
        insert ab;
      
      TF4SF__Employment_Information__c emp = new TF4SF__Employment_Information__c(
        TF4SF__Employer__c = 'Employer',TF4SF__Employer_J1__c = 'Employer', TF4SF__Employer_J2__c = 'Employer', 
        TF4SF__Employer_J3__c = 'Employer',TF4SF__Application__c = app1.Id);
      insert emp;

            List<TF4SF__Product_Codes__c> pclist = new  List<TF4SF__Product_Codes__c>();
            TF4SF__Product_Codes__c pc1 = new TF4SF__Product_Codes__c (Name = 'Name', TF4SF__Product__c = 'ALL',
            TF4SF__Sub_Product__c = 'Test Sub Product', Product_Image__c = 'Test Image',TF4SF__Sub_Product_Code__c = 'ALL',
            Landing_Page_Description__c = ' Test Title');
            insert pc1;
            pclist.add(pc1);
            
            OnlineEncryption__c One = new  OnlineEncryption__c();
            one.Key__c = 'Test';
            one.Vector__c = 'Vector test';
            insert one;
            
            test.StartTest();
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            String JsonMsg = '{"FirstName":"TestFirstName","MiddleName":"TestMiddleName","LastName":"TestLastName","FullName":"TestFullName","Suffix":"Mr","Address1":"TestAddres1","Address2":"TestAddres2","City":"NY","State":"NewYork","Zip":"4357","ZipPlusFour":"TestZipPlusFour","PrimaryPhone":"1234567890","WorkPhone":"(509)525-2100","TaxIdentifier":"Gst543","MemberNumber":"807","DOB":"20/12/1990","emailAddress":"test@test.com","IDType":"Bussiness","IDNumber":"6465","IDStateIssued":"NewYork","IDIssueDate":"15/02/1976","IDExpirationDate":"15/02/1989","IsEmployee":"PEmp","'+ u.id +'","' + u.id +'"}'; 
            req.requestBody = Blob.valueof(JsonMsg);
            System.debug('req.requestBody' + req.requestBody);

            req.requestURI = '/authPrefill/v1/';  
            req.httpMethod = 'POST';
            RestContext.request = req;
            RestContext.response = res;
          
		    RestContext.request.params.put('onbId', app1.Id);
            PageReference pageRef = Page.TF4SF__OnlinePage;   
            pageRef.getParameters().put('id', String.valueOf(app1.Id));
            Test.setCurrentPage(pageRef);
			
			Test.setMock(HttpCalloutMock.class, new OnlineBankingControllerMock());

            OnlineBankingController onCtrl = new OnlineBankingController();
            OnlineBankingController.startApplication();
            OnlineBankingController.parseProductType(app1);
            test.stopTest();
    }   
}
}