@isTest
global class BatchtoSendChildAppReminderEmailsTest{ 

@isTest static void method_one(){

    
        TF4SF__Application__c app = new TF4SF__Application__c ();
        app.TF4SF__Application_Status__c = 'Open';
        app.TF4SF__Product__c = 'Home Loan';
        app.TF4SF__Sub_Product__c =  'Home Loan - Short App';
        
        insert app;
         List<Trustee_Guarantor__c> trustlst=new  List<Trustee_Guarantor__c> ();
        
      List<EmailTemplate> etTrustee=new List<EmailTemplate>();
      insert etTrustee;
       
       List<EmailTemplate> etBusiness =new  List<EmailTemplate>();
      insert etBusiness;
       
    
        Database.QueryLocator QL;
        Database.BatchableContext BC;
        BatchtoSendChildAppReminderEmails instance1 = new BatchtoSendChildAppReminderEmails();
        instance1.start(BC);
        instance1.execute(BC,trustlst);
        instance1.finish(BC);
}
}