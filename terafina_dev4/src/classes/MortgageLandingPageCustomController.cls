global with sharing class MortgageLandingPageCustomController {
    //To be deleted
    public String productCode {get; set;}
    public String subProductCode {get; set;}
    public String ipaddress {get; set;}
    public String userAgent {get; set;}
    public String promoCode {get; set;}
    public String version { get; set; }
    public String zipCode { get; set; }
    public String state { get; set; }
    public String productCategory { get; set; }
    public User loggedInUser { get; set; }
    public String userId { get; set; }
    public TF4SF__Application_Configuration__c appConfig = TF4SF__Application_Configuration__c.getOrgDefaults();
    public String nameSpace { get; set; }

    public MortgageLandingPageCustomController() {
        this.productCode = ApexPages.currentPage().getParameters().get('prdCode');
        System.debug('The product code is ##### ' + productCode);
        this.subProductCode = ApexPages.currentPage().getParameters().get('subPrdCode');
        System.debug('The Sub Product code is ##### ' + subProductCode);
        this.ipaddress = ApexPages.currentPage().getHeaders().get('X-Salesforce-SIP');
        System.debug('The Ip Address is ##### ' + ipaddress);
        this.userAgent = ApexPages.currentPage().getHeaders().get('USER-AGENT');
        System.debug('The Ip Address is ##### ' + userAgent);
        this.version = ApexPages.currentPage().getParameters().get('v');
        System.debug('The Version is ##### ' + version);
        this.nameSpace = (String.isEmpty(appConfig.TF4SF__Namespace__c)) ? '' : appConfig.TF4SF__Namespace__c;
        this.promoCode = ApexPages.currentPage().getParameters().get('PromoCode');
        System.debug('The Promo Code is ## ' + promoCode);
        this.zipCode = ApexPages.currentPage().getParameters().get('ZipCode');
        System.debug('The ZipCode is ##### ' + ZipCode);
        this.state = ApexPages.currentPage().getParameters().get('State');
        System.debug('The State is ##### ' + state);
        this.productCategory = ApexPages.currentPage().getParameters().get('ProductCategory');
        System.debug('The productCategory is ##### ' + productCategory);
        userId = UserInfo.getUserId();
        loggedInUser = [SELECT Id, TF4SF__Channel__c, Name, Email, TF4SF__Location__c, Profile.Name FROM User WHERE Id = :userId];
    }

    public PageReference beginApp() {
        try { // Instantiate all of the required Objects
            TF4SF__Application__c app = new TF4SF__Application__c();
            TF4SF__Application2__c app2 = new TF4SF__Application2__c();
            TF4SF__Employment_Information__c emp = new TF4SF__Employment_Information__c();
            TF4SF__Identity_Information__c iden = new TF4SF__Identity_Information__c();
            TF4SF__About_Account__c acc = new TF4SF__About_Account__c();
            TF4SF__Application_Activity__c appAct = new TF4SF__Application_Activity__c();
            app = setAppValues(app, subProductCode, ipaddress, loggedInUser, promoCode);



            if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Application_Version__c.isCreateable()) {
              app.TF4SF__Application_Version__c = (version == null) ? appConfig.TF4SF__Application_Version__c : version;
            }
            if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__User_Agent__c.isCreateable()) {
              app.TF4SF__User_Agent__c = userAgent;
            }
            
            
            System.debug('productCategory ==>' + productCategory);
            if (Schema.sObjectType.TF4SF__Application__c.fields.Product_Category__c.isCreateable()) { 
                
                if(String.isNotBlank(productCategory)) {
                    if(productCategory.equalsIgnoreCase('purchase')) {
                        app.Product_Category__c = 'Purchase';
                    }
                    else if( productCategory.equalsIgnoreCase('refinance')){
                        app.Product_Category__c = 'Refinance';
                    }
                }
                    
            }
            
        
            
            if (TF4SF__Application__c.SObjectType.getDescribe().isCreateable()) { insert app; }
            if (Schema.sObjectType.TF4SF__Application2__c.fields.TF4SF__Application__c.isCreateable()) { app2.TF4SF__Application__c = app.Id; }
            if (Schema.sObjectType.TF4SF__Employment_Information__c.fields.TF4SF__Application__c.isCreateable()) { emp.TF4SF__Application__c = app.Id; }
            if (Schema.sObjectType.TF4SF__Identity_Information__c.fields.TF4SF__Application__c.isCreateable()) { iden.TF4SF__Application__c = app.Id; }
            if (Schema.sObjectType.TF4SF__About_Account__c.fields.TF4SF__Application__c.isCreateable()) { acc.TF4SF__Application__c = app.Id; }
            if (Schema.sObjectType.TF4SF__About_Account__c.fields.TF4SF__Zip_Code_Mrt__c.isCreateable()) { acc.TF4SF__Zip_Code_Mrt__c = String.isNotBlank(zipCode) ? zipCode : null ;}
            if (Schema.sObjectType.TF4SF__About_Account__c.fields.TF4SF__State_AboutAccount__c.isCreateable()) { acc.TF4SF__State_AboutAccount__c = String.isNotBlank(state) ? state : null ;}
            if (Schema.sObjectType.TF4SF__Application_Activity__c.fields.TF4SF__Application__c.isCreateable()) { appact.TF4SF__Application__c = app.Id; }
            if (Schema.sObjectType.TF4SF__Application_Activity__c.fields.TF4SF__Channel__c.isCreateable()) { appact.TF4SF__Channel__c = app.TF4SF__Current_Channel__c; }
            if (Schema.sObjectType.TF4SF__Application_Activity__c.fields.TF4SF__Name__c.isCreateable()) { appact.TF4SF__Name__c = app.TF4SF__Current_Person__c; }
            if (Schema.sObjectType.TF4SF__Application_Activity__c.fields.TF4SF__Action__c.isCreateable()) { appact.TF4SF__Action__c = 'Created the Application'; }
            if (Schema.sObjectType.TF4SF__Application_Activity__c.fields.TF4SF__Activity_Time__c.isCreateable()) { appact.TF4SF__Activity_Time__c = System.now(); }
            
            
            if (TF4SF__Application2__c.SObjectType.getDescribe().isCreateable()) { insert app2; }
            if (TF4SF__Employment_Information__c.SObjectType.getDescribe().isCreateable()) { insert emp; }
            if (TF4SF__Identity_Information__c.SObjectType.getDescribe().isCreateable()) { insert iden; }
            if (TF4SF__About_Account__c.SObjectType.getDescribe().isCreateable()) { insert acc; }
            if (TF4SF__Application_Activity__c.SObjectType.getDescribe().isCreateable()) { insert appact; }

            String userToken = decrypt(app.TF4SF__User_Token__c);
            String dspUrl = nameSpace + 'dsp';
            PageReference p = new PageReference(TF4SF__SiteUrl__c.getOrgDefaults().TF4SF__Url__c + dspUrl);
            Cookie id = ApexPages.currentPage().getCookies().get('id');
            Cookie ut = ApexPages.currentPage().getCookies().get('ut');
            id = new Cookie('id', app.Id, null, -1, true);
            ut = new Cookie('ut', userToken, null, -1, true);
            System.debug('p ==>'+ p);
            // Set the new cookie for the page
            ApexPages.currentPage().setCookies(new Cookie[]{id, ut});
            p.setRedirect(false);
            return p;
        } catch (Exception e) {
            System.debug('Error was encountered in the Landing Page Controller ' + e.getStackTraceString());
        }

        return null;
    }

    global static TF4SF__Application__c setAppValues(TF4SF__Application__c app, String subProductCode, String ipaddress, User appUser, String promoCode) {
        // Instantiating the custom settings for the product codes
        TF4SF__Product_Codes__c pc = new TF4SF__Product_Codes__c();
        TF4SF__Application_Configuration__c appConfig = TF4SF__Application_Configuration__c.getOrgDefaults();
        pc = (subProductCode == null) ? null : TF4SF__Product_Codes__c.getValues(subProductCode);
        system.debug('the pc code is '+TF4SF__Product_Codes__c.getInstance(subProductCode));
        if (pc != null) {
            System.debug('LandingPageController - setAppValues() - pc: ' + pc);
            if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Product__c.isCreateable()) { app.TF4SF__Product__c = pc.TF4SF__Product__c; }
            if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Sub_Product__c.isCreateable()) { app.TF4SF__Sub_Product__c = pc.TF4SF__Sub_Product__c; }
            if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Theme_URL__c.isCreateable()) { app.TF4SF__Theme_URL__c = pc.TF4SF__Product_Theme__c; }
            if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Sub_Product_Description__c.isCreateable()) { app.TF4SF__Sub_Product_Description__c = pc.TF4SF__Description__c; }
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'The product Code is Invalid'));
        }

       setAppToken(app);
        if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Created_Channel__c.isCreateable() && Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Current_Channel__c.isCreateable()) {
            if (app.TF4SF__Person_Number__c != null) {
                app.TF4SF__Created_Channel__c = 'Online Banking';
                app.TF4SF__Current_Channel__c = 'Online Banking';
            } else {
                if (appUser.TF4SF__Channel__c == 'Branch') {
                    app.TF4SF__Created_Channel__c = 'Branch';
                    app.TF4SF__Current_Channel__c = 'Branch';
                } else if (appUser.TF4SF__Channel__c == 'Call Center') {
                    app.TF4SF__Created_Channel__c = 'Call Center';
                    app.TF4SF__Current_Channel__c = 'Call Center';
                } else if (appUser.TF4SF__Channel__c == 'Online') {
                    app.TF4SF__Created_Channel__c = 'Online';
                    app.TF4SF__Current_Channel__c = 'Online';
                } else {
                    if (appUser.TF4SF__Channel__c != null) {
                        app.TF4SF__Created_Channel__c = appUser.TF4SF__Channel__c;
                        app.TF4SF__Current_Channel__c = appUser.TF4SF__Channel__c;
                    }
                }
            }
        }

        if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__IP_Address__c.isCreateable()) { app.TF4SF__IP_Address__c = ipaddress; }
        if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Created_timestamp__c.isCreateable()) { app.TF4SF__Created_timestamp__c = System.now(); }
        if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Current_timestamp__c.isCreateable()) { app.TF4SF__Current_timestamp__c = System.now(); }
        if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Application_Page__c.isCreateable()) { app.TF4SF__Application_Page__c = 'GetStartedPage'; }
        if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Application_Status__c.isCreateable()) { app.TF4SF__Application_Status__c = 'Open'; }
        if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Created_Branch_Name__c.isCreateable()) { app.TF4SF__Created_Branch_Name__c = appUser.TF4SF__Location__c; }
        if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Created_User_Email_Address__c.isCreateable()) { app.TF4SF__Created_User_Email_Address__c = appUser.Email; }
        if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Created_Person__c.isCreateable()) { app.TF4SF__Created_Person__c = appUser.Id; }
        if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Current_Branch_Name__c.isCreateable()) { app.TF4SF__Current_Branch_Name__c = appUser.TF4SF__Location__c; }
        if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Created_User_Email_Address__c.isCreateable()) { app.TF4SF__Created_User_Email_Address__c = appUser.Email; }
        if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Current_Person__c.isCreateable()) { app.TF4SF__Current_Person__c = appUser.Id; }
        //if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Promo_Code__c.isCreateable()) { app.TF4SF__Promo_Code__c = promoCode; }
        if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Type_Of_Checking__c.isCreateable()) { app.TF4SF__Type_Of_Checking__c = app.TF4SF__Sub_Product__c; }
        

        if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Number_of_Products__c.isCreateable()) { app.TF4SF__Number_of_Products__c = 1; }
        return app;
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////// CRYPTO METHODS TO ENCRYPT THE USERTOKEN FOR OFFLINE APPLICATION//////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    private static final String RANDOM_CHARS = 
      'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

    private static blob key {
        private get {
            return EncodingUtil.base64Decode(TF4SF__Application_Configuration__c.getOrgDefaults().TF4SF__key__c);
        }
        private set;
    }

    private static Decimal timeoutSeconds {
        private get {
            TF4SF__Application_Configuration__c ac = TF4SF__Application_Configuration__c.getOrgDefaults();
            Decimal timeoutSecs = ac.TF4SF__Timeout_Seconds__c;
            return timeoutSecs;
        }
        private set;
    }

    private static Decimal popupSeconds {
        private get {
            TF4SF__Application_Configuration__c ac = TF4SF__Application_Configuration__c.getOrgDefaults();
            Decimal popupSecs = ac.TF4SF__Popup_Seconds__c;
            return popupSecs;
        }
        private set;
    }

    private static Integer timeoutMinutes {
        get {
            return Integer.valueOf(timeoutSeconds / 60);
        }
        private set;
    }

    private static String getRandomString(Integer len) {
        String mode = String.valueOf(RANDOM_CHARS.length() - 1);
        String retVal = '';
        if (len != null && len >= 1) {
            Integer chars = 0;
            Integer random;
            do {
                random = Math.round(Math.random() * Integer.valueOf(mode));
                retVal += RANDOM_CHARS.substring(random, random + 1);
                chars++;
            } while (chars < len);
        }

        return retVal;
    }  
    
    public static string encrypt(String clearText) {
        return EncodingUtil.base64Encode(crypto.encryptWithManagedIV('AES128', key, Blob.valueOf(clearText)));
    }

    public static string decrypt(String cipherText) {
        return crypto.decryptWithManagedIV('AES128', key, EncodingUtil.base64Decode(cipherText)).toString();
    }

    public static void setAppToken(TF4SF__Application__c app) {
        String userToken = getRandomString(25);
        if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__User_Token__c.isUpdateable()) { app.TF4SF__User_Token__c = encrypt(userToken); }
        if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__User_Token_Expires__c.isUpdateable()) { app.TF4SF__User_Token_Expires__c = System.now().addMinutes(timeoutMinutes); }
    }
}