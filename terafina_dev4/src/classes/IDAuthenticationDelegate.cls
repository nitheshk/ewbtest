/*************************************************
@Author : Nag
@Date : 8/2/2018
@Description : This will make service calls
****************************************************/
public with sharing class IDAuthenticationDelegate {

    public static IDAuthResponse processIDDocument(IDAuthRequest request){
        IDAuthResponse authRes = new IDAuthResponse();
        try{
            if(request!=null){
                
                authRes = IDAuthenticationService.callIdAuthService(request);
            }
        }catch(Exception ex){
            system.debug('Exception : ' + ex);
        }
        return authRes;
    }

}