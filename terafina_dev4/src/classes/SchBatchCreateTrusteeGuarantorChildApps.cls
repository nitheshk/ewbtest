global class SchBatchCreateTrusteeGuarantorChildApps implements Schedulable{ 

 
   public static String sched = '0 0 0,4,8,12,16,20 ? * *';

   global static String scheduleMe() {
       SchBatchCreateTrusteeGuarantorChildApps SC = new SchBatchCreateTrusteeGuarantorChildApps (); 
       return System.schedule('PayverisProcessACHApplications', sched, SC);
   }

   global void execute(SchedulableContext sc) {
       BatchtoCreateTrusteeGuarantorChildApps b1 = new BatchtoCreateTrusteeGuarantorChildApps();
       ID batchprocessid = Database.executeBatch(b1,1);           
   }
}