global class StatusUpdateForDeposit_Batch implements 
Database.Batchable <sObject> , Database.Stateful, Database.AllowsCallouts {

  global Database.QueryLocator start(Database.BatchableContext bc) {
        //**********Get records from object Application 
       String query =('SELECT Id, Time_elapsed_for_abandoned__c,TF4SF__Application_Status__c  FROM TF4SF__Application__c where (TF4SF__Product__c=\'Checking\' or TF4SF__Product__c=\'Savings\' or TF4SF__Product__c=\'ALL\') and  Time_elapsed_for_abandoned__c >=20 and TF4SF__Application_Status__c =\'Open\' order by createdDate DESC limit 10');  
       
       return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<TF4SF__Application__c> listApp) {
        
        for (TF4SF__Application__c app : listApp) {
            //****updates the status to abandoned after an hour
            app.TF4SF__Application_Status__c ='Abandoned';
            System.debug('App Status-->' + app.TF4SF__Application_Status__c);
        }
        if(listApp.size () > 0)
        update listApp;       
        
    }
    
    global void finish(Database.BatchableContext BC) {
    ScheduleStatusUpdateForDeposit.SchedulerMethod();
        
    }
}