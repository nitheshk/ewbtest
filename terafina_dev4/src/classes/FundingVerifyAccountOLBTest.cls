@isTest

public class FundingVerifyAccountOLBTest{

@isTest static void Test_one(){

Map<String, String> tdata = new Map<String, String>();

 String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
      Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
      User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
      EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US', ProfileId = p.Id,
      TimeZoneSidKey = 'America/Los_Angeles',
      UserName = uniqueUserName);
      System.runAs(u) {

     TF4SF__Application__c app = new TF4SF__Application__c ();
        app.TF4SF__Product__c = 'Checking';
        app.TF4SF__Sub_Product__c = 'Checking - Checking';
        insert app;
          
     TF4SF__Application2__c app2=new TF4SF__Application2__c();
          app2.TF4SF__Application__c=app.id;
          insert app2;
          
        TF4SF__Identity_Information__c iden=new TF4SF__Identity_Information__c();
        iden.TF4SF__Application__c=app.id; 
          insert iden;
          
        TF4SF__Employment_Information__c emp=new TF4SF__Employment_Information__c();
         emp.TF4SF__Application__c=app.id; 
          insert emp;
          
        TF4SF__About_Account__c ab = new TF4SF__About_Account__c();
          ab.TF4SF__Application__c = app.id;
          ab.TF4SF__Routing_Number_CHK__c='455';
          ab.TF4SF__CHK_Account_Number__c='5643';
          ab.TF4SF__Account_type_FI_CHK__c='test';
          ab.TF4SF__Routing_Number_SAV__c='566';
          ab.TF4SF__SAV_Account_Number__c='546546';
          ab.TF4SF__Account_Type_FI_Sav__c='test';
        insert ab;
          
        
        VELOSETTINGS__C vo = new VELOSETTINGS__C();
        vo.Name='test';
        vo.TokenEndPoint__c = 'rsgsdgsdf';
        vo.Request_GUID_Name__c = 'RequestUUID';
        vo.ProcessAPI_Timeout__c = 120000;
        insert vo;
        
        tdata.put('id',(String)app.id);
        tdata.put('infoDebug','true');
        tdata.put('userparam','passparam');
        tdata.put('passparam','passparam');
        tdata.put('fiid','fiid');
        
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new FundingVerifyAccountOLBMock());
        FundingVerifyAccountOLB.main(tdata);
        FundingVerifyAccountOLB fv=new FundingVerifyAccountOLB(); 
        FundingVerifyAccountOLB.VerifyAccount(app.id,'Token', 'fiid', 'userparam', 'passparam');
        FundingOTPSelection.VerifyAccountOLB(app.id,'Token','RunID', 'CFIID', 'FIID', 'userparam', 'passparam','Contentlogon','test');  
       
        //Question set 
        List<String> mfaList=new List<String>{'test1','test2'};
        FundingVerifyAccountOLB.VerifyAccountOLB(mfaList);
        test.stopTest();

}
}
}