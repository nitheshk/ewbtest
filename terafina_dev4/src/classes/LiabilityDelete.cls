public class LiabilityDelete {

    //This is to delete the joint applicants Liability details if Joints are added to the application and then removed
    public static void deleteJointLiabilities(String appId,List<Liability_Details__c> lstDelete,Map<String,String> appJointData) {

        List<Liability_Details__c> lstResult = new List<Liability_Details__c>();
        List<Liability_Details__c> lstLiabilityDeleteJ1 = new List<Liability_Details__c>();
        List<Liability_Details__c> lstLiabilityDeleteJ2 = new List<Liability_Details__c>();
        List<Liability_Details__c> lstLiabilityDeleteJ3 = new  List<Liability_Details__c>();

        String jointOne = ( String.isNotBlank(appJointData.get('J1')) ? appJointData.get('J1') : 'True');
        String jointTwo= ( String.isNotBlank(appJointData.get('J2')) ? appJointData.get('J2') : 'True');
        String jointThree = ( String.isNotBlank(appJointData.get('J3')) ? appJointData.get('J3') : 'True');

        System.debug('appId ==>' + appId);
        System.debug('jointOne ==>' + jointOne);
        System.debug('jointTwo ==>' + jointTwo);
        System.debug('jointThree ==>' + jointThree);

        try{
                //Get all the Liabilitys of the application
                if (String.isNotBlank(appId)) {
                    lstResult = [SELECT Id,Applicant_Type__c FROM Liability_Details__c WHERE Application__c =: appId LIMIT 5000];                  
                }
                else if (lstDelete.size() > 0)
                {
                    lstResult = lstDelete.clone();
                }

                if (lstResult.size() > 0) {

                    for( Liability_Details__c objLiability : lstResult ) {    

                            //Store the J1 Liability in a list                             
                            if ( objLiability.Applicant_Type__c == 'J1') {
                                lstLiabilityDeleteJ1.add(objLiability);
                            }

                            //Store the J2 Liability in a list                             
                            if ( objLiability.Applicant_Type__c == 'J2') {
                                lstLiabilityDeleteJ2.add(objLiability);
                            }

                            //Store the J3 Liability in a list                             
                            if ( objLiability.Applicant_Type__c == 'J3') {
                                lstLiabilityDeleteJ3.add(objLiability);
                            }
                    }

                    System.debug('lstLiabilityDeleteJ2 ==>' + lstLiabilityDeleteJ2);

                    //**********************************  Joint 1 ***************************************************
                    //If Joint 1 is removed, delete Joint 1 Liability from Liability details objects
                    if (jointOne == 'false' ) { 
                        
                        if (lstLiabilityDeleteJ1.size() > 0) { 
                            delete lstLiabilityDeleteJ1;
                        }
                                                                    
                    }

                    //**********************************  Joint 1 End ***************************************************


                    //**********************************  Joint 2 ***************************************************
                    //If Joint 2 is removed, delete Joint 2 Liability from Liability details objects              
                    if (jointTwo  == 'false') {
                        System.debug('Inside joint 2 delete ==>' + lstLiabilityDeleteJ2);
                        if (lstLiabilityDeleteJ2.size() > 0) { 
                            delete lstLiabilityDeleteJ2;
                        }
                    }
                    

                    //**********************************  Joint 2 End  ***************************************************


                    //**********************************  Joint 3 ***************************************************

                    //If Joint 3 is removed, delete Joint 3 Liability from Liability details objects  
                    if (jointThree  == 'false') {       
                        
                        if (lstLiabilityDeleteJ3.size() > 0) { 
                            delete lstLiabilityDeleteJ3;
                        }
                        
                    }

                    //**********************************  Joint 3 End  ***************************************************

                }



            }catch(exception ex) {
                System.debug('Excpetion in deleteJointLiability ' + ex.getMessage());
                System.debug('Excpetion in deleteJointLiability line number' + ex.getLineNumber());                
            }

    }
    
  
}