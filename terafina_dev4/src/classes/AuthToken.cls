global with sharing class AuthToken {

    global static string Auth() {
        String GUID = GUIDGenerator.getGUID();
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        VELOSETTINGS__C velo = VELOSETTINGS__C.getOrgDefaults();
        Integer timeOut = Integer.ValueOf(velo.ProcessAPI_Timeout__c);
         
        req.setEndpoint(velo.TokenEndPoint__c);
        System.debug('Endpoint : '+ velo.TokenEndpoint__c);
        req.setTimeout(timeOut);
        System.debug('TimeOut :'+timeOut);
        req.setHeader(velo.Request_GUID_Name__c,GUID);
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        String body = 'client_id='+velo.ClientId__c+'&client_secret='+velo.ClientSecret__c+'&grant_type=client_credentials';
               
        req.setBody(body);
        req.setMethod('POST');
        HttpResponse res = null;
        if(!Test.isRunningTest()){
            res = h.send(req);
        }else{
            AuthTokenMock aM = new AuthTokenMock();
            res = aM.respond(req);
        }
        system.debug('Req is '+req.getbody());
        system.debug('Res is '+res.getbody());

        
        Map<String, Object> tokenMap = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
        String Token = String.valueOf(tokenMap.get('access_token'));
        system.debug('Access Token is '+tokenMap.get('access_token'));
        system.debug('token typw is '+tokenMap.get('token_type'));
        system.debug('expires_in is '+tokenMap.get('expires_in'));
        system.debug('GUID is '+GUID);

        return Token;
    }

}