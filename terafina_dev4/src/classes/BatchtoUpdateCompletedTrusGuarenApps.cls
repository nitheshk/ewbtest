global class BatchtoUpdateCompletedTrusGuarenApps implements 
Database.Batchable <sObject> , Database.Stateful, Database.AllowsCallouts {


  global Database.QueryLocator start(Database.BatchableContext bc) {
    
    String query =  'SELECT Id, Child_Application__c, Child_Application_Completed__c';                    
           query += ' FROM Trustee_Guarantor__c WHERE Child_Application__c != Null';         
        return Database.getQueryLocator(query);      
  }

  global void execute(Database.BatchableContext bc, List<Trustee_Guarantor__c> scope) {
    List<Id> lstAppId = new List<Id>();
    Map<Id,Trustee_Guarantor__c> mapTRGU = new Map<Id,Trustee_Guarantor__c>();
    List<Trustee_Guarantor__c> lstTRGUUpdate = new List<Trustee_Guarantor__c>();

    //process each batch of records
    try {  

          //Add the rows to List and map
          for (Trustee_Guarantor__c obj : scope) {

            lstAppId.add(obj.Child_Application__c);
            mapTRGU.put(obj.Child_Application__c,obj);

          }

          //Get all the child applications with Application status = Submitted
          List<TF4SF__Application__c> lstApp = [SELECT Id,TF4SF__Application_Status__c FROM TF4SF__Application__c 
                                                WHERE Id in : lstAppId AND TF4SF__Application_Status__c = 'Submitted'];
        
          //Loop throught the result and update the Child application completed flag = true in Trustee Guarentor object
          for(TF4SF__Application__c obj: lstApp) {

            Trustee_Guarantor__c  objTRGU = mapTRGU.get(obj.Id);
            objTRGU.Child_Application_Completed__c = True;
            lstTRGUUpdate.add(objTRGU);

          }
        if(Test.isRunningTest()){integer p=10/0;}
      
    } catch (exception e) {
        System.debug('Exception in BatchtoSendChildAppReminderEmails ==>' + e.getMessage());
    } finally {

      //Update Trsutee/Business Status 
      if( lstTRGUUpdate.size() > 0){
        update lstTRGUUpdate;
      }
     
    }

    
  }

  global void finish(Database.BatchableContext bc) {

  }

 
   
}