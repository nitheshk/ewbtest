public with sharing class UXResumeAppButtonExtenstion {

    public TF4SF__Application__c app{get; set;}
    public String appId{get;set;}

    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public UXResumeAppButtonExtenstion(ApexPages.StandardController stdController) {
        //this.App = (TF4SF__Application__c)stdController.getRecord();
        appId = ApexPages.currentPage().getParameters().get('id');
        app = [SELECT TF4SF__Application_Status__c, Id,TF4SF__Online_Banking_Enrollment__c,TF4SF__Check_Order__c,
               TF4SF__ATM_Card__c,TF4SF__Custom_DateTime2__c,TF4SF__Custom_DateTime3__c,TF4SF__Custom_DateTime4__c
               FROM TF4SF__Application__c WHERE Id =: appId];
    }

}