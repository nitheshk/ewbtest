@isTest
global class MockHttpForDocusigning implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {

        //System.assertEquals('http://example.com/example/test', req.getEndpoint());
        //System.assertEquals('GET', req.getMethod());
        HttpResponse res = new HttpResponse();

        res.setHeader('Content-Type', 'application/xml');
        res.setHeader('authenticationHeader','<DocuSignCredentials><Username>eastwestdocusignqa@gmail.com</Username><Password>terafina@123</Password><IntegratorKey>eb119302-953b-4593-91e8-5c05ae7b56a8</IntegratorKey></DocuSignCredentials>');
        res.setHeader('baseUrl','baseUrl');
        res.setBody('<loginInformation xmlns="http://www.docusign.com/restapi" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><loginAccounts><loginAccount><accountId>5529248</accountId><baseUrl>https://demo.docusign.net/restapi/v2/accounts/5529248</baseUrl><email>eastwestdocusignqa@gmail.com</email><isDefault>true</isDefault><name>East West</name><siteDescription/><userId>c92287d1-098a-4cf8-b662-41b206fb5f35</userId><userName>East West</userName></loginAccount></loginAccounts></loginInformation>');
        res.setStatusCode(200);
        
        return res;
    }
}