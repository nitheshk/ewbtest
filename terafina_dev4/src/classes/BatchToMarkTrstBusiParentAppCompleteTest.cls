@isTest
global class BatchToMarkTrstBusiParentAppCompleteTest{ 

@isTest static void method_one(){


        List<TF4SF__Application__c> lstApp=new  List<TF4SF__Application__c>();
        TF4SF__Application__c app = new TF4SF__Application__c ();
        app.TF4SF__Application_Status__c = 'Submitted';
        app.TF4SF__Product__c = 'Home Loan';
        app.TF4SF__Sub_Product__c =  'Home Loan - Short App';
        lstApp.add(app);
        insert lstApp;
      
      List<Trustee_Guarantor__c> trustg=new List<Trustee_Guarantor__c>(); 
       Trustee_Guarantor__c tg=new Trustee_Guarantor__c();
        tg.Child_Application_Completed__c=true;
        tg.Application__c=app.id;
        trustg.add(tg); 
        insert tg;
        
        Database.QueryLocator QL;
        Database.BatchableContext BC;
        BatchToMarkTrstBusiParentAppComplete instance1 = new BatchToMarkTrstBusiParentAppComplete();
        instance1.start(BC);
        instance1.execute(BC,lstApp);
        instance1.finish(BC);

}
}