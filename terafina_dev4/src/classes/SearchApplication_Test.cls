@isTest

public class SearchApplication_Test 
{
        
 @isTest static void startApplication_method() {
     String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
      Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
      User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
              EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
              LocaleSidKey = 'en_US', ProfileId = p.Id,
              TimeZoneSidKey = 'America/Los_Angeles',
              UserName = uniqueUserName);
    System.runAs(u) {
            
            
      TF4SF__Application__c app1 = new TF4SF__Application__c(
        TF4SF__External_App_Stage__c = '1',TF4SF__External_AppStage_CrossSell1__c = 'Pending',
        TF4SF__External_AppStage_CrossSell2__c = 'Pending',TF4SF__External_AppStage_CrossSell3__c = 'Pending',
        TF4SF__Primary_Offer__c = 'Credit Cards - Visa Platinum',TF4SF__Second_Offer__c = 'Personal Loans - Certificate Secured',
        TF4SF__Third_Offer__c = 'Vehicle Loans - Auto',
        TF4SF__Current_Person__c = u.Id,TF4SF__Custom_Checkbox4__c=true,TF4SF__Current_Branch_Name__c='Personal-Loans',
        TF4SF__Mailing_Zip_Code__c='57423',TF4SF__Zip_Code_Prev__c='12345',TF4SF__Preferred_Contact_Method__c='Work',
        TF4SF__Membership_Qualification_Type__c = 'Qualifying Member or Roommate', TF4SF__Custom_Text5__c='test',
        TF4SF__Tertiary_Phone_Number__c='',  OwnerId  = u.Id, 

        
        TF4SF__First_Name__c = 'TestFirst',TF4SF__Last_Name__c = 'TestLast', TF4SF__Email_Address__c = 'test@test.com',
        TF4SF__Product__c = 'Checking',TF4SF__Sub_Product__c = 'Checking - Checking',TF4SF__City__c = 'There',
        TF4SF__Primary_Phone_Number__c = '8889557212',TF4SF__State__c = 'AZ',TF4SF__Street_Address_1__c = '123 That Street', 
        TF4SF__Street_Address_2__c = '',TF4SF__Zip_Code__c = '89898',TF4SF__Months__c = '5', TF4SF__Middle_Name__c = 'TestMiddle',
        TF4SF__Housing_Status__c = 'Own', TF4SF__Monthly_Payment__c = 3000.00,TF4SF__Years__c = 2.0,TF4SF__Secondary_Phone_Number__c = '8885551313');
        insert app1;

         TF4SF__Identity_Information__c ident1 = new TF4SF__Identity_Information__c(
        TF4SF__Application__c = app1.Id,TF4SF__SSN_Prime__c = '999999999',TF4SF__Date_of_Birth__c = '01/01/1970',
        TF4SF__SSN_J1__c = '999999991',TF4SF__Date_of_Birth_J1__c = '01/01/1970',
        TF4SF__SSN_J2__c = '999999992',TF4SF__Date_of_Birth_J2__c = '01/01/1970',
        TF4SF__SSN_J3__c = '999999993',TF4SF__Date_of_Birth_J3__c = '01/01/1970',
        TF4SF__ID_Type__c = 'Drivers License', TF4SF__Identity_Number_Primary__c = '99999999991', TF4SF__State_Issued__c = 'AZ',
        TF4SF__ID_Type_J1__c = 'Drivers License', TF4SF__Identity_Number_J1__c = '99999999992', TF4SF__State_Issued_J1__c = 'AZ',
        TF4SF__ID_Type_J2__c = 'Drivers License', TF4SF__Identity_Number_J2__c = '99999999993', TF4SF__State_Issued_J2__c = 'AZ',
        TF4SF__Country_Issued__c='CA'
      );
      insert ident1;

      String FirstName = 'FirstName';
      String LastName = 'LastName';
      String SSN = '12545555';
      String Email = 'test@test.com';
      String PersonNumber = '996555545';
      String AccountNumber = '646524554555';
      String  Number1 = '56645655445';
      String Address = 'Sample Address';
      String DateOfBirth = '01/01/1970';
      Integer c = 6;
      String PhoneNumber = '56565656565';
      String Membershipnumber = '1234';
      
      Test.StartTest();
        
      PageReference pageRef = Page.SearchApplication;
      Test.setCurrentPage(pageRef);
      pageRef.getParameters().put('id',app1.id);
      SearchApplication objclass1 = new SearchApplication();
      objclass1.executeAppSearch();
        
      Test.StopTest();
    }
  }
    public static testmethod void SearchApplicationTest1() {

    String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
              EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
              LocaleSidKey = 'en_US', ProfileId = p.Id,
              TimeZoneSidKey = 'America/Los_Angeles',
              UserName = uniqueUserName);
    System.runAs(u) {
            
            
   TF4SF__Application__c app1 = new TF4SF__Application__c(
        TF4SF__External_App_Stage__c = '1',TF4SF__External_AppStage_CrossSell1__c = 'Pending',
        TF4SF__External_AppStage_CrossSell2__c = 'Pending',TF4SF__External_AppStage_CrossSell3__c = 'Pending',
        TF4SF__Primary_Offer__c = 'Credit Cards - Visa Platinum',TF4SF__Second_Offer__c = 'Personal Loans - Certificate Secured',
        TF4SF__Third_Offer__c = 'Vehicle Loans - Auto',
        TF4SF__Current_Person__c = u.Id,TF4SF__Custom_Checkbox4__c=true,TF4SF__Current_Branch_Name__c='Personal-Loans',
        TF4SF__Mailing_Zip_Code__c='57423',TF4SF__Zip_Code_Prev__c='12345',TF4SF__Preferred_Contact_Method__c='Work',
        TF4SF__Tertiary_Phone_Number__c='',  OwnerId  = u.Id, 

        
        TF4SF__First_Name__c = 'TestFirst',TF4SF__Last_Name__c = 'TestLast', TF4SF__Email_Address__c = 'test@test.com',
        TF4SF__Product__c = 'Checking',TF4SF__Sub_Product__c = 'Checking - Checking',TF4SF__City__c = 'There',
        TF4SF__Primary_Phone_Number__c = '8889557212',TF4SF__State__c = 'AZ',TF4SF__Street_Address_1__c = '123 That Street', 
        TF4SF__Street_Address_2__c = '',TF4SF__Zip_Code__c = '89898',TF4SF__Months__c = '5', TF4SF__Middle_Name__c = 'TestMiddle',
        TF4SF__Housing_Status__c = 'Own', TF4SF__Monthly_Payment__c = 3000.00,TF4SF__Years__c = 2.0,TF4SF__Secondary_Phone_Number__c = '8885551313');
        insert app1;

    TF4SF__Identity_Information__c ident1 = new TF4SF__Identity_Information__c(
        TF4SF__Application__c = app1.Id,TF4SF__SSN_Prime__c = '999999999',TF4SF__Date_of_Birth__c = '01/01/1970',
        TF4SF__SSN_J1__c = '999999991',TF4SF__Date_of_Birth_J1__c = '01/01/1970',
        TF4SF__SSN_J2__c = '999999992',TF4SF__Date_of_Birth_J2__c = '01/01/1970',
        TF4SF__SSN_J3__c = '999999993',TF4SF__Date_of_Birth_J3__c = '01/01/1970',
        TF4SF__ID_Type__c = 'Drivers License', TF4SF__Identity_Number_Primary__c = '99999999991', TF4SF__State_Issued__c = 'AZ',
        TF4SF__ID_Type_J1__c = 'Drivers License', TF4SF__Identity_Number_J1__c = '99999999992', TF4SF__State_Issued_J1__c = 'AZ',
        TF4SF__ID_Type_J2__c = 'Drivers License', TF4SF__Identity_Number_J2__c = '99999999993', TF4SF__State_Issued_J2__c = 'AZ',
        TF4SF__Country_Issued__c='CA');
        insert ident1;

      String FirstName = 'FirstName';
      String LastName = 'LastName';
      String SSN = '12545555';
      String Email = 'test@test.com';
      String PersonNumber = '996555545';
      String AccountNumber = '646524554555';
      String  Number1 = '56645655445';
      String Address = 'Sample Address';
      String DateOfBirth = '01/01/1970';
      Integer c = 6;
      String PhoneNumber = '56565656565';
      String Membershipnumber = '1234';
  }
    PageReference pageRef = Page.SearchApplication;
    Test.setCurrentPage(pageRef);
    SearchApplication sc = new SearchApplication();
    sc.appfirstnameQuery = 'firstname';
    sc.applastnameQuery = 'testlastname';
    sc.appssnLast4Query = '';
    sc.appEmailQuery = '';
    sc.FromDate = '1/26/2017';
    sc.ToDate = '4/26/2018';
    sc.mobileQuery='';  
  
   
    sc.executeAppSearch();
    sc.getApplicationDetails();
    sc.getApplicationList();
    sc.getappWrapperList();
    //sc.appfirstPage();
    //sc.apppreviousPage();
    //sc.appnextPage();
    //sc.applastPage();
    sc.getapphasPrevious();
    sc.getapphasNext();
    sc.getappPageNumber();
    sc.getappTotalPages();
         sc.getappTotalPages1();
         sc.getappTotalPages2();
         sc.getappTotalPages3();
         sc.getappTotalPages4();
         sc.getappTotalPages5();
         sc.getappTotalPages6();
         sc.getappTotalPages7();
    sc.getappTotalResults();
    sc.appclearAll();
  }

  public static testmethod void SearchApplicationTest2() {
    PageReference pageRef = Page.SearchApplication;
    Test.setCurrentPage(pageRef);
    
    SearchApplication sc = new SearchApplication();
    sc.appfirstnameQuery = 'testfirstname';
    sc.applastnameQuery = 'testlastname';
    sc.appssnLast4Query = '';
    sc.appEmailQuery = 'test@test123.com';
    sc.FromDate = '1/26/2017';
    sc.ToDate = '4/26/2018';
    sc.mobileQuery='485454554';  

    sc.executeAppSearch();
    sc.getApplicationDetails();
    sc.getApplicationList();
    sc.getappWrapperList();
    //sc.appfirstPage();
    //sc.apppreviousPage();
    //sc.appnextPage();
    //sc.applastPage();
    sc.getapphasPrevious();
    sc.getapphasNext();
    sc.getappPageNumber();
    sc.getappTotalPages();
    sc.getappTotalResults();
    sc.appclearAll();
  }

  public static testmethod void SearchApplicationTest3() {
    PageReference pageRef = Page.SearchApplication;
    Test.setCurrentPage(pageRef);
    
    SearchApplication sc = new SearchApplication();
    
    sc.appfirstnameQuery = 'testfirstname';
    sc.applastnameQuery = 'testlastname';
    sc.appssnLast4Query = '4345';
    sc.appEmailQuery = 'test@test123.com';
    sc.FromDate = '1/26/2017';
    sc.ToDate = '4/26/2018';
   
    sc.executeAppSearch();
    sc.getApplicationDetails();
    sc.getApplicationList();
    sc.getappWrapperList();
   //sc.appfirstPage();
    //sc.apppreviousPage();
    //sc.appnextPage();
    //sc.applastPage();
    sc.getapphasPrevious();
    sc.getapphasNext();
    sc.getappPageNumber();
    sc.getappTotalPages();
    sc.getappTotalResults();
    sc.appclearAll();
  }

  public static testmethod void SearchApplicationTest4() {
    PageReference pageRef = Page.SearchApplication;
    Test.setCurrentPage(pageRef);
    
    SearchApplication sc = new SearchApplication();
    
    sc.appfirstnameQuery = 'testfirstname';
    sc.applastnameQuery = 'testlastname';
    sc.appssnLast4Query = '4345';
    sc.appEmailQuery = '';
    sc.FromDate = '1/26/2017';
    sc.ToDate = '4/26/2018';
    sc.mobileQuery='';  

    sc.executeAppSearch();
    sc.getApplicationDetails();
    sc.getApplicationList();
    sc.getappWrapperList();
    //sc.appfirstPage();
    //sc.apppreviousPage();
    //sc.appnextPage();
    //sc.applastPage();
    sc.getapphasPrevious();
    sc.getapphasNext();
    sc.getappPageNumber();
    sc.getappTotalPages();
    sc.getappTotalResults();
    sc.appclearAll();
  }

  public static testmethod void SearchApplicationTest5() {
    PageReference pageRef = Page.SearchApplication;
    Test.setCurrentPage(pageRef);
    
    SearchApplication sc = new SearchApplication();
    
    sc.appfirstnameQuery = 'testfirstname';
    sc.applastnameQuery = '';
    sc.appssnLast4Query = '4345';
    sc.appEmailQuery = '';
    sc.FromDate = '1/26/2017';
    sc.ToDate = '4/26/2018';
    sc.mobileQuery='';  

    sc.executeAppSearch();
    sc.getApplicationDetails();
    sc.getApplicationList();
    sc.getappWrapperList();
    //sc.appfirstPage();
    //sc.apppreviousPage();
    //sc.appnextPage();
    //sc.applastPage();
    sc.getapphasPrevious();
    sc.getapphasNext();
    sc.getappPageNumber();
    sc.getappTotalPages();
    sc.getappTotalResults();
    sc.appclearAll();
  }

  public static testmethod void SearchApplicationTest6() {
    PageReference pageRef = Page.SearchApplication;
    Test.setCurrentPage(pageRef);
    
    SearchApplication sc = new SearchApplication();
    
    sc.appfirstnameQuery = 'testfirstname';
    sc.applastnameQuery = '';
    sc.appssnLast4Query = '4345';
    sc.appEmailQuery = 'test@test123.com';
    sc.FromDate = '1/26/2017';
    sc.ToDate = '4/26/2018';
    sc.mobileQuery='';  
    sc.executeAppSearch();
    sc.getApplicationDetails();
    sc.getApplicationList();
    sc.getappWrapperList();
    //sc.appfirstPage();
    //sc.apppreviousPage();
    //sc.appnextPage();
    //sc.applastPage();
    sc.getapphasPrevious();
    sc.getapphasNext();
    sc.getappPageNumber();
    sc.getappTotalPages();
    sc.getappTotalResults();
      sc.appclearAll();
  }

  public static testmethod void SearchApplicationTest7() {
    PageReference pageRef = Page.SearchApplication;
    Test.setCurrentPage(pageRef);
    
    SearchApplication sc = new SearchApplication();
    
    sc.appfirstnameQuery = '';
    sc.applastnameQuery = 'testlastname';
    sc.appssnLast4Query = '4345';
    sc.appEmailQuery = '';
    sc.FromDate = '1/26/2017';
    sc.ToDate = '4/26/2018';
    sc.mobileQuery='';  

    sc.executeAppSearch();
    sc.getApplicationDetails();
    sc.getApplicationList();
    sc.getappWrapperList();
    //sc.appfirstPage();
    //sc.apppreviousPage();
    //sc.appnextPage();
    //sc.applastPage();
    sc.getapphasPrevious();
    sc.getapphasNext();
    sc.getappPageNumber();
    sc.getappTotalPages();
    sc.getappTotalResults();
      sc.appclearAll();
  }

  public static testmethod void SearchApplicationTest8() {
    PageReference pageRef = Page.SearchApplication;
    Test.setCurrentPage(pageRef);
    
    SearchApplication sc = new SearchApplication();
    
    sc.appfirstnameQuery = '';
    sc.applastnameQuery = 'testlastname';
    sc.appssnLast4Query = '4345';
    sc.appEmailQuery = 'test@test123.com';
    sc.FromDate = '1/26/2017';
    sc.ToDate = '4/26/2018';
    sc.mobileQuery='';  

    sc.executeAppSearch();
    sc.getApplicationDetails();
    sc.getApplicationList();
    sc.getappWrapperList();
    //sc.appfirstPage();
    //sc.apppreviousPage();
    //sc.appnextPage();
    //sc.applastPage();
    sc.getapphasPrevious();
    sc.getapphasNext();
    sc.getappPageNumber();
    sc.getappTotalPages();
    sc.getappTotalResults();
      sc.appclearAll();
  }

  
  public static testmethod void SearchApplicationTest10() {
    PageReference pageRef = Page.SearchApplication;
    Test.setCurrentPage(pageRef);
    
    SearchApplication sc = new SearchApplication();
    
    sc.appfirstnameQuery = 'testfirstname';
    sc.applastnameQuery = '';
    sc.appssnLast4Query = '';
    sc.appEmailQuery = 'test@test123.com';
    sc.FromDate = '1/26/2017';
    sc.ToDate = '4/26/2018';
   
    sc.executeAppSearch();
    sc.getApplicationDetails();
    sc.getApplicationList();
    sc.getappWrapperList();
    //sc.appfirstPage();
    //sc.apppreviousPage();
    //sc.appnextPage();
    //sc.applastPage();
    sc.getapphasPrevious();
    sc.getapphasNext();
    sc.getappPageNumber();
    sc.getappTotalPages();
    sc.getappTotalResults();
      sc.appclearAll();
  }

  public static testmethod void SearchApplicationTest11() {
    PageReference pageRef = Page.SearchApplication;
    Test.setCurrentPage(pageRef);
    
    SearchApplication sc = new SearchApplication();
    
    sc.appfirstnameQuery = '';
    sc.applastnameQuery = 'testlastname';
    sc.appssnLast4Query = '';
    sc.appEmailQuery = '';
    sc.FromDate = '1/26/2017';
    sc.ToDate = '4/26/2018';
    sc.mobileQuery='';  
    sc.executeAppSearch();
    sc.getApplicationDetails();
    sc.getApplicationList();
    sc.getappWrapperList();
    //sc.appfirstPage();
    //sc.apppreviousPage();
    //sc.appnextPage();
    //sc.applastPage();
    sc.getapphasPrevious();
    sc.getapphasNext();
    sc.getappPageNumber();
    sc.getappTotalPages();
    sc.getappTotalResults();
      sc.appclearAll();
  }

  public static testmethod void SearchApplicationTest12() {
    PageReference pageRef = Page.SearchApplication;
    Test.setCurrentPage(pageRef);
    
    SearchApplication sc = new SearchApplication();
    
    sc.appfirstnameQuery = '';
    sc.applastnameQuery = 'testlastname';
    sc.appssnLast4Query = '';
    sc.appEmailQuery = 'test@test123.com';
    sc.FromDate = '1/26/2017';
    sc.ToDate = '4/26/2018';
    sc.mobileQuery='';  

    sc.executeAppSearch();
    sc.getApplicationDetails();
    sc.getApplicationList();
    sc.getappWrapperList();
    //sc.appfirstPage();
    //sc.apppreviousPage();
    //sc.appnextPage();
    //sc.applastPage();
    sc.getapphasPrevious();
    sc.getapphasNext();
    sc.getappPageNumber();
    sc.getappTotalPages();
    sc.getappTotalResults();
      sc.appclearAll();
  }

  public static testmethod void SearchApplicationTest13() {
    PageReference pageRef = Page.SearchApplication;
    Test.setCurrentPage(pageRef);
    
    SearchApplication sc = new SearchApplication();
    
    sc.appfirstnameQuery = '';
    sc.applastnameQuery = '';
    sc.appssnLast4Query = '4345';
    sc.appEmailQuery = '';
    sc.FromDate = '1/26/2017';
    sc.ToDate = '4/26/2018';
    sc.mobileQuery='';  

    sc.executeAppSearch();
    sc.getApplicationDetails();
    sc.getApplicationList();
    sc.getappWrapperList();
    //sc.appfirstPage();
    //sc.apppreviousPage();
    //sc.appnextPage();
    //sc.applastPage();
    sc.getapphasPrevious();
    sc.getapphasNext();
    sc.getappPageNumber();
    sc.getappTotalPages();
    sc.getappTotalResults();
      sc.appclearAll();
  }

  public static testmethod void SearchApplicationTest14() {
    PageReference pageRef = Page.SearchApplication;
    Test.setCurrentPage(pageRef);
    
    SearchApplication sc = new SearchApplication();
    
    sc.appfirstnameQuery = '';
    sc.applastnameQuery = '';
    sc.appssnLast4Query = '4345';
    sc.appEmailQuery = 'test@test123.com';
    sc.FromDate = '1/26/2017';
    sc.ToDate = '4/26/2018';
    sc.mobileQuery='';  

    sc.executeAppSearch();
    sc.getApplicationDetails();
    sc.getApplicationList();
    sc.getappWrapperList();
    //sc.appfirstPage();
    //sc.apppreviousPage();
    //sc.appnextPage();
    //sc.applastPage();
    sc.getapphasPrevious();
    sc.getapphasNext();
    sc.getappPageNumber();
    sc.getappTotalPages();
    sc.getappTotalResults();
      sc.appclearAll();
  }

  public static testmethod void SearchApplicationTest15() {
    PageReference pageRef = Page.SearchApplication;
    Test.setCurrentPage(pageRef);
    
    SearchApplication sc = new SearchApplication();
    
    sc.appfirstnameQuery = '';
    sc.applastnameQuery = '';
    sc.appssnLast4Query = '';
    sc.appEmailQuery = 'test@test123.com';
    sc.FromDate = '1/26/2017';
    sc.ToDate = '4/26/2018';
    sc.mobileQuery='';  
    sc.executeAppSearch();
    sc.getApplicationDetails();
    sc.getApplicationList();
    sc.getappWrapperList();
    //sc.appfirstPage();
    //sc.apppreviousPage();
    //sc.appnextPage();
    //sc.applastPage();
    sc.getapphasPrevious();
    sc.getapphasNext();
    sc.getappPageNumber();
    sc.getappTotalPages();
    sc.getappTotalResults();
      sc.appclearAll();
  }

  public static testmethod void SearchApplicationTest16() {
    PageReference pageRef = Page.SearchApplication;
    Test.setCurrentPage(pageRef);
    
    SearchApplication sc = new SearchApplication();
    
    sc.appfirstnameQuery = '';
    sc.applastnameQuery = '';
    sc.appssnLast4Query = '';
    sc.appEmailQuery = '';
    sc.FromDate = '1/26/2017';
    sc.ToDate = '4/26/2018';
    sc.mobileQuery='';  

    sc.executeAppSearch();
    sc.getApplicationDetails();
    sc.getApplicationList();
    sc.getappWrapperList();
    //sc.appfirstPage();
    //sc.apppreviousPage();
    //sc.appnextPage();
    ////sc.applastPage();
    sc.getapphasPrevious();
    sc.getapphasNext();
    sc.getappPageNumber();
    sc.getappTotalPages();
    sc.getappTotalResults();
      sc.appclearAll();
  }
    public static testmethod void SearchApplicationTest17() {
    PageReference pageRef = Page.SearchApplication;
    Test.setCurrentPage(pageRef);
    
    SearchApplication sc = new SearchApplication();
    sc.appfirstnameQuery = '';
    sc.applastnameQuery = '';
    sc.appssnLast4Query = '';
    sc.appEmailQuery = 'test@test123.com';
    sc.FromDate = '1/26/2017';
    sc.ToDate = '4/26/2018';
    sc.mobileQuery='782584454';  

    sc.executeAppSearch();
    sc.getApplicationDetails();
    sc.getApplicationList();
    sc.getappWrapperList();
    //sc.appfirstPage();
    //sc.apppreviousPage();
    //sc.appnextPage();
    //sc.applastPage();
    sc.getapphasPrevious();
    sc.getapphasNext();
    sc.getappPageNumber();
    sc.getappTotalPages();
    sc.getappTotalResults();
    sc.appclearAll();
  }
    public static testmethod void SearchApplicationTest21() {
    PageReference pageRef = Page.SearchApplication;
    Test.setCurrentPage(pageRef);
    
    SearchApplication sc = new SearchApplication();
    
    sc.appfirstnameQuery = '';
    sc.applastnameQuery = '';
    sc.appssnLast4Query = '';
    sc.appEmailQuery = null;
    sc.mobileQuery =null;
    sc.FromDate = '1/26/2017';
    sc.ToDate = '4/26/2018';

    sc.executeAppSearch();
    sc.getApplicationDetails();
    sc.getApplicationList();
    sc.getappWrapperList();
    //sc.appfirstPage();
    //sc.apppreviousPage();
    //sc.appnextPage();
    //sc.applastPage();
    sc.getapphasPrevious();
    sc.getapphasNext();
    sc.getappPageNumber();
    sc.getappTotalPages();
    sc.getappTotalResults();
    sc.appclearAll();
  }
    public static testmethod void SearchApplicationTest23() {
    PageReference pageRef = Page.SearchApplication;
    Test.setCurrentPage(pageRef);
    
    SearchApplication sc = new SearchApplication();
    
    sc.appfirstnameQuery = 'test';
    sc.applastnameQuery = '';
    sc.appssnLast4Query = '';
    sc.appEmailQuery = '';
    sc.mobileQuery ='';
    sc.FromDate = '1/26/2017';
    sc.ToDate = '4/26/2018';
    sc.executeAppSearch();
 
}
}