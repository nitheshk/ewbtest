global with sharing class MortgageShortAppLandingPageCntrl {

	public PageReference createApp()
	{
		// Instantiate all of the required Objects
        TF4SF__Application__c app = new TF4SF__Application__c();
        TF4SF__Application2__c app2 = new TF4SF__Application2__c();
        TF4SF__Employment_Information__c emp = new TF4SF__Employment_Information__c();
        TF4SF__Identity_Information__c iden = new TF4SF__Identity_Information__c();
        TF4SF__About_Account__c acc = new TF4SF__About_Account__c();
       
       	if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Application_Status__c.isCreateable()) { app.TF4SF__Application_Status__c = 'Open'; }
        if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Product__c.isCreateable()) { app.TF4SF__Product__c = 'Home Loan'; }
        if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Sub_Product__c.isCreateable()) { app.TF4SF__Sub_Product__c =  'Home Loan - Short App'; }


		CryptoHelperCustom.setAppToken(app);
		if (TF4SF__Application__c.SObjectType.getDescribe().isCreateable()) { insert app; }
		if (Schema.sObjectType.TF4SF__Application2__c.fields.TF4SF__Application__c.isCreateable()) { app2.TF4SF__Application__c = app.Id; }
		if (Schema.sObjectType.TF4SF__Employment_Information__c.fields.TF4SF__Application__c.isCreateable()) { emp.TF4SF__Application__c = app.Id; }
		if (Schema.sObjectType.TF4SF__Identity_Information__c.fields.TF4SF__Application__c.isCreateable()) { iden.TF4SF__Application__c = app.Id; }
		if (Schema.sObjectType.TF4SF__About_Account__c.fields.TF4SF__Application__c.isCreateable()) { acc.TF4SF__Application__c = app.Id; }
		

		if (TF4SF__Application2__c.SObjectType.getDescribe().isCreateable()) { insert app2; }
        if (TF4SF__Employment_Information__c.SObjectType.getDescribe().isCreateable()) { insert emp; }
        if (TF4SF__Identity_Information__c.SObjectType.getDescribe().isCreateable()) { insert iden; }
        if (TF4SF__About_Account__c.SObjectType.getDescribe().isCreateable()) { insert acc; }
       
        

        String userToken = CryptoHelperCustom.decrypt(app.TF4SF__User_Token__c);

        PageReference p = new PageReference('/Mortgageshortapp');
        Cookie id = ApexPages.currentPage().getCookies().get('id');
        Cookie ut = ApexPages.currentPage().getCookies().get('ut');
        id = new Cookie('id', app.Id, null, -1, true);
        ut = new Cookie('ut', userToken, null, -1, true);

        // Set the new cookie for the page
        ApexPages.currentPage().setCookies(new Cookie[]{id, ut});
        p.setRedirect(false);
        return p;
	}
}