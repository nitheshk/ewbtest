@isTest
global class PrintDisclosuresControllerTest{
    
@isTest static void Test_one(){ 

   TF4SF__Application_Configuration__c appConfig = new TF4SF__Application_Configuration__c();
     appConfig.TF4SF__Application_Code__c = 'dsp4_0';
     appConfig.TF4SF__Theme__c = 'dsp4_0';
     appConfig.TF4SF__Key__c = 'YMmKJt5QxS7KlAEASMsj4Q==';
     appConfig.TF4SF__DL_Endpoint_URL__c = 'https://app1.idware.net/DriverLicenseParserRest.svc/ParseImage';
     insert appConfig;   
    
    TF4SF__Application__c app=new TF4SF__Application__c (
      TF4SF__Last_Name__c='TestLast',TF4SF__First_Name__c='TestFirst');
      insert app;
    
    List<String> discName = new List<String>();
    List<TF4SF__Disclosure_Names__c> dis=new List<TF4SF__Disclosure_Names__c>();
    TF4SF__Disclosure_Names__c obj=new TF4SF__Disclosure_Names__c();
     obj.Name='TestName';
     obj.TF4SF__Disclosure_Label__c='TestLabel';
     obj.Product_Code__c='Savings';
     dis.add(obj);
     insert dis; 
    
    	Blob b = Blob.valueOf('Test Data');
        Attachment att = new Attachment();
        att.name = 'testname';
        att.Body = b;
        att.Description = 'test';
        att.ParentId = app.id;
        att.ContentType = 'test';
        insert(att);
    
    test.startTest();
    PrintDisclosuresController pdc=new PrintDisclosuresController();
    pdc.onload();
    test.stopTest();
    
    } 
}