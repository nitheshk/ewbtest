@isTest(SeeAllData=true) 
global class StatusUpdate_BatchTest{

@isTest static void method_one(){

 TF4SF__Application__c app1 = new TF4SF__Application__c(TF4SF__Application_Status__c ='open',TF4SF__First_Name__c='firstname',TF4SF__Product__c = 'Checking', 
    TF4SF__Sub_Product__c = 'Checking - Checking');
    insert app1;
    
    List<TF4SF__Application__c> appList = new List<TF4SF__Application__c>();
        appList.add(app1);
    
        Database.QueryLocator QL;
        Database.BatchableContext BC;
        StatusUpdate_Batch instance1 = new StatusUpdate_Batch ();
        instance1.start(BC);
        instance1.execute(BC,appList);
        instance1.finish(BC);
}
}