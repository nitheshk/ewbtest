global class CustomerInfoFundsDetails{
    global SourceofFunds SourceofFunds;
    global class SourceofFunds {
        public String Source;   //FundfromEMP
        public Integer MiscDemographic; //2
        public CustomerIntentforIntWireTransfer CustomerIntentforIntWireTransfer;
    }
    global class CustomerIntentforIntWireTransfer {
        public Boolean TransferServices; //Yes/No
        public ForeignInActivity ForeignInActivity;
        public ForeignOutActivity ForeignOutActivity;
    }
    global class ForeignInActivity {
        public Boolean ForeignIncomingActivity;  //Yes/No
        public Integer IncomingWires;   //3
        public String IncomingWiresAmount;  //233445.34
    }
    global class ForeignOutActivity {
        public Boolean ForeignOutgoingActivity;  //Yes/No
        public Integer OutgoingWires;   //3
        public String OutgoingWiresAmount;  //233445.34
    }
    public static CustomerInfoFundsDetails parse(String json){
        return (CustomerInfoFundsDetails) System.JSON.deserialize(json, CustomerInfoFundsDetails.class);
    }

}