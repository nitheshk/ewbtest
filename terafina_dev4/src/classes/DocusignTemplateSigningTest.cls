@isTest
private class DocusignTemplateSigningTest {


    @isTest static void testDocuSigning(){
    
        Map<String, String> tdata = new Map<String, String>();
        
        TF4SF__Application__c app = new TF4SF__Application__c();
        app.TF4SF__Product__c = 'Home Loan';
        app.TF4SF__First_Name__c='Test';
        app.TF4SF__Last_Name__c = 'Test';
        app.TF4SF__Email_Address__c= 'test@test.com';
        app.TF4SF__Sub_Product__c = 'Home Loan - Short App';
        app.Docusign_EnvelopeID__c='be5fb086-79ad-481b-98f6-24cc107e9fe4';
        insert app;
        
        tdata.put('id',(String)app.id);
        Test.startTest();
        DocusignTemplateSigning docuSignObj=new DocusignTemplateSigning();
        Test.setMock(HttpCalloutMock.class, new MockHttpForDocusigning());
        try{
        docuSignObj.main(tdata);
        }
        catch(exception ex){}
        Test.stopTest();
    }

}