global with sharing class ResumeEmailValidationExtension implements TF4SF.DSP_Interface {
    
    global static Map<String, String> main(Map<String, String> tdata) {
        Map<String, String> data = new Map<String, String>();
        String Options = '',emailAddress='',phoneNo='',abandonAppId='',message='', userId = '', factorId = '', smsId = '', oktaStatus = '';
        String appId = tdata.get('id');
        String emailId = tdata.get('emailId');  
        Integer status=0;
        String responseFactorId = '', responseStatus = '';
        Boolean sendOTP = false;
        List<OKTA__c> oktaList = new List<OKTA__c>();
        Map<String, String> enrollMap = new Map<String, String>();
        //Get recent abandoned app based on EmailId & Application Status
        try
        {
        List <TF4SF__Application__c>  listApp = [SELECT Id, TF4SF__Application_Status__c, TF4SF__Sub_Product__c ,
        TF4SF__Email_Address_J2__c, TF4SF__Email_Address_J3__c, TF4SF__Email_Address_J__c, TF4SF__Email_Address__c,
        TF4SF__Primary_Phone_Number__c, TF4SF__Primary_Phone_Number_J__c, TF4SF__Primary_Phone_Number_J3__c, 
        TF4SF__Primary_Phone_Number_J2__c, OKTA_UserId__c, OKTA_FactorId__c, OKTA_SMSId__c, Okta_Status__c, TF4SF__Country_Code__c FROM TF4SF__Application__c 
        where Id =: appId order by createdDate DESC limit 1];
        
        
       //Validate Email/Phone from abandoned app 
         if (!listApp.isempty()) {           

                abandonAppId=listApp[0].Id;
                emailAddress=listApp[0].TF4SF__Email_Address__c;
                phoneNo=listApp[0].TF4SF__Country_Code__c+ValidatePromoCodeEmailExtension.PhoneCleanup(listApp[0].TF4SF__Primary_Phone_Number__c);
                if (String.isNotBlank(emailId)) {
                    oktaList = [SELECT Id, Email_Address__c, Factor_Id__c, SMS_Id__c, Status__c, User_Id__c FROM OKTA__c WHERE Email_Address__c =: emailId.toLowerCase() LIMIT 1];
                    if (!oktaList.isempty()) {
                        userId=oktaList[0].User_Id__c;
                        factorId=oktaList[0].Factor_Id__c;
                        smsId=oktaList[0].SMS_Id__c;
                        oktaStatus = oktaList[0].Status__c;
                    }
                }              
     
                if(String.isNotBlank(userId)) {  
                    if (String.isNotBlank(factorId) && oktaStatus == 'ACTIVE') {
                        responseFactorId = EmailValidationExtension.challenge(appId, AuthToken.Auth(), userId, smsId);
                        responseStatus = oktaList[0].Status__c;
                        sendOTP = true;
                        //data.put('Application__c.OKTA_FactorId__c',responseFactorId);
                    } else if (String.isNotBlank(phoneNo)) {
                        enrollMap = EmailValidationExtension.enrollSMS(appId, AuthToken.Auth(), userId, phoneNo);
                        responseFactorId = enrollMap.get('factorId');
                        responseStatus = enrollMap.get('oktaStatus');
                        sendOTP = true;                       
                    }                   
                    
                    if (sendOTP) {
                        status=200;  
                    } 
                }
            }  
        }
        catch(Exception ex){
            message=ex.getMessage()+'Line number-->'+ ex.getLineNumber()+ sendOTP ;
        }  
           
        //Prepare JSON & Send application details to frontend  
        Options += '{';
        Options += '"status": "'+status+'",';
        Options += '"dspid": "'+abandonAppId+'",';
        Options += '"emailid": "'+emailAddress+'",';               
        Options += '"phoneno": "'+phoneNo+'",';  
        Options += '"message": "'+message+'"'; 
        Options += '}';  
        
          
        data.put('JsonResult',Options);
        system.debug('option'+Options);
        if (status == 200) {
            EmailValidationExtension.updateOkta(appId, emailId, userId, responseFactorId, responseStatus, smsId);
        }
        return data;
    }
    
}