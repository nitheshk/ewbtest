@isTest
public class ResumeEmailValidationExtensionTest{

@isTest static void test_method_one() {


Map<String, String> tdata = new Map<String, String>();
Map<String, String> verifyEnroll = new Map<String, String>();
Map<String, String> data = new Map<String, String>();

     List <TF4SF__Application__c>  listCurrentApp=new List <TF4SF__Application__c>();
         TF4SF__Application__c app = new TF4SF__Application__c();
         app.TF4SF__Product__c = 'Checking';
         app.TF4SF__Sub_Product__c = 'Checking - Checking';
         app.TF4SF__Email_Address__c='test@gmail.com'; 
         app.TF4SF__First_Joint_Applicant__c = true;
         app.TF4SF__Second_Joint_Applicant__c = true;
         app.TF4SF__Third_Joint_Applicant__c = true;
         app.TF4SF__Application_Status__c='Abandoned';
         app.TF4SF__Primary_Phone_Number__c='1245786532';
         app.OKTA_UserId__c='test';
         app.OKTA_factorId__c='test';
         app.TF4SF__Primary_Phone_Number_J2__c='897997898';
    	 app.TF4SF__Primary_Phone_Number_J3__c='546546654';
         app.TF4SF__Email_Address_J2__c='test@gmail.com';
         app.TF4SF__Email_Address_J3__c='test@gmail.com';
         app.TF4SF__Email_Address_J__c='test@gmail.com';
         app.OKTA_SMSId__c='test';
         app.OKTA_Status__c='ACTIVE';
    	 app.TF4SF__Country_Code__c='34344';
        
        insert app;
        
        List<OKTA__c> oktaList = new List<OKTA__c>();
        OKTA__c okta=new OKTA__c();
        okta.Factor_Id__c='test';
        okta.SMS_Id__c='test';
        okta.Status__c='ACTIVE';
        okta.User_Id__c='test';
        okta.Email_Address__c='test@gmail.com';
        oktaList.add(okta);
        insert oktaList;
        
        test.startTest();
        tdata.put('id',app.Id);
        tdata.put('emailId',app.TF4SF__Email_Address__c); 
        Test.setMock(HttpCalloutMock.class, new ResumeEmailValidationExtensionMock());                          
        ResumeEmailValidationExtension.main(tdata);
        Test.stopTest();
}
}