@isTest
private class EW_ResumeApplication_Test {
    
     	// Follwoing Error genrated
           /* 01:58:25.332 (536686612)|USER_DEBUG|[63]|DEBUG|Exception in decrypt ==> Invalid initialization vector. Must be 16 bytes.
            01:58:25.332 (536700562)|SYSTEM_MODE_EXIT|true
            01:58:25.332 (539303481)|EXCEPTION_THROWN|[10]|System.NullPointerException: Argument 1 cannot be null
            01:58:25.332 (539424902)|USER_DEBUG|[261]|DEBUG|Execption in validateOTP ==>Argument 1 cannot be null
            01:58:25.332 (539468187)|USER_DEBUG|[262]|DEBUG|Execption in validateOTP Line number ==>10
            01:58:25.332 (539476917)|SYSTEM_MODE_EXIT|false*/
    
    
    @isTest static void test_method_one() {
        
         List<TF4SF__Application__c> appLst=new  List<TF4SF__Application__c>();
         TF4SF__Application__c app=new TF4SF__Application__c();
         	app.TF4SF__Application_Status__c= 'Save for Later';
            app.TF4SF__Sub_Product__c = 'Home Loan - Short App';
            app.TF4SF__Email_Address_J2__c='testj2@gmail.com';
            app.TF4SF__Email_Address_J3__c='testj3@gmail.com';
            app.TF4SF__Email_Address_J__c='testj@gmail.com';
            app.TF4SF__Email_Address__c='test@gmail.com'; 
            app.TF4SF__Primary_Phone_Number__c='9555666666';
            app.TF4SF__Primary_Phone_Number_J__c='990)2189(392)';
            app.TF4SF__Primary_Phone_Number_J3__c='9557666666'; 
            app.TF4SF__Primary_Phone_Number_J2__c='9553566666'; 
            app.TF4SF__Product__c='Home Loan';
            appLst.add(app);
            insert appLst;
        Map< String , String > searchParamMap=new Map< String , String >();
        searchParamMap.put('applicantEmailId','test@gmail.com');
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new mockTest_EWResume());
        EW_ResumeApplication.searchApplication(searchParamMap);
        Test.stopTest();
 
}
    @isTest static void test_method_two() {
        
        TF4SF__Application_Configuration__c a = new TF4SF__Application_Configuration__c();
        a.TF4SF__Key__c = 'H+RYeZyh1kh5ZpWExkQbpw==';
        a.TF4SF__Timeout_Seconds__c =20;
         a.TF4SF__Theme__c= 'SDF';
        a.TF4SF__Application_Code__c = '546';
        insert a;
        
         List<TF4SF__Application__c> appLst=new  List<TF4SF__Application__c>();
         TF4SF__Application__c app=new TF4SF__Application__c();
         	app.TF4SF__Application_Status__c= 'Save for Later';
            app.TF4SF__Sub_Product__c = 'Home Loan - Short App';
            app.TF4SF__Email_Address_J2__c='testj2@gmail.com';
            app.TF4SF__Email_Address_J3__c='testj3@gmail.com';
            app.TF4SF__Email_Address_J__c='testj@gmail.com';
            app.TF4SF__Email_Address__c='test@gmail.com'; 
            app.TF4SF__Primary_Phone_Number__c='9555666666';
            app.TF4SF__Primary_Phone_Number_J__c='990)2189(392)';
            app.TF4SF__Primary_Phone_Number_J3__c='9557666666'; 
            app.TF4SF__Primary_Phone_Number_J2__c='9553566666'; 
            app.TF4SF__Product__c='Home Loan';
            appLst.add(app);
            insert appLst;
        
 		List<OKTA__c> oktaList =  new List<OKTA__c>();
        OKTA__c okta=new OKTA__c();
        okta.Email_Address__c='test1@test.com';
        okta.Factor_Id__c='test';
        okta.SMS_Id__c='test';
        okta.Status__c='ACTIVE';
        okta.User_Id__c=app.owner.id;
        oktaList.add(okta);
        insert oktaList;  
        
        Map<String,String> mapData=new Map<String,String>();
        mapData.put('applicationId',app.Id);
        mapData.put('applicantEmailId','test@test.com');
        mapData.put('otp','254532');
        mapData.put('email','test1@test.com');
        mapData.put('userId',app.owner.id);
                
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new mockTest_EWResume());
        EW_ResumeApplication obj=new EW_ResumeApplication();
        EW_ResumeApplication.sendOTP(mapData);
        EW_ResumeApplication.validateOTP(mapData);
        test.stopTest();
 
}
}