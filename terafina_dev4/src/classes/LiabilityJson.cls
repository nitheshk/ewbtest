public class LiabilityJson {

    public class PA {
        public String Id;
        public String Nature_of_Liability_c;
        public String Name_of_Creditor_c;
        public String Applicant_Type_c;
        public String Monthly_Pledge_c;
        public String Monthly_Payment_c;
        public String Paid_Off_c;
    }


    public class J1 {
        public String Id;
        public String Nature_of_Liability_c;
        public String Name_of_Creditor_c;
        public String Applicant_Type_c;
        public String Monthly_Pledge_c;
        public String Monthly_Payment_c;
        public String Paid_Off_c;
    }
    
    public class J2 {
         public String Id;
        public String Nature_of_Liability_c;
        public String Name_of_Creditor_c;
        public String Applicant_Type_c;
        public String Monthly_Pledge_c;
        public String Monthly_Payment_c;
        public String Paid_Off_c;
    }
    
    public class J3 {
        public String Id;
        public String Nature_of_Liability_c;
        public String Name_of_Creditor_c;
        public String Applicant_Type_c;
        public String Monthly_Pledge_c;
        public String Monthly_Payment_c;
        public String Paid_Off_c;
    }
    
    
    
     public PA PA;
     public J1 J1;
     public J2 J2;
     public J3 J3;

        
    public static List<LiabilityJson> parse(String json) {
        return (List<LiabilityJson>) System.JSON.deserialize(json, List<LiabilityJson>.class);
    }
}