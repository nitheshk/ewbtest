global class AddressVerification implements TF4SF.DSP_Interface {  
    
    global static Map<String, String> main (Map<String, String> tdata) {
        Map<String, String> data = tdata.clone(); 

        String appId = tdata.get('id');
        String StreetAddress1 = tdata.get('Street_Address_1__c');
        String StreetAddress2 =tdata.get('Street_Address_2__c');
        String State = tdata.get('State__c');
        String City = tdata.get('City__c');
        String ZipCode = tdata.get('Zip_Code__c');
        String Country = tdata.get('Country__c');
        String Token = '';
        String responseJson = '';        
        String statuscode = '';
        String GUID;
        HttpRequest req2;
        HttpResponse res2;
        try
        { 
            TF4SF__Application__c app = [SELECT Id, Name, TF4SF__First_Name__c, TF4SF__Last_Name__c, TF4SF__Email_Address__c, TF4SF__Street_Address_2__c, TF4SF__Street_Address_1__c, TF4SF__City__c, TF4SF__State__c, TF4SF__Zip_Code__c, TF4SF__Country__c, TF4SF__Primary_Phone_Number__c, TF4SF__User_Token__c FROM TF4SF__Application__c WHERE Id =: appId];
            TF4SF__Identity_Information__c iden = [SELECT Id, TF4SF__Application__c,Custom_Picklist13__c,TF4SF__SSN_Prime__c, TF4SF__Date_of_Birth__c FROM TF4SF__Identity_Information__c WHERE TF4SF__Application__c =: appId];
            if ((iden.Custom_Picklist13__c!='No' && (State == 'CA' || State == 'NV' || State == 'WA' || State == 'TX' || State == 'GA' || State == 'MA' || State == 'NY')) || (Country=='China' && iden.Custom_Picklist13__c=='No')) {
            //if (State == 'CA' || State == 'NV' || State == 'WA' || State == 'TX' || State == 'GA' || State == 'MA' || State == 'NY')  {
                VELOSETTINGS__C velo = VELOSETTINGS__C.getOrgDefaults();
                Integer timeOut = Integer.ValueOf(velo.ProcessAPI_Timeout__c);
                GUID = GUIDGenerator.getGUID();
                Token = AuthToken.Auth();    
                Http h2 = new Http();
                req2 = new HttpRequest();
                req2.setTimeout(timeOut);
                req2.setEndpoint(velo.EndPoint__c+'api/process/addressvalidator/addressverify');
                req2.setHeader(velo.Request_GUID_Name__c,GUID);
                req2.setHeader('Authorization','Bearer '+Token);
                req2.setHeader('Accept','application/json');
                req2.setHeader('Content-Type', 'application/json');
                req2.setMethod('POST');
                String idBody = '';

                StreetAddress2='';
                idBody = '{';
                    idBody += '"StreetAddress1": "'+StreetAddress1+'",'; 
                    //Pass StreetAddress2 empty          
                    idBody += '"StreetAddress2": "",';            
                    idBody += '"State": "'+State+'",'; 
                    idBody += '"City": "'+City+'",';
                    idBody += '"Zip": "'+ZipCode+'",';
                    if (String.isNotBlank(Country)) {
                        idBody += '"Country": "'+Country+'"';
                    } else {
                        idBody += '"Country": "US"';
                    }
                idBody += '}';

                //idBody = '{"StreetAddress1": "43433 Mission Blvd","StreetAddress2": "Suite 210","City": "Fremont","State": "CA","Zip": "94539","Country": "US"}';
                //idBody ='{"StreetAddress1": "43433 Mission Blvd","State": "CA","City": "Fremont","Zip": "94539","Country": "US"}';
                req2.setBody(idBody);
                res2 = h2.send(req2);
                responseJson = res2.getBody();
                system.debug('Req is '+req2);
                system.debug('Req body is '+req2.getBody());
                system.debug('Res is '+res2); 
                system.debug('Res Body is '+res2.getbody());
                boolean error = false;
                Melissa_Error_Code__mdt [] ErrorCodes = [SELECT Id, DeveloperName, MasterLabel, Long_Description__c, Show_Error__c FROM Melissa_Error_Code__mdt WHERE Show_Error__c = true];
                Map<String, Object> resultMap = (Map<String, Object>)JSON.deserializeUntyped(res2.getbody());
                Map<String, Object> dataMap = (Map<String, Object>)resultMap.get('data');
                List<Object> resultCodes = (List<Object>)dataMap.get('ResultCodes');
                for (Object resultCode : resultCodes) {
                    Map<String, Object> codeMap = (Map<String, Object>)resultCode;
                    if (ErrorCodes.size() > 0) {
                        for (Melissa_Error_Code__mdt ErrorCode : ErrorCodes) {
                            if (ErrorCode.DeveloperName == String.valueOf(codeMap.get('Code'))) {
                                error = true;
                                break;
                            }
                        }
                    }
                }
                if (error == false) {
                    data.put('status',res2.getbody());
                    statuscode='200';
                } else {
                    statusCode='400';
                }
                //Requests debug can fail due to heavy payload so adding simple debug with GUID for end to end tracking
                DebugLogger.InsertDebugLog(appId, GUID, 'AddressVerification Request GUID');
                DebugLogger.InsertDebugLog(appId, req2.getBody(), 'AddressVerification Request'); 
                DebugLogger.InsertDebugLog(appId, res2.getBody(), 'AddressVerification Response');   
            } else {
                statusCode='400';
            }    
        }
        catch(Exception ex)
        {
          data.put('status','---------- ' + ex.getMessage() + ' Line Number==>' + ex.getLineNumber());
          statuscode = '0';
          DebugLogger.InsertDebugLog(appId, GUID, 'AddressVerification Request GUID');
        DebugLogger.InsertDebugLog(appId, req2.getBody(), 'AddressVerification Request'); 
        DebugLogger.InsertDebugLog(appId, res2.getBody(), 'AddressVerification Response');
        }       
        data.put('statuscode',statuscode);
        return data;
    }
}