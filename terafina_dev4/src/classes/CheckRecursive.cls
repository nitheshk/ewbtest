public with sharing class CheckRecursive {

    private static boolean run = true;
    private static boolean escalateOnce = true;
    public static boolean escalatedOnce = false;
    public static boolean looprun = true;
    public static boolean isRecursive = true;
    public static boolean isRecursiveSendMail = true;
    
    public static boolean runOnce() {
        if (run) {
            run = false;
            return true;
        } else {
            return run;
        }
    }
     

    public static boolean runEscalationOnce() {
        if (escalateOnce) {
            escalateOnce = false;
            return true;
        } else {
            return escalateOnce;
        }
    }
}