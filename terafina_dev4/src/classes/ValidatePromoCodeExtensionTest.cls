@isTest
public class ValidatePromoCodeExtensionTest{
     
    static testmethod void validatemyTest(){
     Map<String, String> data = new Map<String, String>();
     Map<String, String> tdata = new Map<String, String>();
      
      TF4SF__Application__c ap = new TF4SF__Application__c();
        ap.TF4SF__Product__c = 'Home Loan';
        ap.TF4SF__First_Name__c='test';
        ap.TF4SF__Last_Name__c = 'Test';
        ap.TF4SF__Application_Status__c='Open';
        ap.TF4SF__Email_Address__c= 'test@test.com';
        ap.TF4SF__Primary_Phone_Number__c='1234567890';
        ap.TF4SF__Sub_Product__c = 'Home Loan - Short App';
        insert ap;
        
        VELOSETTINGS__C vo = new VELOSETTINGS__C();
        vo.Name='test';
        vo.TokenEndPoint__c = 'rsgsdgsdf';
        vo.Request_GUID_Name__c = 'RequestUUID';
        vo.ProcessAPI_Timeout__c = 120000;
        insert vo;
    
      tdata.put('id',(String)ap.id);
      tdata.put('emailId',ap.TF4SF__Email_Address__c);
      tdata.put('PhoneNo',ap.TF4SF__Primary_Phone_Number__c);
      tdata.put('promocode','promocode');
    
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new ValidatePromoCodeExtension_Mock ());
        ValidatePromoCodeExtension ob = new ValidatePromoCodeExtension();    
        ValidatePromoCodeExtension.main(tdata);
        test.stopTest();
    }

}