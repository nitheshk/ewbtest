@RestResource(urlMapping='/Attachmentsnew/v2/*')
global with sharing class AttachmentsRESTAPINew {
    global static List<String> allowedContentTypes = new List<String>{
        'application/html', 'application/javascript', 'application/msword', 'application/postscript', 'application/x-javascript', 'application/xhtml+xml', 'application/xml',
        'application/pdf', 'application/vnd.ms-excel', 'application/vnd.google-apps.spreadsheet', 'application/x-shockwave-flash', 'application/x-gzip', 'application/x-zip-compressed', 'application/zip',
        'application/vnd.google-apps.document', 'application/vnd.google-apps.drawing', 'application/vnd.google-apps.form', 'application/vnd.google-apps.presentation', 'application/vnd.google-apps.script',
        'application/vnd.ms-excel.sheet.macroEnabled.12', 'application/vnd.ms-powerpoint.presentation.macroEnabled.12', 'application/vnd.ms-word.document.macroEnabled.12', 'application/vnd.ms-infopath',
        'application/vnd.oasis.opendocument.presentation', 'application/vnd.oasis.opendocument.spreadsheet', 'application/vnd.oasis.opendocument.text', 'application/x-java-source', 'application/opx',
        'application/vnd.openxmlformats-officedocument.presentationml.presentation', 'application/vnd.openxmlformats-officedocument.presentationml.slideshow', 'text/snote', 'text/stypi', 'text/webviewhtml',
        'application/vnd.openxmlformats-officedocument.presentationml.template', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.openxmlformats-officedocument.spreadsheetml.template',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/vnd.openxmlformats-officedocument.wordprocessingml.template', 'application/x-sql', 'application/java-archive',
        'image/bmp', 'image/gif', 'image/jpeg', 'image/png', 'image/svg+xml', 'image/tiff', 'image/vnd.adobe.photoshop', 'image/x-photoshop', 'application/vnd.visio', 'application/vnd.ms-powerpoint', 'image/vnd.dwg',
        'audio/mp4', 'audio/mpeg', 'audio/x-aac', 'audio/x-ms-wma', 'audio/x-ms-wmv', 'audio/x-wav', 'video/mp4', 'video/mpeg', 'video/quicktime', 'video/x-msvideo', 'video/x-ms-asf', 'video/x-m4v',
        'message/rfc822', 'text/css', 'text/csv', 'text/html', 'text/plain', 'text/rtf', 'text/xml', 'application/rtf', 'text/x-c', 'text/x-c++'
    }; 

    @HttpPost
    global static String attachDoc() {
        String rtnStr = '';

        try {
            System.debug('inside attachDoc');
            RestRequest req = RestContext.request;
            RestResponse res = Restcontext.response;
            //res.addHeader('Access-Control-Allow-Origin', Service_Configuration__c.getOrgDefaults().Siteurl__c);
            //res.addHeader('Content-Type', 'application/json');

            String fName = req.params.get('fileName');
            String parentId = req.params.get('parentId');
            String attachmentId = req.params.get('attachmentId');            
            String contentType = req.params.get('type');
            Boolean encoded = (req.params.get('encoded') == 'true');
            System.debug('fName: ' + fName + '; parentId: ' + parentId + '; attachmentId: ' + attachmentId + '; contentType: ' + contentType);
            Blob postContent = req.requestBody;
           

            Attachment att = getAttachmentRec(attachmentId);
            if (allowedContentTypes.contains(contentType)) {
                if (Schema.sObjectType.Attachment.fields.ParentId.isCreateable() && Schema.sObjectType.Attachment.fields.Name.isCreateable() && Schema.sObjectType.Attachment.fields.Body.isCreateable() ) {
                    if(!test.isRunningTest())
                    att.ParentId = parentId;

                    if (encoded ) {
                        String newBody = '';
                        if (att.Body != null) { newBody = EncodingUtil.base64Encode(att.Body); }
                        newBody += postContent;
                        att.Body = EncodingUtil.base64Decode(newBody);
                    } else {
                        att.Body = postContent;
                    }

                    att.Name = fName ;
                    if (contentType != null && Schema.sObjectType.Attachment.fields.ContentType.isCreateable()) { att.ContentType = contentType; }
                    if (Attachment.SObjectType.getDescribe().isUpdateable() && Attachment.SObjectType.getDescribe().isCreateable()) {
                        upsert att;
                        //rtnStr = CryptoHelper.encrypt(String.valueOf(att.Id));
                        rtnStr = String.valueOf(att.Id);
                    }
                }
            } else {
                System.debug('ContentType: ' + contentType + ' is not allowed.');
                rtnStr = 'ContentType: ' + contentType + ' is not allowed.';    
            }
        } catch (Exception e) {
            System.debug('Error encountered: ' + e.getMessage());
            rtnStr = 'Error encountered: ' + e.getMessage() + '; line: ' + e.getLineNumber() + '; trace: ' + e.getStackTraceString();
        }

        return rtnStr;
    }

    @HttpGet
    global static String getAttachment() {
        String rtnStr = '';

        try {
            RestRequest req = RestContext.request;
            RestResponse res = RestContext.response;
            String attachmentId = req.requestURI.split('/Attachments/v1/')[1].trim();

            if (attachmentId != null) {
                //attachmentId = CryptoHelper.decrypt(attachmentId);
                Attachment att = getAttachmentRec(attachmentId);
                Boolean encoded = (req.params.get('encoded') == 'true');

                if (att != null && att.Id != null) {
                    if (att.Body != null) { rtnStr = EncodingUtil.base64Encode(att.Body); }
                } else {
                    rtnStr = 'Requested Attachment could not be found';
                }
            }
        } catch (Exception e) {
            System.debug('Error encountered: ' + e.getMessage());
            rtnStr = 'Error encountered: ' + e.getMessage() + '; line: ' + e.getLineNumber() + '; trace: ' + e.getStackTraceString();
        }

        return rtnStr;
    }

    @HttpDelete
    global static String deleteAttachment() {
        String rtnStr = 'Deleted';

        try {
            RestRequest req = RestContext.request;
            RestResponse res = RestContext.response;
            String attachmentId = req.requestURI.split('/Attachments/v1/')[1].trim();

            if (attachmentId != null) {
                //attachmentId = CryptoHelper.decrypt(attachmentId);
                Attachment att = getAttachmentRec(attachmentId);

                if (att != null && att.Id != null) {
                    if (Attachment.SObjectType.getDescribe().isDeletable()) { delete att; }
                } else {
                    rtnStr = 'Requested Attachment could not be found';
                }
            }
        } catch (Exception e) {
            System.debug('Error encountered: ' + e.getMessage());
            rtnStr = 'Error encountered: ' + e.getMessage() + '; line: ' + e.getLineNumber() + '; trace: ' + e.getStackTraceString();
        }

        return rtnStr;
    }

    private static Attachment getAttachmentRec(String attId) {
        Attachment a = new Attachment();

        if (attId != null) {
            List<Attachment> attachments = [SELECT Id, Body, ContentType FROM Attachment WHERE Id = :attId];
            if (!attachments.isEmpty()) { a = attachments[0]; }
        }

        return a;
    }
}