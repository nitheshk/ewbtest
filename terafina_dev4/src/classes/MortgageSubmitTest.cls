@isTest(SeeAllData=true)
public class MortgageSubmitTest{

static testmethod void Test_one(){
 Map<String, String> tdata=new Map<String, String>();
  TF4SF__Application__c app = new TF4SF__Application__c();
     app.TF4SF__Product__c = 'Home Loan';
     app.TF4SF__First_Name__c='test';
     app.TF4SF__Last_Name__c = 'Test';
     app.TF4SF__Email_Address__c= 'test@test.com';
     app.TF4SF__Street_Address_1__c='TestStreet1';
     app.TF4SF__Housing_Status_J__c = 'Rent';
     app.TF4SF__Housing_Status__c = 'Rent';
     app.TF4SF__Monthly_Payment__c = 303;
     app.TF4SF__User_Token__c ='6kRY1PU8C52TXkCOEX8z3pfzt';
     app.TF4SF__Monthly_Payment_J__c = 4839;
     app.Year_Prev_1_To_PA__c = '2011';
     app.Months_Prev_1_To_PA__c ='11';
     app.Year_Prev_1_From_PA__c = '2010';
     app.Months_Prev_1_From_PA__c = '11';
     app.Year_Prev_To_PA__c = '2011';
     app.Months_Prev_To_PA__c = '11';
     app.Year_Prev_From_PA__c = '2007';
     app.Months_Prev_From_PA__c  = '11';
     app.TF4SF__Street_Address_2__c='TestStreet2';
     app.TF4SF__Custom_Text3__c = '10';
     app.TF4SF__Custom_Text2__c = 'Single Application';
     app.TF4SF__Months__c = '10';
     app.TF4SF__City__c='USA';
     app.TF4SF__State__c='USA';
     app.TF4SF__Zip_Code__c='43534';
     app.TF4SF__Primary_Phone_Number__c='1321423444';
     app.TF4SF__User_Token__c= 'Testtoken';
     app.TF4SF__Sub_Product__c = 'Home Loan - Short App';
     app.TF4SF__Mailing_Street_Address_1__c = 'addres 1';
     app.TF4SF__Mailing_City__c = 'city';
     app.TF4SF__Mailing_State__c = 'NJ';
     app.TF4SF__Mailing_Zip_Code__c = '07410';
     app.TF4SF__Street_Address_1_Prev__c = 'STREET PREV';
     app.TF4SF__City_Prev__c = 'CITUY';
     app.TF4SF__State_Prev__c = 'PA';
     app.TF4SF__Zip_Code_Prev__c = '06505';
     app.Street_Address_1_Prev_1_PA__c = 'ADDRESS';
     app.City_Prev_1_PA__c = 'CIOT';
     app.State_Prev_1_PA__c = 'NJ';
     app.Zip_Code_Prev_1_PA__c = '12345';
     insert app;
     TF4SF__Identity_Information__c ident = new TF4SF__Identity_Information__c(TF4SF__Application__c = app.Id,
     TF4SF__SSN_Prime__c = '999999999',TF4SF__Date_of_Birth__c = '01/01/1970');
     insert ident;
     List<Income_Details__c> incomeDetails= new List<Income_Details__c>();

        Income_Details__c ld=new Income_Details__c ();
        ld.Applicant_Type__c='J1';
        ld.Application__c =app.id;
        insert ld;

        ld=new Income_Details__c ();
        ld.Applicant_Type__c='PA';
        ld.Application__c =app.id;
        insert ld;

        Liability_Details__c liability=new Liability_Details__c ();
        liability.Applicant_Type__c='J1';
        liability.Application__c =app.id;
        insert liability;
        liability=new Liability_Details__c ();
        liability.Applicant_Type__c='PA';
        liability.Application__c =app.id;
        insert liability;

        Asset_Details__c asset=new Asset_Details__c ();
        asset.Applicant_Type__c='J1';
        asset.Application__c =app.id;
        insert asset;
        asset=new Asset_Details__c ();
        asset.Applicant_Type__c='PA';
        asset.Application__c =app.id;
        insert asset;


        TF4SF__About_Account__c objAbtAcc=new TF4SF__About_Account__c();
        objAbtAcc.TF4SF__Application__c=app.id;
        insert objAbtAcc;    
    
       // TF4SF__Product_Codes__c objPcode=new TF4SF__Product_Codes__c();
       // objPcode.name='HL15YR';
       // objPcode.TF4SF__Sub_Product__c='Home Loan - 15 Year Fixed';
       // insert objPcode;




        TF4SF__Application2__c objApp2=new TF4SF__Application2__c();
        objApp2.TF4SF__Application__c=app.id;
        objApp2.Ethnicity__c = 'I do not wish to disclose';
        objApp2.Ethnicity_J1__c = 'I do not wish to disclose';
        objApp2.Race__c = 'White';
        insert objApp2;

        tdata.put('AppId',(String)app.id);
        tdata.put('id',(String)app.id);
        system.debug('App id is '+(String)app.id);
        test.startTest();
        Test.setMock(HttpCalloutMock.class,new MortgageSubmit_Mock());
        //MortgageSubmit obj=new MortgageSubmit();
        //MortgageSubmit.main(tdata);
        MortgageSubmit.main(tdata);
        test.stopTest();
}

/**
 *Test All the scenarios for Asset , Liability and Income
 */
static testmethod void Test_ALI(){
 Map<String, String> tdata=new Map<String, String>();
  TF4SF__Application__c app = new TF4SF__Application__c();
     app.TF4SF__Product__c = 'Home Loan';
     app.TF4SF__First_Name__c='test';
     app.TF4SF__Last_Name__c = 'Test';
     app.TF4SF__Email_Address__c= 'test@test.com';
     app.TF4SF__Street_Address_1__c='TestStreet1';
     app.TF4SF__Housing_Status_J__c = 'Other';
     app.TF4SF__Housing_Status__c = 'Other';
     app.TF4SF__Monthly_Payment__c = 303;
     app.Year_Prev_1_To_PA__c = '2011';
     app.Months_Prev_1_To_PA__c ='11';
     app.Year_Prev_1_From_PA__c = '2010';
     app.Months_Prev_1_From_PA__c = '11';
     app.TF4SF__Monthly_Payment_J__c = 4839;
     app.TF4SF__Street_Address_2__c='TestStreet2';
     app.TF4SF__Custom_Text3__c = '10';
     app.Year_Prev_To_PA__c = '2011';
     app.Months_Prev_To_PA__c = '11';
     app.Year_Prev_From_PA__c = '2007';
     app.Months_Prev_From_PA__c  = '11';
     app.TF4SF__Custom_Text2__c = 'Business Entity';
     app.TF4SF__Months__c = '10';
     app.TF4SF__City__c='USA';
     app.TF4SF__State__c='USA';
     app.TF4SF__Zip_Code__c='43534';
     app.TF4SF__Primary_Phone_Number__c='1321423444';
     app.TF4SF__User_Token__c= 'Testtoken';
     app.TF4SF__Sub_Product__c = 'Home Loan - Short App';
     insert app;
     TF4SF__Identity_Information__c ident = new TF4SF__Identity_Information__c(TF4SF__Application__c = app.Id,
     TF4SF__SSN_Prime__c = '999999999',TF4SF__Date_of_Birth__c = '01/01/1970');
     insert ident;
     List<Income_Details__c> incomeDetails= new List<Income_Details__c>();

        Income_Details__c ld=new Income_Details__c ();
        ld.Applicant_Type__c='J1';
        ld.Application__c =app.id;
        insert ld;

        ld=new Income_Details__c ();
        ld.Applicant_Type__c='PA';
        ld.Application__c =app.id;
        insert ld;

        ld=new Income_Details__c ();
        ld.Applicant_Type__c='J1';
        ld.Application__c =app.id;
        insert ld;

        ld=new Income_Details__c ();
        ld.Applicant_Type__c='PA';
        ld.Application__c =app.id;
        ld.type__c ='Employment';
        ld.Employee_From__c = '01/01/2011';
        ld.Employee_To__c = '01/01/2017';
        insert ld;

        ld=new Income_Details__c ();
        ld.Applicant_Type__c='PA';
        ld.income__C =3030;
        ld.Over_Time__c ='13';
        ld.Bonus__c = 43;
        ld.commission__c = '44';
        ld.Monthly_Rent__c =33;
        ld.Monthly_Income__c =33;
        ld.Application__c =app.id;
        ld.type__c ='Business / Self-employed';
        insert ld;
        ld=new Income_Details__c ();
        ld.Applicant_Type__c='PA';
        ld.Application__c =app.id;
        ld.type__c ='Others';
        insert ld;
        ld=new Income_Details__c ();
        ld.Applicant_Type__c='PA';
        ld.Application__c =app.id;
        ld.type__c ='Military Pay';
        insert ld;
        ld=new Income_Details__c ();
        ld.Applicant_Type__c='PA';
        ld.income__C =3030;
        ld.Bonus__c = 43;
        ld.commission__c = '44';
        ld.Monthly_Rent__c =33;
        ld.Monthly_Income__c =33;
        ld.Application__c =app.id;
        ld.type__c ='Independent Contractor';
        insert ld;
        ld=new Income_Details__c ();
        ld.Applicant_Type__c='PA';
        ld.Application__c =app.id;
        ld.income__C =3030;
        ld.Bonus__c = 43;
        ld.commission__c = '44';
        ld.Monthly_Rent__c =33;
        ld.Monthly_Income__c =33;
        ld.type__c ='Pension';
        insert ld;
        ld=new Income_Details__c ();
        ld.Applicant_Type__c='PA';
        ld.Application__c =app.id;
        ld.type__c ='Employment';
        ld.income__C =3030;
        ld.Over_Time__c ='13';
        ld.Bonus__c = 43;
        ld.commission__c = '44';
        ld.Monthly_Rent__c =33;
        ld.Monthly_Income__c =33;
        ld.Employee_From__c = '01/01/2011';
        ld.Employee_To__c = '01/01/2017';
        insert ld;
        ld=new Income_Details__c ();
        ld.Applicant_Type__c='PA';
        ld.Application__c =app.id;
        ld.type__c ='Others';
        ld.income__C =3030;
        ld.Bonus__c = 43;
        ld.commission__c = '44';
        ld.Monthly_Rent__c =33;
        ld.Monthly_Income__c =33;
        ld.Employee_From__c = '01/01/2011';
        ld.Employee_To__c = '01/01/2017';
        insert ld;
        ld=new Income_Details__c ();
        ld.Applicant_Type__c='PA';
        ld.Application__c =app.id;
        ld.type__c ='Others';
        ld.income__C =3030;
        ld.Bonus__c = 43;
        ld.commission__c = '44';
        ld.Monthly_Rent__c =33;
        ld.Monthly_Income__c =33;
        ld.Employee_From__c = '01/01/2011';
        ld.Employee_To__c = '01/01/2017';
        insert ld;


        Liability_Details__c liability=new Liability_Details__c ();
        liability.Applicant_Type__c='J1';
        liability.Application__c =app.id;
        insert liability;
        liability=new Liability_Details__c ();
        liability.Applicant_Type__c='PA';
        liability.Application__c =app.id;
        insert liability;

        liability=new Liability_Details__c ();
        liability.Applicant_Type__c='PA';
        liability.Application__c =app.id;
        liability.Paid_Off__c = 'Yes';
        liability.Nature_of_Liability__c = 'Automobile Loans';
        insert liability;
        
        liability=new Liability_Details__c ();
        liability.Applicant_Type__c='PA';
        liability.Application__c =app.id;
        liability.Paid_Off__c = 'Yes';
        liability.Nature_of_Liability__c = 'Revolving Charge Accounts';
        insert liability;

        liability=new Liability_Details__c ();
        liability.Applicant_Type__c='PA';
        liability.Application__c =app.id;
        liability.Paid_Off__c = 'No';
        liability.Nature_of_Liability__c = 'Alimony/Child Support';
        liability.Monthly_Pledge__c = 403;
        insert liability;
        liability=new Liability_Details__c ();
        liability.Applicant_Type__c='PA';
        liability.Application__c =app.id;
        liability.Paid_Off__c = 'Yes';
        liability.Nature_of_Liability__c = 'Real Estate Loan';
        insert liability;
        liability=new Liability_Details__c ();
        liability.Applicant_Type__c='PA';
        liability.Application__c =app.id;
        liability.Paid_Off__c = 'Yes';
        liability.Nature_of_Liability__c = 'Other';
        insert liability;
        Asset_Details__c asset=new Asset_Details__c ();
        asset.Applicant_Type__c='J1';
        asset.Application__c =app.id;
        insert asset;
        asset=new Asset_Details__c ();
        asset.Applicant_Type__c='J1';
        asset.Application__c =app.id;
        insert asset;
        asset=new Asset_Details__c ();
        asset.Applicant_Type__c='PA';
        asset.Application__c =app.id;
        asset.AssetsType__c = 'Real Estate';
        insert asset;
        asset=new Asset_Details__c ();
        asset.Applicant_Type__c='PA';
        asset.Application__c =app.id;
        asset.AssetsType__c = 'Other';
        insert asset;
        asset=new Asset_Details__c ();
        asset.Applicant_Type__c='PA';
        asset.Application__c =app.id;
        asset.AssetsType__c = 'Brokerage';
        asset.Account_Balance__c =3939;
        insert asset;
        asset=new Asset_Details__c ();
        asset.Applicant_Type__c='PA';
        asset.Application__c =app.id;
        asset.Account_Balance__c =3939;
        asset.AssetsType__c = 'Checking' ;
        insert asset;
        asset=new Asset_Details__c ();
        asset.Applicant_Type__c='PA';
        asset.Account_Balance__c =3939;
        asset.Application__c =app.id;
        asset.AssetsType__c = 'Savings';
        insert asset;
        asset=new Asset_Details__c ();
        asset.Applicant_Type__c='J1';
        asset.Account_Balance__c =3939;
        asset.Application__c =app.id;
        asset.AssetsType__c = 'Trust';
        insert asset;
        asset=new Asset_Details__c ();
        asset.Applicant_Type__c='PA';
        asset.Application__c =app.id;
        asset.Account_Balance__c =3939;
        asset.AssetsType__c = 'Money Market';
        insert asset;
        asset=new Asset_Details__c ();
        asset.Applicant_Type__c='J1';
        asset.Application__c =app.id;
        asset.Account_Balance__c =3939;
        asset.AssetsType__c = 'Certificate of Deposit';
        insert asset;
        asset=new Asset_Details__c ();
        asset.Applicant_Type__c='PA';
        asset.Application__c =app.id;
        asset.Account_Balance__c =3939;
        asset.AssetsType__c = 'Annuity';
        insert asset;
        asset=new Asset_Details__c ();
        asset.Applicant_Type__c='PA';
        asset.Application__c =app.id;
        asset.Account_Balance__c =3939;
        asset.AssetsType__c = 'Retirement';
        insert asset;
        asset=new Asset_Details__c ();
        asset.Applicant_Type__c='PA';
        asset.Application__c =app.id;
        asset.Account_Balance__c =3939;
        asset.AssetsType__c = 'None';
        insert asset;
        



        TF4SF__About_Account__c objAbtAcc=new TF4SF__About_Account__c();
        objAbtAcc.TF4SF__Application__c=app.id;
        objAbtAcc.TF4SF__Occupancy__c = 'Primary Residence';
        insert objAbtAcc;    
    
       // TF4SF__Product_Codes__c objPcode=new TF4SF__Product_Codes__c();
       // objPcode.name='HL15YR';
       // objPcode.TF4SF__Sub_Product__c='Home Loan - 15 Year Fixed';
       // insert objPcode;




        TF4SF__Application2__c objApp2=new TF4SF__Application2__c();
        objApp2.TF4SF__Application__c=app.id;
        objApp2.Ethnicity__c = 'I do not wish to disclose';
        objApp2.Ethnicity_J1__c = 'I do not wish to disclose';
        insert objApp2;

        tdata.put('AppId',(String)app.id);
        tdata.put('id',(String)app.id);
        system.debug('App id is '+(String)app.id);
        test.startTest();
        Test.setMock(HttpCalloutMock.class,new MortgageSubmit_Mock());
        //MortgageSubmit obj=new MortgageSubmit();
        //MortgageSubmit.main(tdata);
        MortgageSubmit.main(tdata);
        test.stopTest();
}

/**
 * Testing the trust application
 */
static testmethod void Test_Trust(){
 Map<String, String> tdata=new Map<String, String>();
  TF4SF__Application__c app = new TF4SF__Application__c();
     app.TF4SF__Product__c = 'Home Loan';
     app.TF4SF__First_Name__c='test';
     app.TF4SF__Last_Name__c = 'Test';
     app.TF4SF__Email_Address__c= 'test@test.com';
     app.TF4SF__Street_Address_1__c='TestStreet1';
     app.TF4SF__Street_Address_2__c='TestStreet2';
     app.TF4SF__Custom_Text3__c = '10';
     app.Year_Prev_1_To_PA__c = '2011';
     app.Months_Prev_1_To_PA__c ='11';
     app.Year_Prev_1_From_PA__c = '2010';
     app.Months_Prev_1_From_PA__c = '11';
     app.Year_Prev_To_PA__c = '2011';
     app.Months_Prev_To_PA__c = '11';
     app.Year_Prev_From_PA__c = '2007';
     app.Months_Prev_From_PA__c  = '11';
     app.TF4SF__Custom_Text2__c = 'Trust';
     app.TF4SF__Months__c = '10';
     app.TF4SF__City__c='USA';
     app.TF4SF__State__c='USA';
     app.TF4SF__Zip_Code__c='43534';
     app.TF4SF__Primary_Phone_Number__c='1321423444';
     app.TF4SF__User_Token__c= 'Testtoken';
     app.TF4SF__Sub_Product__c = 'Home Loan - Short App';
     insert app;
     TF4SF__Identity_Information__c ident = new TF4SF__Identity_Information__c(TF4SF__Application__c = app.Id,
     TF4SF__SSN_Prime__c = '999999999',TF4SF__Date_of_Birth__c = '01/01/1970');
     insert ident;
     List<Income_Details__c> incomeDetails= new List<Income_Details__c>();

        Income_Details__c ld=new Income_Details__c ();
        ld.Applicant_Type__c='J1';
        ld.Application__c =app.id;
        insert ld;

        ld=new Income_Details__c ();
        ld.Applicant_Type__c='PA';
        ld.Application__c =app.id;
        insert ld;

        ld=new Income_Details__c ();
        ld.Applicant_Type__c='J1';
        ld.Application__c =app.id;
        insert ld;

        ld=new Income_Details__c ();
        ld.Applicant_Type__c='PA';
        ld.Application__c =app.id;
        ld.type__c ='Employment';
        ld.Employee_From__c = '01/01/2011';
        ld.Employee_To__c = '01/01/2017';
        insert ld;

        ld=new Income_Details__c ();
        ld.Applicant_Type__c='PA';
        ld.Application__c =app.id;
        ld.type__c ='Business / Self-employed';
        insert ld;
        ld=new Income_Details__c ();
        ld.Applicant_Type__c='PA';
        ld.Application__c =app.id;
        ld.type__c ='Others';
        insert ld;
        ld=new Income_Details__c ();
        ld.Applicant_Type__c='PA';
        ld.Application__c =app.id;
        ld.type__c ='Military Pay';
        insert ld;
        ld=new Income_Details__c ();
        ld.Applicant_Type__c='PA';
        ld.Application__c =app.id;
        ld.type__c ='Independent Contractor';
        insert ld;
        ld=new Income_Details__c ();
        ld.Applicant_Type__c='PA';
        ld.Application__c =app.id;
        ld.type__c ='Pension';
        insert ld;
        ld=new Income_Details__c ();
        ld.Applicant_Type__c='PA';
        ld.Application__c =app.id;
        ld.type__c ='Employment';
        ld.Employee_From__c = '01/01/2011';
        ld.Employee_To__c = '01/01/2017';

        insert ld;


        Liability_Details__c liability=new Liability_Details__c ();
        liability.Applicant_Type__c='J1';
        liability.Application__c =app.id;
        insert liability;
        liability=new Liability_Details__c ();
        liability.Applicant_Type__c='PA';
        liability.Application__c =app.id;
        insert liability;

        liability=new Liability_Details__c ();
        liability.Applicant_Type__c='PA';
        liability.Application__c =app.id;
        liability.Paid_Off__c = 'Yes';
        liability.Nature_of_Liability__c = 'Automobile Loans';
        insert liability;
        
        liability=new Liability_Details__c ();
        liability.Applicant_Type__c='PA';
        liability.Application__c =app.id;
        liability.Paid_Off__c = 'Yes';
        liability.Nature_of_Liability__c = 'Revolving Charge Accounts';
        insert liability;

        liability=new Liability_Details__c ();
        liability.Applicant_Type__c='PA';
        liability.Application__c =app.id;
        liability.Paid_Off__c = 'Yes';
        liability.Nature_of_Liability__c = 'Alimony/Child Support';
        insert liability;
        liability=new Liability_Details__c ();
        liability.Applicant_Type__c='PA';
        liability.Application__c =app.id;
        liability.Paid_Off__c = 'Yes';
        liability.Nature_of_Liability__c = 'Real Estate Loan';
        insert liability;
        liability=new Liability_Details__c ();
        liability.Applicant_Type__c='PA';
        liability.Application__c =app.id;
        liability.Paid_Off__c = 'Yes';
        liability.Nature_of_Liability__c = 'Other';
        insert liability;
        Asset_Details__c asset=new Asset_Details__c ();
        asset.Applicant_Type__c='J1';
        asset.Application__c =app.id;
        insert asset;
        asset=new Asset_Details__c ();
        asset.Applicant_Type__c='J1';
        asset.Application__c =app.id;
        insert asset;
        asset=new Asset_Details__c ();
        asset.Applicant_Type__c='PA';
        asset.Application__c =app.id;
        asset.AssetsType__c = 'Real Estate';
        insert asset;
        asset=new Asset_Details__c ();
        asset.Applicant_Type__c='PA';
        asset.Application__c =app.id;
        asset.AssetsType__c = 'Other';
        insert asset;
        asset=new Asset_Details__c ();
        asset.Applicant_Type__c='PA';
        asset.Application__c =app.id;
        asset.AssetsType__c = 'Brokerage';
        insert asset;
        asset=new Asset_Details__c ();
        asset.Applicant_Type__c='PA';
        asset.Application__c =app.id;
        asset.AssetsType__c = 'Checking' ;
        insert asset;
        asset=new Asset_Details__c ();
        asset.Applicant_Type__c='PA';
        asset.Application__c =app.id;
        asset.AssetsType__c = 'Savings';
        insert asset;
        asset=new Asset_Details__c ();
        asset.Applicant_Type__c='PA';
        asset.Application__c =app.id;
        asset.AssetsType__c = 'Trust';
        insert asset;
        asset=new Asset_Details__c ();
        asset.Applicant_Type__c='PA';
        asset.Application__c =app.id;
        asset.AssetsType__c = 'Money Market';
        insert asset;
        asset=new Asset_Details__c ();
        asset.Applicant_Type__c='PA';
        asset.Application__c =app.id;
        asset.AssetsType__c = 'Certificate of Deposit';
        insert asset;


        TF4SF__About_Account__c objAbtAcc=new TF4SF__About_Account__c();
        objAbtAcc.TF4SF__Application__c=app.id;
        objAbtAcc.TF4SF__Occupancy__c = 'SecondHome';
        insert objAbtAcc;    
    
       // TF4SF__Product_Codes__c objPcode=new TF4SF__Product_Codes__c();
       // objPcode.name='HL15YR';
       // objPcode.TF4SF__Sub_Product__c='Home Loan - 15 Year Fixed';
       // insert objPcode;




        TF4SF__Application2__c objApp2=new TF4SF__Application2__c();
        objApp2.TF4SF__Application__c=app.id;
        insert objApp2;

        tdata.put('AppId',(String)app.id);
        tdata.put('id',(String)app.id);
        system.debug('App id is '+(String)app.id);
        test.startTest();
        Test.setMock(HttpCalloutMock.class,new MortgageSubmit_Mock());
        //MortgageSubmit obj=new MortgageSubmit();
        //MortgageSubmit.main(tdata);
        MortgageSubmit.main(tdata);
        test.stopTest();
}


/**
 * Testing the Joint Applicant
 */
static testmethod void Test_Joint_Applicant(){
 Map<String, String> tdata=new Map<String, String>();
  TF4SF__Application__c app = new TF4SF__Application__c();
     app.TF4SF__Product__c = 'Home Loan';
     app.TF4SF__First_Name__c='test';
     app.TF4SF__Last_Name__c = 'Test';
     app.TF4SF__Email_Address__c= 'test@test.com';
     app.TF4SF__Street_Address_1__c='TestStreet1';
     app.TF4SF__Street_Address_2__c='TestStreet2';
     app.TF4SF__Custom_Text3__c = '10';
     app.Year_Prev_1_To_PA__c = '2011';
     app.Months_Prev_1_To_PA__c ='11';
     app.Year_Prev_1_From_PA__c = '2010';
     app.Months_Prev_1_From_PA__c = '11';
     app.Year_Prev_To_PA__c = '2011';
     app.Months_Prev_To_PA__c = '11';
     app.Year_Prev_From_PA__c = '2007';
     app.Months_Prev_From_PA__c  = '11';
     app.TF4SF__Custom_Text2__c = 'Joint Application';
     app.TF4SF__Months__c = '10';
     app.TF4SF__Housing_Status_J__c = 'OWn';
     app.TF4SF__Housing_Status__c = 'Own';
     app.TF4SF__Monthly_Payment__c = 303;
     app.TF4SF__Monthly_Payment_J__c = 4839;
     app.TF4SF__City__c='USA';
     app.TF4SF__State__c='USA';
     app.TF4SF__Zip_Code__c='43534';
     app.TF4SF__Primary_Phone_Number__c='1321423444';
     app.TF4SF__User_Token__c= 'Testtoken';
     app.TF4SF__Sub_Product__c = 'Home Loan - Short App';
     insert app;
     TF4SF__Identity_Information__c ident = new TF4SF__Identity_Information__c(TF4SF__Application__c = app.Id,
     TF4SF__SSN_Prime__c = '999999999',TF4SF__Date_of_Birth__c = '01/01/1970');
     insert ident;
     List<Income_Details__c> incomeDetails= new List<Income_Details__c>();

        Income_Details__c ld=new Income_Details__c ();
        ld.Applicant_Type__c='J1';
        ld.Application__c =app.id;
        insert ld;

        ld=new Income_Details__c ();
        ld.Applicant_Type__c='PA';
        ld.Application__c =app.id;
        insert ld;

        ld=new Income_Details__c ();
        ld.Applicant_Type__c='J1';
        ld.Application__c =app.id;
        insert ld;

        ld=new Income_Details__c ();
        ld.Applicant_Type__c='PA';
        ld.Application__c =app.id;
        ld.type__c ='Employment';
        ld.Employee_From__c = '01/01/2011';
        ld.Employee_To__c = '01/01/2017';
        insert ld;

        ld=new Income_Details__c ();
        ld.Applicant_Type__c='PA';
        ld.Application__c =app.id;
        ld.type__c ='Business / Self-employed';
        insert ld;
        ld=new Income_Details__c ();
        ld.Applicant_Type__c='PA';
        ld.Application__c =app.id;
        ld.type__c ='Others';
        insert ld;
        ld=new Income_Details__c ();
        ld.Applicant_Type__c='PA';
        ld.Application__c =app.id;
        ld.type__c ='Military Pay';
        insert ld;
        ld=new Income_Details__c ();
        ld.Applicant_Type__c='PA';
        ld.Application__c =app.id;
        ld.type__c ='Independent Contractor';
        insert ld;
        ld=new Income_Details__c ();
        ld.Applicant_Type__c='PA';
        ld.Application__c =app.id;
        ld.type__c ='Pension';
        insert ld;
        ld=new Income_Details__c ();
        ld.Applicant_Type__c='PA';
        ld.Application__c =app.id;
        ld.type__c ='Employment';
        ld.Employee_From__c = '01/01/2011';
        ld.Employee_To__c = '01/01/2017';

        insert ld;


        Liability_Details__c liability=new Liability_Details__c ();
        liability.Applicant_Type__c='J1';
        liability.Application__c =app.id;
        insert liability;
        liability=new Liability_Details__c ();
        liability.Applicant_Type__c='PA';
        liability.Application__c =app.id;
        insert liability;

        liability=new Liability_Details__c ();
        liability.Applicant_Type__c='PA';
        liability.Application__c =app.id;
        liability.Paid_Off__c = 'Yes';
        liability.Nature_of_Liability__c = 'Automobile Loans';
        insert liability;
        
        liability=new Liability_Details__c ();
        liability.Applicant_Type__c='PA';
        liability.Application__c =app.id;
        liability.Paid_Off__c = 'Yes';
        liability.Nature_of_Liability__c = 'Revolving Charge Accounts';
        insert liability;

        liability=new Liability_Details__c ();
        liability.Applicant_Type__c='PA';
        liability.Application__c =app.id;
        liability.Paid_Off__c = 'Yes';
        liability.Nature_of_Liability__c = 'Alimony/Child Support';
        insert liability;
        liability=new Liability_Details__c ();
        liability.Applicant_Type__c='PA';
        liability.Application__c =app.id;
        liability.Paid_Off__c = 'Yes';
        liability.Nature_of_Liability__c = 'Real Estate Loan';
        insert liability;
        liability=new Liability_Details__c ();
        liability.Applicant_Type__c='PA';
        liability.Application__c =app.id;
        liability.Paid_Off__c = 'Yes';
        liability.Monthly_Pledge__c = 100;
        liability.Nature_of_Liability__c = 'Other';
        insert liability;
        Asset_Details__c asset=new Asset_Details__c ();
        asset.Applicant_Type__c='J1';
        asset.Application__c =app.id;
        insert asset;
        asset=new Asset_Details__c ();
        asset.Applicant_Type__c='J1';
        asset.Application__c =app.id;
        insert asset;
        asset=new Asset_Details__c ();
        asset.Applicant_Type__c='PA';
        asset.Application__c =app.id;
        asset.AssetsType__c = 'Real Estate';
        insert asset;
        asset=new Asset_Details__c ();
        asset.Applicant_Type__c='PA';
        asset.Application__c =app.id;
        asset.AssetsType__c = 'Other';
        insert asset;
        asset=new Asset_Details__c ();
        asset.Applicant_Type__c='PA';
        asset.Application__c =app.id;
        asset.AssetsType__c = 'Brokerage';
        insert asset;
        asset=new Asset_Details__c ();
        asset.Applicant_Type__c='PA';
        asset.Application__c =app.id;
        asset.AssetsType__c = 'Checking' ;
        insert asset;
        asset=new Asset_Details__c ();
        asset.Applicant_Type__c='PA';
        asset.Application__c =app.id;
        asset.AssetsType__c = 'Savings';
        insert asset;
        asset=new Asset_Details__c ();
        asset.Applicant_Type__c='PA';
        asset.Application__c =app.id;
        asset.AssetsType__c = 'Trust';
        insert asset;
        asset=new Asset_Details__c ();
        asset.Applicant_Type__c='PA';
        asset.Application__c =app.id;
        asset.AssetsType__c = 'Money Market';
        insert asset;
        asset=new Asset_Details__c ();
        asset.Applicant_Type__c='PA';
        asset.Application__c =app.id;
        asset.AssetsType__c = 'Certificate of Deposit';
        insert asset;


        TF4SF__About_Account__c objAbtAcc=new TF4SF__About_Account__c();
        objAbtAcc.TF4SF__Application__c=app.id;
        objAbtAcc.TF4SF__Occupancy__c = 'Investor';
        insert objAbtAcc;    
    
       // TF4SF__Product_Codes__c objPcode=new TF4SF__Product_Codes__c();
       // objPcode.name='HL15YR';
       // objPcode.TF4SF__Sub_Product__c='Home Loan - 15 Year Fixed';
       // insert objPcode;




        TF4SF__Application2__c objApp2=new TF4SF__Application2__c();
        objApp2.TF4SF__Application__c=app.id;
        objApp2.Ethnicity_J1__c = 'I do not wish to disclose';
        objApp2.Ethnicity__c = 'Not Hispanic or Latino';
        objApp2.Race__c = 'American Indian Or Alaska Native';
        objApp2.Gender__c = 'Male';
        insert objApp2;

        tdata.put('AppId',(String)app.id);
        tdata.put('id',(String)app.id);
        system.debug('App id is '+(String)app.id);
        test.startTest();
        Test.setMock(HttpCalloutMock.class,new MortgageSubmit_Mock());
        //MortgageSubmit obj=new MortgageSubmit();
        //MortgageSubmit.main(tdata);
        MortgageSubmit.main(tdata);
        test.stopTest();
}

}