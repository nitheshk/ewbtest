global with sharing class SaveW9FormAttachment implements TF4SF.DSP_Interface {
    
    private static void generatePDFForm(String appId,String pdfName,String pdfVisualPage){

            Blob body;
            //PageReference pdf = Page.w9TaxFormPdf;  
            PageReference pdf = new PageReference(pdfVisualPage);
            pdf.getParameters().put('id',appId);  
            //pdf.getParameters().put('p','pdf');      
            if(test.isRunningTest())    {
                body = Blob.valueOf('test');
            }
            else    {  
                body = pdf.getContent();  
            }
                       
           //Delete old attachments
            List<Attachment> attachtoDel = new List<Attachment>();
            attachtoDel = [SELECT Id, Name, ParentId FROM Attachment WHERE Name =: pdfName AND ParentId = :appId];
            
            if (attachtoDel.size() > 0) {
                delete attachtoDel;
            }
            //Create New attachment for application 
            Attachment att = new Attachment();
            att.name = pdfName;
            att.Body = body;
            att.isPrivate = false;
            att.Description = pdfVisualPage;
            att.ParentId = appId;
            att.ContentType = 'application/pdf';
            insert att;  
    }
    
    global static Map<String, String> main(Map<String, String> tdata) {
    
        Map<String, String> data = tdata.clone();
        String appId = tdata.get('id');   
        String message='',status='0',Options='';
        String W8_FORM_FILE_NAME = Label.W8Form_File_Name;
        String W9_FORM_FILE_NAME = Label.W9Form_File_Name;
        try{
            if(appId!=null && String.isNotBlank(appId)){
                TF4SF__Identity_Information__c identityInformation=[SELECT Custom_Picklist13__c,NRA_Citizenship__c FROM TF4SF__Identity_Information__c WHERE TF4SF__Application__c =: appId limit 1];    
                system.debug('***' + identityInformation);
                if(identityInformation.Custom_Picklist13__c=='Yes')   {
                    //w9 form 
                    if(identityInformation.NRA_Citizenship__c==Label.UI_NonResident_Select_Field_Value){
                        generatePDFForm(appId,W8_FORM_FILE_NAME,'/w8TaxFormPdf');
                    }
                    else{
                        generatePDFForm(appId,W9_FORM_FILE_NAME,'/w9TaxFormPdf');                       
                    }

                    status='200';
                }
                else    {
                    //w8form
                    generatePDFForm(appId,W8_FORM_FILE_NAME,'/w8TaxFormPdf');
                    status='200';
                } 
            }
        }
        catch(Exception ex){
            message='---------- ' + ex.getMessage() + ' Line Number==>' + ex.getLineNumber();      
        }
        
        Options += '{';
        Options += '"status": "'+status+'",';
        Options += '"message": "'+message+'"';    
        Options += '}';
        data.put('status',Options);
        
        return data; 
        
        /*

        try {
            if(appId!=null && appId!='')
            {
            Blob body;
            PageReference pdf = Page.w9TaxFormPdf;  
            pdf.getParameters().put('id',appId);  
            //pdf.getParameters().put('p','pdf');      
            if(test.isRunningTest()){
             body = Blob.valueOf('test');
           }
                else{
                     body = pdf.getContent();  
                }
                 
                       
           //Delete old attachments

            List<Attachment> attachtoDel = new List<Attachment>();
            attachtoDel = [SELECT Id, Name, ParentId FROM Attachment WHERE Name = 'w9TaxForm.pdf' AND ParentId = :appId];
            
            if (attachtoDel.size() > 0) {
                delete attachtoDel;
            }

            Attachment att = new Attachment();
            att.name = 'w9TaxForm.pdf';
            att.Body = body;
            att.isPrivate = false;
            att.Description = 'w9TaxForm';
            att.ParentId = appId;
            att.ContentType = 'application/pdf';
            insert att;  

            status='200';
            }     
        } catch (Exception e) {
            message='---------- ' + e.getMessage() + ' Line Number==>' + e.getLineNumber();           
        }
        
        data.put('status',Options);
        Options += '{';
        Options += '"status": "'+status+'",';
        Options += '"message": "'+message+'"';    
        Options += '}';
        
        data.put('status',Options);
        return data; 
        */
        
    }
    }