global class StatusUpdate_Batch implements Database.Batchable<sObject>{ 
    global Database.QueryLocator start(Database.BatchableContext BC) {
        //**********Get records from object Application 
        return Database.getQueryLocator('SELECT Id, Time_elapsed_for_abandoned__c,TF4SF__Application_Status__c  FROM TF4SF__Application__c where TF4SF__Product__c=\'Home Loan\' and  Time_elapsed_for_abandoned__c >=20 and TF4SF__Application_Status__c =\'Open\'');
    }
    
    global void execute(Database.BatchableContext BC, List<TF4SF__Application__c> listApp) {
        
        for (TF4SF__Application__c app : listApp) {
            //****updates the status to abandoned after an hour
            app.TF4SF__Application_Status__c ='Abandoned';
        }
        if(listApp.size () > 0){
        update listApp;
       
        }
        
    }
    
    global void finish(Database.BatchableContext BC) {
        ScheduleUpdateStausBatchApex.SchedulerMethod();
    }
}