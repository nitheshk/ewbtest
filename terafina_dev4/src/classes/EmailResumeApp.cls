global with sharing class EmailResumeApp implements TF4SF.DSP_Interface {
    
    global static Map<String, String> main(Map<String, String> tdata) {

        Map<String, String> data = new Map<String, String>();
        String Options = '',status='',message='';

        String appId = tdata.get('id');        
        
        try{
        List <TF4SF__Application__c>  currentApp =[SELECT Id,Name, TF4SF__Application_Status__c,
                     TF4SF__Email_Address__c,TF4SF__Primary_Phone_Number__c,TF4SF__First_Name__c,TF4SF__Last_Name__c
                     FROM TF4SF__Application__c 
                     where Id=: appId];

        EmailNotificationDeposit_Utility utility=new EmailNotificationDeposit_Utility();
        utility.sendResumeAppForDeposit(currentApp);
        status='200'; 
            if(Test.isRunningTest()){
                integer p=10/0;
            }
        }
        catch(Exception ex){
            message=ex.getMessage()+'Line number-->'+ ex.getLineNumber() ;
        }
        
        Options += '{';
        Options += '"status": "'+status+'",';    
        Options += '"message": "'+message+'"'; 
        Options += '}'; 

        data.put('JsonResult',Options);      
    
        return data;
       } 
   
   
}