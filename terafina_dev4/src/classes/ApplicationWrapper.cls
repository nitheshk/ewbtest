global class ApplicationWrapper {
	
    public TF4SF__Application__c App {get; set;}
    public TF4SF__Application2__c App2 {get; set;}
    public TF4SF__Employment_Information__c Employment {get; set;}
    public TF4SF__Identity_Information__c Identity {get; set;}
    public TF4SF__About_Account__c About {get; set;}

    public ApplicationWrapper() {}
    public ApplicationWrapper(TF4SF__Application__c app, TF4SF__Application2__c app2, TF4SF__Employment_Information__c employment, TF4SF__Identity_Information__c identity, TF4SF__About_Account__c about) {
        this.App = app;
        this.App2 = app2;
        this.Employment = employment;
        this.Identity = identity;
        this.About = about;
        System.debug('Application.App:' + this.App);
        System.debug('Application.App2:' + this.App2);
        System.debug('Application.Employment:' + this.Employment);
        System.debug('Application.Identity:' + this.Identity);
        System.debug('Application.About:' + this.About);
    }
}