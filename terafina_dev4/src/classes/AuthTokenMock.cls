@isTest
public class AuthTokenMock implements HttpCalloutMock {

    public HTTPResponse respond(HTTPRequest req) {

        HttpResponse res = new HttpResponse();
        res.setBody('{"access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IkFkYjBMTVdKUHhuRzNOb2EwTk5STk52UVYxdyJ9.eyJ1bmlxdWVfbmFtZSI6IjdwZTJjelRkMzdqNjdNeUprN2Q2Iiwicm9sZSI6InBhcnRuZXIiLCJpc3MiOiJodHRwOi8vYWRmcy5lYXN0d2VzdGJhbmsuY29tL2FkZnMvc2VydmljZXMvdHJ1c3QiLCJhdWQiOiJodHRwczovL29wZW5hcGlzZXJ2aWNlcy5lYXN0d2VzdGJhbmsuY29tL2FwaSIsImV4cCI6MTUzNTE0MjMyMiwibmJmIjoxNTMxNTQyMzIyfQ.w3ecDE3OiDLBjT71CqJPLUQhXe1o1yD_uuLn_PEe3gPNGcEgB-x7K4BB6_tzm6WSnIlXWk81fl5ok_vynDWo5CQTS2uWXpDP1zmox3qUgtU7lLMX1kkOt3Lmt8tjjEmsjUS1o1SLu36jrv7v9mkWcnTitKbmOO43Lk5QRbJ4AzUAoilPfqduHwAObg0q1eaObx7-dsQTKlnNh12D3m_K72ePQdbIh_a8ddoryvKeeiUdh-vfq-6gxvY5MY3gcwA-0OrA4pi8ZngTrCjrgNqlXG9C8XOSBVcojUr7IEY0d5TXc6TxFlw_e0MiNXnVsxyreDEta7doZjrI_0SSFCTHuw","token_type":"bearer","expires_in":3599999}');
        res.setStatusCode(200);
        res.setStatus('OK');
        return res;
    }

}