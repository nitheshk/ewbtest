global with sharing class CreateShadowAccount implements TF4SF.DSP_Interface {
	
    global static Map<String, String> main (Map<String, String> tdata) {
    	Map<String, String> data = tdata.clone();
        String appId = data.get('id');
		String emailid = data.get('EmailId');
	    ValidatePromoCodeEmailExtension.createShadowAccount(appId, AuthToken.Auth(), emailid.toLowerCase(), '');
        return data;
    }
    
}