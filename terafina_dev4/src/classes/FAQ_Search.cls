global with sharing class FAQ_Search{
    public string searchString{get;set;}
    public List<List<FAQ__c>> result{get;set;}    
    public List<List<FAQ__c>> topLikedArticles{get;set;}  
    public String title{get;set;}
    public String description{get;set;}
    public String recordId{get;set;}
    public Static Boolean dispErr{get;set;} 
    public Static Boolean dispLikedArtErr{get;set;}  
    public Static Boolean enableLikeDislike{get;set;}  
    public Static Boolean trackArticleView{get;set;}  
    public Decimal likeCount {get;set;}
   
    
    //Function to search for article based on the search text 
    public void search()
    { 
        try{      
                 List<FAQ__c> lstFAQ ;
                if(String.isNotEmpty(searchstring)) {
                    String searchquery = 'FIND \'' +searchstring+'\'  IN ALL FIELDS RETURNING FAQ__c(Id,Title__c,Description__c)';
                    result = null;
                    result= Search.query(searchquery); 
                    //If no results found display 'No results' in the VF page                
                    lstFAQ= ((List<FAQ__c>)result[0]);
                    if (lstFAQ.size() <=0) {
                        dispErr = true;                
                    } 
                    else {
                        dispErr = false;                
                    }
                } else {
                    List<List<FAQ__c>> lstRes = new List<List<FAQ__c>>();
                    lstFAQ  =[SELECT Id,View_Count__c,Title__c,Description__c FROM FAQ__c WHERE View_Count__c !=NULL ORDER BY View_Count__c DESC LIMIT 10];
                    if(lstFAQ.size() > 0 ) {  
                        lstRes.add(lstFAQ);
                        result = lstRes;
                        dispErr = false; 
                    }
                    else {
                        dispErr = true;                
                    }
                }                 

            } catch(exception ex) {
                system.debug('Exception ==>' + ex.getMessage());
            }  
    }  


    //To display top 10 most viewed article on FAQ page
    public void showAllArticles()
    {
        try{
                
                List<FAQ__c> lstFAQ;
                String searchquery ;
                searchString = System.currentPageReference().getParameters().get('q');
                //If URL has search string,serach based on search string
                if (String.isNotEmpty(searchString)) {                                
                    searchquery = 'FIND \'' +searchString +'\'  IN ALL FIELDS RETURNING FAQ__c(Id,Title__c,Description__c)';
                    result= Search.query(searchquery); 
                }
                //Display most viewed articles 
                else {
                    List<List<FAQ__c>> lstRes = new List<List<FAQ__c>>();
                    lstFAQ  =[SELECT Id,View_Count__c,Title__c,Description__c FROM FAQ__c WHERE View_Count__c !=NULL ORDER BY View_Count__c DESC LIMIT 10];
                    if( lstFAQ.size() > 0 ) {  
                        lstRes.add(lstFAQ);
                        result = lstRes;
                        
                    }
                }  
                //If no results found display 'No results' in the VF page
               if (result.isEmpty() && result == null && result.size() <=0) {
                    dispErr = true;                
                }       
                else {
                     dispErr = false;                 
                }   

                
                //Top viewed articles
                lstFAQ =[SELECT Id,View_Count__c,Title__c,Description__c FROM FAQ__c WHERE View_Count__c !=NULL ORDER BY View_Count__c DESC LIMIT 5];
                if( lstFAQ.size() > 0 ) {  
                    List<List<FAQ__c>> lstRes = new List<List<FAQ__c>>();
                    lstRes.add(lstFAQ);
                    topLikedArticles = lstRes;   
                    dispLikedArtErr = false;                 
                }
                else
                {
                    dispLikedArtErr = true;
                }
                
            } catch (exception ex) {
                system.debug('Exception ==>' + ex.getMessage());
            } 
    }
    
    //Function to display the article details on Article page
    /*public void showArticle() {
             
        try{
                
                //Get article Id
                recordId = Apexpages.currentpage().getparameters().get('id');
                Id recId;
                if (String.isNotBlank(recordId)) { recId = Id.valueOf(String.escapeSingleQuotes(recordId)); }
                //Get Search string
                searchString = apexpages.currentpage().getparameters().get('q');
                                            

                //Query the article details
                if (String.isNotEmpty(Apexpages.currentpage().getparameters().get('id')) || Test.isRunningTest()) {

                    trackArticleView = checkCookie(Apexpages.currentpage().getparameters().get('id'),'vst');
                    
                    List<FAQ__c> res =[SELECT Id,Title__c,Description__c,View_Count__c FROM FAQ__c WHERE Id =: String.escapeSingleQuotes(String.valueOf(recId)) LIMIT 1];
                    if (res.size() > 0) {
                        title = res[0].Title__c;
                        description = res[0].Description__c;
                                          

                        //Update the view count of the article  
                        if (trackArticleView) {                
                            if (res[0].View_Count__c  != null) {
                                if (Schema.sObjectType.FAQ__c.fields.View_Count__c.isCreateable() || Schema.sObjectType.FAQ__c.fields.View_Count__c.isUpdateable()) {
                                    res[0].View_Count__c  = res[0].View_Count__c + 1;
                                }
                            }
                            else {
                                if (Schema.sObjectType.FAQ__c.fields.View_Count__c.isCreateable() || Schema.sObjectType.FAQ__c.fields.View_Count__c.isUpdateable()) {
                                    res[0].View_Count__c  = 1;
                                }
                            }                        
                            
                            if (FAQ__c.SObjectType.getDescribe().isUpdateable()) {
                                update res; 
                            }
                        }                                               
                    }
                }
           
            } catch(exception ex) {
                system.debug('Exception ==>' + ex.getMessage() + ' Line Number ' + ex.getLineNumber());
            }        
    }*/
    
    public Component.Apex.OutputText getHTMLContent(){
           Component.Apex.OutputText oppText = new Component.Apex.OutputText(escape = false);
           
           try{
                //Get article Id
                recordId = Apexpages.currentpage().getparameters().get('id');
                Id recId;
                if (String.isNotBlank(recordId)) { recId = Id.valueOf(String.escapeSingleQuotes(recordId)); }
                //Get Search string
                searchString = apexpages.currentpage().getparameters().get('q');
                                            

                //Query the article details
                if (String.isNotEmpty(Apexpages.currentpage().getparameters().get('id')) || Test.isRunningTest()) {

                    trackArticleView = checkCookie(Apexpages.currentpage().getparameters().get('id'),'vst');
                    
                    List<FAQ__c> res =[SELECT Id,Title__c,Description__c,View_Count__c FROM FAQ__c WHERE Id =: String.escapeSingleQuotes(String.valueOf(recId)) LIMIT 1];
                    if (res.size() > 0) {
                        title = res[0].Title__c;
                        description = res[0].Description__c;
                                          

                        //Update the view count of the article  
                        if (trackArticleView) {                
                            if (res[0].View_Count__c  != null) {
                                if (Schema.sObjectType.FAQ__c.fields.View_Count__c.isCreateable() || Schema.sObjectType.FAQ__c.fields.View_Count__c.isUpdateable()) {
                                    res[0].View_Count__c  = res[0].View_Count__c + 1;
                                }
                            }
                            else {
                                if (Schema.sObjectType.FAQ__c.fields.View_Count__c.isCreateable() || Schema.sObjectType.FAQ__c.fields.View_Count__c.isUpdateable()) {
                                    res[0].View_Count__c  = 1;
                                }
                            }                        
                            
                            if (FAQ__c.SObjectType.getDescribe().isUpdateable()) {
                                //update res; 
                            }
                        }                                               
                    }
                }
           
            } catch(exception ex) {
                system.debug('Exception ==>' + ex.getMessage() + ' Line Number ' + ex.getLineNumber());
            }   	
        
        
           oppText.value = description;
           return oppText ;
	}

    //To check/create cookie
    public static Boolean checkCookie(string recordId,string ckType) {
        Cookie c_ut = null;      
        String cookieValue = null ; 
        Boolean boolResult;
        

        try{
            Cookie cookie = ApexPages.currentPage().getCookies().get('DSP_faq');
            
            if (cookie != null) {
                //Get the cookie value
               cookieValue = cookie.getValue();
               System.debug('Cookie value ==>' + cookieValue);
               //If cookie value matches this article  Id
               String ckValue = recordId + ckType;
                if (cookieValue.indexOf(ckValue) >= 0 ) {
                    boolResult = false;
                    System.debug('Cookie found.Cookie value is ==> ' + cookieValue);
                }
                 //If cookie value does not match this article  Id, add this article ID to cookie
                else
                {
                    boolResult = true;
                    cookieValue = cookieValue  + recordId + ckType;

                    //Delete cookie
                    c_ut = new Cookie('DSP_faq', '',null,0,false); 
                    ApexPages.currentPage().setCookies(new Cookie[]{c_ut});

                    //Create the cookie with new value
                    c_ut = new Cookie('DSP_faq', cookieValue, null, 1000, true);
                    ApexPages.currentPage().setCookies(new Cookie[]{c_ut});
                    System.debug('Cookie did not have this article.New Cookie value is ==> ' + cookieValue);
                }   
            } 
            else 
            //If cookie does not exsit, create a new cookie with article ID as its value.
            {               
                boolResult = true;
                String ckValue = recordId + ckType ;
                c_ut = new Cookie('DSP_faq', ckValue, null, 1000, true);
                ApexPages.currentPage().setCookies(new Cookie[]{c_ut});
                System.debug('Cookie created for first time ==>' + ckValue);
            }
        } catch (exception ex) {
             System.debug('Exception in checkCookie =>' + ex.getMessage() + ' Line Number '+ ex.getLineNumber());
        }
        return boolResult;
    }

    
}