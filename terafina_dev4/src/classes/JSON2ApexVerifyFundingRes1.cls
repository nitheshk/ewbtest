public class JSON2ApexVerifyFundingRes1 {  

    public class Status {
        public String StatusCode {get;set;} 
        public String Severity {get;set;} 
        public String StatusDesc {get;set;} 
        public StatusDetail StatusDetail {get;set;} 

        public Status(System.JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'StatusCode') {
                            StatusCode = parser.getText();
                        } else if (text == 'Severity') {
                            Severity = parser.getText();
                        } else if (text == 'StatusDesc') {
                            StatusDesc = parser.getText();
                        } else if (text == 'StatusDetail') {
                            StatusDetail = new StatusDetail(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'Status consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public Data data {get;set;} 

    public JSON2ApexVerifyFundingRes1(System.JSONParser parser) {
        while (parser.nextToken() != System.JSONToken.END_OBJECT) {
            if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                String text = parser.getText();
                if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                    if (text == 'data') {
                        data = new Data(parser);
                    } else {
                        System.debug(LoggingLevel.WARN, 'JSON2Apex consuming unrecognized property: '+text);
                        consumeObject(parser);
                    }
                }
            }
        }
    }
    
    public class RealTimeAccountVerificationInfoStatusList {
        public List<RealTimeAccountVerificationInfoStatus> RealTimeAccountVerificationInfoStatus {get;set;} 

        public RealTimeAccountVerificationInfoStatusList(System.JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'RealTimeAccountVerificationInfoStatus') {
                            RealTimeAccountVerificationInfoStatus = arrayOfRealTimeAccountVerificationInfoStatus(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'RealTimeAccountVerificationInfoStatusList consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class Return_Z {
        public String RqUID {get;set;} 
        public Status Status {get;set;} 
        public RealTimeAccountVerificationInfoStatusList RealTimeAccountVerificationInfoStatusList {get;set;} 

        public Return_Z(System.JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'RqUID') {
                            RqUID = parser.getText();
                        } else if (text == 'Status') {
                            Status = new Status(parser);
                        } else if (text == 'RealTimeAccountVerificationInfoStatusList') {
                            RealTimeAccountVerificationInfoStatusList = new RealTimeAccountVerificationInfoStatusList(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'Return_Z consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class VerifyAccountRealTimeResponse {
        public Return_Z return_Z {get;set;} // in json: return

        public VerifyAccountRealTimeResponse(System.JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'return') {
                            return_Z = new Return_Z(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'VerifyAccountRealTimeResponse consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class StatusDetail {
        public Object RequestStatus {get;set;} 
        public Object Severity {get;set;} 
        public Object MessageData {get;set;} 

        public StatusDetail(System.JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'RequestStatus') {
                            RequestStatus = parser.readValueAs(Object.class);
                        } else if (text == 'Severity') {
                            Severity = parser.readValueAs(Object.class);
                        } else if (text == 'MessageData') {
                            MessageData = parser.readValueAs(Object.class);
                        } else {
                            System.debug(LoggingLevel.WARN, 'StatusDetail consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class Param {
        public String Name {get;set;} 
        public String Value {get;set;} 

        public Param(System.JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'Name') {
                            Name = parser.getText();
                        } else if (text == 'Value') {
                            Value = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Param consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class VerifyAccRealTime {
        public Body body {get;set;} 

        public VerifyAccRealTime(System.JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'body') {
                            body = new Body(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'VerifyAccRealTime consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class RealTimeAccountVerificationInfoStatus {
        public Status Status {get;set;} 
        public String CFIID {get;set;} 
        public String AccountNumber {get;set;} 
        public String AccountTypeId {get;set;} 
        public String FIID {get;set;} 
        public String ABA {get;set;} 
        public String CreditABA {get;set;} 
        public String VerificationMode {get;set;} 
        public String VerificationStatus {get;set;} 
        public String FailureReasonCode {get;set;} 
        public String FailureReasonDesc {get;set;} 
        public Object AccountTypeCode {get;set;} 
        public String RunId {get;set;} 
        public TwoFARealtimeAcctVerificationParam TwoFARealtimeAcctVerificationParam {get;set;} 
        public Object BatchId {get;set;} 
        public String FailureCategory {get;set;} 
        public Object MatchType {get;set;} 

        public RealTimeAccountVerificationInfoStatus(System.JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'Status') {
                            Status = new Status(parser);
                        } else if (text == 'CFIID') {
                            CFIID = parser.getText();
                        } else if (text == 'AccountNumber') {
                            AccountNumber = parser.getText();
                        } else if (text == 'AccountTypeId') {
                            AccountTypeId = parser.getText();
                        } else if (text == 'FIID') {
                            FIID = parser.getText();
                        } else if (text == 'ABA') {
                            ABA = parser.getText();
                        } else if (text == 'CreditABA') {
                            CreditABA = parser.getText();
                        } else if (text == 'VerificationMode') {
                            VerificationMode = parser.getText();
                        } else if (text == 'VerificationStatus') {
                            VerificationStatus = parser.getText();
                        } else if (text == 'FailureReasonCode') {
                            FailureReasonCode = parser.getText();
                        } else if (text == 'FailureReasonDesc') {
                            FailureReasonDesc = parser.getText();
                        } else if (text == 'AccountTypeCode') {
                            AccountTypeCode = parser.readValueAs(Object.class);
                        } else if (text == 'RunId') {
                            RunId = parser.getText();
                        } else if (text == 'TwoFARealtimeAcctVerificationParam') {
                            TwoFARealtimeAcctVerificationParam = new TwoFARealtimeAcctVerificationParam(parser);
                        } else if (text == 'BatchId') {
                            BatchId = parser.readValueAs(Object.class);
                        } else if (text == 'FailureCategory') {
                            FailureCategory = parser.getText();
                        } else if (text == 'MatchType') {
                            MatchType = parser.readValueAs(Object.class);
                        } else {
                            System.debug(LoggingLevel.WARN, 'RealTimeAccountVerificationInfoStatus consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class Data {
        public VerifyAccRealTime VerifyAccRealTime {get;set;} 

        public Data(System.JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'VerifyAccRealTime') {
                            VerifyAccRealTime = new VerifyAccRealTime(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'Data consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class Body {
        public VerifyAccountRealTimeResponse VerifyAccountRealTimeResponse {get;set;} 

        public Body(System.JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'VerifyAccountRealTimeResponse') {
                            VerifyAccountRealTimeResponse = new VerifyAccountRealTimeResponse(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'Body consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class TwoFARealtimeAcctVerificationParam {
        public List<Param> Param {get;set;} 

        public TwoFARealtimeAcctVerificationParam(System.JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'Param') {
                            Param = arrayOfParam(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'TwoFARealtimeAcctVerificationParam consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    
    public static JSON2ApexVerifyFundingRes1 parse(String json) {
        System.JSONParser parser = System.JSON.createParser(json);
        return new JSON2ApexVerifyFundingRes1(parser);
    }
    
    public static void consumeObject(System.JSONParser parser) {
        Integer depth = 0;
        do {
            System.JSONToken curr = parser.getCurrentToken();
            if (curr == System.JSONToken.START_OBJECT || 
                curr == System.JSONToken.START_ARRAY) {
                depth++;
            } else if (curr == System.JSONToken.END_OBJECT ||
                curr == System.JSONToken.END_ARRAY) {
                depth--;
            }
        } while (depth > 0 && parser.nextToken() != null);
    }
    

    private static List<RealTimeAccountVerificationInfoStatus> arrayOfRealTimeAccountVerificationInfoStatus(System.JSONParser p) {
        List<RealTimeAccountVerificationInfoStatus> res = new List<RealTimeAccountVerificationInfoStatus>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new RealTimeAccountVerificationInfoStatus(p));
        }
        return res;
    }









    private static List<Param> arrayOfParam(System.JSONParser p) {
        List<Param> res = new List<Param>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Param(p));
        }
        return res;
    }








}