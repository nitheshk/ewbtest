global with sharing class EmailValidationExtension implements TF4SF.DSP_Interface {
    
    global static Map<String, String> main(Map<String, String> tdata) {
        Map<String, String> data = new Map<String, String>();
        String Options = '',emailAddress='',phoneNo='',abandonAppId='',message='', userId = '', factorId = '', smsId = '', oktaStatus = '';
        String appId = tdata.get('id');
        String emailId = tdata.get('emailId');  
        Integer status = 0;
        String responseFactorId = '', responseStatus = '';
        Boolean sendOTP = false;
        List<OKTA__c> oktaList = new List<OKTA__c>();
        Map<String, String> enrollMap = new Map<String, String>();
        //Get recent abandoned app based on EmailId & Application Status
        try
        {
        List <TF4SF__Application__c>  listApp = [SELECT Id, TF4SF__Application_Status__c,TF4SF__Sub_Product__c ,
        TF4SF__Email_Address_J2__c, TF4SF__Email_Address_J3__c, TF4SF__Email_Address_J__c, TF4SF__Email_Address__c,
        TF4SF__Primary_Phone_Number__c, TF4SF__Primary_Phone_Number_J__c, TF4SF__Primary_Phone_Number_J3__c, 
        TF4SF__Primary_Phone_Number_J2__c, OKTA_UserId__c, OKTA_FactorId__c, OKTA_SMSId__c, Okta_Status__c, TF4SF__Country_Code__c FROM TF4SF__Application__c 
        where TF4SF__Email_Address__c =: emailId AND
        (TF4SF__Application_Status__c='Open' OR TF4SF__Application_Status__c='Abandoned' OR TF4SF__Application_Status__c='Save for Later') order by createdDate DESC limit 1];
        
       //Validate Email/Phone from abandoned app 
         if (!listApp.isempty()) {           

                abandonAppId=listApp[0].Id;
                emailAddress=emailId;
                phoneNo=listApp[0].TF4SF__Country_Code__c+ValidatePromoCodeEmailExtension.PhoneCleanup(listApp[0].TF4SF__Primary_Phone_Number__c);
                if (String.isNotBlank(emailId)) {
                    oktaList = [SELECT Id, Email_Address__c, Factor_Id__c, SMS_Id__c, Status__c, User_Id__c FROM OKTA__c WHERE Email_Address__c =: emailId.toLowerCase() LIMIT 1];
                    if (!oktaList.isempty()) {
                        userId=oktaList[0].User_Id__c;
                        factorId=oktaList[0].Factor_Id__c;
                        smsId=oktaList[0].SMS_Id__c;
                        oktaStatus = oktaList[0].Status__c;
                    }
                }              
     
                if(String.isNotBlank(userId)) {  
                    if (String.isNotBlank(factorId) && oktaStatus == 'ACTIVE') {
                        responseFactorId = challenge(abandonAppId, AuthToken.Auth(), userId, smsId);
                        responseStatus = oktaList[0].Status__c;
                        sendOTP = true;                        
                    } else if (String.isNotBlank(phoneNo)) {
                        enrollMap = enrollSMS(abandonAppId, AuthToken.Auth(), userId, phoneNo);
                        responseFactorId = enrollMap.get('factorId');
                        responseStatus = enrollMap.get('oktaStatus');
                        sendOTP = true;                       
                    }                   
                   
                     if(sendOTP) {
                    //Update current APP Email with retrieval email id & to retrive phone no based on email in OTP class.
					
                     List <TF4SF__Application__c>  currentApp =[SELECT Id, TF4SF__Application_Status__c,TF4SF__Sub_Product__c ,
                     TF4SF__Email_Address__c,TF4SF__Primary_Phone_Number__c, Retrieve_Email__c, Okta_Status__c, Okta_factorId__c
                     FROM TF4SF__Application__c 
                     where Id=: appId];

                        if(!currentApp.isempty())
                        {
                            currentApp[0].Retrieve_Email__c=emailId;
                            update currentApp;
                        }
                        status=200;  
                    } 
                }
            }  
        }
        catch(Exception ex){
            DebugLogger.InsertDebugLog(appId,ex.getMessage(),'Error in Line Number==>' + ex.getLineNumber()); 
        }  

        //Prepare JSON & Send application details to frontend  
        Options += '{';
        Options += '"status": "'+status+'",';
        Options += '"dspid": "'+abandonAppId+'",';
        Options += '"emailid": "'+emailAddress+'",';               
        Options += '"phoneno": "'+phoneNo+'",';  
        Options += '"message": "'+message+'"'; 
        Options += '}';  
        
          
        data.put('JsonResult',Options);
        system.debug('option'+Options);
        data.put('emailId', emailId);
        updateOkta(appId, emailId, userId, responseFactorId, responseStatus, smsId);
        return data;
    }

    public static void updateOkta(String appId, String emailId, String userId, String factorId, String Status, String smsId) {
        List<OKTA__c> oktaList = [SELECT Id, Email_Address__c, Factor_Id__c, SMS_Id__c, Status__c, User_Id__c FROM OKTA__c WHERE Email_Address__c =: emailId LIMIT 1];
        List<OKTA__c> updateOkta = new List<OKTA__c>();
        for (OKTA__c okta : oktaList) {
           if (String.isNotBlank(emailId)) {
                okta.Email_Address__c=emailId;
            }
            if (String.isNotBlank(userId)) {
                okta.User_Id__c = userId;
            }
            if (String.isNotBlank(factorId)) {
                okta.Factor_Id__c = factorId;
            }
            if (String.isNotBlank(Status)) {
                okta.Status__c = Status;
            }
            if (String.isNotBlank(smsId)) {
                okta.SMS_Id__c = smsId;
            }
            updateOkta.add(okta);
        }
        
        update updateOkta;
    }

    public static Map<String, String> enrollSMS(String appId, String Token, String userId, String Phone) {
        String factorId = '', oktaStatus = '';
        Map<String, String> enrollResult = new Map<String, String>();        
        String GUID = '';
        HttpRequest req = null;
        HttpResponse res = null;
        try {
            Http h = new Http();
            req = new HttpRequest();
            GUID = GUIDGenerator.getGUID();
            VELOSETTINGS__C velo = VELOSETTINGS__C.getOrgDefaults();
            Integer timeOut = Integer.ValueOf(velo.ProcessAPI_Timeout__c);
            req.setTimeout(timeOut);
            req.setHeader(velo.Request_GUID_Name__c,GUID);
            req.setEndpoint(velo.Endpoint__c+'onb/api/process/iam/factors/sms');
            req.setHeader('Authorization','Bearer '+Token);
            req.setHeader('Accept','application/json');
            req.setHeader('Content-Type', 'application/json');
            req.setMethod('POST');

            String body = '{';
            body += '"userId" : "'+userId+'",';            
            body += '"phoneNumber": "'+Phone+'"';           
            body += '}';

                
            req.setBody(body);
            res = h.send(req);            
            system.debug('Req header is '+req.getHeader('Authorization'));
            system.debug('Req is '+req.getBody());
            system.debug('Res is '+res.getbody());

            Map<String, Object> OktaMap = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
            factorId = String.valueOf(OktaMap.get('id'));
            oktaStatus = String.valueOf(OktaMap.get('status'));
            enrollResult.put('factorId', factorId);
            enrollResult.put('oktaStatus', oktaStatus);

            DebugLogger.InsertDebugLog(appId, GUID, 'EmailValidationExtenstion EnrollSMS Request GUID');
            DebugLogger.InsertDebugLog(appId, req.getBody(), 'EmailValidationExtenstion EnrollSMS Request');
            DebugLogger.InsertDebugLog(appId, res.getBody(), 'EmailValidationExtension EnrollSMS Response');
        } catch (Exception e) {
            system.debug('exception '+e.getMessage());
            DebugLogger.InsertDebugLog(appId, GUID, 'EmailValidationExtenstion EnrollSMS Request GUID');
            DebugLogger.InsertDebugLog(appId, req.getBody(), 'EmailValidationExtenstion EnrollSMS Request');
            DebugLogger.InsertDebugLog(appId, res.getBody(), 'EmailValidationExtension EnrollSMS Response');
        }
        return enrollResult;
    }


    public static String challenge(String appId, String Token, String userId, String factorId) {
        String GUID = '';
        try {
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            GUID = GUIDGenerator.getGUID();
            VELOSETTINGS__C velo = VELOSETTINGS__C.getOrgDefaults();
            Integer timeOut = Integer.ValueOf(velo.ProcessAPI_Timeout__c);
            req.setTimeout(timeOut);
            req.setHeader(velo.Request_GUID_Name__c,GUID);
            req.setEndpoint(velo.Endpoint__c+'onb/api/process/iam/challenge');
            req.setHeader('Authorization','Bearer '+Token);
            req.setHeader('Accept','application/json');
            req.setHeader('Content-Type', 'application/json');
            req.setMethod('POST');

            String body = '{';
            body += '"userId" : "'+userId+'",';
            body += '"factorId": "'+factorId+'"';
            body += '}';

                
            req.setBody(body);
            HttpResponse res = h.send(req);          
            system.debug('Req header is '+req.getHeader('Authorization'));
            system.debug('Req is '+req.getBody());
            system.debug('Res is '+res.getbody());

            DebugLogger.InsertDebugLog(appId, GUID, 'EmailValidationExtenstion Challenge Request GUID');
            DebugLogger.InsertDebugLog(appId, req.getBody(), 'EmailValidationExtenstion Challenge Request');
            DebugLogger.InsertDebugLog(appId, res.getBody(), 'EmailValidationExtension Challenge Response');
        } catch (Exception e) {
            DebugLogger.InsertDebugLog(appId, GUID, 'EmailValidationExtenstion Challenge Request GUID');
            system.debug('exception '+e.getMessage());
        }
        return factorId;
    }   
   
}