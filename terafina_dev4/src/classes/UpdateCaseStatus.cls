@RestResource(urlMapping='/CaseUpdate/v1/*')
global without sharing class UpdateCaseStatus {
    @HttpPost
    global static void UpdateStatus() {
        
        System.debug('inside UpdateStatus');
        
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        try {
            String ipaddress = req.headers.get('X-Salesforce-SIP');
            String body = req.requestBody.toString(); 
            System.debug('length: ' + body.length() + '; body1: ' + body);
            
            String CaseNumber = '';
            String CaseStatus = '';
            String appId = '';
            String DoNotBank;
            String PoliticalExposedPerson;
            //{ "CaseId": "CAS-01556-V2W3H2", "CaseStatus": "Close & Proceed" }
            try {
                if (body != null && body != '') {
                    Map<String, Object> CaseMap = (Map<String, Object>)JSON.deserializeUntyped(body);
                    CaseNumber = String.valueOf(CaseMap.get('CaseNumber'));
                    CaseStatus = String.valueOf(CaseMap.get('CaseStatus'));
                    appId = String.valueOf(CaseMap.get('ApplicationId'));
                    DoNotBank = String.valueOf(CaseMap.get('DoNotBank'));
                    PoliticalExposedPerson = String.valueOf(CaseMap.get('PoliticalExposedPerson'));
                }
            } catch(Exception e) {
                system.debug('Error at: ' + e.getMessage() + e.getStackTraceString());              
            }
        
            if (String.isNotBlank(CaseNumber) && String.isNotBlank(appId)) {
				try {
					TF4SF__Application__c app = [SELECT Id, Name, TF4SF__Application_Status__c, TF4SF__Primary_Product_Status__c, Primary_Product_Case_Id__c, Primary_Product_Case_Status__c FROM TF4SF__Application__c WHERE Primary_Product_Case_Id__c =: CaseNumber LIMIT 1];
					if (app != null) {
						if (CaseStatus == 'Close & Proceed') {
							CaseSubmitApp(app.Id, CaseStatus, DoNotBank, PoliticalExposedPerson);
						} else if (CaseStatus == 'Close & Reject') {
                            AppUpdateDeclined(app.Id, CaseStatus, DoNotBank, PoliticalExposedPerson);
                        }  
					}
					res.statusCode = 200;
					res.addHeader('Content-Type', 'application/json');
					res.responseBody = Blob.valueOf('{"Case Update": "Success", "Message": "Case Updated"}');
				} catch (exception ex) {
					res.statusCode = 200;
               		res.addHeader('Content-Type', 'application/json');
                	res.responseBody = Blob.valueOf('{"Case Update": "Falied", "Message": "Case Not Found"}');
				}
            } 
          
        } catch(Exception e) {
            //Add debugging info!! - send to debug logs
            system.debug( 'Online Error: ' + e.getMessage() + e.getStackTraceString());
            res.statusCode = 500;
            res.addHeader('Content-Type', 'application/json');
            res.responseBody = Blob.valueOf('{"Case Update": "Falied", "Message": "'+e.getMessage()+'"}');
            
        }
    }

    @future(callout=true)
    global static void CaseSubmitApp(String appId, String CaseStatus, String DoNotBank, String PoliticalExposedPerson) {
        
        String Token = AuthToken.Auth();
        
        String result = '';
        String reqBody = '';
        String applicationId = '\'' + appId + '\'';
        //System.debug('applicationId//productnumber:'+applicationId+'--'+productnumber);
        //select all fields in Application
        queryHelper.ObjectInfo appInfo = queryHelper.selectStar('TF4SF__Application__c');
        String queryString =  appInfo.query;
        queryString += 'WHERE ID = ' + applicationId ;
        TF4SF__Application__c app = Database.query(queryString);
        String productType = app.TF4SF__Product__c;

        //this.Application = app;
        queryHelper.ObjectInfo app2Info = queryHelper.selectStar('TF4SF__Application2__c');
        String app2String =  app2Info.query;
        app2String += 'WHERE TF4SF__Application__c = ' + applicationId ;
        TF4SF__Application2__c app2 = Database.query(app2String);

        queryHelper.ObjectInfo empInfo = queryHelper.selectStar('TF4SF__Employment_Information__c');
        String employmentQuery =  empInfo.query;
        employmentQuery += 'WHERE TF4SF__Application__c = ' + applicationId;
        TF4SF__Employment_Information__c employment = Database.query(employmentQuery);
        //System.debug('employment:' + employment);

        queryHelper.ObjectInfo identityInfo = queryHelper.selectStar('TF4SF__Identity_Information__c');
        String identityQuery = identityInfo.query;
        identityQuery += 'WHERE TF4SF__Application__c = ' + applicationId;
        TF4SF__Identity_Information__c identity = Database.query(identityQuery);
        System.debug('identity:' + identity);

        queryHelper.ObjectInfo aboutAccountInfo = queryHelper.selectStar('TF4SF__About_Account__c');
        String aboutAccountQuery = aboutAccountInfo.query;
        aboutAccountQuery += 'WHERE TF4SF__Application__c = ' + applicationId;
        //System.debug('abtaccountquery:'+aboutAccountQuery);
        TF4SF__About_Account__c about = Database.query(aboutAccountQuery);
        //System.debug('about account:' + about); 
        if (String.isNotBlank(PoliticalExposedPerson)) {
            if (PoliticalExposedPerson == 'false') {
                identity.Custom_Picklist12__c = 'No';    
            } else {
                identity.Custom_Picklist12__c = 'Yes';    
            }
        }

        if (String.isNotBlank(CaseStatus)) {
            app.Primary_Product_Case_Status__c = CaseStatus;
        }
        
        DepositSubmit.Application application = new DepositSubmit.Application(app, app2, employment, identity, about);
        
        List<Attachment> attList = [SELECT Id, Body, ContentType, Description, Name, ParentId, CreatedDate FROM Attachment WHERE ParentId =: app.Id];
        System.debug('attachemnst:' + attList); 
        
        DepositSubmit.SynergyDocument[] sygDoc = new DepositSubmit.SynergyDocument[]{};
        for (Attachment att : attList) {
            if (att.Name == 'Front' || att.Name == 'Back') {} else {
                //sygDoc.add(new DepositSubmit.SynergyDocument(att));
            }
        } 
        
        DepositSubmit.CustomerInformation[] CustomerInfo = new DepositSubmit.CustomerInformation[] {new DepositSubmit.CustomerInformation(application)};
        DepositSubmit.ApplicationReq applicationInfo = new DepositSubmit.ApplicationReq(application);
        
        
        reqBody = '{';
        reqBody += applicationInfo.getAppJSON('UpdateCaseStatus');
        for (DepositSubmit.CustomerInformation custInfo : CustomerInfo) {
                reqBody += custInfo.getcustomerJSON();
        }
        reqBody += '"Documents": [';
        for (DepositSubmit.SynergyDocument doc : sygDoc) {  
         reqBody += doc.getDocJSON();
        }
        reqBody = reqBody.removeEnd(',');
        reqBody += ']';
        //reqBody = reqBody.removeEnd(',');
        reqBody += '}';
        System.debug('After Concatenating result --> ' + reqBody);
        //Nag : Replace null string value with null object
        String finalReqBody = reqBody.replaceAll('"null"', 'null');
        System.debug('After Concatenating finalReqBody --> ' + finalReqBody);
        String ResponseJson = execute(appId, finalReqBody, Token, productType);
        Map<String, Object> SubmitMap = (Map<String, Object>)JSON.deserializeUntyped(ResponseJson);
        Map<String, Object> dataMap = (Map<String, Object>)SubmitMap.get('data');
        if(!test.isRunningTest() && dataMap != null){
            String CaseNumber = String.valueof(dataMap.get('CaseNumber'));
            
            String ApplicationStatus = String.valueOf(dataMap.get('ApplicationStatus'));
            String CustomerNumber = String.valueOf(dataMap.get('CustomerNumber'));
            String AccNumber = String.valueOf(dataMap.get('AccountNumber'));
            if (ApplicationStatus == 'Approved'){
                finalAppUpdate(appId, CustomerNumber, AccNumber, CaseStatus, DoNotBank, PoliticalExposedPerson);
            } else if (ApplicationStatus == 'Pending') {
                pendingCaseUpdate(appId, CaseNumber, CaseStatus, DoNotBank, PoliticalExposedPerson);
            }
        }
    }

    public static void finalAppUpdate(String appId, String CustomerNumber, String AccNumber, String CaseStatus, String DoNotBank, String PoliticalExposedPerson) {
        TF4SF__Application__c app = [SELECT Id, Name, TF4SF__Email_Address__c, TF4SF__External_App_ID__c, TF4SF__External_App_Stage__c, TF4SF__Primary_Product_Status__c, TF4SF__Application_Status__c, Primary_Product_Case_Status__c, TF4SF__Application_Page__c FROM TF4SF__Application__c WHERE Id =: appId];
        app.TF4SF__Application_Page__c = 'StartFundingPage';
        app.TF4SF__Primary_Product_Status__c = 'Approved';
        app.TF4SF__External_App_Stage__c = CustomerNumber;
        app.TF4SF__External_App_ID__c = AccNumber;
        app.Primary_Product_Case_Status__c = CaseStatus;
        //app.TF4SF__Application_Status__c = '';
        update app;
        List<TF4SF__Application__c> appList = new List<TF4SF__Application__c>();
        appList.add(app);
        //Approved email sent via trigger.
        ApplicationTriggerHandler appHandler = new ApplicationTriggerHandler();
        appHandler.sendConfirmationEmailForDeposit(appList);
        updateIden(appId, PoliticalExposedPerson);
    }

    public static void pendingCaseUpdate(String appId, String CaseNumber, String CaseStatus, String DoNotBank, String PoliticalExposedPerson) {
        TF4SF__Application__c app = [SELECT Id, Name, TF4SF__Email_Address__c, TF4SF__External_App_ID__c, TF4SF__External_App_Stage__c, TF4SF__Primary_Product_Status__c, Primary_Product_Case_Id__c, Primary_Product_Case_Status__c, TF4SF__Application_Status__c, TF4SF__Application_Page__c FROM TF4SF__Application__c WHERE Id =: appId];
        //app.TF4SF__Application_Page__c = 'StartFundingPage';
        app.TF4SF__Primary_Product_Status__c = 'Pending Review';
        //app.TF4SF__External_App_Stage__c = CustomerNumber;
        //app.TF4SF__External_App_ID__c = AccNumber;
        app.Primary_Product_Case_Id__c = CaseNumber;
        app.Primary_Product_Case_Status__c = CaseStatus;
        //app.TF4SF__Application_Status__c = '';
        update app;
        updateIden(appId, PoliticalExposedPerson);
    }

    public static void AppUpdateDeclined(String appId, String CaseStatus, String DoNotBank, String PoliticalExposedPerson) {
        TF4SF__Application__c app = [SELECT Id, Name, TF4SF__Email_Address__c, TF4SF__External_App_ID__c, TF4SF__External_App_Stage__c, TF4SF__Primary_Product_Status__c, TF4SF__Application_Status__c, Primary_Product_Case_Status__c, TF4SF__Application_Page__c FROM TF4SF__Application__c WHERE Id =: appId];
        app.TF4SF__Application_Page__c = 'ConfirmationPage';
        app.TF4SF__Primary_Product_Status__c = 'Declined';
        app.Primary_Product_Case_Status__c = CaseStatus;
        update app;
        List<TF4SF__Application__c> appList = new List<TF4SF__Application__c>();
        appList.add(app);
        ApplicationTriggerHandler appHandler = new ApplicationTriggerHandler();
        appHandler.sendDeclinedEmailForDeposit(appList);
        updateIden(appId, PoliticalExposedPerson);
    }

    public static void updateIden(String appId, String PoliticalExposedPerson) {
        TF4SF__Identity_Information__c iden = [SELECT Id, TF4SF__Application__c, Custom_Picklist12__c FROM TF4SF__Identity_Information__c WHERE TF4SF__Application__c =: appId];
        String PEP = '';
        if (String.isNotBlank(PoliticalExposedPerson)) {
            if (PoliticalExposedPerson == 'false') {
                PEP = 'No';   
            } else {
                PEP = 'Yes';    
            }
        }
        if (PEP != iden.Custom_Picklist12__c) {
            iden.Custom_Picklist12__c = PEP;    
            update iden;
        }
    }
    @TestVisible 
    private static String execute(String appId, String json, String Token, String productType) {
        System.debug('appId, JSON: ' + appId  + '---' + json);
        String responseJson = '';
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        
        VELOSETTINGS__C velo = VELOSETTINGS__C.getOrgDefaults();
        Integer timeOut = Integer.ValueOf(velo.ProcessAPI_Timeout__c);
        String GUID = GUIDGenerator.getGUID();
        req.setTimeout(timeOut);
        req.setHeader(velo.Request_GUID_Name__c,GUID);
        //MVP 2
        req.setEndpoint(velo.Endpoint__c+'api/process/caseapproved/submission');
        //MVP 1 & 1.5
        // if(productType=='Checking') {
        //   req.setEndpoint(velo.Endpoint__c+'api/process/caseapproved/checking');
        // } else{
        //   req.setEndpoint(velo.Endpoint__c+'api/process/caseapproved/saving');
        // }
       
        req.setHeader('Authorization','Bearer '+Token);
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Accept','application/json');
        req.setMethod('POST');
        
        req.setBody(json);
        HttpResponse res = h.send(req);
        responseJson = res.getBody();
        system.debug('Res is '+res.getbody());
        DebugLogger.InstantDebugLog(appId, GUID, 'Case SubmitApplication Request GUID');
        DebugLogger.InstantDebugLog(appId, req.getBody(), 'Case SubmitApplication Request');
        DebugLogger.InstantDebugLog(appId, res.getBody(), 'Case SubmitApplication Response');  
        return responseJSON;
    }
}