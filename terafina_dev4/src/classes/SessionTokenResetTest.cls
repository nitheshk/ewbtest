@isTest
private class SessionTokenResetTest {
  
  @isTest static void test_method_one() {
    // Implement test code

    String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
       Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
       User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
    	EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',LocaleSidKey = 'en_US', ProfileId = p.Id,
    	TimeZoneSidKey = 'America/Los_Angeles',UserName = uniqueUserName,TF4SF__Location__c='US',TF4SF__Channel__c='online');
   
      System.runAs(u) {
     
      TF4SF__Application__c app=new TF4SF__Application__c();
      app.TF4SF__Application_Status__c='open';
      app.Ownerid=u.id;
      app.TF4SF__Application_Page__c='AddressInfoPage';
      app.TF4SF__Current_Channel__c='online';
      app.TF4SF__User_Token__c='AnxzH/+Kmj85t2Wzkj5qETFWtwltJXsC5dcjGEgtGa89TTX4OIYGRky9UDclje65';
      app.TF4SF__User_Token_Expires__c=System.now();    
      insert app;
      Map<String, String> appData = new Map<String, String>();
      appData.put('appId', app.id);
          
      Test.startTest();
      SessionTokenReset.main(appData);
      Test.stopTest();
    }
  }  
}