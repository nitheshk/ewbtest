@isTest
private class AuthTokenTest {
	
	@isTest static void test_method_one() {
		
		VELOSETTINGS__C vo = new VELOSETTINGS__C();
                vo.Name='test';
                vo.Request_GUID_Name__c = 'RequestUUID';
                vo.ProcessAPI_Timeout__c = 120000;
                vo.ClientId__c = '8er2cx5d37h96GtJk9b9';
                vo.ClientSecret__c = '8er2cx5d37h96GtJk9b9';
                vo.EndPoint__c = 'https://devopenapi.velobank.com/';
                vo.FundingEndpoint__c ='https://devopenapi.velobank.com/';
                vo.TokenEndPoint__c = 'https://openapiservices.eastwestbank.com/AuthorizationServer/api/token/client/jwt';
                insert vo;

                test.StartTest();
                Test.setMock(HttpCalloutMock.class, new AuthTokenMock()); 
                
                AuthToken.Auth();
                test.StopTest();
        

	}
	
}