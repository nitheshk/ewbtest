@isTest
public class EmailOTPExtentionTest{
     
    static testmethod void validatemyTest(){
     Map<String, String> data = new Map<String, String>();
     Map<String, String> tdata = new Map<String, String>();
      
      TF4SF__Application__c ap = new TF4SF__Application__c();
        ap.TF4SF__Product__c = 'Home Loan';
        ap.TF4SF__First_Name__c='test';
        ap.TF4SF__Last_Name__c = 'Test';
        ap.TF4SF__Application_Status__c='Open';
        ap.TF4SF__Email_Address__c= 'test@test.com';
        ap.TF4SF__Sub_Product__c = 'Home Loan - Short App';
        insert ap;
    
      tdata.put('AppId',(String)ap.id);
      tdata.put('emailId',ap.TF4SF__Email_Address__c);
  
    
        test.startTest();
        EmailOTPExtention ob = new EmailOTPExtention ();    
        EmailOTPExtention.sendOTP('emailId');
        EmailOTPExtention.main(tdata);
        test.stopTest();
    }

}