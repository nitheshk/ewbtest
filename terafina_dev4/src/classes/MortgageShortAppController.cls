//Controller for Mortgage Short app 
global with sharing class MortgageShortAppController {
   

    public MortgageShortAppController () {
       
    }

    public static Boolean zipValid{get;set;}    
    @remoteaction
    public static String saveData(String jsonData){
        JSONDeserialize obj = new JSONDeserialize();
        String strReturn ='';
        
        try{
            
            String id = '';
            string ut = '';

            System.debug('jsonData ==>' + jsonData);
            
            obj.wrapper = (ApplicationWrapper) JSON.deserializeStrict(jsonData, ApplicationWrapper.class);

            System.debug('obj.wrapper ==>' + obj.wrapper);
            id = obj.wrapper.id;
            ut = obj.wrapper.ut;

            if(String.isNotBlank(id) && String.isNotBlank(ut) && obj != null){
                //Check if App ID and User token are valid

                if (CryptoHelperCustom.userTokenIsValid(id, ut) || test.isRunningTest()){


                    //Get Application
                    TF4SF__Application__c app = [SELECT Id,TF4SF__User_Token__c, TF4SF__User_Token_Expires__c ,
                                                TF4SF__First_Name__c, TF4SF__Last_Name__c, TF4SF__Primary_Phone_Number__c,
                                                TF4SF__Email_Address__c, OwnerId, TF4SF__SiteUrl__c,
                                                TF4SF__Product__c,TF4SF__Sub_Product__c
                                                FROM TF4SF__Application__c where Id =: id];

                    if (app != null) {
                        //Update the objects
                        TF4SF__About_Account__c acc= [SELECT Id,TF4SF__Application__c, TF4SF__Property_Type__c, TF4SF__Occupancy__c, TF4SF__Purchase_P__c,
                                                     TF4SF__Total_Loan_Amount__c, TF4SF__Down_Payment__c
                                                     FROM TF4SF__About_Account__c 
                                                     WHERE TF4SF__Application__c =: id];
                        
                        if (acc != null){


                            //About account object update
                            acc.TF4SF__Property_Type__c = obj.wrapper.propertyType;
                            acc.TF4SF__Occupancy__c = obj.wrapper.occupancy;    

                            if (String.isNotBlank(obj.wrapper.purchasePrice)){
                                acc.TF4SF__Purchase_P__c = Decimal.valueOf(obj.wrapper.purchasePrice);
                            }

                            if (String.isNotBlank(obj.wrapper.loanAmount)){
                                acc.TF4SF__Total_Loan_Amount__c = Decimal.valueOf(obj.wrapper.loanAmount);
                            }

                            if (String.isNotBlank(obj.wrapper.downPayment)){
                                acc.TF4SF__Down_Payment__c = Decimal.valueOf(obj.wrapper.downPayment);
                            }

                            update acc;
                            

                            //Application object update
                            app.TF4SF__First_Name__c =  obj.wrapper.firstName;
                            app.TF4SF__Last_Name__c = obj.wrapper.lastName;
                            app.TF4SF__Email_Address__c = obj.wrapper.email;
                            app.TF4SF__Primary_Phone_Number__c = obj.wrapper.phoneNumber;

                            //Check if current page is the last page
                            //If yes, update the status to Submitted 
                            if ( obj.wrapper.pageType.equalsIgnoreCase('budget')){

                                

                                //Update Application Status to
                                app.TF4SF__Application_Status__c = 'Submitted'; 
                                strReturn = 'Application Submitted';    

                                //Expire token
                                CryptoHelperCustom.expireToken(app);    

                                //Assign Mortgage short app qeue as the owner of this application
                                QueueSObject que = [SELECT Id,QueueId FROM QueueSObject WHERE Queue.Name = 'Mortgage Preapplication queue' LIMIT 1];
                                if (que != null){
                                    app.OwnerId = que.QueueId;
                                }   
                                                                

                            } else {
                                
                                List<TF4SF__Application__c> tokenList = new List<TF4SF__Application__c>();
                                tokenList.add(app);
                                CryptoHelperCustom.refreshTokens(tokenList);
                                strReturn = 'Valid';
                            } 
                            
                            //Update appplication
                            update app; 
                        }

                    }
                                    
                }
                else
                {
                    strReturn = 'Session Expired';
                }
            }
            else
            {
                strReturn = 'Session Expired';
            }
            



        } catch (exception ex) {
            System.debug('Exception in saveData ' + ex.getMessage());
            System.debug('Exception in saveData Line Number' + ex.getLineNumber());
            strReturn = 'Session Expired';
        }

        System.debug('obj.wrapper ==>' +  obj.wrapper);
        
        return strReturn;
    }


    @remoteaction
    public static String getData(String jsonData){

        JSONDeserialize obj = new JSONDeserialize();
        String strResult ='session expired';
        try{

            String id = '';
            string ut = '';

            System.debug('jsonData ==>' + jsonData);
            
            obj.wrapper = (ApplicationWrapper) JSON.deserializeStrict(jsonData, ApplicationWrapper.class);

            id = obj.wrapper.id;
            ut = obj.wrapper.ut;

            if(String.isNotBlank(id) && String.isNotBlank(ut) && obj != null){
                //Check if App ID and User token are valid

                if ( Test.isRunningTest() || CryptoHelperCustom.userTokenIsValid(id, ut) ){


                    //Get Application
                    TF4SF__Application__c app = [SELECT Id,TF4SF__User_Token__c, TF4SF__User_Token_Expires__c ,
                                                TF4SF__First_Name__c, TF4SF__Last_Name__c, TF4SF__Primary_Phone_Number__c,
                                                TF4SF__Email_Address__c
                                                FROM TF4SF__Application__c where Id =: id];

                    if(app != null) {

                        //Get About Account details
                        TF4SF__About_Account__c acc= [SELECT Id, TF4SF__Property_Type__c, TF4SF__Occupancy__c, TF4SF__Purchase_P__c,
                                                     TF4SF__Total_Loan_Amount__c, TF4SF__Down_Payment__c
                                                     FROM TF4SF__About_Account__c 
                                                     WHERE TF4SF__Application__c =: id];
                        
                        if (acc != null){
                            //Build json string
                            strResult = '{';
                            strResult += '\"pageType\":\"\",' ;
                            strResult += '\"propertyType\":\"'+acc.TF4SF__Property_Type__c+'\",' ;
                            strResult += '\"occupancy\":\"'+acc.TF4SF__Occupancy__c+'\",' ;
                            strResult += '\"purchasePrice\":\"'+acc.TF4SF__Purchase_P__c+'\",' ;
                            strResult += '\"loanAmount\":\"'+acc.TF4SF__Total_Loan_Amount__c+'\",' ;
                            strResult += '\"downPayment\":\"'+acc.TF4SF__Down_Payment__c+'\",' ;

                            strResult += '\"firstName\":\"'+app.TF4SF__First_Name__c+'\",' ;
                            strResult += '\"lastName\":\"'+app.TF4SF__Last_Name__c+'\",' ;
                            strResult += '\"email\":\"'+app.TF4SF__Email_Address__c+'\",' ;
                            strResult += '\"phoneNumber\":\"'+app.TF4SF__Primary_Phone_Number__c+'\"' ;

                            strResult =  strResult.replace('null', '');
                            strResult +='}';                            

                        }                           
                    }
                    
                    List<TF4SF__Application__c> tokenList = new List<TF4SF__Application__c>();
                    tokenList.add(app);
                    CryptoHelperCustom.refreshTokens(tokenList);
                    update app;
                }
                else
                {
                    strResult = 'Session Expired';
                }
            }


        } catch (exception ex) {
            System.debug('Exception in saveData ' + ex.getMessage());
            System.debug('Exception in saveData Line Number' + ex.getLineNumber());
            strResult = 'Session Expired';
        }
        return strResult ;
    }
    
     @RemoteAction
    global static ZipWrapper isValidZipcode(String zipcodeVal) {
       
        ZipWrapper  wrapper  ;
        System.debug('--Inside validator--');
        zipValid =true;


        List<Zip_Code__c> listZipcode;
        if (!string.isBlank(zipcodeVal)) {
            if(zipcodeVal.length()== 3) {
              zipcodeVal = '00'+zipcodeVal;
            }
            if(zipcodeVal.length()== 4) {
              zipcodeVal = '0'+zipcodeVal;
            }

            listZipcode = [SELECT Id,Code__c,County__c,Place__c,State__c 
                           FROM Zip_Code__c 
                           WHERE Code__c =: zipcodeVal LIMIT 1];
        }



        if(listZipcode.size() > 0){
            zipValid = false;
            wrapper = new ZipWrapper(listZipcode[0].Code__c,listZipcode[0].State__c,'Success');            
        }else {
            wrapper = new ZipWrapper(null,null,'Invalid');

        }
        return wrapper;
    }
    
     global class ZipWrapper{

        String zip;
        String state;
        String strResult;
        
        public ZipWrapper(String zip,String state,String strResult)
        {
            This.zip = zip;
            This.state = state;
            This.strResult = strResult;            
        }

    }


     @RemoteAction
    global static boolean expireSession(String jsonData) {

        JSONDeserialize obj = new JSONDeserialize();
        String id = '';
        
        obj.wrapper = (ApplicationWrapper) JSON.deserializeStrict(jsonData, ApplicationWrapper.class);

        id = obj.wrapper.id;

        System.debug('expireSession - id = ' + id);
        TF4SF__Application__c app = [SELECT Id, TF4SF__User_Token_Expires__c, TF4SF__Application_Status__c FROM TF4SF__Application__c WHERE Id = :id];
        app.TF4SF__Application_Status__c = 'Session Expired';
        CryptoHelperCustom.expireToken(app);
        if (TF4SF__Application__c.SObjectType.getDescribe().isUpdateable()) { update app; }
        return true;
    }

    @RemoteAction
    global static String extendExpiration(String jsonData) {

        JSONDeserialize obj = new JSONDeserialize();
        String strResult ='session expired';
        
        String id = '';
        
        obj.wrapper = (ApplicationWrapper) JSON.deserializeStrict(jsonData, ApplicationWrapper.class);

        id = obj.wrapper.id;
        String token; //lookup the user's token and reset it
         TF4SF__Application__c app = [SELECT Id,  TF4SF__User_Token__c FROM  TF4SF__Application__c WHERE Id = :id LIMIT 1];
        if (app.TF4SF__User_Token__c != null) { token = app.TF4SF__User_Token__c; }
        List<TF4SF__Application__c> tokenList = new List<TF4SF__Application__c>();
        tokenList.add(app);
        CryptoHelperCustom.refreshTokens(tokenList);
        return 'refreshToken' + token;
    }

    public  class ApplicationWrapper {

        public String id;
        public String ut;
        public String pageType;
        public String propertyType; 
        public String occupancy;    
        public String purchasePrice;
        public String loanAmount;
        public String downPayment;
        public String firstName;
        public String lastName;
        public String email;
        public String phoneNumber;


    }

    public class JSONDeserialize {
        public ApplicationWrapper wrapper {
            get;
            set;
        }
    }

}