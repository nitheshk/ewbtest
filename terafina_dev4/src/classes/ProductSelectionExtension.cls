global with sharing class ProductSelectionExtension implements TF4SF.DSP_Interface {
    
    global static Map<String, String> main(Map<String, String> tdata) {
        Map<String, String> data = new Map<String, String>();
        String Options = '';
        String appId = tdata.get('id');
        List<TF4SF__Product_Codes__c> pcList = TF4SF__Product_Codes__c.getall().values();
        List<TF4SF__Product_Codes__c> newpcList = new List<TF4SF__Product_Codes__c>();
        for (TF4SF__Product_Codes__c pc : pcList) {
            if (pc.TF4SF__Product__c == 'Checking' || pc.TF4SF__Product__c == 'Savings') {
                newpcList.add(pc);
            }
        }
        Options += '[';
        if (!newpcList.isEmpty()) {
            for (TF4SF__Product_Codes__c pc : newpcList) {
                String MainDesc = pc.TF4SF__Description_TextArea1__c;
                String PopDesc = pc.Landing_Page_Description__c;
                if (String.isNotBlank(pc.TF4SF__Description_TextArea2__c)) {
                    MainDesc += pc.TF4SF__Description_TextArea2__c;
                }
                if (String.isNotBlank(pc.TF4SF__Description_TextArea3__c)) {
                    MainDesc += pc.TF4SF__Description_TextArea3__c;
                }
                if (String.isNotBlank(pc.TF4SF__Description_TextArea4__c)) {
                    MainDesc += pc.TF4SF__Description_TextArea4__c;
                }
                if (String.isNotBlank(pc.TF4SF__Description_TextArea5__c)) {
                    MainDesc += pc.TF4SF__Description_TextArea5__c;
                }
                if (String.isNotBlank(pc.Description_TextArea6__c)) {
                    MainDesc += pc.Description_TextArea6__c;
                }

                
                if (String.isNotBlank(pc.Landing_Page_Description2__c)) {
                    PopDesc += pc.Landing_Page_Description2__c;
                }
                Options += '{';
                Options += '"id": "'+pc.Id+'",';
                if (String.isNotBlank(pc.TF4SF__Description__c)) {
                    Options += '"titleDisplay": "'+pc.TF4SF__Description__c+'",';    
                } else {
                    Options += '"titleDisplay": "'+pc.TF4SF__Sub_Product__c+'",';
                }
                Options += '"title": "'+pc.TF4SF__Sub_Product__c+'",';
                Options += '"Description": {';
                if (String.isNotBlank(MainDesc)) {
                    Options +=  '"prodDesc": "'+MainDesc+'"';
                }
                Options +=  '},';
                Options +=  '"Details": "'+PopDesc+'"';
                Options += ',';    
                Options += '"AdditionalServices": [';
                Options += '{';
                Options += '"addTitle": "Debit Card",';
                Options += '"addDetails": "Lorem ipsum dolor sit amet, dolor debitis an ius, vis id honestatis inciderint. Pri ea quem graecis comprehensam. An purto dolor dissentiet quo, et eam esse dicat ponderum. No oporteat accusata expetenda vix, tibique suavitate conclusionemque vix id, duo viris nullam epicurei an."';
                Options += '},';
                Options = Options.removeEnd(',');
                Options += ']},';
            }
        }   
        Options = Options.removeEnd(',');
        Options += ']';

        data.put('productlist',Options);
        return data;
    }

}