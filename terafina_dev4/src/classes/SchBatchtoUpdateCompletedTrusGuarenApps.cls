global class SchBatchtoUpdateCompletedTrusGuarenApps implements Schedulable{

   
    public static String sched = '0 0 0,2,8,12,16,20 ? * *';

   global static String scheduleMe() {
       SchBatchtoUpdateCompletedTrusGuarenApps SC = new SchBatchtoUpdateCompletedTrusGuarenApps(); 
       return System.schedule('PayverisProcessACHApplications', sched, SC);
   }

   global void execute(SchedulableContext sc) {
       BatchtoUpdateCompletedTrusGuarenApps b1 = new BatchtoUpdateCompletedTrusGuarenApps();
       ID batchprocessid = Database.executeBatch(b1,200);           
   }
}