/*
 * Block comments with details of changes
 */
global with sharing class DriverLicenseScan implements TF4SF.DSP_Interface {
    global Map<String, String> main(Map<String, String> tdata) {
        Map<String, String> data = new Map<String, String>();
        data = tdata.clone();
        String backImage = '';
        String appId = data.get('id');
        String idType=data.get('idType');       
        Blob imageBack;
        Blob imageFront;

        try {
            Map<String, String> resultData = new Map<String, String>();
            resultData = IdScan(data);

            data.remove('attachmentId');
            data.remove('extensionClass');

            data.put('Application__c.First_Name__c', resultData.get('GivenName'));
            data.put('Application__c.Last_Name__c', resultData.get('Surname'));
            data.put('Application__c.Street_Address_1__c', resultData.get('AddressLine1'));
            data.put('Application__c.City__c', resultData.get('City'));
            data.put('Application__c.State__c', resultData.get('State'));
            data.put('Application__c.Zip_Code__c', resultData.get('PostalCode'));
            data.put('Identity_Information__c.Date_of_Birth__c', resultData.get('DateOfBirth'));
            data.put('Identity_Information__c.Issue_Date__c', resultData.get('IssueDate'));
            data.put('Identity_Information__c.Expiry_Date__c', resultData.get('ExpirationDate'));
            data.put('Identity_Information__c.Identity_Number_Primary__c', resultData.get('DocumentNumber'));
            data.put('Identity_Information__c.Country_Issued__c', resultData.get('IssueCountryCode'));
            data.put('Identity_Information__c.State_Issued__c', resultData.get('IssueStateCode'));
            data.put('RecordStatus' , resultData.get('RecordStatus'));

            //data.put('Identity_Information__c.Id_Selected__c',idType);
            if(test.isRunningTest()){Integer a = 10/0;}
            return data;
        } catch (Exception ex) {
            List<TF4SF__Field_Logic_New__c> lsitFieldLogic = [SELECT Id, TF4SF__Value__c FROM TF4SF__Field_Logic_New__c WHERE TF4SF__RecType__c = 'Page Labels' AND TF4SF__Page_Type__c = 'GetStartedPage' AND TF4SF__Label_Name__c = 'DL_Scan_Error_Message'];
            data.clear();
            // On exception clearing first name , last name and removing the attachment from the application.
            List<TF4SF__Application__c> appList = [SELECT Id, TF4SF__Last_Name__c, TF4SF__First_Name__c, TF4SF__Street_Address_1__c, TF4SF__Street_Address_2__c, TF4SF__City__c, TF4SF__State__c, TF4SF__Zip_Code__c, TF4SF__Suffix__c, TF4SF__Middle_Name__c FROM TF4SF__Application__c WHERE Id = :appId];

            if (appList.size() > 0) {
                appList[0].TF4SF__First_Name__c = '';
                appList[0].TF4SF__Last_Name__c = '';
                appList[0].TF4SF__Street_Address_1__c = '';
                appList[0].TF4SF__Street_Address_2__c = '';
                appList[0].TF4SF__City__c = '';
                appList[0].TF4SF__State__c = '';
                appList[0].TF4SF__Zip_Code__c = '';
                appList[0].TF4SF__Suffix__c = '';
                appList[0].TF4SF__Middle_Name__c = '';
                if (TF4SF__Application__c.SObjectType.getDescribe().isUpdateable()) { update appList; }

                List<TF4SF__Identity_Information__c> IdList = [SELECT Id, TF4SF__Country_Issued__c, TF4SF__State_Issued__c, TF4SF__Identity_Number_Primary__c, TF4SF__Issue_Date__c, TF4SF__Expiry_Date__c, TF4SF__ID_Type__c, TF4SF__Application__c FROM TF4SF__Identity_Information__c WHERE TF4SF__Application__c = :appId];
                System.debug('appide' + appid);

                if (IdList.size() > 0) {
                    IdList[0].TF4SF__Issue_Date__c='';
                    IdList[0].TF4SF__Expiry_Date__c='';
                    IdList[0].TF4SF__ID_Type__c='';
                    IdList[0].TF4SF__Identity_Number_Primary__c='';
                    IdList[0].TF4SF__Country_Issued__c='';
                    IdList[0].TF4SF__State_Issued__c='';
                    if (TF4SF__Identity_Information__c.SObjectType.getDescribe().isUpdateable()) { update IdList; }
                }

                List<Attachment> attachments = [SELECT name FROM Attachment WHERE ParentID = :appList[0].Id];
                if (attachments.size() > 0) {
                    if (Attachment.sObjectType.getDescribe().isDeletable()) { delete attachments; }
                }
            }

            if (lsitFieldLogic != null && (!lsitFieldLogic.isEmpty()) ) {
                data.put('Exception', lsitFieldLogic[0].TF4SF__Value__c );
            } else {
                data.put('Exception', ex.getMessage());
            }

            System.debug('Exception : ' + ex.getMessage());
            System.debug('Exception inline : ' + ex.getLineNumber());
            return data;
        }
    }

    global static Map<String, String> IdScan (Map<String, String> tdata) {
        Map<String, String> data = new Map<String, String>();
        String appId = tdata.get('id');
        String responseJson = '';
        String requestJson = '';
        String GUID = GUIDGenerator.getGUID();
        String PP_SCANNED_LABEL = Label.PP_Scanned_IMG_Label;
        String DL_SCANNED_FRONT_LABEL = Label.DL_Scanned_IMG_Front_Label;
        String DL_SCANNED_BACK_LABEL = Label.DL_Scanned_IMG_Back_Label;

        try {

            
            String idType = tdata.get('idType');
            if (idType == 'Driving License' || idType == 'Drivers License') {
                idType = 'DriversLicense';
            } else {
                idType = idType;
            }
            String backImage = null;
            String frontImage = null;
            String Token = '';
            TF4SF__Application__c app = [SELECT Id, Name, TF4SF__User_Token__c FROM TF4SF__Application__c WHERE Id =: appId];
            List<Attachment> attList = [SELECT Id, Name, Body, ContentType, ParentID FROM Attachment WHERE ParentID =: appId ORDER BY CreatedDate DESC LIMIT 2];
            system.debug('the atts are '+attList);
            for (Attachment att : attList) {

                system.debug('the att id : '+att.Id);

                if (idType=='Drivers License' && String.isNotBlank(frontImage) && String.isNotBlank(backImage)) {
                    break;
                } 
                else if(idType=='Passport' && String.isNotBlank(backImage)) {
                     break;
                }
                if (!String.isBlank(att.Name)) {
                    String nameContent = att.Name;
                    if (String.isNotBlank(IdType)) {
                        if (nameContent.containsIgnoreCase(DL_SCANNED_FRONT_LABEL)) {
                            system.debug('the frontImage is '+nameContent);
                            frontImage = EncodingUtil.base64Encode(att.body);
                        } else if (nameContent.containsIgnoreCase(DL_SCANNED_BACK_LABEL)) {
                            system.debug('the backImage is '+nameContent);
                            backImage = EncodingUtil.base64Encode(att.body);
                        }
                        else if(nameContent.containsIgnoreCase(PP_SCANNED_LABEL))
                        {
                            frontImage = EncodingUtil.base64Encode(att.body);
                            backImage = EncodingUtil.base64Encode(att.body);
                        }
                    }
                }
                
            }

            //system.debug('the frontImage are '+frontImage);
            //system.debug('the backImage are '+backImage);
            VELOSETTINGS__C velo = VELOSETTINGS__C.getOrgDefaults();
            Token = AuthToken.Auth();    
            Integer timeOut = Integer.ValueOf(velo.ProcessAPI_Timeout__c);
        
                
            Http h2 = new Http();
            HttpRequest req2 = new HttpRequest();
            req2.setTimeout(timeOut);
            req2.setHeader(velo.Request_GUID_Name__c,GUID);
            req2.setEndpoint(velo.EndPoint__c+'api/process/idverification/trueid');
            req2.setHeader('Authorization','Bearer '+Token);
            req2.setHeader('Accept','application/json');
            req2.setHeader('Content-Type','application/json');
            req2.setMethod('POST');
            String idBody = '';

            idBody += '{';
                idBody += '"Settings": {';
                    idBody += '"AccountNumber": "45055",';
                    idBody += '"Workflow": "TrueID_Online_Flow",';
                    idBody += '"Mode": "testing",';
                    idBody += '"Reference": "payload.RequestUUID",';
                    idBody += '"Locale": "en_US",';
                    idBody += '"Venue": "online"';
                idBody += '},';
                idBody += '"Document": {';
                    idBody += '"DocumentType": "'+idType+'",';

                //Passing front & back same image id IdType is Passport

                if(idType=='Passport') {
                    idBody += '"Front": "'+backImage+'",';
                } else {
                    idBody += '"Front": "'+frontImage+'",';
                }              
                   
                idBody += '"Back": "'+backImage+'"';
                idBody += '}';
            idBody += '}';

            req2.setBody(idBody);
            HttpResponse res2 = h2.send(req2);
            system.debug('Req is '+req2.getbody()); 
            system.debug('Res is '+res2); 
            responseJson = res2.getBody();
            requestJson = req2.getbody();
           
            system.debug('Res is '+responseJson); 
            Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(responseJson);
            system.debug('m '+m); 
            
            Map<String, Object> q = (Map<String, Object>)m.get('data');
            system.debug('q '+q); 
            List<Object> qList = (List<Object>)q.get('PassThroughs');
            system.debug('qList '+qList); 
            String k = '';
            String DateOfBirth = '';
            String GivenName = '';
            String Surname = '';
            String NationalityName = '';
            String ExpirationDate = '';
            String IssueDate = '';
            String DocumentNumber = '';
            String DocumentClassName = '';
            String AddressLine1 = '';
            String City = '';
            String State = '';
            String PostalCode = '';
            String IssueStateCode = '';
            String IssueCountryCode = '';
            
            
            for (Object e : qList) {
                Map<String, Object> w = (Map<String, Object>)e;
                //system.debug('the response w data is '+w);
                k = String.valueOf(w.get('Data'));
                break;
            }
            //system.debug('the response data is '+k);
            Map<String, Object> l = (Map<String, Object>)JSON.deserializeUntyped(k);
            Map<String, Object> p = (Map<String, Object>)l.get('IDAuthResultResponse');
            Map<String, Object> y = (Map<String, Object>)p.get('AuthenticationResult');
            Map<String, Object> r = (Map<String, Object>)y.get('Fields');
            //system.debug('the data is '+r);
            List<Object> rList = (List<Object>)r.get('IDAuthFieldData');
            for (Object s : rList) {
                Map<String, Object> t = (Map<String, Object>)s;
                //system.debug('the values is '+t);
                if (String.isBlank(DateOfBirth)) { DateOfBirth = String.valueOf(t.get('DOB')); }
                if (String.isBlank(GivenName)) { GivenName = String.valueOf(t.get('GivenName')); }
                if (String.isBlank(Surname)) { Surname = String.valueOf(t.get('Surname')); }
                if (String.isBlank(NationalityName)) { NationalityName = String.valueOf(t.get('NationalityName')); }
                if (String.isBlank(ExpirationDate)) { ExpirationDate = String.valueOf(t.get('ExpirationDate')); }
                if (String.isBlank(IssueDate)) { IssueDate = String.valueOf(t.get('IssueDate')); }
                
                if (idType == 'Passport') {
                    if (String.isBlank(IssueCountryCode)) { IssueStateCode = String.valueOf(t.get('IssuingStateName')); }
                } else {
                    if (String.isBlank(IssueStateCode)) { IssueStateCode = String.valueOf(t.get('IssuingStateName')); }    
                }

                if (String.isBlank(DocumentNumber)) { DocumentNumber = String.valueOf(t.get('DocumentNumber')); }
                if (String.isBlank(DocumentClassName)) { DocumentClassName = String.valueOf(t.get('DocumentClassName')); }
                if (String.isBlank(AddressLine1)) { AddressLine1 = String.valueOf(t.get('AddressLine1')); }
                if (String.isBlank(City)) { City = String.valueOf(t.get('City')); }
                if (String.isBlank(State)) { State = String.valueOf(t.get('State')); }
                if (String.isBlank(PostalCode)) { PostalCode = String.valueOf(t.get('PostalCode')); }
                System.debug('Inside IDAuthFieldData Loop @@@@@:');
                data.put('RecordStatus' , 'match'); // UI Expecting both SSN NRA flow to send common flag
            }
            if (String.isNotBlank(DateOfBirth)) { 
                DateOfBirth = LNformatDateString(DateOfBirth);
            }
            if (String.isNotBlank(ExpirationDate)) { 
                ExpirationDate = LNformatDateString(ExpirationDate);
            }
            if (String.isNotBlank(IssueDate) && TryParseDate(IssueDate)) { 
                IssueDate = LNformatDateString(IssueDate);
            }
            else
            {
                IssueDate='';
            }
            system.debug('DateOfBirth ## '+DateOfBirth);
            system.debug('GivenName ## '+GivenName);
            system.debug('Surname ## '+Surname);
            system.debug('NationalityName ## '+NationalityName);
            system.debug('ExpirationDate ## '+ExpirationDate);
            system.debug('IssueDate ## '+IssueDate);
            system.debug('DocumentNumber ## '+DocumentNumber);
            system.debug('DocumentClassName ## '+DocumentClassName);
            system.debug('City ## '+City);
            system.debug('AddressLine1 ## '+AddressLine1);
            system.debug('PostalCode ## '+PostalCode);
            system.debug('State ## '+State); 
            system.debug('IssueStateCode ## '+IssueStateCode);   

            data.put('GivenName', GivenName);
            data.put('Surname', Surname);
            data.put('AddressLine1', AddressLine1);
            //data.put('Application__c.Street_Address_2__c', Address2);
            data.put('City', City);
            data.put('State', State);
            data.put('PostalCode', PostalCode);
            //data.put('Application__c.Suffix__c', NameSuffix);
            //data.put('Application__c.Middle_Name__c', MiddleName);
            data.put('DateOfBirth', DateOfBirth);
            data.put('IssueDate', IssueDate);
            data.put('ExpirationDate', ExpirationDate);
           // data.put('Identity_Information__c.ID_Type__c', 'Drivers License');
            data.put('DocumentNumber', DocumentNumber);
            //data.put('Identity_Information__c.Country_Issued__c', Country);
            //data.put('Identity_Information__c.State_Issued__c', IssuedBy);
            data.put('idType', idType);
            data.put('IssueStateCode', IssueStateCode);
            data.put('IssueCountryCode', IssueCountryCode);
            System.debug('Request-->'+requestJson);
            DebugLogger.InsertDebugLog(appId, GUID, 'DriverLicenseScan Request GUID');
            DebugLogger.InsertDebugLog(appId, requestJson, 'DriverLicenseScan Request');
            DebugLogger.InsertDebugLog(appId, responseJson, 'DriverLicenseScan Response');
        } catch (Exception e) {
            System.debug('Exception inline : ' + e.getLineNumber());
            system.debug('exception '+e.getMessage());
            System.debug('Request-->'+requestJson);
            DebugLogger.InsertDebugLog(appId, GUID, 'DriverLicenseScan Request GUID');
            DebugLogger.InsertDebugLog(appId, requestJson, 'DriverLicenseScan Request');
            DebugLogger.InsertDebugLog(appId, responseJson, 'DriverLicenseScan Response');
        }
        return data;

    }

    global static String SendDocument (Map<String, String> tdata) {

        String appId = tdata.get('id');
        String backImage = '';
        String frontImage = '';
        String responseJson = '';
        String Token = '';
        String DocumentContent = '';
        String ContentType = '';
        String attId = '';
        try {
        TF4SF__Application__c app = [SELECT Id, Name, TF4SF__User_Token__c FROM TF4SF__Application__c WHERE Id =: appId];
        List<Attachment> attList = [SELECT Id, Name, Body, ContentType, ParentID FROM Attachment WHERE ParentID =: appId];
        system.debug('the atts are '+attList);
        for (Attachment att : attList) {
            if (!String.isBlank(att.Name)) {
                String nameContent = att.Name;
                DocumentContent = EncodingUtil.base64Encode(att.body);
                if (att.ContentType == 'application/pdf') {
                    ContentType = 'pdf';    
                } else if (att.ContentType == 'image/jpg') {
                    ContentType = 'jpg';    
                } else if (att.ContentType == 'image/jpeg') {
                    ContentType = 'jpg';    
                } else if (att.ContentType == 'image/png') {
                    ContentType = 'png';    
                }
                attId = att.Id;
            }
            if (String.isNotBlank(DocumentContent)) {
                break;
            } 
        }

        system.debug('the DocumentContent are '+DocumentContent);
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        Token = AuthToken.Auth();

        Http h2 = new Http();
        HttpRequest req2 = new HttpRequest();
        req2.setEndpoint('http://velo-addrdocs-systerm-api.cloudhub.io/api/system/docrepository/sendsubmission');
        req2.setHeader('Authorization','Bearer '+Token);
        req2.setHeader('Accept','application/json');
        req2.setHeader('Content-Type', 'application/json');
        req2.setMethod('POST');
        String idBody = '';
        idBody += '{';
            idBody += '"SourceIdentifierId": "'+appId+'",';
            idBody += '"DocumentSource": "Deposit",';
            idBody += '"DcoumentType": "DriversLicense",';
            idBody += '"DocumentExtenstion": "'+ContentType+'",';
            idBody += '"DocumentContent": "'+DocumentContent+'"';
        idBody += '}';

        req2.setBody(idBody);
        HttpResponse res2 = h2.send(req2);
        system.debug('Req is '+req2.getbody()); 
        system.debug('Res is '+res2); 
        responseJson = res2.getBody();
        system.debug('Res is '+res2.getbody()); 
        }
        catch(Exception ex) {
          System.debug('Exception inline2 : ' + ex.getLineNumber());
            system.debug('exception '+ex.getMessage());
        }
        return responseJson;
    }
    
    public static string LNformatDateString(String oldDate) {
        String newDate = '';
        newDate = oldDate.replaceAll('Day=','').replaceAll('Month=','').replaceAll('Year=','').replace('{','').replace('}','');
        List<String> dateList = new List<String>();
        dateList = newDate.split(',');
        String Day = dateList[0].trim();
        String Month = dateList[1].trim();
        String Year = dateList[2].trim();
        if (Day.length() > 0 && Day.length() < 2) {
            Day = '0'+Day;
        }
        if (Month.length() > 0 && Month.length() < 2) {
            Month = '0'+Month;
        }
        newDate = Month+'/'+Day+'/'+Year; // change date in MM/DD/YYYY format.
        return newDate;
    }

    public string formatDateString(String oldDate) {
        String newDate = '';
        List<String> DateList = oldDate.split('-');
        newDate = DateList[1] + '/' + DateList[2] + '/' + DateList[0]; // change date in MM/DD/YYYY format.
        return newDate;
    }

    public static Boolean TryParseDate(String oldDate) {

        Boolean valid=true;
        Date theDate;
        try {
        // parse works in the case of 10/11/1983
        theDate = Date.parse(oldDate);
        }
        catch(Exception e) {

        }
        if (theDate == null) {
        try {
            // valueof works in the case of 1983-10-11
            theDate = Date.valueOf(oldDate);
        }
        catch(Exception e) {}
        }
        if (theDate == null) {
        // couldn't parse
         valid=false;
        }

        return valid;
    }

}