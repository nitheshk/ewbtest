public class FactorsSMSResponse{
        public String id;   //mblgcky3vjfy83I8y0h7
        public String factorType;   //sms
        public String provider; //OKTA
        public String vendorName;   //OKTA
        public String status;   //PENDING_ACTIVATION
        public String created;  //2018-09-24T04:32:52.000Z
        public String lastUpdated;  //2018-09-24T04:32:52.000Z
        public Profile profile;
       
        public class Profile {
            public String phoneNumber;  //+16508479693
        }
        
        public static FactorsSMSResponse parse(String json){
            return (FactorsSMSResponse) System.JSON.deserialize(json, FactorsSMSResponse.class);
        }
}