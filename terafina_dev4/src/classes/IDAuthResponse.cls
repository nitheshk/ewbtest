/*************************************************
@Author : Nag
@Date : 8/3/2018
@Description : This Plain apex for IDAuthResponse payload
****************************************************/
global class IDAuthResponse {
public class DatasourceFields {
    }

    public class AppendedFields {
        public String FieldName;
        public String Data;
    }

    public class DatasourceResults {
        public String DatasourceName;
        public List<DatasourceFields> DatasourceFields;
        public List<AppendedFields> AppendedFields;
        public List<DatasourceFields> Errors;
        public List<DatasourceFields> FieldGroups;
    }

    public Data data;
    public Result result;

    public class Record {
        public String TransactionRecordID;
        public String RecordStatus;
        public List<DatasourceResults> DatasourceResults;
        public List<DatasourceFields> Errors;
        public Rule Rule;
    }

    public class Data {
        public String TransactionID;
        public String DocumentType;
        public String UploadedDt;
        public String CountryCode;
        public String ProductName;
        public Record Record;
        public List<DatasourceFields> Errors;
    }

    public class Rule {
        public String RuleName;
        public String Note;
    }

    public class Result {
        public Boolean success;
        public String code;
        public String message;
    }

    
    public static IDAuthResponse parse(String json) {
        return (IDAuthResponse) System.JSON.deserialize(json, IDAuthResponse.class);
    }
    }