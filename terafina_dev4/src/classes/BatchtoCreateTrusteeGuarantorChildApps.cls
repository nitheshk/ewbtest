global class BatchtoCreateTrusteeGuarantorChildApps implements
Database.Batchable <sObject> , Database.Stateful, Database.AllowsCallouts { 


  global Database.QueryLocator start(Database.BatchableContext bc) {
   
     String query =  'SELECT Id, Child_Application__c , Child_Application_Completed__c, Email_Address__c, First_Name__c,  Middle_Name__c,';
           query += ' Last_Name__c, Suffix__c,Email_Sent_Date__c,Application__r.TF4SF__Email_Address__c,Application__r.TF4SF__Custom_Text2__c,';
           query += ' Phone_Number__c, Email_Sent__c, Application__c, Application__r.TF4SF__Product__c, Application__r.TF4SF__Sub_Product__c';
           query += ' FROM Trustee_Guarantor__c WHERE Child_Application__c = NULL AND Application__r.TF4SF__Product__c = \'Home Loan\'';
           //query += ' AND Application__r.TF4SF__Application_Status__c = \'Submitted\' ';
        return Database.getQueryLocator(query);      
  } 

  global void execute(Database.BatchableContext bc, List<Trustee_Guarantor__c> scope) {
    
    List<Id> lstAppId = new List<Id>();
    Map<Id,TF4SF__About_Account__c> mapAboutAccount = new Map<Id,TF4SF__About_Account__c>();


    //process each batch of records
    try { 

        for(Trustee_Guarantor__c obj : scope) {

          //Get the Application ID and add it to the list
          lstAppId.add(obj.Application__c);        
        }

        //Get About Account details of all the application in this batch
        List<TF4SF__About_Account__c> listAboutAccount = [SELECT Id,TF4SF__Application__c,TF4SF__Mortgage_Applied_For__c,TF4SF__Purchase_P__c,
                                                          TF4SF__Down_Payment__c, Down_Payment_By_Percentage__c, TF4SF__Total_Loan_Amount__c,
                                                          TF4SF__Street_Address_1_AboutAccount__c, TF4SF__Street_Address_2_AboutAccount__c,
                                                          TF4SF__City_AboutAccount__c, TF4SF__State_AboutAccount__c , TF4SF__Zip_Code_Mrt__c,
                                                          TF4SF__County_AboutAccount__c, MRT_Built_Year__c, TF4SF__Property_Type__c,
                                                          TF4SF__Occupancy__c
                                                          FROM TF4SF__About_Account__c WHERE TF4SF__Application__c in :lstAppId];
        //Put About account row in a map
        for(TF4SF__About_Account__c obj : listAboutAccount) {
          mapAboutAccount.put(obj.TF4SF__Application__c,obj);
        }

        //Call Createchild App function
        if ( scope.size() > 0 && mapAboutAccount.size() > 0) {
        
            ChildApplication.createChildApplication(scope,mapAboutAccount);
          
        }
        if(Test.isRunningTest()){integer a = 10/0;}
      
    } catch (exception e) {
        System.debug('Exception ==>' + e.getMessage());
    } finally {
     
    }
  }

  global void finish(Database.BatchableContext bc) {
  }

 
   
  }