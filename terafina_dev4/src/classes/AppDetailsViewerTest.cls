@isTest
private class AppDetailsViewerTest {
    // When application Id is passed to StandardController
    private static testMethod void testReadMode() {
        TF4SF__Application__c app = new TF4SF__Application__c();
        app.TF4SF__Product__c='Home Loan';
        app.TF4SF__sub_Product__c='Home Loan - Short App';
        insert app;
        
        TF4SF__Employment_Information__c emp =new TF4SF__Employment_Information__c();
        emp.TF4SF__Application__c=app.id;
        insert emp;
        
        TF4SF__Identity_Information__c identity =new TF4SF__Identity_Information__c();
        identity.TF4SF__Application__c=app.id;
        insert identity;
        
        TF4SF__About_Account__c account =new TF4SF__About_Account__c();
        account.TF4SF__Application__c=app.id;
        insert account;
        
        TF4SF__Application2__c app2 =new TF4SF__Application2__c();
        app2.TF4SF__Application__c=app.id;
        insert app2;
        
        
        Test.startTest();
        Test.setCurrentPage(Page.AppDetailsViewer);
        ApexPages.currentPage().getParameters().put('id',app.Id);
        ApexPages.StandardController stdController = new ApexPages.StandardController(app);
        AppDetailsViewer DepositAppDetailsObj = new AppDetailsViewer(stdController);
        
        DepositAppDetailsObj.saveApplication();
        system.assertNotEquals(0, DepositAppDetailsObj.getApplicationFieldSet().size()) ;
        system.assertNotEquals(0, DepositAppDetailsObj.getEmployeeFieldSet().size()) ;
        system.assertNotEquals(0, DepositAppDetailsObj.getIdentityFieldSet().size()) ;
        system.assertNotEquals(0, DepositAppDetailsObj.getAccountFieldSet().size()) ;
        system.assertNotEquals(0, DepositAppDetailsObj.getApplication2FieldSet().size()) ;


        // Covering default constructor with application id 
         AppDetailsViewer DepositAppDetailsEmptyObj = new AppDetailsViewer();
         DepositAppDetailsEmptyObj.enableEditMode();
         system.assertEquals(True, DepositAppDetailsEmptyObj.isEditMode);
         
         
        // Edit mode 
        
        Test.setCurrentPage(Page.AppDetailsViewerRedirect);
        ApexPages.currentPage().getParameters().put('id',app.Id);
        ApexPages.currentPage().getParameters().put('editmode','true');
        stdController = new ApexPages.StandardController(app);
        AppDetailsViewerRedirect DepositAppDetailsRedirectObj = new AppDetailsViewerRedirect();
        DepositAppDetailsRedirectObj = new AppDetailsViewerRedirect(stdController);
        DepositAppDetailsRedirectObj.validateAndRedirect();
        Test.stopTest();
        
    }

}