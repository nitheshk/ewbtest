@isTest
public class ValidateOTPExtensionTest{
     
    static testmethod void validatemyTest(){
     Map<String, String> data = new Map<String, String>();
     Map<String, String> tdata = new Map<String, String>();
     Map<String, String> verifyEnroll = new Map<String, String>();
        
      List <TF4SF__Application__c>  listCurrentApp =new  List <TF4SF__Application__c> ();
  
        TF4SF__Application__c ap = new TF4SF__Application__c();
        ap.TF4SF__Product__c = 'Home Loan';
        ap.TF4SF__First_Name__c='test';
        ap.TF4SF__Last_Name__c = 'Test';
        ap.TF4SF__Application_Status__c='Abandoned';
        ap.TF4SF__Email_Address__c= 'test@test.com';
        ap.TF4SF__Email_Address_J__c='test@gmail.com';
        ap.TF4SF__Primary_Phone_Number__c='1234567890';
        ap.TF4SF__Sub_Product__c = 'Home Loan - Short App';
        ap.OKTA_UserId__c='';
        ap.OKTA_factorId__c='';
        ap.OKTA_SMSId__c='';
        ap.OKTA_Status__c='ACTIVE';
        listCurrentApp.add(ap);
        insert listCurrentApp;                                             
        
      System.debug('email' + listCurrentApp[0].TF4SF__Email_Address__c);

       List<OKTA__c> oktaList = new List<OKTA__c>();
        OKTA__c okt=new OKTA__c();
        okt.Email_Address__c='Test@gmail.com';
        okt.Status__c='ACTIVE';
        oktaList.add(okt);
        insert oktaList;
        
       VELOSETTINGS__C vo = new VELOSETTINGS__C();
        vo.Name='test';
        vo.TokenEndPoint__c = 'rsgsdgsdf';
        vo.Request_GUID_Name__c = 'RequestUUID';
        vo.ProcessAPI_Timeout__c = 120000;
        insert vo;
        
      tdata.put('id',(String)ap.id);
      tdata.put('emailId',ap.TF4SF__Email_Address__c);
      tdata.put('PhoneNo',ap.TF4SF__Primary_Phone_Number__c);
      tdata.put('promocode','promocode');
      tdata.put('otp','otp');
      verifyEnroll.put('smsId','smsId');
      verifyEnroll.put('oktaStatus','oktaStatus');

   
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new ValidateOTPExtension_Mock());
        ValidateOTPExtension ob = new ValidateOTPExtension();  
        ValidateOTPExtension.verify(ap.Id, 'Token', 'userId', 'PhoneCode', 'factorId');
        ValidateOTPExtension.verifySMSRegisteration(ap.id, 'Token', 'userId', 'PhoneCode', 'factorId');
        ValidateOTPExtension.main(tdata);
        test.stopTest();
    }

}