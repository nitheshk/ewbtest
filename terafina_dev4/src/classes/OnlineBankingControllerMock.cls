@isTest
global class OnlineBankingControllerMock implements HttpCalloutMock{
global HTTPResponse respond(HTTPRequest request) {
    HttpResponse resp = new HttpResponse();
    resp.setHeader('Content-Type', 'application/json');
    resp.setBody('{ "FirstName": "TestFirstName", "MiddleName": "TestMiddleName", "LastName": "TestLastName", "FullName": "TestFullName", "Suffix": "Mr", "Address1": "TestAddres1", "Address2": "TestAddres2", "City": "NY", "State": "NewYork", "Zip": "4357", "ZipPlusFour": "TestZipPlusFour", "PrimaryPhone": "1234567890", "WorkPhone": "(509)525-2100", "TaxIdentifier": "Gst543", "MemberNumber": "807", "DOB": "20/12/1990", "emailAddress": "test@test.com", "IDType": "Bussiness", "IDNumber": "6465", "IDStateIssued": "NewYork", "IDIssueDate": "15/02/1976", "IDExpirationDate": "15/02/1989", "IsEmployee": "PEmp" }');
    resp.setStatusCode(200);
    return resp;
}
}