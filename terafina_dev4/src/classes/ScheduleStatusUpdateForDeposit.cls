//This scheduler schedules the ScheduleUpdateStausBatchApexDeposit everyday 10PM  
global with sharing class ScheduleStatusUpdateForDeposit implements Schedulable {

  global void execute(SchedulableContext sc) {
    StatusUpdateForDeposit_Batch batch = New StatusUpdateForDeposit_Batch ();
    database.executeBatch(batch);
    //delete schedule
    CronTrigger ct = [SELECT Id, CronExpression FROM CronTrigger WHERE Id = :SC.getTriggerId()];
    //Abort the job. A new one will be scheduled after the batchJob finishes
    System.abortJob(SC.getTriggerId());
  } 

  public static void SchedulerMethod() {
    //To schedule the batch cron expression used.
    ScheduleStatusUpdateForDeposit statusUpdate = new ScheduleStatusUpdateForDeposit();
    try {
      String day = string.valueOf(System.now().day());
      String month = string.valueOf(System.now().month());
      String hour = string.valueOf(System.now().hour());
      String minute = string.valueOf(System.now().minute() + 1);
      String second = string.valueOf(System.now().second());
      String year = string.valueOf(System.now().year());

      if (Integer.valueof(minute)>60) {
        minute ='0';
      }
      String strJobName = 'Deposit_Status_Update-' + second + '_' + minute + '_' + hour + '_' + day + '_' + month + '_' + year;
      String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
      System.schedule(strJobName, strSchedule,statusUpdate);

    } catch(Exception e) {
      StatusUpdateForDeposit_Batch batch = New StatusUpdateForDeposit_Batch ();
      database.executeBatch(batch);
    } 
  }
}