public class JsonParser{
        public list<Products> Products{get;set;}
        public Status Status{get;set;}
    public class Status{
        public String TransactionStatus{get;set;}
        public String ActionType{get;set;}
        public String RequestId{get;set;}
        public String Reference{get;set;}
        public String ConversationId{get;set;}
    }
    public class Products{
        public String ExecutedStepName{get;set;}
        public String ProductStatus{get;set;}
        public String ProductConfigurationName{get;set;}
        public list<Items> Items{get;set;}
        public String ProductType{get;set;}
    }
    public class Items{
        public String ItemName{get;set;}
        public String ItemStatus{get;set;}
    }
}