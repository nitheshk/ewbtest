global with sharing class SessionTokenReset implements Tf4SF.DSP_Interface {
  global static Map<String, String> main (Map<String, String> tdata) {
    Map<String, String> data = tdata.clone();
    String appId = data.get('appId');
    TF4SF__Application__c app = [SELECT Id, TF4SF__User_Token__c, TF4SF__User_Token_Expires__c FROM TF4SF__Application__c WHERE Id = :appId];

    if (CryptoHelperCustom.userTokenIsValid(appId, CryptoHelperCustom.decrypt(app.TF4SF__User_Token__c))) {
      CryptoHelperCustom.setAppToken(app);
      update app;
      data.put('apex__ut', CryptoHelperCustom.decrypt(app.TF4SF__User_Token__c));            
    }

    return data;
  }
}