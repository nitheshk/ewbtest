global class DepositPreSubmit implements TF4SF.DSP_Interface { 

	global static Map<String, String> main (Map<String, String> tdata) {
		Long time1 = DateTime.now().getTime();
		Boolean infoDebug = false;
		Map<String, String> data = tdata.clone();
		String appId = tdata.get('id');
		String Token = '';
		String responseJson = ''; 
		System.debug('appId ' + appId);
		
		try {
			if (String.isNotBlank(appId)) {
				infoDebug = (tdata.get('infoDebug') == 'true');
				TF4SF__Application__c app = [SELECT Id, Name, TF4SF__Sub_Product__c FROM TF4SF__Application__c WHERE Id =: appId];
				//TF4SF__Employment_Information__c emp = [SELECT Id, Name, TF4SF__Occupation__c FROM TF4SF__Employment_Information__c WHERE TF4SF__Application__c =: appId];
				//TF4SF__Identity_Information__c iden = [SELECT Id, TF4SF__Application__c, TF4SF__SSN_Prime__c, TF4SF__Date_of_Birth__c, TF4SF__Custom_Text17__c, Custom_Picklist12__c FROM TF4SF__Identity_Information__c WHERE TF4SF__Application__c =: appId];

				Long time1a = DateTime.now().getTime();
				if (infoDebug == true ) { data.put('debug-server-errors', 'DepositPreSubmit - Query Time: ' + (time1a - time1) + 'ms'); }
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'DepositPreSubmit - Query Time: ' + (time1a - time1) + 'ms'));

				VELOSETTINGS__C velo = VELOSETTINGS__C.getOrgDefaults();
				Token = AuthToken.Auth();
				data.put('Token', Token);

				//Save disclosures need to be revoked
				//SaveDisclosures(appId);
				
				Long time1b = DateTime.now().getTime();
				if (infoDebug == true ) { data.put('debug-server-errors', 'DepositPreSubmit - Before applyNowProductSync Time: ' + (time1b - time1a) + 'ms'); }
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'DepositPreSubmit - Before applyNowProductSync Time: ' + (time1b - time1a) + 'ms'));

				data = applyNowProductSync(appId, app.TF4SF__Sub_Product__c, data);
				Long time1c = DateTime.now().getTime();
				if (infoDebug == true ) { data.put('debug-server-errors', 'DepositPreSubmit - After applyNowProductSync Time: ' + (time1c - time1b) + 'ms'); }
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'DepositPreSubmit - After applyNowProductSync Time: ' + (time1c - time1b) + 'ms'));
			}
		} catch (Exception ex) {
			data.put('status','---------- ' + ex.getMessage() + ' Line Number==>' + ex.getLineNumber());
			data.put('server-errors', 'Error encountered in DepositPreSubmit class: ' + ex.getMessage() + '; line: ' + ex.getLineNumber() + '; type: ' + ex.getTypeName() + '; stack trace: ' + ex.getStackTraceString());
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error encountered in DepositPreSubmit class: ' + ex.getMessage() + '; line: ' + ex.getLineNumber() + '; type: ' + ex.getTypeName() + '; stack trace: ' + ex.getStackTraceString()));
		}

		return data;
	}

	private static Map<String, String> applyNowProductSync(String appId, String productName, Map<String, String> data) {
		return applyNowProduct(appId, productName, data);
	}

	@future(callout=true)
	@testvisible private static void applyNowProductASync(String appId, String productName, Map<String, String> data) {
		System.debug('entered future callout');
		applyNowProduct(appId, productName, data);
	}

	@testvisible private static void SaveDisclosures(String appId) {
		//SaveDisclosureAttachments.main(appId);
	}

	private static Map<String, String> applyNowProduct(String appId, String productName, Map<String, String> data) {
		String StatusCode = '0';
		Long time1 = DateTime.now().getTime();

		try {
			String ApplicationReferenceId = '';
			String OverAllApplicationResultDesription = '';
			String OverAllApplicationResultStatus = '';
			String ApplicationAction = '';
			String ApplicationActionDescription = '';
			String ApplicationResultStatus = '';
			String CoreResultId = '';
			String ReferenceId = ''; 
			TF4SF__Application__c app = new TF4SF__Application__c();
			DepositSubmit tip = new DepositSubmit();

			String Token = data.get('Token');
			//System.debug('appid, productnumber: ' + appid + '--' + productnumber);
			Long time1a = DateTime.now().getTime();
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'applyNowProduct - Before tip.ProcessDecision Time: ' + (time1a - time1) + 'ms'));
			String responseJSON = tip.ProcessDecision(appId, Token);
			system.debug('responseJSON %%%%% :' + responseJSON);
			Long time1b = DateTime.now().getTime();
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'applyNowProduct - After tip.ProcessDecision Time: ' + (time1b - time1a) + 'ms'));
			System.debug('Application Status Response' + responseJSON);

			// Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(responseJSON);
			//app = [SELECT Id, Name, TF4SF__External_App_Stage__c, TF4SF__External_AppStage_CrossSell1__c, TF4SF__External_AppStage_CrossSell2__c, TF4SF__External_AppStage_CrossSell3__c, TF4SF__Primary_Product_Status__c,  TF4SF__External_App_ID__c, TF4SF__Primary_Offer_Status__c, TF4SF__External_AppID_CrossSell1__c, TF4SF__Second_Offer_Status__c, TF4SF__External_AppID_CrossSell2__c, TF4SF__Third_Offer_Status__c, TF4SF__External_AppID_CrossSell3__c, TF4SF__Sub_Product__c, TF4SF__Primary_Offer__c, TF4SF__Second_Offer__c, TF4SF__Third_Offer__c FROM TF4SF__Application__c WHERE Name = :ApplicationReferenceId];
			//System.debug('Application Status Response'+responseJSON);
			//Hardcoded Json To get Questions
			//String json = '{\"data\":{\"Status\":{\"ConversationId\":\"31000101087565\",\"RequestId\":\"293910775\",\"TransactionStatus\":\"pending\",\"ActionType\":\"answers\",\"Reference\":\"Reference1\"},\"Products\":[{\"ProductType\":\"InstantVerify\",\"ExecutedStepName\":\"verification\",\"ProductConfigurationName\":\"public.testing.redherrings_verification_open\",\"ProductStatus\":\"pass\",\"Items\":[{\"ItemName\":\"TaskAgeUnder18WillNotBeFoundCheck\",\"ItemStatus\":\"pass\"},{\"ItemName\":\"LexIDDeathMatch\",\"ItemStatus\":\"pass\"}]},{\"ProductType\":\"IIDQA\",\"ExecutedStepName\":\"authentication\",\"ProductConfigurationName\":\"public.testing.redherrings_authentication_open\",\"ProductStatus\":\"pending\",\"QuestionSet\":{\"QuestionSetId\":93850555,\"Questions\":[{\"QuestionId\":322146535,\"Key\":5001,\"Type\":\"singlechoice\",\"Text\":{\"Statement\":\"Whatcolorisyour1985Saab900?\"},\"HelpText\":{\"Statement\":\"Mayincludevehicleswhichyourecentlysoldorvehiclesforwhichyouco-signed\"},\"Choices\":[{\"ChoiceId\":1584338765,\"Text\":{\"Statement\":\"Beige\"}},{\"ChoiceId\":1584338775,\"Text\":{\"Statement\":\"Green\"}},{\"ChoiceId\":1584338785,\"Text\":{\"Statement\":\"Red\"}},{\"ChoiceId\":1584338795,\"Text\":{\"Statement\":\"White\"}},{\"ChoiceId\":1584338805,\"Text\":{\"Statement\":\"Ihaveneverbeenassociatedwiththisvehicle\"}}]},{\"QuestionId\":322146545,\"Key\":30141,\"Type\":\"singlechoice\",\"Text\":{\"Statement\":\"Whichofthefollowingpeoplehaveyouknown?\"},\"HelpText\":{\"Statement\":\"Namesmaybelistedaslast-namefirst-name,includemaidennamesorcontainslightmisspellings.\"},\"Choices\":[{\"ChoiceId\":1584338815,\"Text\":{\"Statement\":\"ErnestGianna\"}},{\"ChoiceId\":1584338825,\"Text\":{\"Statement\":\"LeeAokuso\"}},{\"ChoiceId\":1584338835,\"Text\":{\"Statement\":\"LianaOssipova\"}},{\"ChoiceId\":1584338845,\"Text\":{\"Statement\":\"NanaAfrifa\"}},{\"ChoiceId\":1584338855,\"Text\":{\"Statement\":\"IdonotknowANYofthepeoplelisted\"}}]},{\"QuestionId\":322146555,\"Key\":5551,\"Type\":\"singlechoice\",\"Text\":{\"Statement\":\"Inwhichofthefollowingcitieshaveyouattendedcollege?\"},\"HelpText\":{\"Statement\":\"Selectthecityofthecollegeyouhaveattended.\"},\"Choices\":[{\"ChoiceId\":1584338865,\"Text\":{\"Statement\":\"Annapolis\"}},{\"ChoiceId\":1584338875,\"Text\":{\"Statement\":\"Howell\"}},{\"ChoiceId\":1584338885,\"Text\":{\"Statement\":\"Kerrville\"}},{\"ChoiceId\":1584338895,\"Text\":{\"Statement\":\"Wilmington\"}},{\"ChoiceId\":1584338905,\"Text\":{\"Statement\":\"Noneoftheabove\"}}]}]}}]},\"result\":{\"success\":true,\"code\":\"200\",\"message\":\"Success\"}}';
			//savekycquestions(appId,json);
			//data.put('status',json);
			//Hardcoded Json ends here 
			PreSubmitWrapper wrapper = null;
			if(String.isNotBlank(responseJSON)){
				wrapper = PreSubmitWrapper.parse(responseJSON); 
			}
			Long time1c = DateTime.now().getTime();
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'applyNowProduct - After PreSubmitWrapper.parse Time: ' + (time1c - time1b) + 'ms'));
			app = [SELECT Id, TF4SF__External_App_Stage__c, TF4SF__External_App_ID__c FROM TF4SF__Application__c WHERE Id=: appId];
			Long time1d = null;
			if (wrapper != null && wrapper.Data.ApplicationStatus == 'Pending') {
				//Sending it to front end for saving
				data.put('AppStatus', 'Pending');
				data.put('CaseNumber', wrapper.Data.CaseNumber);
				//DepositPreSubmit.savekycquestions(appId,wrapper);
				if (wrapper.Data.InstantIdResponse == null) {
					data.put('Application__c.Application_Status__c', 'Submitted');
				} 
				data.put('Application__c.Primary_Product_Case_Id__c', wrapper.Data.CaseNumber);
				
				if (String.isNotBlank(wrapper.Data.CustomerNumber)) {
					data.put('Application__c.External_App_Stage__c', wrapper.Data.CustomerNumber);
				} else {
					data.put('Application__c.External_App_Stage__c', app.TF4SF__External_App_Stage__c);
				}
				data.put('Application__c.External_App_ID__c', wrapper.Data.AccountNumber);
				data.put('Application__c.Primary_Product_Status__c', 'Pending Review');
				SubmitApplication.updateApp(data); 
				time1d = DateTime.now().getTime();
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'applyNowProduct - After SubmitApplication.updateApp() - Pending: Time: ' + (time1d - time1c) + 'ms'));
			} else if (wrapper != null && wrapper.Data.ApplicationStatus == 'Approved') {
				//Sending it to front end for saving
				data.put('CaseNumber', wrapper.Data.CaseNumber);
				data.put('AccountNumber', wrapper.Data.AccountNumber);
				if (String.isNotBlank(wrapper.Data.CustomerNumber)) {
					data.put('Application__c.External_App_Stage__c', wrapper.Data.CustomerNumber);
				} else {
					data.put('Application__c.External_App_Stage__c', app.TF4SF__External_App_Stage__c);
				}
				data.put('AppStatus', 'Approved');
				data.put('Application__c.Primary_Product_Case_Id__c', wrapper.Data.CaseNumber);
				data.put('Application__c.External_App_Stage__c', wrapper.Data.CustomerNumber);
				data.put('Application__c.External_App_ID__c', wrapper.Data.AccountNumber);
				data.put('Application__c.Primary_Product_Status__c', 'Approved');
				data.put('Application__c.Application_Status__c','Submitted');
				SubmitApplication.updateApp(data);
				//SaveDisclosureAttachments saveDisclosure = new SaveDisclosureAttachments();
				//saveDisclosure.save(appId);
				//SynergyDocupload syDocUpload = new SynergyDocupload();
				//syDocUpload.UploadDocs( appId, '', appId, wrapper.Data.AccountNumber, wrapper.Data.CustomerNumber);
				time1d = DateTime.now().getTime();
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'applyNowProduct - After SubmitApplication.updateApp() - Approved: Time: ' + (time1d - time1c) + 'ms'));
			} else if (wrapper != null && wrapper.Data.ApplicationStatus == 'Declined') {
				//Redirect to decline page
				data.put('AppStatus', 'Declined');
				data.put('CaseNumber', wrapper.Data.CaseNumber);
				data.put('Application__c.Primary_Product_Case_Id__c', wrapper.Data.CaseNumber);
				if (String.isNotBlank(wrapper.Data.CustomerNumber)) {
					data.put('Application__c.External_App_Stage__c', wrapper.Data.CustomerNumber);
				} else {
					data.put('Application__c.External_App_Stage__c', app.TF4SF__External_App_Stage__c);
				}
				data.put('Application__c.External_App_ID__c', wrapper.Data.AccountNumber);
				data.put('Application__c.Primary_Product_Status__c', 'Declined');
				data.put('Application__c.Application_Status__c', 'Submitted');
				SubmitApplication.updateApp(data);
				time1d = DateTime.now().getTime();
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'applyNowProduct - After SubmitApplication.updateApp() - Decline: Time: ' + (time1d - time1c) + 'ms'));
			}

			//Need to be reworked
			if(wrapper != null){
				data.put('status', JSON.Serialize(wrapper.Data.InstantIdResponse));
			}
			StatusCode = '200';
		} catch (Exception e) {
			//ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Sorry! An error occured submitting the application: ' + e.getMessage() + '; stack trace: ' + e.getStackTraceString()));
			System.debug('An error was encountered in the applyNow ######## (' + e.getLineNumber() + '): ' + e.getMessage());
		}

		//Calling Class to send Submission Email - FUTURE METHOD CALL """"""""""""""""""even if Core Integration FAILS""""""""""
		if (appId != null && appId != '') {
			//System.debug('appId ====== ' + appId);
			try {
				//System.debug('Calling EmailUtility.sendSubmissionEmail method');s
				//EmailUtility.sendSubmissionEmail(appId);
				SaveDepositDisclosures(appId);
			} catch (Exception ex) {
				System.debug('An error occured in EmailUtility.sendSubmissionEmail() with error ####### ' + ex.getMessage() + ' ' + 'In Line ::::::::: ' + ex.getLineNumber());
			}
		}

		Long time2 = DateTime.now().getTime();
		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'applyNowProduct - Total Time: ' + (time2 - time1) + 'ms'));
		data.put('statuscode', StatusCode);
		return data;
	}
	@future(callout=true)
	public static void SaveDepositDisclosures(String appId) {
		try {
			TF4SF__Application__c app = [SELECT Id, TF4SF__Product__c, Tf4SF__Sub_Product__c From TF4SF__Application__c WHERE Id =: appId];
			String prodCode = app.TF4SF__Product__c;
			List<String> dList = New List<String>();
			List<TF4SF__Disclosure_Names__c> dName = [SELECT Id, Product_Code__c, TF4SF__Disclosure_Label__c FROM TF4SF__Disclosure_Names__c WHERE Product_Code__c =: prodCode];
			for (TF4SF__Disclosure_Names__c d : dName) { dList.add(d.TF4SF__Disclosure_Label__c); }
			List<TF4SF__Disclosure__c> discList = [SELECT Id, Name, (SELECT Id FROM ATTACHMENTS ORDER by CreatedDate DESC LIMIT 1) FROM TF4SF__Disclosure__c WHERE Name IN: dList];
			List<Attachment> insertNewAtt = new List<Attachment>();
			List<Attachment> attList = new List<Attachment>();
			for (TF4SF__Disclosure__c disc: discList) {
				attList.add(disc.Attachments);
			}
			
			for (Attachment att : attList) {
				Attachment attach = new Attachment();
				attach.Name = att.Name;
				attach.Body = att.Body;
				attach.ContentType = att.ContentType;
				attach.ParentId = appId;
				insertNewAtt.add(att);
			}
			
			if (insertNewAtt.size() > 0)  {
				insert insertNewAtt;
			}
		} catch (exception e) {
			system.debug('Error Saving Disclosures '+e.getLineNumber());
		}
		
	}

 //   public static void savekycquestions(string appid, PreSubmitWrapper wrapper) {
	//	try {
	//		PreSubmitWrapper r = wrapper;
	//		System.debug('KYC Res--->' + r);  

	//		if (r.Data.InstantIdResponse.Products.size() > 1) {
	//			kyc_oow__c oow = new kyc_oow__c();
	//			oow.application_id__c = appid;
	//			oow.kyc_id__c = r.data.InstantIdResponse.Status.Conversationid;

	//			for (Integer q = 0; q <= r.Data.InstantIdResponse.Products.size(); q++) {
	//				String ProductType = String.valueOf(r.Data.InstantIdResponse.Products[q].ProductType);
	//				if ( ProductType == 'IIDQA') {
	//					oow.QuestionSetId__c = String.valueOf(r.Data.InstantIdResponse.Products[q].QuestionSet.QuestionSetId);

	//					for (Integer j = 0; j < r.Data.InstantIdResponse.Products[q].QuestionSet.Questions.size(); j++) {
	//						if ( j == 0) { 
	//							//question-1 starts here
	//							oow.Question_1_Text__c = String.valueOf(r.Data.InstantIdResponse.Products[q].QuestionSet.Questions[0].Text.Statement);
	//							oow.Question_1_ID__c = String.valueOf(r.Data.InstantIdResponse.Products[q].QuestionSet.Questions[0].QuestionId);
	//							List<PreSubmitWrapper.Choices> listChoices = r.Data.InstantIdResponse.Products[q].QuestionSet.Questions[0].Choices;

	//							for (Integer i = 0; i < listChoices.size(); i++) {
	//								//Choice-1
	//								if (i == 0) {
	//									oow.Question_1_Choice_1__c = String.valueOf(r.Data.InstantIdResponse.Products[q].QuestionSet.Questions[0].Choices[i].Text.Statement);
	//									oow.Question_1_Choice_1_Id__c = String.valueOf(r.Data.InstantIdResponse.Products[q].QuestionSet.Questions[0].Choices[i].ChoiceId);
	//								}

	//								//Choice-2
	//								if (i == 1) {
	//									oow.Question_1_Choice_2__c = String.valueOf(r.Data.InstantIdResponse.Products[q].QuestionSet.Questions[0].Choices[i].Text.Statement);
	//									oow.Question_1_Choice_2_Id__c = String.valueOf(r.Data.InstantIdResponse.Products[q].QuestionSet.Questions[0].Choices[i].ChoiceId);
	//								}

	//								//Choice-3
	//								if (i == 2) {
	//									oow.Question_1_Choice_3__c = String.valueOf(r.Data.InstantIdResponse.Products[q].QuestionSet.Questions[0].Choices[i].Text.Statement);
	//									oow.Question_1_Choice_3_Id__c = String.valueOf(r.Data.InstantIdResponse.Products[q].QuestionSet.Questions[0].Choices[i].ChoiceId);
	//								}

	//								//Choice-4
	//								if (i == 3) {
	//									oow.Question_1_Choice_4__c = String.valueOf(r.Data.InstantIdResponse.Products[q].QuestionSet.Questions[0].Choices[i].Text.Statement);
	//									oow.Question_1_Choice_4_Id__c = String.valueOf(r.Data.InstantIdResponse.Products[q].QuestionSet.Questions[0].Choices[i].ChoiceId);
	//								}

	//								//Choice-5
	//								if (i == 4) {
	//									oow.Question_1_Choice_4__c = String.valueOf(r.Data.InstantIdResponse.Products[q].QuestionSet.Questions[0].Choices[i].Text.Statement);
	//									oow.Question_1_Choice_5_Id__c = String.valueOf(r.Data.InstantIdResponse.Products[q].QuestionSet.Questions[0].Choices[i].ChoiceId);
	//								}
	//							//Question-1 ends here
	//							}
	//						}

	//						if (j == 1) {
	//							//Question-2 starts here
	//							oow.Question_2_Text__c = String.valueOf(r.Data.InstantIdResponse.Products[q].QuestionSet.Questions[1].Text.Statement);
	//							oow.Question_2_ID__c = String.valueOf(r.Data.InstantIdResponse.Products[q].QuestionSet.Questions[1].QuestionId);
								
	//							List<PreSubmitWrapper.Choices> listChoices1 = r.Data.InstantIdResponse.Products[q].QuestionSet.Questions[1].Choices;

	//							for (Integer i = 0; i < listChoices1.size(); i++) {
	//								//Choice-1
	//								if (i == 0) {
	//									oow.Question_2_Choice_1__c = String.valueOf(r.Data.InstantIdResponse.Products[q].QuestionSet.Questions[1].Choices[i].Text.Statement);
	//									oow.Question_2_Choice_1_Id__c = String.valueOf(r.Data.InstantIdResponse.Products[q].QuestionSet.Questions[1].Choices[i].ChoiceId);
	//								}

	//								//Choice-2
	//								if (i == 1) {
	//									oow.Question_2_Choice_2__c = String.valueOf(r.Data.InstantIdResponse.Products[q].QuestionSet.Questions[1].Choices[i].Text.Statement);
	//									oow.Question_2_Choice_2_Id__c = String.valueOf(r.Data.InstantIdResponse.Products[q].QuestionSet.Questions[1].Choices[i].ChoiceId);
	//								}

	//								//Choice-3
	//								if (i == 2) {
	//									oow.Question_2_Choice_3__c = String.valueOf(r.Data.InstantIdResponse.Products[q].QuestionSet.Questions[1].Choices[i].Text.Statement);
	//									oow.Question_2_Choice_3_Id__c = String.valueOf(r.Data.InstantIdResponse.Products[q].QuestionSet.Questions[1].Choices[i].ChoiceId);
	//								}

	//								//Choice-4
	//								if (i == 3) {
	//									oow.Question_2_Choice_4__c = String.valueOf(r.Data.InstantIdResponse.Products[q].QuestionSet.Questions[1].Choices[i].Text.Statement);
	//									oow.Question_2_Choice_4_Id__c = String.valueOf(r.Data.InstantIdResponse.Products[q].QuestionSet.Questions[1].Choices[i].ChoiceId);
	//								}

	//								//Choice-5
	//								if (i == 4) {
	//									oow.Question_2_Choice_5__c = String.valueOf(r.Data.InstantIdResponse.Products[q].QuestionSet.Questions[1].Choices[i].Text.Statement);
	//									oow.Question_2_Choice_5_Id__c = String.valueOf(r.Data.InstantIdResponse.Products[q].QuestionSet.Questions[1].Choices[i].ChoiceId);
	//								}
	//							//Question-2 sends here
	//							}
	//						}

	//						if (j == 2) {
	//							//Question-3 starts here
	//							oow.Question_3_Text__c = String.valueOf(r.Data.InstantIdResponse.Products[q].QuestionSet.Questions[2].Text.Statement);
	//							oow.Question_3_ID__c = String.valueOf(r.Data.InstantIdResponse.Products[q].QuestionSet.Questions[2].QuestionId);
								
	//							List<PreSubmitWrapper.Choices> listChoices1 = r.Data.InstantIdResponse.Products[q].QuestionSet.Questions[2].Choices;

	//							for(Integer i=0;i<listChoices1.size();i++) {
	//								//Choice-1
	//								if (i == 0) {
	//									oow.Question_3_Choice_1__c = String.valueOf(r.Data.InstantIdResponse.Products[q].QuestionSet.Questions[2].Choices[i].Text.Statement);
	//									oow.Question_3_Choice_1_Id__c = String.valueOf(r.Data.InstantIdResponse.Products[q].QuestionSet.Questions[2].Choices[i].ChoiceId);
	//								}

	//								//Choice-2
	//								if (i == 1) {
	//									oow.Question_3_Choice_2__c = String.valueOf(r.Data.InstantIdResponse.Products[q].QuestionSet.Questions[2].Choices[i].Text.Statement);
	//									oow.Question_3_Choice_2_Id__c = String.valueOf(r.Data.InstantIdResponse.Products[q].QuestionSet.Questions[2].Choices[i].ChoiceId);
	//								}

	//								//Choice-3
	//								if (i == 2) {
	//									oow.Question_3_Choice_3__c = String.valueOf(r.Data.InstantIdResponse.Products[q].QuestionSet.Questions[2].Choices[i].Text.Statement);
	//									oow.Question_3_Choice_3_Id__c = String.valueOf(r.Data.InstantIdResponse.Products[q].QuestionSet.Questions[2].Choices[i].ChoiceId);
	//								}

	//								//Choice-4
	//								if (i == 3) {
	//									oow.Question_3_Choice_4__c = String.valueOf(r.Data.InstantIdResponse.Products[q].QuestionSet.Questions[2].Choices[i].Text.Statement);
	//									oow.Question_3_Choice_4_Id__c = String.valueOf(r.Data.InstantIdResponse.Products[q].QuestionSet.Questions[2].Choices[i].ChoiceId);
	//								}

	//								//Choice-5
	//								if (i == 4) {
	//									oow.Question_3_Choice_5__c = String.valueOf(r.Data.InstantIdResponse.Products[q].QuestionSet.Questions[2].Choices[i].Text.Statement);
	//									oow.Question_3_Choice_5_Id__c = String.valueOf(r.Data.InstantIdResponse.Products[q].QuestionSet.Questions[2].Choices[i].ChoiceId);
	//								}
	//							//Question-3 ends here
	//							}
	//						}
	//					}

	//					break;
	//				}
	//			}

	//			insert oow;
	//		}
	//	} catch(Exception ex) {
	//		System.debug('An error occured in  SaveKYCQuestions() with error ####### ' + ex.getMessage() + ' ' + 'In Line ::::::::: ' + ex.getLineNumber());
	//	}
	//}
}