public class JSON2ApexValidateBankInfoRes { 

    public class GetFISeedDataResponse {
        public Return_Y return_Z {get;set;} // in json: return

        public GetFISeedDataResponse(System.JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'return') {
                            return_Z = new Return_Y(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'GetFISeedDataResponse consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public Data data {get;set;} 

    public JSON2ApexValidateBankInfoRes(System.JSONParser parser) {
        while (parser.nextToken() != System.JSONToken.END_OBJECT) {
            if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                String text = parser.getText();
                if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                    if (text == 'data') {
                        data = new Data(parser);
                    } else {
                        System.debug(LoggingLevel.WARN, 'JSON2Apex consuming unrecognized property: '+text);
                        consumeObject(parser);
                    }
                }
            }
        }
    }
    
    public class InstantVerificationEnabledAcctTypeList {
        public Object InstantVerificationEnabledAcctType {get;set;} 
        public InstantVerificationEnabledAcctTypeList(System.JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'InstantVerificationEnabledAcctType') {
                            InstantVerificationEnabledAcctType = parser.readValueAs(Object.class);
                        } else {
                            System.debug(LoggingLevel.WARN, 'InstantVerificationEnabledAcctTypeList consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class FIInfo {
        public String FIID {get;set;} 
        public String CurrentStatus {get;set;} 
        public String FIName {get;set;} 
        public Object FILoginUrl {get;set;} 
        public Object FIPhone {get;set;} 
        public Object FIFax {get;set;} 
        public RoutingInformationList RoutingInformationList {get;set;} 
        public FIVerificationMode FIVerificationMode {get;set;} 
        public FICredentials FICredentials {get;set;} 
        public AccountTypeInfoList AccountTypeInfoList {get;set;} 
        public String IsHostFI {get;set;} 

        public FIInfo(System.JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'FIID') {
                            FIID = parser.getText();
                        } else if (text == 'CurrentStatus') {
                            CurrentStatus = parser.getText();
                        } else if (text == 'FIName') {
                            FIName = parser.getText();
                        } else if (text == 'FILoginUrl') {
                            FILoginUrl = parser.readValueAs(Object.class);
                        } else if (text == 'FIPhone') {
                            FIPhone = parser.readValueAs(Object.class);
                        } else if (text == 'FIFax') {
                            FIFax = parser.readValueAs(Object.class);
                        } else if (text == 'RoutingInformationList') {
                            RoutingInformationList = new RoutingInformationList(parser);
                        } else if (text == 'FIVerificationMode') {
                            FIVerificationMode = new FIVerificationMode(parser);
                        } else if (text == 'FICredentials') {
                            FICredentials = new FICredentials(parser);
                        } else if (text == 'AccountTypeInfoList') {
                            AccountTypeInfoList = new AccountTypeInfoList(parser);
                        } else if (text == 'IsHostFI') {
                            IsHostFI = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'FIInfo consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class CreateUserResponse_Z {
        public Body body {get;set;} 

        public CreateUserResponse_Z(System.JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'body') {
                            body = new Body(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'CreateUserResponse_Z consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class CreateUserResponse {
        public Return_Z return_Z {get;set;} // in json: return

        public CreateUserResponse(System.JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'return') {
                            return_Z = new Return_Z(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'CreateUserResponse consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class Status_Z {
        public String StatusCode {get;set;} 
        public String Severity {get;set;} 
        public String StatusDesc {get;set;} 
        public StatusDetail_Z StatusDetail {get;set;} 

        public Status_Z(System.JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'StatusCode') {
                            StatusCode = parser.getText();
                        } else if (text == 'Severity') {
                            Severity = parser.getText();
                        } else if (text == 'StatusDesc') {
                            StatusDesc = parser.getText();
                        } else if (text == 'StatusDetail') {
                            StatusDetail = new StatusDetail_Z(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'Status_Z consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class GetFISeedResponse {
        public Body_Z body {get;set;} 

        public GetFISeedResponse(System.JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'body') {
                            body = new Body_Z(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'GetFISeedResponse consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class Body {
        public CreateUserResponse CreateUserResponse {get;set;} 

        public Body(System.JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'CreateUserResponse') {
                            CreateUserResponse = new CreateUserResponse(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'Body consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class Status {
        public String StatusCode {get;set;} 
        public String Severity {get;set;} 
        public String StatusDesc {get;set;} 
        public StatusDetail StatusDetail {get;set;} 

        public Status(System.JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'StatusCode') {
                            StatusCode = parser.getText();
                        } else if (text == 'Severity') {
                            Severity = parser.getText();
                        } else if (text == 'StatusDesc') {
                            StatusDesc = parser.getText();
                        } else if (text == 'StatusDetail') {
                            StatusDetail = new StatusDetail(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'Status consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class FILoginParametersInfo {
        public String CredentialsParamId {get;set;} 
        public String CredentialsParamNumber {get;set;} 
        public String CredentialsParamType {get;set;} 
        public String CredentialsCaption {get;set;} 
        public String CredentialsVariableName {get;set;} 
        public String CredentialsSize {get;set;} 
        public String CredentialsMaxLength {get;set;} 
        public String CredentialsParamEditable {get;set;} 
        public String CredentialsParamSensitivityCode {get;set;} 

        public FILoginParametersInfo(System.JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'CredentialsParamId') {
                            CredentialsParamId = parser.getText();
                        } else if (text == 'CredentialsParamNumber') {
                            CredentialsParamNumber = parser.getText();
                        } else if (text == 'CredentialsParamType') {
                            CredentialsParamType = parser.getText();
                        } else if (text == 'CredentialsCaption') {
                            CredentialsCaption = parser.getText();
                        } else if (text == 'CredentialsVariableName') {
                            CredentialsVariableName = parser.getText();
                        } else if (text == 'CredentialsSize') {
                            CredentialsSize = parser.getText();
                        } else if (text == 'CredentialsMaxLength') {
                            CredentialsMaxLength = parser.getText();
                        } else if (text == 'CredentialsParamEditable') {
                            CredentialsParamEditable = parser.getText();
                        } else if (text == 'CredentialsParamSensitivityCode') {
                            CredentialsParamSensitivityCode = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'FILoginParametersInfo consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class Return_Y {
        public String RqUID {get;set;} 
        public Status_Z Status {get;set;} 
        public FIInfo FIInfo {get;set;} 

        public Return_Y(System.JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'RqUID') {
                            RqUID = parser.getText();
                        } else if (text == 'Status') {
                            Status = new Status_Z(parser);
                        } else if (text == 'FIInfo') {
                            FIInfo = new FIInfo(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'Return_Y consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class Return_Z {
        public String RqUID {get;set;} 
        public Status Status {get;set;} 
        public String CEUserID {get;set;} 

        public Return_Z(System.JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'RqUID') {
                            RqUID = parser.getText();
                        } else if (text == 'Status') {
                            Status = new Status(parser);
                        } else if (text == 'CEUserID') {
                            CEUserID = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Return_Z consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class FIRoutingInformation {
        public String ABA {get;set;} 
        public String RoutingStatus {get;set;} 
        public String RoutingType {get;set;} 
        public Object AccountNumberPrefix {get;set;} 
        public InstantVerificationEnabledAcctTypeList InstantVerificationEnabledAcctTypeList {get;set;} 

        public FIRoutingInformation(System.JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'ABA') {
                            ABA = parser.getText();
                        } else if (text == 'RoutingStatus') {
                            RoutingStatus = parser.getText();
                        } else if (text == 'RoutingType') {
                            RoutingType = parser.getText();
                        } else if (text == 'AccountNumberPrefix') {
                            AccountNumberPrefix = parser.readValueAs(Object.class);
                        } else if (text == 'InstantVerificationEnabledAcctTypeList') {
                            InstantVerificationEnabledAcctTypeList = new InstantVerificationEnabledAcctTypeList(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'FIRoutingInformation consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class FIVerificationMode {
        public String TrialDeposit {get;set;} 
        public String OnlineVerification {get;set;} 
        public String Host {get;set;} 

        public FIVerificationMode(System.JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'TrialDeposit') {
                            TrialDeposit = parser.getText();
                        } else if (text == 'OnlineVerification') {
                            OnlineVerification = parser.getText();
                        } else if (text == 'Host') {
                            Host = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'FIVerificationMode consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class MessageData {

        public MessageData(System.JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        {
                            System.debug(LoggingLevel.WARN, 'MessageData consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class FICredentials {
        public FILoginParametersList FILoginParametersList {get;set;} 

        public FICredentials(System.JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'FILoginParametersList') {
                            FILoginParametersList = new FILoginParametersList(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'FICredentials consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class RoutingInformationList {
        public List<FIRoutingInformation> FIRoutingInformation {get;set;} 

        public RoutingInformationList(System.JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'FIRoutingInformation') {
                            FIRoutingInformation = arrayOfFIRoutingInformation(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'RoutingInformationList consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class Data {
        public CreateUserResponse_Z CreateUserResponse {get;set;} 
        public GetFISeedResponse GetFISeedResponse {get;set;} 

        public Data(System.JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'CreateUserResponse') {
                            CreateUserResponse = new CreateUserResponse_Z(parser);
                        } else if (text == 'GetFISeedResponse') {
                            GetFISeedResponse = new GetFISeedResponse(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'Data consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class FILoginParametersList {
        public List<FILoginParametersInfo> FILoginParametersInfo {get;set;} 

        public FILoginParametersList(System.JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'FILoginParametersInfo') {
                            FILoginParametersInfo = arrayOfFILoginParametersInfo(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'FILoginParametersList consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class AccountTypeInfoList {
        public List<AccountTypeInfo> AccountTypeInfo {get;set;} 

        public AccountTypeInfoList(System.JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'AccountTypeInfo') {
                            AccountTypeInfo = arrayOfAccountTypeInfo(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'AccountTypeInfoList consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class Body_Z {
        public GetFISeedDataResponse GetFISeedDataResponse {get;set;} 

        public Body_Z(System.JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'GetFISeedDataResponse') {
                            GetFISeedDataResponse = new GetFISeedDataResponse(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'Body_Z consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class StatusDetail {
        public Object RequestStatus {get;set;} 
        public Object Severity {get;set;} 
        public Object MessageData {get;set;} 

        public StatusDetail(System.JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'RequestStatus') {
                            RequestStatus = parser.readValueAs(Object.class);
                        } else if (text == 'Severity') {
                            Severity = parser.readValueAs(Object.class);
                        } else if (text == 'MessageData') {
                            MessageData = parser.readValueAs(Object.class);
                        } else {
                            System.debug(LoggingLevel.WARN, 'StatusDetail consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class StatusDetail_Z {
        public Object RequestStatus {get;set;} 
        public Object Severity {get;set;} 
        public List<MessageData> MessageData {get;set;} 

        public StatusDetail_Z(System.JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'RequestStatus') {
                            RequestStatus = parser.readValueAs(Object.class);
                        } else if (text == 'Severity') {
                            Severity = parser.readValueAs(Object.class);
                        } else if (text == 'MessageData') {
                            MessageData = arrayOfMessageData(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'StatusDetail_Z consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class AccountTypeInfo {
        public String ActTypeId {get;set;} 
        public String ActTypeName {get;set;} 
        public String AllowedIn {get;set;} 
        public String AllowedOut {get;set;} 
        public String ActMinLen {get;set;} 
        public String ActMaxLen {get;set;} 
        public String ActGroup {get;set;} 
        public String DebitTransCode {get;set;} 
        public String CreditTransCode {get;set;} 

        public AccountTypeInfo(System.JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'ActTypeId') {
                            ActTypeId = parser.getText();
                        } else if (text == 'ActTypeName') {
                            ActTypeName = parser.getText();
                        } else if (text == 'AllowedIn') {
                            AllowedIn = parser.getText();
                        } else if (text == 'AllowedOut') {
                            AllowedOut = parser.getText();
                        } else if (text == 'ActMinLen') {
                            ActMinLen = parser.getText();
                        } else if (text == 'ActMaxLen') {
                            ActMaxLen = parser.getText();
                        } else if (text == 'ActGroup') {
                            ActGroup = parser.getText();
                        } else if (text == 'DebitTransCode') {
                            DebitTransCode = parser.getText();
                        } else if (text == 'CreditTransCode') {
                            CreditTransCode = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'AccountTypeInfo consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    
    public static JSON2ApexValidateBankInfoRes parse(String json) {
        System.JSONParser parser = System.JSON.createParser(json);
        return new JSON2ApexValidateBankInfoRes(parser);
    }
    
    public static void consumeObject(System.JSONParser parser) {
        Integer depth = 0;
        do {
            System.JSONToken curr = parser.getCurrentToken();
            if (curr == System.JSONToken.START_OBJECT || 
                curr == System.JSONToken.START_ARRAY) {
                depth++;
            } else if (curr == System.JSONToken.END_OBJECT ||
                curr == System.JSONToken.END_ARRAY) {
                depth--;
            }
        } while (depth > 0 && parser.nextToken() != null);
    }
    









    private static List<MessageData> arrayOfMessageData(System.JSONParser p) {
        List<MessageData> res = new List<MessageData>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new MessageData(p));
        }
        return res;
    }



    private static List<FILoginParametersInfo> arrayOfFILoginParametersInfo(System.JSONParser p) {
        List<FILoginParametersInfo> res = new List<FILoginParametersInfo>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new FILoginParametersInfo(p));
        }
        return res;
    }







    private static List<FIRoutingInformation> arrayOfFIRoutingInformation(System.JSONParser p) {
        List<FIRoutingInformation> res = new List<FIRoutingInformation>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new FIRoutingInformation(p));
        }
        return res;
    }





    private static List<AccountTypeInfo> arrayOfAccountTypeInfo(System.JSONParser p) {
        List<AccountTypeInfo> res = new List<AccountTypeInfo>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new AccountTypeInfo(p));
        }
        return res;
    }











}