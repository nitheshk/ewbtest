@isTest
global class ProductSelectionExtensionTest{

@isTest static void Test1(){

     Map<String, String> tdata = new Map<String, String> ();
     
     TF4SF__Application__c app = new TF4SF__Application__c();
       app.TF4SF__Product__c = 'Home Loan';
       app.TF4SF__Sub_Product__c = 'Home Loan - Short App';
       insert app;
     
            List<TF4SF__Product_Codes__c> pclist = new  List<TF4SF__Product_Codes__c>();
            TF4SF__Product_Codes__c pc1 = new TF4SF__Product_Codes__c (Name = 'Name', TF4SF__Product__c = 'Checking',
            TF4SF__Description_TextArea1__c='test1',TF4SF__Description_TextArea2__c='test2',TF4SF__Description_TextArea3__c='test3',
            TF4SF__Description_TextArea4__c='test4',TF4SF__Description_TextArea5__c='test5',Description_TextArea6__c='test6',
            TF4SF__Sub_Product__c = 'Test Sub Product', Product_Image__c = 'Test Image', Landing_Page_Description__c = ' Test Title',TF4SF__Description__c='test');
            insert pc1;
            pclist.add(pc1);
            
            TF4SF__Product_Codes__c pc = new TF4SF__Product_Codes__c();
            pc.Name='Home Loan';
            pc.TF4SF__Sub_Product__c='Home Loan';
            pc.TF4SF__Product__c='Checking';
            insert pc;
           
           
            tdata.put('id',app.Id);

            Test.startTest();
            ProductSelectionExtension ps= new ProductSelectionExtension();   
            ProductSelectionExtension.main(tdata);
            test.stopTest();
}
}