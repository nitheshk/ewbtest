global class SubmitApplication implements TF4SF.DSP_Interface {
    
    global static Map<String, String> main (Map<String, String> tdata) {
        Map<String, String> data = tdata.clone();
        String appId = tdata.get('id');
        //String kycAnswers = tdata.get('json');
        String q1responseid= tdata.get('q1responseid'); 
        String q2responseid= tdata.get('q2responseid');
        String q3responseid= tdata.get('q3responseid');
        String Token = '';
        String responseJson = '';
        String accNumber = '';
        String custNumber = '';
        TF4SF__Application__c app = [SELECT Id, Name, TF4SF__First_Name__c, TF4SF__Last_Name__c, TF4SF__Email_Address__c, TF4SF__Street_Address_2__c, TF4SF__Street_Address_1__c, TF4SF__City__c, TF4SF__State__c, TF4SF__Zip_Code__c, TF4SF__Primary_Phone_Number__c, TF4SF__User_Token__c, TF4SF__External_App_Stage__c FROM TF4SF__Application__c WHERE Id =: appId];
        TF4SF__Identity_Information__c iden = [SELECT Id, TF4SF__Application__c, TF4SF__SSN_Prime__c, TF4SF__Date_of_Birth__c FROM TF4SF__Identity_Information__c WHERE TF4SF__Application__c =: appId];
        VELOSETTINGS__C velo = VELOSETTINGS__C.getOrgDefaults();
        Token = AuthToken.Auth();  
        data.put('Token',Token);
        String kycAnswers = PrepareKYCAnswerJSON(appId, q1responseid, q2responseid, q3responseid );
        if (kycAnswers != null) {
            String Response = SubmitApp(appId, Token, kycAnswers);
            System.debug('response'+ Response);
            Map<String, String> rData = new Map<String, String>();
            
            PreSubmitWrapper wrapper = null;
			if(String.isNotBlank(Response)){
				wrapper = PreSubmitWrapper.parse(Response); 
			}
            if (wrapper != null && wrapper.Data.ApplicationStatus == 'Pending') {
				//Sending it to front end for saving
				data.put('AppStatus', 'Pending');
				data.put('CaseNumber', wrapper.Data.CaseNumber);
				//DepositPreSubmit.savekycquestions(appId,wrapper);
				if (wrapper.Data.InstantIdResponse == null) {
					data.put('Application__c.Application_Status__c', 'Submitted');
                    data.put('status', '');
				} else {
                    data.put('status', JSON.Serialize(wrapper.Data.InstantIdResponse));
                }
				data.put('Application__c.Primary_Product_Case_Id__c', wrapper.Data.CaseNumber);
				
				if (String.isNotBlank(wrapper.Data.CustomerNumber)) {
					data.put('Application__c.External_App_Stage__c', wrapper.Data.CustomerNumber);
				} else {
					data.put('Application__c.External_App_Stage__c', app.TF4SF__External_App_Stage__c);
				}
				data.put('Application__c.External_App_ID__c', wrapper.Data.AccountNumber);
				data.put('Application__c.Primary_Product_Status__c', 'Pending Review');
			}
            rData = ResponseSave(Response);
            data.put('Application__c.Primary_Product_Status__c', rData.get('ApplicationStatus'));
            data.put('Application__c.Primary_Product_Case_Id__c', rData.get('CaseNum'));
            if (String.isNotBlank(rData.get('CustNum'))) {
                data.put('Application__c.External_App_Stage__c', rData.get('CustNum'));
            } else {
                data.put('Application__c.External_App_Stage__c', app.TF4SF__External_App_Stage__c);
            }
            data.put('Application__c.External_App_Stage__c', rData.get('CustNum'));
            data.put('Application__c.External_App_ID__c', rData.get('AccNum'));
            updateApp(data);
            accNumber = rData.get('AccNum');
            custNumber = rData.get('CustNum');
        }
        

        //data.put('Response',Response);
        //SynergyDocupload syDocUpload = new SynergyDocupload();
        //syDocUpload.UploadDocs( appId, '', appId, accNumber, custNumber);
        return data;
    }

    //@future(callout=true) 
    public static void updateApp(Map<String, String> data) {
        String appId = data.get('id');
        TF4SF__Application__c app = [SELECT Id, Name, TF4SF__Primary_Product_Status__c, Primary_Product_Case_Id__c, TF4SF__External_App_ID__c, TF4SF__External_App_Stage__c,TF4SF__Email_Address__c FROM TF4SF__Application__c WHERE Id=: appId];
        app.Primary_Product_Case_Id__c = data.get('Application__c.Primary_Product_Case_Id__c');
        if (String.isBlank(app.TF4SF__External_App_Stage__c)) {
        	app.TF4SF__External_App_Stage__c = data.get('Application__c.External_App_Stage__c');
        }
        app.TF4SF__External_App_ID__c = data.get('Application__c.External_App_ID__c');
        if (String.isNotBlank(app.TF4SF__External_App_ID__c) && String.isNotBlank(app.TF4SF__External_App_Stage__c)) {
            app.TF4SF__Application_Page__c = 'StartFundingPage';
        }
        app.TF4SF__Primary_Product_Status__c = data.get('Application__c.Primary_Product_Status__c');
        update app;

        List<TF4SF__Application__c> appList = new List<TF4SF__Application__c>();
        appList.add(app);
        ApplicationTriggerHandler appHandler = new ApplicationTriggerHandler();
        if (app.TF4SF__Primary_Product_Status__c == 'Approved') {
            appHandler.sendConfirmationEmailForDeposit(appList);    
        } else if (app.TF4SF__Primary_Product_Status__c == 'Declined') {
            appHandler.sendDeclinedEmailForDeposit(appList);
        } else if (app.TF4SF__Primary_Product_Status__c == 'Pending Review') {
            appHandler.sendPendingEmailForDeposit(appList);    
        }
        
        
        
    }

    global static Map<String, String> ResponseSave(String JSONResponse) {
        Map<String, String> qdata = new Map<String, String>();
        Map<String, Object> k = (Map<String, Object>)JSON.deserializeUntyped(JSONResponse);
        Map<String, Object> dataMap = (Map<String, Object>)k.get('data');
        String ApplicationStatus = String.valueOf(dataMap.get('ApplicationStatus'));
        if (ApplicationStatus == 'Pending') {
            ApplicationStatus = 'Pending Review';
        }
        qData.put('ApplicationStatus', ApplicationStatus);
        qData.put('CaseNum', String.valueOf(dataMap.get('CaseNumber')));
        if (String.isNotBlank(String.valueOf(dataMap.get('CustomerNumber')))) {
            qData.put('CustNum', String.valueOf(dataMap.get('CustomerNumber')));
        }
        if (String.isNotBlank(String.valueOf(dataMap.get('AccountNumber')))) {
            qData.put('AccNum', String.valueOf(dataMap.get('AccountNumber')));    
        }
        //qData.put('CustNum', String.valueOf(dataMap.get('CustomerNumber')));
        
        return qdata;

    }

    global static String SubmitApp(String appId, String Token, String kycBody) {
        String result = '';
        String reqBody = '';
        String applicationId = '\'' + appId + '\'';
        //System.debug('applicationId//productnumber:'+applicationId+'--'+productnumber);
        //select all fields in Application
        queryHelper.ObjectInfo appInfo = queryHelper.selectStar('TF4SF__Application__c');
        String queryString =  appInfo.query;
        queryString += 'WHERE ID = ' + applicationId ;
        TF4SF__Application__c app = Database.query(queryString);
        String productType=app.TF4SF__Product__c;

        //this.Application = app;
        queryHelper.ObjectInfo app2Info = queryHelper.selectStar('TF4SF__Application2__c');
        String app2String =  app2Info.query;
        app2String += 'WHERE TF4SF__Application__c = ' + applicationId ;
        TF4SF__Application2__c app2 = Database.query(app2String);

        queryHelper.ObjectInfo empInfo = queryHelper.selectStar('TF4SF__Employment_Information__c');
        String employmentQuery =  empInfo.query;
        employmentQuery += 'WHERE TF4SF__Application__c = ' + applicationId;
        TF4SF__Employment_Information__c employment = Database.query(employmentQuery);
        //System.debug('employment:' + employment);

        queryHelper.ObjectInfo identityInfo = queryHelper.selectStar('TF4SF__Identity_Information__c');
        String identityQuery = identityInfo.query;
        identityQuery += 'WHERE TF4SF__Application__c = ' + applicationId;
        TF4SF__Identity_Information__c identity = Database.query(identityQuery);
        System.debug('identity:' + identity);

        queryHelper.ObjectInfo aboutAccountInfo = queryHelper.selectStar('TF4SF__About_Account__c');
        String aboutAccountQuery = aboutAccountInfo.query;
        aboutAccountQuery += 'WHERE TF4SF__Application__c = ' + applicationId;
        //System.debug('abtaccountquery:'+aboutAccountQuery);
        TF4SF__About_Account__c about = Database.query(aboutAccountQuery);
        //System.debug('about account:' + about); 

        DepositSubmit.Application application = new DepositSubmit.Application(app, app2, employment, identity, about);
        DepositSubmit tip = new DepositSubmit();
        //tip.ProcessDecisionRequest pdr = new tip.ProcessDecisionRequest(application.app.Id, Token);
        String json = tip.ProcessDecisionJSONRequest(application.app.Id, Token);
        
        //result = result.removeEnd(',');

        List<Attachment> attList = [SELECT Id, Body, ContentType, Description, Name, ParentId, CreatedDate FROM Attachment WHERE ParentId =: app.Id];
        List<TF4SF__Debug_Logs__c> trueId = [SELECT Id, TF4SF__Application__c, TF4SF__Debug_Message__c, TF4SF__Source__c, TF4SF__Timestamp__c FROM TF4SF__Debug_Logs__c WHERE TF4SF__Application__c =: app.Id AND TF4SF__Source__c = 'DriverLicenseScan Response'];    
        
        DepositSubmit.ApplicationReq applicationInfo = new DepositSubmit.ApplicationReq(application);
        DepositSubmit.CustomerInformation[] CustomerInfo = new DepositSubmit.CustomerInformation[] {new DepositSubmit.CustomerInformation(application)};
        DepositSubmit.IdVerificationRequest IdVerify = new DepositSubmit.IdVerificationRequest(application);
        DepositSubmit.IdAuthenticationResponse[] idAuthenticateInfo = new DepositSubmit.IdAuthenticationResponse[]{}; 
        //for (TF4SF__Debug_Logs__c deb : trueId) { idAuthenticateInfo.add(new DepositSubmit.IdAuthenticationResponse(deb)); }
        for (Integer i = trueId.size() - 1; i >= 0; i--) { idAuthenticateInfo.add(new DepositSubmit.IdAuthenticationResponse(trueId[i])); }
        DepositSubmit.SynergyDocument[] sygDoc = new DepositSubmit.SynergyDocument[]{};
        for (Attachment att : attList) { if (att.Name == 'Front' || att.Name == 'Back') {} else { /*sygDoc.add(new DepositSubmit.SynergyDocument(att));*/ } }


        String IdVerifyJSON = IdVerify.getIdVerifyJSON();
        String ApplicationJSON = applicationInfo.getAppJSON('submit'); 
        String CustInfoJSON = '';
        for (DepositSubmit.CustomerInformation custInfo : CustomerInfo) {
            CustInfoJSON += custInfo.getcustomerJSON();
        }
        String IdAuthJSON = '';
        IdAuthJSON += '"IdAuthenticationResponse": [';
        for (DepositSubmit.IdAuthenticationResponse idAuth : idAuthenticateInfo) {
            IdAuthJSON += idAuth.response+',';
        }
        IdAuthJSON = IdAuthJSON.removeEnd(',');
        IdAuthJSON += ']';
        String DocuReqJSON = '';
        DocuReqJSON += '"Documents": [';
        for (DepositSubmit.SynergyDocument doc : sygDoc) {  
         DocuReqJSON += doc.getDocJSON();
        }
        DocuReqJSON = DocuReqJSON.removeEnd(',');
        DocuReqJSON += '],';
        
        System.debug('Before Concatenating result --> ' + kycBody);
        //reqBody += result;
        reqBody = '{[';   
        reqBody += '"Pass": true,';
        reqBody = kycBody.removeEnd(',');
        reqBody += '},';
        //reqBody += json;
        reqBody += '{';
        reqBody += IdVerifyJSON;
        reqBody += IdAuthJSON;
        reqBody += ApplicationJSON;
        reqBody += CustInfoJSON;
        reqBody += DocuReqJSON;
        reqBody += getInstantIdCVIInformationJSON(appId); // Note: InstantIdCVIInformationJSON Method to be created to pass the InstantIdPassthrough response from preSubmit.
        reqBody += getPinPointResponseJSON(appId); // Note: PinPointResponseJSON Method to be created to pass the pinpoint response from preSubmit.
        reqBody = reqBody.removeEnd(',');
        reqBody += '}';
        reqBody += ']';
         System.debug('After Concatenating result --> ' + reqBody);
        return execute(appId, reqBody, Token, productType);
    }

    global static String getPinPointResponseJSON(String appId) {
        String ReqBody = ''; 
        try {
            List<TF4SF__Debug_Logs__c> presubmitlog = DebugLogRetrieve(appId);
            if (presubmitlog.size() > 0) {
                // TF4SF__Debug_Logs__c presubmitlog= [SELECT Id, TF4SF__Source__c, TF4SF__Debug_Message__c, TF4SF__Application__c FROM TF4SF__Debug_Logs__c WHERE TF4SF__Application__c =:appId
                //     AND TF4SF__Source__c= 'DepositPreSubmit Response' order by createdDate DESC  LIMIT 1];
                Map<String, Object> wrapper = (Map<String, Object>)JSON.deserializeUntyped(presubmitlog[0].TF4SF__Debug_Message__c);
                Map<String, Object> Data = (Map<String, Object>)wrapper.get('data');
                string pinpointResponse=JSON.Serialize(Data.get('PinpointResponse'));
                
                // PreSubmitWrapper wrapper=PreSubmitWrapper.parse(presubmitlog.TF4SF__Debug_Message__c); 
                // string pinpointResponse=JSON.Serialize(wrapper.Data.PinPointResponse);
                ReqBody = '"PinPointResponse":'+pinpointResponse; 
            }
        } catch (Exception e) {
            ReqBody = '"PinPointResponse":{}'; 
        }
        
        return ReqBody;
    }

    global static String getInstantIdCVIInformationJSON(String appId) {
        String ReqBody = ''; 
        try {
            List<TF4SF__Debug_Logs__c> presubmitlog = DebugLogRetrieve(appId);
            if (presubmitlog.size() > 0) {
                // TF4SF__Debug_Logs__c presubmitlog= [SELECT Id, TF4SF__Source__c, TF4SF__Debug_Message__c, TF4SF__Application__c FROM TF4SF__Debug_Logs__c WHERE TF4SF__Application__c =:appId
                //     AND TF4SF__Source__c= 'DepositPreSubmit Response' order by createdDate DESC  LIMIT 1];
                PreSubmitWrapper wrapper=PreSubmitWrapper.parse(presubmitlog[0].TF4SF__Debug_Message__c); 
                string InstantIdCVIInformation=JSON.Serialize(wrapper.Data.InstantIdResponse.PassThroughs);
                ReqBody = '"InstantIdCVIInformation": '+InstantIdCVIInformation+',';
            }
        } catch (Exception e) {
            ReqBody = '"InstantIdCVIInformation": [],';
        }
        return ReqBody;
    }

    global static List<TF4SF__Debug_Logs__c> DebugLogRetrieve (String appId) {
        try {
            List<TF4SF__Debug_Logs__c> debugRetrieve = new List<TF4SF__Debug_Logs__c>();
            debugRetrieve = [SELECT Id, TF4SF__Source__c, TF4SF__Debug_Message__c, TF4SF__Application__c FROM TF4SF__Debug_Logs__c WHERE TF4SF__Application__c =: appId AND TF4SF__Source__c = 'SubmitApplication Response' order by createdDate DESC  LIMIT 1];
            if (debugRetrieve.isEmpty()) {
                debugRetrieve = [SELECT Id, TF4SF__Source__c, TF4SF__Debug_Message__c, TF4SF__Application__c FROM TF4SF__Debug_Logs__c WHERE TF4SF__Application__c =: appId AND TF4SF__Source__c = 'DepositPreSubmit Response' order by createdDate DESC  LIMIT 1];
                if (debugRetrieve.isEmpty()) {
                   debugRetrieve = null; 
                }
            }
            return debugRetrieve;
        } catch (Exception e) {
            return null;
        }
    }

    global static String PrepareKYCAnswerJSON(String appId, String q1responseid, String q2responseid, String q3responseid) {
        
        String reqBody = '';
        try {
            //KYC_OOW__c kyc = [SELECT Id, KYC_Id__c, Application_Id__c, Question_1_ID__c, Question_1_Response__c, Question_2_Id__c, Question_2_Response__c, Question_3_Id__c, Question_3_Response__c, Question_4_Id__c, Question_4_Response__c, Question_5_Id__c, Question_5_Response__c, QuestionsetId__c FROM KYC_OOW__c WHERE Application_Id__c =:appId  order by createdDate DESC LIMIT 1];
            List<TF4SF__Debug_Logs__c> kyc = DebugLogRetrieve(appId);
            //TF4SF__Debug_Logs__c kyc = [SELECT Id, TF4SF__Source__c, TF4SF__Debug_Message__c, TF4SF__Application__c FROM TF4SF__Debug_Logs__c WHERE TF4SF__Application__c =: appId AND TF4SF__Source__c = 'DepositPreSubmit Response' order by createdDate DESC  LIMIT 1];
            if (kyc.size() > 0) {
                PreSubmitWrapper wrapper = PreSubmitWrapper.parse(kyc[0].TF4SF__Debug_Message__c);
                String ConversationId = '';
                String QuestionSetId = '';
                String Question_1_ID = '';
                String Question_2_ID = '';
                String Question_3_ID = '';
                if (wrapper.Data.InstantIdResponse.Products.size()>1)  {  
                        ConversationId = wrapper.data.InstantIdResponse.Status.Conversationid;               
                        for (integer q=0; q<=wrapper.Data.InstantIdResponse.Products.size();q++) {
                            String ProductType = String.valueOf(wrapper.Data.InstantIdResponse.Products[q].ProductType);
                            if ( ProductType == 'IIDQA') {
                                QuestionSetId = String.valueOf(wrapper.Data.InstantIdResponse.Products[q].QuestionSet.QuestionSetId);
                                for (integer j=0;j<wrapper.Data.InstantIdResponse.Products[q].QuestionSet.Questions.size();j++) {                    

                                    if( j==0) { 
                                        //question-1 starts here
                                        Question_1_ID = String.valueOf(wrapper.Data.InstantIdResponse.Products[q].QuestionSet.Questions[0].QuestionId);
                                    }
                                    if (j==1) {
                                        //Question-2 starts here
                                        Question_2_ID = String.valueOf(wrapper.Data.InstantIdResponse.Products[q].QuestionSet.Questions[1].QuestionId);
                                    }
                                    if(j==2) {
                                        //Question-3 starts here
                                        Question_3_ID = String.valueOf(wrapper.Data.InstantIdResponse.Products[q].QuestionSet.Questions[2].QuestionId);
                                    }
                                }  
                                break;
                            }
                        }
                    }
                
                
                //if (String.isNotBlank(q1responseid)) { kyc.Question_1_Response__c = q1responseid; }
                //if (String.isNotBlank(q2responseid)) { kyc.Question_2_Response__c = q2responseid; }
                //if (String.isNotBlank(q3responseid)) { kyc.Question_3_Response__c = q3responseid; }
                
                //update kyc;
                reqBody = '[{';
                reqBody += '"Settings":{'; 
                reqBody += '"AccountNumber":"45065",';
                reqBody += '"Mode":"testing",';
                reqBody += '"ConversationId":"'+ConversationId+'",';
                reqBody += '"Locale":"en_US"';
                reqBody += '},';
                reqBody += '"Answers":{';
                reqBody += '"QuestionSetId":'+QuestionsetId+',';
                reqBody += '"Questions":[';
                reqBody += '{';
                    reqBody += '"QuestionId":'+Question_1_ID+',';
                    reqBody += '"Choices":[';
                    reqBody += '{';
                    reqBody += '"Choice":"'+q1responseid+'"';
                    reqBody += '}';
                    reqBody += ']';
                reqBody += '},';
                if (String.isNotBlank(q2responseid)) { 
                    reqBody += '{';
                        reqBody += '"QuestionId":'+Question_2_ID+',';
                        reqBody += '"Choices":[';
                        reqBody += '{';
                        reqBody += '"Choice":"'+q2responseid+'"';
                        reqBody += '}';
                        reqBody += ']';
                    reqBody += '},';
                }
                if (String.isNotBlank(q3responseid)) { 
                    reqBody += '{';
                        reqBody += '"QuestionId":'+Question_3_ID+',';
                        reqBody += '"Choices":[';
                        reqBody += '{';
                        reqBody += '"Choice":"'+q3responseid+'"';
                        reqBody += '}';
                        reqBody += ']';
                    reqBody += '},';
                }
                reqBody = reqBody.removeEnd(',');
                reqBody += ']';
                reqBody += '},';
                reqBody += '"RequestUUID":"'+appId+'",';
                system.debug('PrepareKYCAnswerJSON--> '+reqBody);
            }
        } catch (Exception e) {
            reqBody = '';
        }
            
          
        return reqBody;
    }

    private static String execute(String appId, String json, String Token,String productType) {
        System.debug('appId, JSON: ' + appId  + '---' + json);
        String responseJson = '';
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        VELOSETTINGS__C velo = VELOSETTINGS__C.getOrgDefaults();
        Integer timeOut = Integer.ValueOf(velo.ProcessAPI_Timeout__c);
        String GUID = GUIDGenerator.getGUID();
        req.setEndpoint(velo.Endpoint__c+'api/process/submit/submission');
        req.setHeader(velo.Request_GUID_Name__c,GUID);
        req.setTimeout(timeOut);
        req.setHeader('Authorization','Bearer '+Token);
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Accept','application/json');
        req.setMethod('POST');
        
        req.setBody(json);
        HttpResponse res = h.send(req);
        responseJson = res.getBody();
        system.debug('Res is '+res.getbody());
        DebugLogger.InsertDebugLog(appId, GUID, 'SubmitApplication Request GUID');
        DebugLogger.InsertDebugLog(appId, res.getBody(), 'SubmitApplication Response');  
        return responseJSON;
    }

}