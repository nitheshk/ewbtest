@isTest
public class PreQualificationLetterController_Test {
    static testmethod void validateStandardController(){
    TF4SF__Application__c ap = new TF4SF__Application__c();
    ap.TF4SF__Product__c = 'Home Loan';
        ap.TF4SF__First_Name__c='test';
        ap.TF4SF__Last_Name__c = 'Test';
        ap.TF4SF__Email_Address__c= 'test@test.com';
        ap.TF4SF__Sub_Product__c = 'Home Loan - Short App';
    insert ap;
    
    TF4SF__About_Account__c ac = new TF4SF__About_Account__c();
    //ac.Name = 'tests';
    ac.TF4SF__Application__c  = ap.id;
    insert ac;
        ApexPages.StandardController sc = new ApexPages.StandardController(ac);
        PreQualificationLetterController testAccPlan = new PreQualificationLetterController(sc);
        
        PageReference pageRef = Page.Prequalification_Letter;
        pageRef.getParameters().put('id', String.valueOf(ac.Id));
        Test.setCurrentPage(pageRef);
        System.currentPageReference().getParameters().put('p', 'pdf');

        testAccPlan.getChooserender();
        PreQualificationLetterController.generatePDF(ap.id);
        }
}