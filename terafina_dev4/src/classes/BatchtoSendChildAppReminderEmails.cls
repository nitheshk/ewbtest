global class BatchtoSendChildAppReminderEmails implements
Database.Batchable <sObject> , Database.Stateful, Database.AllowsCallouts { 


  global Database.QueryLocator start(Database.BatchableContext bc) {
    
    String query =  'SELECT Id, Email_Address__c, First_Name__c,Last_Name__c,';
           query += ' Email_Sent__c,Reminder_Email_1_Sent__c, Reminder_Email_1_Qualify__c,Application__r.TF4SF__Email_Address__c,Application__r.TF4SF__Custom_Text2__c';           
           query += ' FROM Trustee_Guarantor__c WHERE Email_Sent__c= True AND Reminder_Email_1_Sent__c = False';
           query += ' AND Reminder_Email_1_Qualify__c >= 1 ';
        return Database.getQueryLocator(query);      
  }

  global void execute(Database.BatchableContext bc, List<Trustee_Guarantor__c> scope) {
    
    //process each batch of records
    try {        
                 
          //Call Send reminder email function
          ChildApplication.sendReminderEmail(scope,'First');
          
        if(Test.isRunningTest()){integer a = 10/0;}
      
    } catch (exception e) {
        System.debug('Exception in BatchtoSendChildAppReminderEmails ==>' + e.getMessage());
    } finally {
     
    }
  }

  global void finish(Database.BatchableContext bc) {

  }

 
   
}