@IsTest
public class FAQ_Search_Test {
    
    @IsTest
    public static void searchTest(){
         FAQ__c fq= new FAQ__c (Title__c='testing',Description__c='checking');
         Insert fq;
       
        FAQ_Search sc = new FAQ_Search();
        sc.searchString='test';
        sc.search();
        Test.setCurrentPageReference(new PageReference('Page.FAQ')); 
        
        System.currentPageReference().getParameters().put('q', 'test');
        System.currentPageReference().getParameters().put('test', fq.id);
        System.currentPageReference().getParameters().put('id', fq.id);
        sc.showAllArticles();
        //sc.showArticle();
        FAQ_Search.checkCookie(fq.id,'test');
            
    }
      @IsTest
      public static void searchTest1(){
         FAQ__c fq= new FAQ__c (Title__c='testing',Description__c='checking',View_Count__c=2);
         Insert fq;
        FAQ_Search sc = new FAQ_Search();
       Test.setCurrentPageReference(new PageReference('Page.FAQ')); 
        System.currentPageReference().getParameters().put('test', fq.id);
        System.currentPageReference().getParameters().put('q', 'test');
        sc.search();
       
            
    }
     @IsTest
    public static void searchTest2(){
         FAQ__c fq= new FAQ__c (Title__c='testing',Description__c='checking',View_Count__c=2);
         Insert fq;
       
        FAQ_Search sc = new FAQ_Search();
        sc.searchString='test';
        sc.search();
       
        sc.showAllArticles();
        //sc.showArticle();
            
    }
    
    
    
    

}