@isTest
public class DepositSubmitTest{

@isTest static void Test_method_one(){
    
    String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
    Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
    User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
              EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
              LocaleSidKey = 'en_US', ProfileId = p.Id,
              TimeZoneSidKey = 'America/Los_Angeles',
              UserName = uniqueUserName,TF4SF__Channel__c='Online',TF4SF__Location__c='Yakima South 1st St');
    System.runAs(u) {

  TF4SF__Customer__c cus=new TF4SF__Customer__c();
    insert cus;
        
       TF4SF__Application_Configuration__c appConfig = new TF4SF__Application_Configuration__c();
       appConfig.TF4SF__key__c ='YMmKJt5QxS7KlAEASMsj4Q==';
       appConfig.TF4SF__Timeout_Seconds__c=900;
       appConfig.Name = 'key';
       appConfig.TF4SF__Application_Code__c='DS';
       appConfig.TF4SF__Theme__c='DSP';
       insert appConfig;
       
     TF4SF__Application__c app = new TF4SF__Application__c ();
        
  		app.TF4SF__Application_Status__c = 'Open';
        app.TF4SF__Product__c='checking';
        app.TF4SF__Sub_Product__c='checking-Home Loan';
        app.TF4SF__User_Token__c='AnxzH/+Kmj85t2Wzkj5qETFWtwltJXsC5dcjGEgtGa89TTX4OIYGRky9UDclje65';  
        app.TF4SF__First_Name__c='TestFirst'; 
        app.TF4SF__Middle_Name__c='TestMiddle'; 
        app.TF4SF__Last_Name__c ='TestLast';
        app.TF4SF__Suffix__c ='TestSuffix';
        app.TF4SF__Street_Address_1__c = 'TestStreet1';
        app.TF4SF__Street_Address_2__c = 'TestStreet2';
        app.TF4SF__City__c = 'TestCity';
        app.TF4SF__State__c = 'TestState';
        app.TF4SF__Zip_Code__c = '44783';
        app.Ownerid=u.id;
        app.TF4SF__Primary_Phone_Number__c ='4848894344';
        app.TF4SF__Secondary_Phone_Number__c ='4443557847';
        app.TF4SF__Email_Address__c ='test@gmail.com';
        app.TF4SF__IP_Address__c = 'ipaddress';
        app.TF4SF__Current_User_Email_Address__c='standarduser@testorg.com';
        app.TF4SF__Current_Channel__c ='Online';
        app.TF4SF__Promo_Code__c='4355435';
        app.TF4SF__Current_Person__c=u.Id;
        app.TF4SF__External_App_Stage__c='455';
        app.TF4SF__IP_Address__c='test';
        app.Primary_Product_Case_Id__c='3244234';
        app.Primary_Product_Case_Status__c='open';
        app.TF4SF__User_Agent__c=u.id;
        app.TF4SF__Created_Person__c=u.id;
        app.TF4SF__Current_Branch_Name__c ='Home';
        app.TF4SF__User_Token_Expires__c=System.now();
        app.TF4SF__Current_timestamp__c=System.now(); 
        app.TF4SF__Country_Code__c = '+1';
        insert app;
    
    List<TF4SF__Debug_Logs__c> debuglst=new List<TF4SF__Debug_Logs__c>();
    TF4SF__Debug_Logs__c debug=new TF4SF__Debug_Logs__c();
    debug.TF4SF__Application__c=app.id;
    debug.TF4SF__Debug_Message__c='{"data":{"Status":{"ConversationId":"31000105588375","RequestId":"300834245","TransactionStatus":"passed","Reference":"payload.RequestUUID"},"Products":[{"ProductType":"TrueID","ExecutedStepName":"True_ID_Step","ProductConfigurationName":"TrueID_Flow","ProductStatus":"pass"}],"PassThroughs":[{"Type":"true.id","Data":""}]},"result":{"success":true,"code":"200","message":"Success"}}';
    debug.TF4SF__Source__c='DriverLicenseScan Response';
    insert debug;
    //debuglst.add(debug);
        
        TF4SF__Application2__c app2=new TF4SF__Application2__c();
        app2.TF4SF__Application__c=app.id;
        insert app2;
        
        TF4SF__About_Account__c acc=new TF4SF__About_Account__c();
        acc.TF4SF__Application__c=app.id;
        insert acc;
    
         TF4SF__Application_Activity__c appact=new TF4SF__Application_Activity__c();
            appact.TF4SF__Application__c=app.Id;
            appact.TF4SF__Action__c='Resumed the Application';
            appact.TF4SF__Activity_Time__c = System.now();
            appact.TF4SF__Channel__c='online';
            appact.TF4SF__Branch__c='US';  
            appact.TF4SF__Name__c =u.id;
            insert appact;
        
     TF4SF__Identity_Information__c iden = new TF4SF__Identity_Information__c ();
        iden.TF4SF__Application__c = app.id;
        iden.TF4SF__SSN_Prime__c='5656576786';
        iden.Custom_Picklist12__c='Yes';
        iden.TF4SF__Custom_Text17__c='Yes';
        iden.TF4SF__Date_of_Birth__c='07/26/1973';
        iden.TF4SF__Issue_Date__c='07/26/2002';
        iden.TF4SF__Expiry_Date__c='07/26/2020';
        iden.dual_citizenship_country__c = 'India';
        insert iden;
        
     TF4SF__Employment_Information__c em = new TF4SF__Employment_Information__c();
        em.TF4SF__Application__c = app.id;
        em.TF4SF__Occupation__c='test';
        insert em;
        
     TF4SF__Debug_Logs__c db = new TF4SF__Debug_Logs__c();
        db.TF4SF__Application__c = app.id;
        insert db;
        
        List<TF4SF__Product_Codes__c> pclst=new List<TF4SF__Product_Codes__c>();
        TF4SF__Product_Codes__c pc=new TF4SF__Product_Codes__c();
        pc.TF4SF__Sub_Product_Code__c='Checking';
        pc.TF4SF__Sub_Product__c='checking';
        pc.TF4SF__Product__c='Homeloan';
        pc.Product_Code_Value__c='test';
        pc.Name='Test';
        insert pc;
        pclst.add(pc);
    
    
       VELOSETTINGS__C vo = new VELOSETTINGS__C();
        vo.Name='test';
        vo.TokenEndPoint__c = 'rsgsdgsdf';
        vo.Request_GUID_Name__c = 'RequestUUID';
        vo.ProcessAPI_Timeout__c = 120000;
        insert vo;
    
    
     List<Attachment> attList=new  List<Attachment>();
    Blob b = Blob.valueOf('Test Data');
        Attachment att = new Attachment();
        att.name = 'Front';
        att.Body = b;
        att.Description = 'test';
        att.ParentId = app.Id;
        att.ContentType = 'application/pdf';
        insert(att);
        attList.add(att);

    
     test.StartTest();
        Test.setMock(HttpCalloutMock.class, new DepositSubmitMock()); 
      	DepositSubmit dp=new DepositSubmit();
      // DepositSubmit.execute(app.id,json, 'AnxzH/+Kmj85t2Wzkj5qETFWtwltJXsC5dcjGEgtGa89TTX4OIYGRky9UDclje65');
       //dp.ProcessDecisionJSONRequest(app.id,'AnxzH/+Kmj85t2Wzkj5qETFWtwltJXsC5dcjGEgtGa89TTX4OIYGRky9UDclje65');
       dp.ProcessDecision(app.id,'AnxzH/+Kmj85t2Wzkj5qETFWtwltJXsC5dcjGEgtGa89TTX4OIYGRky9UDclje65');
      test.StopTest();
     }
}
}