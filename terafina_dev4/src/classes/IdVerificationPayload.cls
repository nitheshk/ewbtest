global class IdVerificationPayload{
    global IdVerificationRequest IdVerificationRequest;
    global class IdVerificationRequest {
        public boolean AcceptTruliooTermsAndConditions;
        public boolean CleansedAddress;
        public boolean VerboseMode;
        public String ConfigurationName;    //Identity Verification
        public String CountryCode;  //CN
        public DataFields DataFields;
    }
    global class DataFields {
        public PersonInfo PersonInfo;
        public Communication Communication;
        public NationalIds[] NationalIds;
    }
    global class PersonInfo {
        public Integer DayOfBirth;  //5
        public Integer MonthOfBirth;    //6
        public Integer YearOfBirth; //1976
        public AdditionalFields AdditionalFields;
    }
    global class AdditionalFields {
        public String FullName; //胡言明
    }
    global class Communication {
        public String MobileNumber; //8615948058868
    }
    global class NationalIds {
        public String IDNumber; //440861896421345987
        public String Type; //NationalID
    }
    public static IdVerificationPayload parse(String json){
        return (IdVerificationPayload) System.JSON.deserialize(json, IdVerificationPayload.class);
    }

    
}