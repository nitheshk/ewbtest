global with sharing class W8TaxFormController{

  public String fullName {get; set;}
  public String nRAStreetAddress {get; set;}  
  public String nRACity {get; set;}  
  public String nRAState {get; set;}  
  public String nRAZipCode {get; set;}  
  public String nRANailingStreetAddress {get; set;}  
  public String nRANailingCity {get; set;}  
  public String nRANailingStateNRA {get; set;}  
  public String nRANailingZipCode {get; set;}  
  public String nRAMailingCountry {get; set;}  
  public String nRACitizenshipCountry {get; set;}  
  public String dateOfBirth {get; set;}  
  public String sSnPrime {get; set;}  
  public String country {get; set;}  
  public String nRAForeignTaxIdentification {get; set;}  
  public String currentDate {get; set;} 
  public String beneficialRec {get;set;} 
  
    global void onload(){

    }

  public String getRenderAs()
  {
    string renderAs=ApexPages.currentPage().getParameters().get('p');
    if(renderAs!=null & renderAs!='')
       return renderAs;
       else
       return null; 
  }

    global W8TaxFormController() {  
   
    String appId = ApexPages.currentPage().getParameters().get('id');
    beneficialRec='China'; 
    
    if(appId!=null && appId!='')
    {
    List<TF4SF__Application__c> appList;     
    
            
      if (!String.isBlank(appId)) {
      appList = [SELECT Id, TF4SF__Full_Name_PA__c,
      TF4SF__Street_Address_1__c,TF4SF__City__c,State_NRA__c,Zip_Code_NRA__c,TF4SF__State__c,
      TF4SF__Mailing_Street_Address_1__c,TF4SF__Mailing_City__c,Mailing_State_NRA__c,Mailing_Zip_Code_NRA__c,
      Mailing_Country_NRA__c,Custom_Text17__c
       ,(SELECT Id,Country__c,NRA_Citizenship_Country__c,TF4SF__SSN_Prime__c,TF4SF__Date_of_Birth__c FROM TF4SF__Identity_Information__r) 
      FROM TF4SF__Application__c WHERE Id = :appId];
      if (appList.size() > 0) {
      
        currentDate=string.valueof(System.Now());
        
        fullName = appList[0].TF4SF__Full_Name_PA__c;
 
        nRAStreetAddress=appList[0].TF4SF__Street_Address_1__c;
        nRACity=appList[0].TF4SF__City__c;
        nRAState=appList[0].State_NRA__c;
        nRAZipCode=appList[0].Zip_Code_NRA__c;
        
        nRANailingStreetAddress=appList[0].TF4SF__Mailing_Street_Address_1__c;
        nRANailingCity=appList[0].TF4SF__Mailing_City__c;
        nRANailingStateNRA=appList[0].Mailing_State_NRA__c;
        nRANailingZipCode=appList[0].Mailing_Zip_Code_NRA__c;
        nRAMailingCountry=appList[0].Mailing_Country_NRA__c;
 
        nRAForeignTaxIdentification=appList[0].Custom_Text17__c; //Foreign tax identifying number

          
         if (appList[0].TF4SF__Identity_Information__r != null) {
             TF4SF__Identity_Information__c ii = appList[0].TF4SF__Identity_Information__r;
                nRACitizenshipCountry=ii.NRA_Citizenship_Country__c;
                dateOfBirth=ii.TF4SF__Date_of_Birth__c;
                sSnPrime=ii.TF4SF__SSN_Prime__c;
                country=ii.Country__c;
         }
      }
    }  
       
    }
    
   }   
   
    
}