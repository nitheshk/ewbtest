@isTest(SeeAllData=true)

public with sharing class OLBSimulatorTest{

@isTest public static void Test1(){

  String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
            EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US', ProfileId = p.Id,
            TimeZoneSidKey = 'America/Los_Angeles',
            UserName = uniqueUserName,TF4SF__Location__c='Yakima South 1st St');
            
        
        System.runAs(u) {
        
     TF4SF__Application__c app1 = new TF4SF__Application__c(TF4SF__Created_User_Email_Address__c='test@test.com',TF4SF__State__c='katest',TF4SF__Zip_Code__c='591287',TF4SF__City__c='testing',TF4SF__Street_Address_2__c='1234 street',TF4SF__Is_Member__c=true,TF4SF__Street_Address_1__c = '123 That Street', TF4SF__First_Name__c = 'Test Account',TF4SF__Email_Address__c='abc@test.com',
          TF4SF__Middle_Name__c='Test account1',TF4SF__Last_Name__c='test account3',TF4SF__Primary_Phone_Number__c='1234567890',
          TF4SF__Product__c = 'Checking', TF4SF__Sub_Product__c = 'Checking - Student Checking',TF4SF__Person_Number__c='1264758056',
          OwnerId = u.Id, TF4SF__Current_Person__c = u.Id, TF4SF__Created_Channel__c = 'Branch', TF4SF__Current_Channel__c = 'Branch',
          TF4SF__Created_Branch_Name__c = 'Home', TF4SF__Created_Person__c = u.Id, TF4SF__Current_Branch_Name__c = 'Home',
          TF4SF__Application_Page__c = 'CrossSellPage', TF4SF__Type_of_Checking__c = 'Checking - Checking Test',
          TF4SF__Type_of_Certificates__c = 'Certificates - 12 months', TF4SF__Type_of_Credit_Cards__c = 'Credit Cards - Advanced',
          TF4SF__Type_of_Personal_Loans__c = 'Personal Loans - Personal Loan', TF4SF__Type_of_Savings__c = 'Savings - Smart Savings',
          TF4SF__Type_of_Vehicle_Loans__c = 'Vehicle Loans -Vehicle Loan', TF4SF__User_Token__c = 'AnxzH/+Kmj85t2Wzkj5qETFWtwltJXsC5dcjGEgtGa89TTX4OIYGRky9UDclje65',
          TF4SF__Current_Timestamp__c = System.now());
          insert app1;
         

            List<TF4SF__Product_Codes__c> pclist = new  List<TF4SF__Product_Codes__c>();
            TF4SF__Product_Codes__c pc1 = new TF4SF__Product_Codes__c (Name = 'Name', TF4SF__Product__c = 'Checking',
            TF4SF__Sub_Product__c = 'Test Sub Product', Product_Image__c = 'Test Image',
            Landing_Page_Description__c = ' Test Title');
            insert pc1;
            pclist.add(pc1);
            
            TF4SF__Application_Activity__c acc=new TF4SF__Application_Activity__c();
            insert acc;
            
        test.startTest();
        
       OLBSimulator olb=new OLBSimulator();
       OLBSimulator.encryptJSON('json'); 
       test.stopTest();

}
}
    @isTest public static void Test2(){

  String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
            EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US', ProfileId = p.Id,
            TimeZoneSidKey = 'America/Los_Angeles',
            UserName = uniqueUserName,TF4SF__Location__c='Yakima South 1st St');
            
        
        System.runAs(u) {
        
         TF4SF__Application__c app1 = new TF4SF__Application__c(TF4SF__Created_User_Email_Address__c='test@test.com',TF4SF__State__c='katest',TF4SF__Zip_Code__c='591287',TF4SF__City__c='testing',TF4SF__Street_Address_2__c='1234 street',TF4SF__Is_Member__c=true,TF4SF__Street_Address_1__c = '123 That Street', TF4SF__First_Name__c = 'Test Account',TF4SF__Email_Address__c='abc@test.com',
                TF4SF__Middle_Name__c='Test account1',TF4SF__Last_Name__c='test account3',TF4SF__Primary_Phone_Number__c='1234567890',
                TF4SF__Product__c = 'Checking', TF4SF__Sub_Product__c = 'Checking - Student Checking',TF4SF__Person_Number__c='1264758056',
                OwnerId = u.Id, TF4SF__Current_Person__c = u.Id, TF4SF__Created_Channel__c = 'Branch', TF4SF__Current_Channel__c = 'Branch',
                TF4SF__Created_Branch_Name__c = 'Home', TF4SF__Created_Person__c = u.Id, TF4SF__Current_Branch_Name__c = 'Home',
                TF4SF__Application_Page__c = 'CrossSellPage', TF4SF__Type_of_Checking__c = 'Checking - Checking Test',
                TF4SF__Type_of_Certificates__c = 'Certificates - 12 months', TF4SF__Type_of_Credit_Cards__c = 'Credit Cards - Advanced',
                TF4SF__Type_of_Personal_Loans__c = 'Personal Loans - Personal Loan', TF4SF__Type_of_Savings__c = 'Savings - Smart Savings',
                TF4SF__Type_of_Vehicle_Loans__c = 'Vehicle Loans -Vehicle Loan', TF4SF__User_Token__c = 'AnxzH/+Kmj85t2Wzkj5qETFWtwltJXsC5dcjGEgtGa89TTX4OIYGRky9UDclje65',
                TF4SF__Current_Timestamp__c = System.now());
insert app1;

            List<TF4SF__Product_Codes__c> pclist = new  List<TF4SF__Product_Codes__c>();
            Set<TF4SF__Product_Codes__c> subProductSet = new Set<TF4SF__Product_Codes__c>();

            TF4SF__Product_Codes__c pc1 = new TF4SF__Product_Codes__c (Name = 'Name', TF4SF__Product__c = 'Checking',
            TF4SF__Sub_Product__c = 'Test Sub Product', Product_Image__c = 'Test Image',
            Landing_Page_Description__c = ' Test Title');
            insert pc1;
            pclist.add(pc1);
            
            TF4SF__Application_Activity__c acc=new TF4SF__Application_Activity__c();
            insert acc;
            
        test.startTest();
        
       OLBSimulator olb=new OLBSimulator();
       OLBSimulator.decryptData('data');
       olb.getProducts();   
       OLBSimulator.getSubProducts('selectedProduct');
       test.stopTest();

}
}
}