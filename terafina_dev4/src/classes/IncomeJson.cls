public class IncomeJson {

    public class PA {
        public String Id;
        public String Applicant_Type_c;
        public String Name_c;
        public String Income_c;
        public String Application_c;
        public String Type_c ;
        public String Address_1_c;
        public String Address_2_c;
        public String Alimony_Answer_c;
        public String Associate_Dues_c;
        public String Bonus_c;
        public String Business_Name_c;
        public String Business_Type_c;
        public String City_c;
        public String Description_c;
        public String Employer_Name_c;
        public String Income_Type_c;
        public String Job_title_c;
        public String Employee_To_c;
        public String Monthly_Income_c;
        public String Monthly_Rent_c;
        public String Mortgage_Payment_c;
        public String No_of_Mortgage_c;
        public String Occupation_c;
        public String Percent_of_Ownership_c;
        public String Phone_Number_c;
        public String Property_Type_c;
        public String Start_Date_c;
        public String State_c;
        public String Tax_Id_c;                         
        public String Value_of_the_Property_c;
        public String Employee_From_c;
        public String Years_in_professional_c;
        public String Zip_code_c;
        public String Over_Time_c;
        public String Commission_c;
    }


    public class J1 {
        public String Id;
        public String Applicant_Type_c;
        public String Name_c;
        public String Income_c;
        public String Application_c;
        public String Type_c ;
        public String Address_1_c;
        public String Address_2_c;
        public String Alimony_Answer_c;
        public String Associate_Dues_c;
        public String Bonus_c;
        public String Business_Name_c;
        public String Business_Type_c;
        public String City_c;
        public String Description_c;
        public String Employer_Name_c;
        public String Income_Type_c;
        public String Job_title_c;
        public String Employee_To_c;
        public String Monthly_Income_c;
        public String Monthly_Rent_c;
        public String Mortgage_Payment_c;
        public String No_of_Mortgage_c;
        public String Occupation_c;
        public String Percent_of_Ownership_c;
        public String Phone_Number_c;
        public String Property_Type_c;
        public String Start_Date_c;
        public String State_c;
        public String Tax_Id_c;                         
        public String Value_of_the_Property_c;
        public String Employee_From_c;
        public String Years_in_professional_c;
        public String Zip_code_c;
        public String Over_Time_c;
        public String Commission_c;
    }
    
    public class J2 {
        public String Id;
        public String Applicant_Type_c;
        public String Name_c;
        public String Income_c;
        public String Application_c;
        public String Type_c ;
        public String Address_1_c;
        public String Address_2_c;
        public String Alimony_Answer_c;
        public String Associate_Dues_c;
        public String Bonus_c;
        public String Business_Name_c;
        public String Business_Type_c;
        public String City_c;
        public String Description_c;
        public String Employer_Name_c;
        public String Income_Type_c;
        public String Job_title_c;
        public String Employee_To_c;
        public String Monthly_Income_c;
        public String Monthly_Rent_c;
        public String Mortgage_Payment_c;
        public String No_of_Mortgage_c;
        public String Occupation_c;
        public String Percent_of_Ownership_c;
        public String Phone_Number_c;
        public String Property_Type_c;
        public String Start_Date_c;
        public String State_c;
        public String Tax_Id_c;                         
        public String Value_of_the_Property_c;
        public String Employee_From_c;
        public String Years_in_professional_c;
        public String Zip_code_c;
        public String Over_Time_c;
        public String Commission_c;
    }
    
    public class J3 {
        public String Id;
        public String Applicant_Type_c;
        public String Name_c;
        public String Income_c;
        public String Application_c;
        public String Type_c ;
        public String Address_1_c;
        public String Address_2_c;
        public String Alimony_Answer_c;
        public String Associate_Dues_c;
        public String Bonus_c;
        public String Business_Name_c;
        public String Business_Type_c;
        public String City_c;
        public String Description_c;
        public String Employer_Name_c;
        public String Income_Type_c;
        public String Job_title_c;
        public String Employee_To_c;
        public String Monthly_Income_c;
        public String Monthly_Rent_c;
        public String Mortgage_Payment_c;
        public String No_of_Mortgage_c;
        public String Occupation_c;
        public String Percent_of_Ownership_c;
        public String Phone_Number_c;
        public String Property_Type_c;
        public String Start_Date_c;
        public String State_c;
        public String Tax_Id_c;                         
        public String Value_of_the_Property_c;
        public String Employee_From_c;
        public String Years_in_professional_c;
        public String Zip_code_c;
        public String Over_Time_c;
        public String Commission_c;
    }
    
    
    
     public PA PA;
     public J1 J1;
     public J2 J2;
     public J3 J3;

        
    public static List<IncomeJson> parse(String json) {
        return (List<IncomeJson>) System.JSON.deserialize(json, List<IncomeJson>.class);
    }
}