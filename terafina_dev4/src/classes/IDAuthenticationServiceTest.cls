@isTest
private class IDAuthenticationServiceTest {
	
	@isTest static void testIdAuthService() {
		
		IDAuthRequest req = new IDAuthRequest();
		IDAuthResponse response;
		try {

                  VELOSETTINGS__C vo = new VELOSETTINGS__C();
                  vo.Name='test';
                  vo.Request_GUID_Name__c = 'RequestUUID';
                  vo.ProcessAPI_Timeout__c = 120000;
                  vo.ClientId__c = '8er2cx5d37h96GtJk9b9';
                  vo.ClientSecret__c = '8er2cx5d37h96GtJk9b9';
                  vo.EndPoint__c = 'https://devopenapi.velobank.com/';
                  vo.FundingEndpoint__c ='https://devopenapi.velobank.com/';
                  vo.TokenEndPoint__c = 'https://openapiservices.eastwestbank.com/AuthorizationServer/api/token/client/jwt';
                  insert vo;

                  req.applicationSfdcID = 'a044D000001GRsQQAW';
                  req.setConfigurationName('Identity Verification');
                  //Intetionally left to decide for future release to come country code from UI
                  req.setCountryCode('US');
                  req.setAcceptTruliooTermsAndConditions(true);
                  IDAuthRequest.DataFields DataFields = new IDAuthRequest.DataFields();
                  IDAuthRequest.Document Document = new IDAuthRequest.Document();
                  Document.setDocumentType('Passport');

                  Document.setDocumentBackImage('');
                  Document.setDocumentFrontImage('');
                  DataFields.setDocument(Document);
                  req.setDataFields(DataFields);

                  response=IDAuthenticationService.callIdAuthService(req);
           }
           catch(Exception ex) {
           	System.assert(true);
           }
	}
	
}