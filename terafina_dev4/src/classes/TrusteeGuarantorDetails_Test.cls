@isTest
public class TrusteeGuarantorDetails_Test {

  static testMethod void test1() {
 
     String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
     Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
     User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
         EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
         LocaleSidKey = 'en_US', ProfileId = p.Id,
         TimeZoneSidKey = 'America/Los_Angeles',
         UserName = uniqueUserName,TF4SF__Location__c='Yakima South 1st St');
            
        /*
        TF4SF__Application_Configuration__c appConfig = new TF4SF__Application_Configuration__c();
        appConfig.TF4SF__Application_Code__c = 'dsp4_0';
        appConfig.TF4SF__Theme__c = 'dsp4_0';
        appConfig.TF4SF__Key__c = 'YMmKJt5QxS7KlAEASMsj4Q==';
        appConfig.TF4SF__Popup_Seconds__c = 120;
        appConfig.TF4SF__Timeout_Seconds__c = 900;
      
        insert appConfig;   
		  */
        System.runAs(u) {
        
         TF4SF__Customer__c cust = new TF4SF__Customer__c();
        cust.TF4SF__Username__c = 'TestingDeclined';
        insert cust;
        
        
        TF4SF__Application__c app1 = new TF4SF__Application__c(TF4SF__Customer__c=cust.id,TF4SF__Primary_Phone_Number__c='(509)491-8025',TF4SF__Email_Address__c='charo509@yahoo.com',TF4SF__First_Name__c='TestFirst',TF4SF__Middle_Name__c='TestMiddel',TF4SF__Last_Name__c='TestLast',TF4SF__Product__c = 'Checking', TF4SF__Sub_Product__c = 'Test Sub Product',
                OwnerId = u.Id, TF4SF__Current_Person__c = u.Id, TF4SF__Created_Channel__c = 'Branch', TF4SF__Current_Channel__c = 'Branch',
                TF4SF__Created_Branch_Name__c = 'Walla Walla Tietan', TF4SF__Created_Person__c = u.Id, TF4SF__Current_Branch_Name__c = 'WallaWa',
                TF4SF__Application_Page__c = 'CrossSellPage', TF4SF__Type_of_Checking__c = 'Test',
                                                                   
                TF4SF__Type_of_Certificates__c = 'Certificates - 12 months', TF4SF__Type_of_Credit_Cards__c = 'Credit Cards - Advanced',
                TF4SF__Type_of_Personal_Loans__c = 'Personal Loans - Personal Loan', TF4SF__Type_of_Savings__c = 'Savings - Smart Savings',
                TF4SF__Type_of_Vehicle_Loans__c = 'Vehicle Loans -Vehicle Loan', TF4SF__User_Token__c = 'AnxzH/+Kmj85t2Wzkj5qETFWtwltJXsC5dcjGEgtGa89TTX4OIYGRky9UDclje65',
                TF4SF__Street_Address_1__c='828 S 8th Ave',TF4SF__Street_Address_2__c='828 S 8th Ave',TF4SF__City__c='Pasco',TF4SF__State__c='WA',TF4SF__Zip_Code__c='99301-5730',
                TF4SF__Created_User_Email_Address__c='athul.kakathkar@terafinainc.com',TF4SF__Current_User_Email_Address__c='athul.kakathkar@terafinainc.com',
                TF4SF__Application_Status__c='Open',TF4SF__Sub_Product_Description__c='testdesc');
            insert app1;
            
            
            TF4SF__Application_Activity__c appact=new TF4SF__Application_Activity__c();
            appact.TF4SF__Application__c=app1.Id;
            appact.TF4SF__Action__c='Created the Application';
            appact.TF4SF__Activity_Time__c = System.now();
            insert appact;
    
      Test.StartTest();

      Trustee_Guarantor__c lstInsert = new Trustee_Guarantor__c();
      Map<String, String> tdata = new Map<String, String>();
             
      tdata.put('Id',app1.Id);
      tdata.put('Action','Upsert');
      
      tdata.put('Trustee_Guarantor_PA_Data','[{"PA":{"First_Name_c":"TestFirst","Last_Name_c":"TestLast","Middle_Name__c ":"TestMiddel","Suffix_c":"Mr","Type_c":"PA","Phone_Number_c":"976543443","Email_Address_c":"test@gmail.com","Address_c":"testAdress1","Address2_c":"testaddress2","City_c":"TestCity","State_c":"TestState","Zip_Code_c":"546777"}}]');
      tdata.put('Trustee_Guarantor_J1_Data','[{"J1":{"First_Name__c":"TestFirst","Last_Name_c":"TestLast","Middle_Name__c ":"TestMiddel","Suffix_c":"Mr","Type_c":"PA","Phone_Number_c":"976543443","Email_Address_c":"test@gmail.com","Address_c":"testAdress1","Address2_c":"testaddress2","City_c":"TestCity","State_c":"TestState","Zip_Code_c":"546777"}}]');
      tdata.put('Trustee_Guarantor_J2_Data','[{"J2":{"First_Name__c":"TestFirst","Last_Name_c":"TestLast","Middle_Name__c ":"TestMiddel","Suffix_c":"Mr","Type_c":"PA","Phone_Number_c":"976543443","Email_Address_c":"test@gmail.com","Address_c":"testAdress1","Address2_c":"testaddress2","City_c":"TestCity","State_c":"TestState","Zip_Code_c":"546777"}}]');
      tdata.put('Trustee_Guarantor_J3_Data','[{"J3":{"First_Name__c":"TestFirst","Last_Name_c":"TestLast","Middle_Name__c ":"TestMiddel","Suffix_c":"Mr","Type_c":"PA","Phone_Number_c":"976543443","Email_Address_c":"test@gmail.com","Address_c":"testAdress1","Address2_c":"testaddress2","City_c":"TestCity","State_c":"TestState","Zip_Code_c":"546777"}}]');
            
      TrusteeGuarantorDetails.main(tdata);
            
      test.stopTest();

	}
    
}

  static testMethod void test2() {
    
     String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
     Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
     User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
         EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
         LocaleSidKey = 'en_US', ProfileId = p.Id,
         TimeZoneSidKey = 'America/Los_Angeles',
         UserName = uniqueUserName,TF4SF__Location__c='Yakima South 1st St');
            
        
        TF4SF__Application_Configuration__c appConfig = new TF4SF__Application_Configuration__c();
        appConfig.TF4SF__Application_Code__c = 'dsp4_0';
        appConfig.TF4SF__Theme__c = 'dsp4_0';
        appConfig.TF4SF__Key__c = 'YMmKJt5QxS7KlAEASMsj4Q==';
        appConfig.TF4SF__Popup_Seconds__c = 120;
        appConfig.TF4SF__Timeout_Seconds__c = 900;
        
        insert appConfig;   

        System.runAs(u) {
        
         TF4SF__Customer__c cust = new TF4SF__Customer__c();
        cust.TF4SF__Username__c = 'TestingDeclined';
        insert cust;
        
        
        TF4SF__Application__c app1 = new TF4SF__Application__c(TF4SF__Customer__c=cust.id,TF4SF__Primary_Phone_Number__c='(509)491-8025',TF4SF__Email_Address__c='charo509@yahoo.com',TF4SF__First_Name__c='TestFirst',TF4SF__Middle_Name__c='TestMiddel',TF4SF__Last_Name__c='TestLast',TF4SF__Product__c = 'Checking', TF4SF__Sub_Product__c = 'Test Sub Product',
                OwnerId = u.Id, TF4SF__Current_Person__c = u.Id, TF4SF__Created_Channel__c = 'Branch', TF4SF__Current_Channel__c = 'Branch',
                TF4SF__Created_Branch_Name__c = 'Walla Walla Tietan', TF4SF__Created_Person__c = u.Id, TF4SF__Current_Branch_Name__c = 'WallaWa',
                TF4SF__Application_Page__c = 'CrossSellPage', TF4SF__Type_of_Checking__c = 'Test',
                                                                   
                TF4SF__Type_of_Certificates__c = 'Certificates - 12 months', TF4SF__Type_of_Credit_Cards__c = 'Credit Cards - Advanced',
                TF4SF__Type_of_Personal_Loans__c = 'Personal Loans - Personal Loan', TF4SF__Type_of_Savings__c = 'Savings - Smart Savings',
                TF4SF__Type_of_Vehicle_Loans__c = 'Vehicle Loans -Vehicle Loan', TF4SF__User_Token__c = 'AnxzH/+Kmj85t2Wzkj5qETFWtwltJXsC5dcjGEgtGa89TTX4OIYGRky9UDclje65',
                TF4SF__Street_Address_1__c='828 S 8th Ave',TF4SF__Street_Address_2__c='828 S 8th Ave',TF4SF__City__c='Pasco',TF4SF__State__c='WA',TF4SF__Zip_Code__c='99301-5730',
                TF4SF__Created_User_Email_Address__c='athul.kakathkar@terafinainc.com',TF4SF__Current_User_Email_Address__c='athul.kakathkar@terafinainc.com',
                TF4SF__Application_Status__c='Open',TF4SF__Sub_Product_Description__c='testdesc');
            insert app1;
            
            
            TF4SF__Application_Activity__c appact=new TF4SF__Application_Activity__c();
            appact.TF4SF__Application__c=app1.Id;
            appact.TF4SF__Action__c='Created the Application';
            appact.TF4SF__Activity_Time__c = System.now();
            insert appact;
    
      Test.StartTest();

      Trustee_Guarantor__c lstInsert = new Trustee_Guarantor__c();
      Map<String, String> tdata = new Map<String, String>();
             
      tdata.put('Id',app1.Id);
      tdata.put('Action','Upsert');
      
      tdata.put('Trustee_Guarantor_PA_Data','[{"PA":{"First_Name__c":"","Last_Name_c":"","Middle_Name__c ":"","Suffix_c":"","Type_c":"","Phone_Number_c":"","Email_Address_c":"","Address_c":"","Address2_c":"","City_c":"","State_c":"","Zip_Code_c":""}}]');
      tdata.put('Trustee_Guarantor_J1_Data','[{"J1":{"First_Name__c":"","Last_Name_c":"","Middle_Name__c ":"","Suffix_c":"","Type_c":"","Phone_Number_c":"","Email_Address_c":"","Address_c":"","Address2_c":"","City_c":"","State_c":"","Zip_Code_c":""}}]');
      tdata.put('Trustee_Guarantor_J2_Data','[{"J2":{"First_Name__c":"","Last_Name_c":"","Middle_Name__c ":"","Suffix_c":"","Type_c":"","Phone_Number_c":"","Email_Address_c":"","Address_c":"","Address2_c":"","City_c":"","State_c":"","Zip_Code_c":""}}]');
      tdata.put('Trustee_Guarantor_J3_Data','[{"J3":{"First_Name__c":"","Last_Name_c":"","Middle_Name__c ":"","Suffix_c":"","Type_c":"","Phone_Number_c":"","Email_Address_c":"","Address_c":"","Address2_c":"","City_c":"","State_c":"","Zip_Code_c":""}}]');
            
      TrusteeGuarantorDetails.main(tdata);
            
      test.stopTest();
}
}

 static testMethod void test3() {
         
           String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
            EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US', ProfileId = p.Id,
            TimeZoneSidKey = 'America/Los_Angeles',
            UserName = uniqueUserName,TF4SF__Location__c='Yakima South 1st St');
            
        
        TF4SF__Application_Configuration__c appConfig = new TF4SF__Application_Configuration__c();
        appConfig.TF4SF__Application_Code__c = 'dsp4_0';
        appConfig.TF4SF__Theme__c = 'dsp4_0';
        appConfig.TF4SF__Key__c = 'YMmKJt5QxS7KlAEASMsj4Q==';
        appConfig.TF4SF__Popup_Seconds__c = 120;
        appConfig.TF4SF__Timeout_Seconds__c = 900;
       insert appConfig;   

        System.runAs(u) {
            
               TF4SF__Customer__c cust = new TF4SF__Customer__c();
                cust.TF4SF__Username__c = 'TestingDeclined';
                insert cust;
                 
                    TF4SF__Application__c app1 = new TF4SF__Application__c(TF4SF__Customer__c=cust.id,TF4SF__Primary_Phone_Number__c='(509)491-8025',TF4SF__Email_Address__c='charo509@yahoo.com',TF4SF__First_Name__c='TestFirst',TF4SF__Middle_Name__c='TestMiddel',TF4SF__Last_Name__c='TestLast',TF4SF__Product__c = 'Checking', TF4SF__Sub_Product__c = 'Test Sub Product',
                        OwnerId = u.Id, TF4SF__Current_Person__c = u.Id, TF4SF__Created_Channel__c = 'Branch', TF4SF__Current_Channel__c = 'Branch',
                        TF4SF__Created_Branch_Name__c = 'Walla Walla Tietan', TF4SF__Created_Person__c = u.Id, TF4SF__Current_Branch_Name__c = 'WallaWa',
                        TF4SF__Application_Page__c = 'CrossSellPage', TF4SF__Type_of_Checking__c = 'Test',
                                                                           
                        TF4SF__Type_of_Certificates__c = 'Certificates - 12 months', TF4SF__Type_of_Credit_Cards__c = 'Credit Cards - Advanced',
                        TF4SF__Type_of_Personal_Loans__c = 'Personal Loans - Personal Loan', TF4SF__Type_of_Savings__c = 'Savings - Smart Savings',
                        TF4SF__Type_of_Vehicle_Loans__c = 'Vehicle Loans -Vehicle Loan', TF4SF__User_Token__c = 'AnxzH/+Kmj85t2Wzkj5qETFWtwltJXsC5dcjGEgtGa89TTX4OIYGRky9UDclje65',
                        TF4SF__Street_Address_1__c='828 S 8th Ave',TF4SF__Street_Address_2__c='828 S 8th Ave',TF4SF__City__c='Pasco',TF4SF__State__c='WA',TF4SF__Zip_Code__c='99301-5730',
                        TF4SF__Created_User_Email_Address__c='athul.kakathkar@terafinainc.com',TF4SF__Current_User_Email_Address__c='athul.kakathkar@terafinainc.com',
                        TF4SF__Application_Status__c='Open',TF4SF__Sub_Product_Description__c='testdesc',TF4SF__First_Joint_Applicant__c =true ,TF4SF__Second_Joint_Applicant__c =true, TF4SF__Third_Joint_Applicant__c =true );
                    insert app1;
                 
                    TF4SF__Field_Logic_New__c objFl= new TF4SF__Field_Logic_New__c(
                        TF4SF__hideExpression_Fields__c = 'a',
                        TF4SF__IsDisplayed__c = true,
                        TF4SF__IsRequired__c = true,
                        TF4SF__Label__c = 'a',
                        TF4SF__DSP_Field_Name__c ='First_Name__c',
                        TF4SF__sObject__c ='Trustee_Guarantor__c',
                        TF4SF__expressionProperties_Fields__c = 'a',
                        TF4SF__Datatype_Form__c ='a',
                        TF4SF__Data_OptionsTypes__c ='a',
                        TF4SF__Picklist_Values__c ='a',
                        TF4SF__className_Field__c = 'a'                                
                    );
                 
        insert objFl;
        
          test.startTest();             
          TrusteeGuarantorDetails obj = new TrusteeGuarantorDetails ();
          Map<String, String> tdata = new Map<String, String>();
          tdata.put('Id',app1.Id);
          tdata.put('Action','Load');
          TrusteeGuarantorDetails.main(tdata);
          test.stopTest();
      }
            
   }

}