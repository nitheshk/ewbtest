@isTest
public class DSPResumeApplication_Test {
    @isTest static void test_method_one1() {
    
       String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
      EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US', ProfileId = p.Id,
      TimeZoneSidKey = 'America/Los_Angeles',
      UserName = uniqueUserName,TF4SF__Location__c='US',TF4SF__Channel__c='online');

    System.runAs(u) {
        TF4SF__Application_Configuration__c a = new TF4SF__Application_Configuration__c();
        a.TF4SF__Key__c = 'H+RYeZyh1kh5ZpWExkQbpw==';
        a.TF4SF__Timeout_Seconds__c =20;
         a.TF4SF__Theme__c= 'SDF';
        a.TF4SF__Application_Code__c = '546';
        insert a;
        
        TF4SF__Application__c app=new TF4SF__Application__c();
        app.TF4SF__Application_Status__c='open';
        app.Ownerid=u.id;
        app.TF4SF__Application_Page__c='AddressInfoPage';
        app.TF4SF__Current_Channel__c='online';
       insert app;
        
        TF4SF__About_Account__c abt = new TF4SF__About_Account__c();
        abt.TF4SF__Application__c = app.Id;
        insert abt;
    
        PageReference pageRef = Page.DSPResumeApplication;
        Test.setCurrentPage(pageRef);
 
        pageRef.getParameters().put('Id',app.id);   
        pageRef.getParameters().put('usr',u.id);   

        ApexPages.StandardController stc = new ApexPages.StandardController(app);

        test.startTest();
        
        DSPResumeApplication dsp=new DSPResumeApplication(stc);
        
        PageReference objPageRef = dsp.ResumeApp();
      
        test.stopTest();
        
    }
    }
    @isTest static void test_method_one2() {
     TF4SF__Application_Configuration__c a = new TF4SF__Application_Configuration__c();
        a.TF4SF__Key__c = 'H+RYeZyh1kh5ZpWExkQbpw==';
         a.TF4SF__Theme__c= 'SDF';
        a.TF4SF__Application_Code__c = '546';
         a.TF4SF__Timeout_Seconds__c =20;
        insert a;
       String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
      EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US', ProfileId = p.Id,
      TimeZoneSidKey = 'America/Los_Angeles',
      UserName = uniqueUserName,TF4SF__Location__c='US',TF4SF__Channel__c='online');

    System.runAs(u) {
        
        
        TF4SF__Application__c app=new TF4SF__Application__c();
        app.TF4SF__Application_Status__c='open';
        app.Ownerid=u.id;
        app.TF4SF__Application_Page__c='EmailVerificationPage';
        app.TF4SF__Current_Channel__c='online';
       insert app;
        
        TF4SF__About_Account__c abt = new TF4SF__About_Account__c();
        abt.TF4SF__Application__c = app.Id;
        insert abt;
    
        PageReference pageRef = Page.DSPResumeApplication;
        Test.setCurrentPage(pageRef);
 
        pageRef.getParameters().put('Id',app.id);   
        pageRef.getParameters().put('usr',u.id);   

        ApexPages.StandardController stc = new ApexPages.StandardController(app);

        test.startTest();
        
        DSPResumeApplication dsp=new DSPResumeApplication(stc);
        
        PageReference objPageRef = dsp.ResumeApp();
      
        test.stopTest();
        
    }
    }
    @isTest static void test_method_one3() {
     TF4SF__Application_Configuration__c a = new TF4SF__Application_Configuration__c();
        a.TF4SF__Key__c = 'H+RYeZyh1kh5ZpWExkQbpw==';
         a.TF4SF__Theme__c= 'SDF';
          a.TF4SF__Timeout_Seconds__c =20;
        a.TF4SF__Application_Code__c = '546';
        insert a;
       String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
      EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US', ProfileId = p.Id,
      TimeZoneSidKey = 'America/Los_Angeles',
      UserName = uniqueUserName,TF4SF__Location__c='US',TF4SF__Channel__c='online');

    System.runAs(u) {
        
        
        TF4SF__Application__c app=new TF4SF__Application__c();
        app.TF4SF__Application_Status__c='open';
        app.Ownerid=u.id;
        app.TF4SF__Application_Page__c='IdScanPage';
        app.TF4SF__Current_Channel__c='online';
       insert app;
        
        TF4SF__About_Account__c abt = new TF4SF__About_Account__c();
        abt.TF4SF__Application__c = app.Id;
        insert abt;
    
        PageReference pageRef = Page.DSPResumeApplication;
        Test.setCurrentPage(pageRef);
 
        pageRef.getParameters().put('Id',app.id);   
        pageRef.getParameters().put('usr',u.id);   

        ApexPages.StandardController stc = new ApexPages.StandardController(app);

        test.startTest();
        
        DSPResumeApplication dsp=new DSPResumeApplication(stc);
        
        PageReference objPageRef = dsp.ResumeApp();
      
        test.stopTest();
        
    }
    }
    
    @isTest static void test_method_one4() {
     TF4SF__Application_Configuration__c a = new TF4SF__Application_Configuration__c();
        a.TF4SF__Key__c = 'H+RYeZyh1kh5ZpWExkQbpw==';
         a.TF4SF__Theme__c= 'SDF';
          a.TF4SF__Timeout_Seconds__c =20;
        a.TF4SF__Application_Code__c = '546';
        insert a;
       String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
      EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US', ProfileId = p.Id,
      TimeZoneSidKey = 'America/Los_Angeles',
      UserName = uniqueUserName,TF4SF__Location__c='US',TF4SF__Channel__c='online');

    System.runAs(u) {
        
        
        TF4SF__Application__c app=new TF4SF__Application__c();
        app.TF4SF__Application_Status__c='open';
        app.Ownerid=u.id;
        app.TF4SF__Application_Page__c='ReviewSubmitPage';
        app.TF4SF__Current_Channel__c='online';
       insert app;
        
      TF4SF__About_Account__c abt = new TF4SF__About_Account__c();
        abt.TF4SF__Application__c = app.Id;
        insert abt;
        PageReference pageRef = Page.DSPResumeApplication;
        Test.setCurrentPage(pageRef);
 
        pageRef.getParameters().put('Id',app.id);   
        pageRef.getParameters().put('usr',u.id);   

        ApexPages.StandardController stc = new ApexPages.StandardController(app);

        test.startTest();
        
        DSPResumeApplication dsp=new DSPResumeApplication(stc);
        
        PageReference objPageRef = dsp.ResumeApp();
      
        test.stopTest();
        
    }
    }
    @isTest static void test_method_one5() {
     TF4SF__Application_Configuration__c a = new TF4SF__Application_Configuration__c();
        a.TF4SF__Key__c = 'H+RYeZyh1kh5ZpWExkQbpw==';
         a.TF4SF__Theme__c= 'SDF';
          a.TF4SF__Timeout_Seconds__c =20;
        a.TF4SF__Application_Code__c = '546';
        insert a;
       String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
      EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US', ProfileId = p.Id,
      TimeZoneSidKey = 'America/Los_Angeles',
      UserName = uniqueUserName,TF4SF__Location__c='US',TF4SF__Channel__c='online');

    System.runAs(u) {
        
        
        TF4SF__Application__c app=new TF4SF__Application__c();
        app.TF4SF__Application_Status__c='open';
        app.Ownerid=u.id;
        app.TF4SF__Application_Page__c='EmploymentPage';
        app.TF4SF__Current_Channel__c='online';
       insert app;
        
    TF4SF__About_Account__c abt = new TF4SF__About_Account__c();
        abt.TF4SF__Application__c = app.Id;
        insert abt;
        PageReference pageRef = Page.DSPResumeApplication;
        Test.setCurrentPage(pageRef);
 
        pageRef.getParameters().put('Id',app.id);   
        pageRef.getParameters().put('usr',u.id);   

        ApexPages.StandardController stc = new ApexPages.StandardController(app);

        test.startTest();
        
        DSPResumeApplication dsp=new DSPResumeApplication(stc);
        
        PageReference objPageRef = dsp.ResumeApp();
      
        test.stopTest();
        
    }
    }
    @isTest static void test_method_one6() {
     TF4SF__Application_Configuration__c a = new TF4SF__Application_Configuration__c();
        a.TF4SF__Key__c = 'H+RYeZyh1kh5ZpWExkQbpw==';
         a.TF4SF__Theme__c= 'SDF';
          a.TF4SF__Timeout_Seconds__c =20;
        a.TF4SF__Application_Code__c = '546';
        insert a;
       String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
      EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US', ProfileId = p.Id,
      TimeZoneSidKey = 'America/Los_Angeles',
      UserName = uniqueUserName,TF4SF__Location__c='US',TF4SF__Channel__c='online');

    System.runAs(u) {
        
        
        TF4SF__Application__c app=new TF4SF__Application__c();
        app.TF4SF__Application_Status__c='open';
        app.Ownerid=u.id;
        app.TF4SF__Application_Page__c='PersonalInfoPage';
        app.TF4SF__Current_Channel__c='online';
       insert app;
        TF4SF__About_Account__c abt = new TF4SF__About_Account__c();
        abt.TF4SF__Application__c = app.Id;
        insert abt;
    
        PageReference pageRef = Page.DSPResumeApplication;
        Test.setCurrentPage(pageRef);
 
        pageRef.getParameters().put('Id',app.id);   
        pageRef.getParameters().put('usr',u.id);   

        ApexPages.StandardController stc = new ApexPages.StandardController(app);

        test.startTest();
        
        DSPResumeApplication dsp=new DSPResumeApplication(stc);
        
        PageReference objPageRef = dsp.ResumeApp();
      
        test.stopTest();
        
    }
    }
    @isTest static void test_method_one7() {
     TF4SF__Application_Configuration__c a = new TF4SF__Application_Configuration__c();
        a.TF4SF__Key__c = 'H+RYeZyh1kh5ZpWExkQbpw==';
         a.TF4SF__Theme__c= 'SDF';
          a.TF4SF__Timeout_Seconds__c =20;
        a.TF4SF__Application_Code__c = '546';
        insert a;
       String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
      EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US', ProfileId = p.Id,
      TimeZoneSidKey = 'America/Los_Angeles',
      UserName = uniqueUserName,TF4SF__Location__c='US',TF4SF__Channel__c='online');

    System.runAs(u) {
        
        
        TF4SF__Application__c app=new TF4SF__Application__c();
        app.TF4SF__Application_Status__c='open';
        app.Ownerid=u.id;
        app.TF4SF__Application_Page__c='ProductSelectPage';
        app.TF4SF__Current_Channel__c='online';
       insert app;
        
    TF4SF__About_Account__c abt = new TF4SF__About_Account__c();
        abt.TF4SF__Application__c = app.Id;
        insert abt;
        PageReference pageRef = Page.DSPResumeApplication;
        Test.setCurrentPage(pageRef);
 
        pageRef.getParameters().put('Id',app.id);   
        pageRef.getParameters().put('usr',u.id);   

        ApexPages.StandardController stc = new ApexPages.StandardController(app);

        test.startTest();
        
        DSPResumeApplication dsp=new DSPResumeApplication(stc);
        
        PageReference objPageRef = dsp.ResumeApp();
      
        test.stopTest();
        
    }
    }
    
    @isTest static void test_method_one8() {
     TF4SF__Application_Configuration__c a = new TF4SF__Application_Configuration__c();
        a.TF4SF__Key__c = 'H+RYeZyh1kh5ZpWExkQbpw==';
         a.TF4SF__Theme__c= 'SDF';
          a.TF4SF__Timeout_Seconds__c =20;
        a.TF4SF__Application_Code__c = '546';
        insert a;
       String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
      EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US', ProfileId = p.Id,
      TimeZoneSidKey = 'America/Los_Angeles',
      UserName = uniqueUserName,TF4SF__Location__c='US',TF4SF__Channel__c='online');

    System.runAs(u) {
        
        
        TF4SF__Application__c app=new TF4SF__Application__c();
        app.TF4SF__Application_Status__c='open';
        app.Ownerid=u.id;
        app.TF4SF__Application_Page__c='SignupPage';
        app.TF4SF__Current_Channel__c='online';
       insert app;
        
    TF4SF__About_Account__c abt = new TF4SF__About_Account__c();
        abt.TF4SF__Application__c = app.Id;
        insert abt;
        PageReference pageRef = Page.DSPResumeApplication;
        Test.setCurrentPage(pageRef);
 
        pageRef.getParameters().put('Id',app.id);   
        pageRef.getParameters().put('usr',u.id);   

        ApexPages.StandardController stc = new ApexPages.StandardController(app);

        test.startTest();
        
        DSPResumeApplication dsp=new DSPResumeApplication(stc);
        
        PageReference objPageRef = dsp.ResumeApp();
      
        test.stopTest();
        
    }
    }
     @isTest static void test_method_one11() {
     TF4SF__Application_Configuration__c a = new TF4SF__Application_Configuration__c();
        a.TF4SF__Key__c = 'H+RYeZyh1kh5ZpWExkQbpw==';
         a.TF4SF__Theme__c= 'SDF';
          a.TF4SF__Timeout_Seconds__c =20;
        a.TF4SF__Application_Code__c = '546';
        insert a;
       String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
      EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US', ProfileId = p.Id,
      TimeZoneSidKey = 'America/Los_Angeles',
      UserName = uniqueUserName,TF4SF__Location__c='US',TF4SF__Channel__c='online');

    System.runAs(u) {
        
        
        TF4SF__Application__c app=new TF4SF__Application__c();
        app.TF4SF__Application_Status__c='open';
        app.Ownerid=u.id;
        app.TF4SF__Application_Page__c='PrevAddInfoPage';
        app.TF4SF__Current_Channel__c='online';
       insert app;
        TF4SF__About_Account__c abt = new TF4SF__About_Account__c();
        abt.TF4SF__Application__c = app.Id;
        insert abt;
    
        PageReference pageRef = Page.DSPResumeApplication;
        Test.setCurrentPage(pageRef);
 
        pageRef.getParameters().put('Id',app.id);   
        pageRef.getParameters().put('usr',u.id);   

        ApexPages.StandardController stc = new ApexPages.StandardController(app);

        test.startTest();
        
        DSPResumeApplication dsp=new DSPResumeApplication(stc);
        
        PageReference objPageRef = dsp.ResumeApp();
      
        test.stopTest();
        
    }
    }
     @isTest static void test_method_one12() {
     TF4SF__Application_Configuration__c a = new TF4SF__Application_Configuration__c();
        a.TF4SF__Key__c = 'H+RYeZyh1kh5ZpWExkQbpw==';
        a.TF4SF__Theme__c= 'SDF';
         a.TF4SF__Timeout_Seconds__c =20;
        a.TF4SF__Application_Code__c = '546';
        insert a;
       String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
      EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US', ProfileId = p.Id,
      TimeZoneSidKey = 'America/Los_Angeles',
      UserName = uniqueUserName,TF4SF__Location__c='US',TF4SF__Channel__c='online');

    System.runAs(u) {
        
        
        TF4SF__Application__c app=new TF4SF__Application__c();
        app.TF4SF__Application_Status__c='open';
        app.Ownerid=u.id;
        app.TF4SF__Application_Page__c='MaritalStatusPage';
        app.TF4SF__Current_Channel__c='online';
       insert app;
        
    TF4SF__About_Account__c abt = new TF4SF__About_Account__c();
        abt.TF4SF__Application__c = app.Id;
        insert abt;
        PageReference pageRef = Page.DSPResumeApplication;
        Test.setCurrentPage(pageRef);
 
        pageRef.getParameters().put('Id',app.id);   
        pageRef.getParameters().put('usr',u.id);   

        ApexPages.StandardController stc = new ApexPages.StandardController(app);

        test.startTest();
        
        DSPResumeApplication dsp=new DSPResumeApplication(stc);
        
        PageReference objPageRef = dsp.ResumeApp();
      
        test.stopTest();
        
    }
    }
     @isTest static void test_method_one13() {
     TF4SF__Application_Configuration__c a = new TF4SF__Application_Configuration__c();
        a.TF4SF__Key__c = 'H+RYeZyh1kh5ZpWExkQbpw==';
        a.TF4SF__Theme__c= 'SDF';
         a.TF4SF__Timeout_Seconds__c =20;
        a.TF4SF__Application_Code__c = '546';
        insert a;
       String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
      EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US', ProfileId = p.Id,
      TimeZoneSidKey = 'America/Los_Angeles',
      UserName = uniqueUserName,TF4SF__Location__c='US',TF4SF__Channel__c='online');

    System.runAs(u) {
        
        
        TF4SF__Application__c app=new TF4SF__Application__c();
        app.TF4SF__Application_Status__c='open';
        app.Ownerid=u.id;
        app.TF4SF__Application_Page__c='DependentsPage';
        app.TF4SF__Current_Channel__c='online';
       insert app;
        
    TF4SF__About_Account__c abt = new TF4SF__About_Account__c();
        abt.TF4SF__Application__c = app.Id;
        insert abt;
        PageReference pageRef = Page.DSPResumeApplication;
        Test.setCurrentPage(pageRef);
 
        pageRef.getParameters().put('Id',app.id);   
        pageRef.getParameters().put('usr',u.id);   

        ApexPages.StandardController stc = new ApexPages.StandardController(app);

        test.startTest();
        
        DSPResumeApplication dsp=new DSPResumeApplication(stc);
        
        PageReference objPageRef = dsp.ResumeApp();
      
        test.stopTest();
        
    }
    }
     @isTest static void test_method_one14() {
     TF4SF__Application_Configuration__c a = new TF4SF__Application_Configuration__c();
        a.TF4SF__Key__c = 'H+RYeZyh1kh5ZpWExkQbpw==';
        a.TF4SF__Theme__c= 'SDF';
         a.TF4SF__Timeout_Seconds__c =20;
        a.TF4SF__Application_Code__c = '546';
        insert a;
       String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
      EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US', ProfileId = p.Id,
      TimeZoneSidKey = 'America/Los_Angeles',
      UserName = uniqueUserName,TF4SF__Location__c='US',TF4SF__Channel__c='online');

    System.runAs(u) {
        
        
        TF4SF__Application__c app=new TF4SF__Application__c();
        app.TF4SF__Application_Status__c='open';
        app.Ownerid=u.id;
        app.TF4SF__Application_Page__c='IncomeSummaryPage';
        app.TF4SF__Current_Channel__c='online';
       insert app;
        
    TF4SF__About_Account__c abt = new TF4SF__About_Account__c();
        abt.TF4SF__Application__c = app.Id;
        insert abt;
        PageReference pageRef = Page.DSPResumeApplication;
        Test.setCurrentPage(pageRef);
 
        pageRef.getParameters().put('Id',app.id);   
        pageRef.getParameters().put('usr',u.id);   

        ApexPages.StandardController stc = new ApexPages.StandardController(app);

        test.startTest();
        
        DSPResumeApplication dsp=new DSPResumeApplication(stc);
        
        PageReference objPageRef = dsp.ResumeApp();
      
        test.stopTest();
        
    }
    }
     @isTest static void test_method_one15() {
     TF4SF__Application_Configuration__c a = new TF4SF__Application_Configuration__c();
        a.TF4SF__Key__c = 'H+RYeZyh1kh5ZpWExkQbpw==';
        a.TF4SF__Theme__c= 'SDF';
         a.TF4SF__Timeout_Seconds__c =20;
        a.TF4SF__Application_Code__c = '546';
        insert a;
       String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
      EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US', ProfileId = p.Id,
      TimeZoneSidKey = 'America/Los_Angeles',
      UserName = uniqueUserName,TF4SF__Location__c='US',TF4SF__Channel__c='online');

    System.runAs(u) {
        
        
        TF4SF__Application__c app=new TF4SF__Application__c();
        app.TF4SF__Application_Status__c='open';
        app.Ownerid=u.id;
        app.TF4SF__Application_Page__c='AssetSummaryPage';
        app.TF4SF__Current_Channel__c='online';
       insert app;
        
    TF4SF__About_Account__c abt = new TF4SF__About_Account__c();
        abt.TF4SF__Application__c = app.Id;
        insert abt;
        PageReference pageRef = Page.DSPResumeApplication;
        Test.setCurrentPage(pageRef);
 
        pageRef.getParameters().put('Id',app.id);   
        pageRef.getParameters().put('usr',u.id);   

        ApexPages.StandardController stc = new ApexPages.StandardController(app);

        test.startTest();
        
        DSPResumeApplication dsp=new DSPResumeApplication(stc);
        
        PageReference objPageRef = dsp.ResumeApp();
      
        test.stopTest();
        
    }
    }
     @isTest static void test_method_one16() {
     TF4SF__Application_Configuration__c a = new TF4SF__Application_Configuration__c();
        a.TF4SF__Key__c = 'H+RYeZyh1kh5ZpWExkQbpw==';
        a.TF4SF__Theme__c= 'SDF';
         a.TF4SF__Timeout_Seconds__c =20;
        a.TF4SF__Application_Code__c = '546';
        insert a;
       String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
      EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US', ProfileId = p.Id,
      TimeZoneSidKey = 'America/Los_Angeles',
      UserName = uniqueUserName,TF4SF__Location__c='US',TF4SF__Channel__c='online');

    System.runAs(u) {
        
        
        TF4SF__Application__c app=new TF4SF__Application__c();
        app.TF4SF__Application_Status__c='open';
        app.Ownerid=u.id;
        app.TF4SF__Application_Page__c='LiabilitiesSummaryPage';
        app.TF4SF__Current_Channel__c='online';
       insert app;
        
    TF4SF__About_Account__c abt = new TF4SF__About_Account__c();
        abt.TF4SF__Application__c = app.Id;
        insert abt;
        PageReference pageRef = Page.DSPResumeApplication;
        Test.setCurrentPage(pageRef);
 
        pageRef.getParameters().put('Id',app.id);   
        pageRef.getParameters().put('usr',u.id);   

        ApexPages.StandardController stc = new ApexPages.StandardController(app);

        test.startTest();
        
        DSPResumeApplication dsp=new DSPResumeApplication(stc);
        
        PageReference objPageRef = dsp.ResumeApp();
      
        test.stopTest();
        
    }
    }
     @isTest static void test_method_one17() {
     TF4SF__Application_Configuration__c a = new TF4SF__Application_Configuration__c();
        a.TF4SF__Key__c = 'H+RYeZyh1kh5ZpWExkQbpw==';
        a.TF4SF__Theme__c= 'SDF';
         a.TF4SF__Timeout_Seconds__c =20;
        a.TF4SF__Application_Code__c = '546';
        insert a;
       String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
      EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US', ProfileId = p.Id,
      TimeZoneSidKey = 'America/Los_Angeles',
      UserName = uniqueUserName,TF4SF__Location__c='US',TF4SF__Channel__c='online');

    System.runAs(u) {
        
        
        TF4SF__Application__c app=new TF4SF__Application__c();
        app.TF4SF__Application_Status__c='open';
        app.Ownerid=u.id;
        app.TF4SF__Application_Page__c='PurchaseDetailsPage';
        app.TF4SF__Current_Channel__c='online';
       insert app;
        TF4SF__About_Account__c abt = new TF4SF__About_Account__c();
        abt.TF4SF__Application__c = app.Id;
        insert abt;
    
        PageReference pageRef = Page.DSPResumeApplication;
        Test.setCurrentPage(pageRef);
 
        pageRef.getParameters().put('Id',app.id);   
        pageRef.getParameters().put('usr',u.id);   

        ApexPages.StandardController stc = new ApexPages.StandardController(app);

        test.startTest();
        
        DSPResumeApplication dsp=new DSPResumeApplication(stc);
        
        PageReference objPageRef = dsp.ResumeApp();
      
        test.stopTest();
        
    }
    }
     @isTest static void test_method_one18() {
     TF4SF__Application_Configuration__c a = new TF4SF__Application_Configuration__c();
        a.TF4SF__Key__c = 'H+RYeZyh1kh5ZpWExkQbpw==';
        a.TF4SF__Theme__c= 'SDF';
         a.TF4SF__Timeout_Seconds__c =20;
        a.TF4SF__Application_Code__c = '546';
        insert a;
       String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
      EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US', ProfileId = p.Id,
      TimeZoneSidKey = 'America/Los_Angeles',
      UserName = uniqueUserName,TF4SF__Location__c='US',TF4SF__Channel__c='online');

    System.runAs(u) {
        
        
        TF4SF__Application__c app=new TF4SF__Application__c();
        app.TF4SF__Application_Status__c='open';
        app.Ownerid=u.id;
        app.TF4SF__Application_Page__c='PropertyDetailsPage';
        app.TF4SF__Current_Channel__c='online';
       insert app;
        TF4SF__About_Account__c abt = new TF4SF__About_Account__c();
        abt.TF4SF__Application__c = app.Id;
        insert abt;
    
        PageReference pageRef = Page.DSPResumeApplication;
        Test.setCurrentPage(pageRef);
 
        pageRef.getParameters().put('Id',app.id);   
        pageRef.getParameters().put('usr',u.id);   

        ApexPages.StandardController stc = new ApexPages.StandardController(app);

        test.startTest();
        
        DSPResumeApplication dsp=new DSPResumeApplication(stc);
        
        PageReference objPageRef = dsp.ResumeApp();
      
        test.stopTest();
        
    }
    }
     @isTest static void test_method_one19() {
    TF4SF__Application_Configuration__c a = new TF4SF__Application_Configuration__c();
        a.TF4SF__Key__c = 'H+RYeZyh1kh5ZpWExkQbpw==';
        a.TF4SF__Theme__c= 'SDF';
         a.TF4SF__Timeout_Seconds__c =20;
        a.TF4SF__Application_Code__c = '546';
        insert a;
       String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
      EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US', ProfileId = p.Id,
      TimeZoneSidKey = 'America/Los_Angeles',
      UserName = uniqueUserName,TF4SF__Location__c='US',TF4SF__Channel__c='online');

    System.runAs(u) {
        
        
        TF4SF__Application__c app=new TF4SF__Application__c();
        app.TF4SF__Application_Status__c='open';
        app.Ownerid=u.id;
        app.TF4SF__Application_Page__c='GMIQuestionPage';
        app.TF4SF__Current_Channel__c='online';
       insert app;
        
    TF4SF__About_Account__c abt = new TF4SF__About_Account__c();
        abt.TF4SF__Application__c = app.Id;
        insert abt;
        PageReference pageRef = Page.DSPResumeApplication;
        Test.setCurrentPage(pageRef);
 
        pageRef.getParameters().put('Id',app.id);   
        pageRef.getParameters().put('usr',u.id);   

        ApexPages.StandardController stc = new ApexPages.StandardController(app);

        test.startTest();
        
        DSPResumeApplication dsp=new DSPResumeApplication(stc);
        
        PageReference objPageRef = dsp.ResumeApp();
      
        test.stopTest();
        
    }
    }
     @isTest static void test_method_one20() {
     TF4SF__Application_Configuration__c a = new TF4SF__Application_Configuration__c();
        a.TF4SF__Key__c = 'H+RYeZyh1kh5ZpWExkQbpw==';
        a.TF4SF__Theme__c= 'SDF';
         a.TF4SF__Timeout_Seconds__c =20;
        a.TF4SF__Application_Code__c = '546';
        insert a;
       String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
      EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US', ProfileId = p.Id,
      TimeZoneSidKey = 'America/Los_Angeles',
      UserName = uniqueUserName,TF4SF__Location__c='US',TF4SF__Channel__c='online');

    System.runAs(u) {
        
        
        TF4SF__Application__c app=new TF4SF__Application__c();
        app.TF4SF__Application_Status__c='open';
        app.Ownerid=u.id;
        app.TF4SF__Application_Page__c='StartFundingPage';
        app.TF4SF__Current_Channel__c='online';
       insert app;
        
    TF4SF__About_Account__c abt = new TF4SF__About_Account__c();
        abt.TF4SF__Application__c = app.Id;
        insert abt;
        PageReference pageRef = Page.DSPResumeApplication;
        Test.setCurrentPage(pageRef);
 
        pageRef.getParameters().put('Id',app.id);   
        pageRef.getParameters().put('usr',u.id);   

        ApexPages.StandardController stc = new ApexPages.StandardController(app);

        test.startTest();
        
        DSPResumeApplication dsp=new DSPResumeApplication(stc);
        
        PageReference objPageRef = dsp.ResumeApp();
      
        test.stopTest();
        
    }
    }
    
  
}