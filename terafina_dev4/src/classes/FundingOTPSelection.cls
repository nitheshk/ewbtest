global with sharing class FundingOTPSelection implements TF4SF.DSP_Interface {
	
	global static Map<String, String> main (Map<String, String> tdata) {
		Map<String, String> data = new Map<String, String>();
		String appId = tdata.get('id');
		Long time1 = DateTime.now().getTime();
        Boolean infoDebug = false;
        infoDebug = (tdata.get('infoDebug') == 'true');
        String Token = '', StatusCode='0';
        String response1 = '', response2 = '', response3 = '';
        String userMFA = null;
		String userMFAValue = null;
		//userMFAValue = tdata.get('q1responseid');
        userMFAValue = tdata.get('mfaArray');
        if (String.isNotBlank(userMFAValue)) {
            userMFAValue = userMFAValue.replace('[', '').replace(']','');
        }
		String answerMFA = null;

        VELOSETTINGS__C velo = VELOSETTINGS__C.getOrgDefaults();

		try {
            Token = AuthToken.Auth();    
                
            response1 = VerifyAccountOLB(appId,Token,'','','','','',userMFA,userMFAValue);
            system.debug('response1 '+response1);
            String CFIID = '';
            String VerificationStatus = '';
            String ValidateVerificationStatus = '';
            Map<String, Object> verifyMap = (Map<String, Object>)JSON.deserializeUntyped(response1);        
            Map<String, Object> verifyDataMap = (Map<String, Object>)verifyMap.get('data');
            Map<String, Object> verifyAccRealTimeMap = (Map<String, Object>)verifyDataMap.get('VerifyAccRealTime');
            System.debug('verifyAccRealTimeMap ==>' +  verifyAccRealTimeMap);
            Map<String, Object> verifybodyMap = (Map<String, Object>)verifyAccRealTimeMap.get('body');
            Map<String, Object> verifyAccountRealTimeResponseMap = (Map<String, Object>)verifybodyMap.get('VerifyAccountRealTimeResponse');
            Map<String, Object> verifyreturnMap = (Map<String, Object>)verifyAccountRealTimeResponseMap.get('return');
            Map<String, Object> RealTimeAccountVerificationInfoStatusListMap = (Map<String, Object>)verifyreturnMap.get('RealTimeAccountVerificationInfoStatusList');
            List<Object> RealTimeAccountVerificationInfoStatusList = (List<Object>)RealTimeAccountVerificationInfoStatusListMap.get('RealTimeAccountVerificationInfoStatus');
            for (Object RealTimeAccountVerificationInfoStatus : RealTimeAccountVerificationInfoStatusList) {
                Map<String, Object> RealTimeAccountVerificationInfoStatusMap = (Map<String, Object>)RealTimeAccountVerificationInfoStatus;
                CFIID = String.valueOf(RealTimeAccountVerificationInfoStatusMap.get('CFIID'));
                VerificationStatus = String.valueOf(RealTimeAccountVerificationInfoStatusMap.get('VerificationStatus'));
                break;
            }
            if (VerificationStatus == 'Approved') {
                data.put('VerificationStatus', VerificationStatus);
                data.put('response2',response1);
                StatusCode='200';
            } else {
                response2 = FundingValidateAccount.ValidateAccount(appId, Token, CFIID); 
                Map<String, Object> validateMap = (Map<String, Object>)JSON.deserializeUntyped(response2);
                Map<String, Object> validateyDataMap = (Map<String, Object>)validateMap.get('data');
                Map<String, Object> validateAddHostMap = (Map<String, Object>)validateyDataMap.get('AddHost');
                Map<String, Object> validatebodyMap = (Map<String, Object>)validateAddHostMap.get('body');
                Map<String, Object> ValidateAddHostAccountsResponseMap = (Map<String, Object>)validatebodyMap.get('AddHostAccountsResponse');
                Map<String, Object> validatereturnMap = (Map<String, Object>)ValidateAddHostAccountsResponseMap.get('return');
                Map<String, Object> HostAccountVerificationInfoStatusListMap = (Map<String, Object>)validatereturnMap.get('HostAccountVerificationInfoStatusList');
                List<Object> HostAccountVerificationInfoStatusList = (List<Object>)HostAccountVerificationInfoStatusListMap.get('HostAccountVerificationInfoStatus');
                for (Object HostAccountVerificationInfoStatus : HostAccountVerificationInfoStatusList) {
                    Map<String, Object> HostAccountVerificationInfoStatusMap = (Map<String, Object>)HostAccountVerificationInfoStatus;
                    CFIID = String.valueOf(HostAccountVerificationInfoStatusMap.get('CFIID'));
                    ValidateVerificationStatus = String.valueOf(HostAccountVerificationInfoStatusMap.get('VerificationStatus'));
                    break;
                }
                if (ValidateVerificationStatus == 'Approved') {
                    data.put('VerificationStatus', ValidateVerificationStatus);
                    data.put('response2',response1);
                    StatusCode='200';
                }
            }
            data.put('StatusCode',StatusCode);
            //String runId='',cfiId='';
            //runId=res1.data.VerifyAccRealTime.body.VerifyAccountRealTimeResponse.return_Z.RealTimeAccountVerificationInfoStatusList.RealTimeAccountVerificationInfoStatus[0].RunID;
            //cfiId=res1.data.VerifyAccRealTime.body.VerifyAccountRealTimeResponse.return_Z.RealTimeAccountVerificationInfoStatusList.RealTimeAccountVerificationInfoStatus[0].CFIID;
            
            //data.put('About_Account__c.CFIID__c',cfiId);
            //data.put('About_Account__c.RUNID__c',runId);
        } catch (Exception e) {
            data.put('server-errors', 'Error encountered in FundingVerifyAccountOLB class: ' + e.getMessage() + '; line: ' + e.getLineNumber() + '; type: ' + e.getTypeName() + '; stack trace: ' + e.getStackTraceString());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error encountered in FundingVerifyAccountOLB class: ' + e.getMessage() + '; line: ' + e.getLineNumber() + '; type: ' + e.getTypeName() + '; stack trace: ' + e.getStackTraceString()));
            System.debug('exception' + e + ' ' + e.getLineNumber());
        }

		return data;
	}
	
	public static String VerifyAccountOLB(String appId, String Token,String RunID,String CFIID,String FIID,String userparam,String passparam,String userMFA,String userMFAValue) {
        String result = '';
        
        String applicationId = '\'' + appId + '\'';
        //System.debug('applicationId//productnumber:'+applicationId+'--'+productnumber);
        //select all fields in Application
        queryHelper.ObjectInfo appInfo = queryHelper.selectStar('TF4SF__Application__c');
        String queryString =  appInfo.query;
        queryString += 'WHERE ID = ' + applicationId ;
        TF4SF__Application__c app = Database.query(queryString);

        //this.Application = app;
        queryHelper.ObjectInfo app2Info = queryHelper.selectStar('TF4SF__Application2__c');
        String app2String =  app2Info.query;
        app2String += 'WHERE TF4SF__Application__c = ' + applicationId ;
        TF4SF__Application2__c app2 = Database.query(app2String);

        queryHelper.ObjectInfo empInfo = queryHelper.selectStar('TF4SF__Employment_Information__c');
        String employmentQuery =  empInfo.query;
        employmentQuery += 'WHERE TF4SF__Application__c = ' + applicationId;
        TF4SF__Employment_Information__c employment = Database.query(employmentQuery);
        //System.debug('employment:' + employment);

        queryHelper.ObjectInfo identityInfo = queryHelper.selectStar('TF4SF__Identity_Information__c');
        String identityQuery = identityInfo.query;
        identityQuery += 'WHERE TF4SF__Application__c = ' + applicationId;
        TF4SF__Identity_Information__c identity = Database.query(identityQuery);
        System.debug('identity:' + identity);

        queryHelper.ObjectInfo aboutAccountInfo = queryHelper.selectStar('TF4SF__About_Account__c');
        String aboutAccountQuery = aboutAccountInfo.query;
        aboutAccountQuery += 'WHERE TF4SF__Application__c = ' + applicationId;
        //System.debug('abtaccountquery:'+aboutAccountQuery);
        TF4SF__About_Account__c about = Database.query(aboutAccountQuery);
        //System.debug('about account:' + about); 

        DepositSubmit.Application application = new DepositSubmit.Application(app, app2, employment, identity, about);

        try {
            result += '{';
            if (application.app.TF4SF__Product__c == 'Checking') {
                result += '"ABA": "'+application.about.TF4SF__Routing_Number_CHK__c+'",';
                result += '"AccountNumber": "'+application.about.TF4SF__CHK_Account_Number__c+'",';
                result += '"AccountTypeCode": "'+application.about.TF4SF__Account_type_FI_CHK__c+'",';
            } else {
                result += '"ABA": "'+application.about.TF4SF__Routing_Number_SAV__c+'",';
                result += '"AccountNumber": "'+application.about.TF4SF__SAV_Account_Number__c+'",';
                result += '"AccountTypeCode": "'+application.about.TF4SF__Account_Type_FI_Sav__c+'",';
            }

            result += '"AccountTypeGroup": "Cash",';
            result += '"CFIID": "'+application.about.CFIID__c+'",';
            result += '"Email": "'+application.app.TF4SF__Email_Address__c+'",';
            result += '"FIID": "'+application.about.FIID__c+'",';
            result += '"OnlineParamList": [';
                result += '{';
                    result += '"CryptVal": "'+application.app.TF4SF__Login__c+'",';
                    result += '"ParamId": "'+application.about.userparam__c+'"';
                result += '},';
                result += '{';
                    result += '"CryptVal": "'+application.app.TF4SF__Password__c+'",';
                    result += '"ParamId": "'+application.about.passparam__c+'"';
                result += '},';
                result += userMFAValue;
                //result += '{';
                //    result += '"CryptVal": "'+userMFAValue+'",';
                //    result += '"ParamId": "'+application.about.Funding_MFA__c+'"';
                //result += '}';
            result += '],';
            result += '"RqUID": "VerifyAccountRealTimREQUEST",';
            result += '"RunID": "'+application.about.RUNId__c+'"';
            result += '}';

        } catch (Exception e) {
            system.debug('exception'+e+' '+e.getLineNumber());
        }

        return execute(appId,result, Token, 'Verify Account 2');
    }

    private static String execute(String appId, String json, String Token, String CallOut) {
        System.debug('appId, JSON: ' + appId  + '---' + json);
        String responseJson = '';

        try { 
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            //req.setTimeout(120*1000);
            VELOSETTINGS__C velo = VELOSETTINGS__C.getOrgDefaults();
            Integer timeOut = Integer.ValueOf(velo.ProcessAPI_Timeout__c);
            String GUID = GUIDGenerator.getGUID();
            req.setHeader(velo.Request_GUID_Name__c,GUID);
            req.setEndpoint(velo.FundingEndpoint__c+'api/process/verifyAccount');
            req.setHeader('Authorization','Bearer '+Token);
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('Accept','application/json');
            req.setTimeout(timeOut);
            //req.setTimeout(120*1000);
            req.setMethod('POST');
            
            req.setBody(json);
            HttpResponse res = h.send(req);
            system.debug('Req is '+req.getbody());
            responseJson = res.getBody();
            system.debug('Res is '+res.getbody());  

            //DebugLogger.InsertDebugLog(appId, CallOut +', GUID: '+ GUID, 'FundingVerifyAccountOLB Request GUID');
            //DebugLogger.InsertDebugLog(appId, req.getBody(), +Callout+' Request');
            //DebugLogger.InsertDebugLog(appId, res.getBody(), +Callout+' Response'); 
        } catch (Exception e) {
            system.debug('exception'+e+' '+e.getLineNumber());
        }

        return responseJSON;
    }
	
	
}