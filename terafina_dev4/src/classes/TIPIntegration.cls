global with sharing class TIPIntegration implements TF4SF.DSP_Interface {   

    global static Map<String, String> main(Map<String, String> tdata) {
        Long time1 = DateTime.now().getTime();
        Boolean infoDebug = (tdata.get('infoDebug') == 'true');
        Map<String, String> data = new Map<String, String>();
        
        String appId = tdata.get('id');
        data.put('Status','False');
        try {
            //InsertDebugLog(appId,'start TIP integration' ,'Start1');
            TF4SF__Application__c app = [SELECT Id, TF4SF__Product__c, TF4SF__External_App_Id__c
                FROM TF4SF__Application__c WHERE Id = :appId];
            //Morgate Integration Gets called from here.
            if (app.TF4SF__Product__c == 'Home Loan') {
            //this below method shoud return true or false
                data = MortgageSubmit.main(tdata);
                System.debug('echk tis ');
            }                
         } catch (Exception e) {
            data.put('server-errors', 'Error encountered in TIPIntegration class: ' + e.getMessage() + '; line: ' + e.getLineNumber() + '; type: ' + e.getTypeName() + '; stack trace: ' + e.getStackTraceString());
            System.debug('server-errors: ' + e.getMessage());
        }
        TIPIntegration.InsertDebugLog(appId,'Data Status TIP : ' + data.get('Status') ,'MortgageSubmit');

        Long time2 = DateTime.now().getTime();
        if (infoDebug == true ) { data.put('debug-server-errors', 'TIPIntegration - Elapsed Call Time: ' + (time2 - time1) + 'ms'); }
        //data.put('Status','False');

        return data;



       
    }

    @future
    public static void InsertDebugLog (String appId, String json, String Callout) {
        List<TF4SF__Debug_Logs__c> debugList = new List<TF4SF__Debug_Logs__c>();

        try {
            TF4SF__Debug_Logs__c debug = new TF4SF__Debug_Logs__c();
            debug.TF4SF__Application__c = appId;
            debug.TF4SF__Debug_Message__c = json;
            debug.TF4SF__Source__c = Callout;
            debug.TF4SF__Timestamp__c = String.valueOf(System.now());
            debugList.add(debug);
            insert debugList;
        } catch (Exception e) {
            TF4SF__Debug_Logs__c debug = new TF4SF__Debug_Logs__c();
            debug.TF4SF__Application__c = appId;
            debug.TF4SF__Debug_Message__c = 'Unable to create Debug Log : '+ e.getMessage();
            debug.TF4SF__Source__c = Callout;
            debug.TF4SF__Timestamp__c = String.valueOf(System.now());
            debugList.add(debug);
            insert debugList;
        }
    }
    

    global class ObjectInfo {
        public String query {get; set;}
        public List<String> fieldNames {get; set;}
        public ObjectInfo() {}
        public ObjectInfo(String query, List<String>fieldNames) {
            this.query = query;
            this.fieldNames = fieldNames;
        }
    }
    
    global static ObjectInfo selectStar(String objectName) {
        SObjectType objToken = Schema.getGlobalDescribe().get(objectName);
        DescribeSObjectResult objDef = objToken.getDescribe();
        Map<String, SObjectField> fields = objDef.fields.getMap(); 
        Set<String> fieldSet = fields.keySet();
        List<String> fieldsToQuery = new List<String>{};

        for (String fieldName:fieldSet) {
            
            SObjectField fieldToken = fields.get(fieldName);
            DescribeFieldResult selectedField = fieldToken.getDescribe();
            
            //respect CRUD/FLS
            if (selectedField.isAccessible()) {
                fieldsToQuery.add(selectedField.getName());
            } else {
                //System.debug('not accessible: ' + selectedField.getName());
            }
        }

        String queryString = 'SELECT ';
        queryString += String.join(fieldsToQuery, ', ');
        queryString += ' FROM ' + objectName + ' ';
        return new ObjectInfo(queryString, fieldsToQuery);
    }

    }