@RestResource(urlMapping='/authPrefill/v1/*')
global with sharing class OnlineBankingController {
    
    @HttpPost
    global static void startApplication() {
        System.debug('inside startApplication');
        
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String productCode = req.params.get('productCode');
        String subProductCode = req.params.get('subProductCode');
        String campaignId = req.params.get('campaign');
        String PromoCode = req.params.get('PromoCode');
        String ONBID = req.params.get('onbId');
        System.debug('request ONBID: '+ONBID);
        String ipaddress = req.headers.get('X-Salesforce-SIP');

        String productValue;
        String subProductValue;
        String productTheme;
        String body = req.requestBody.toString(); 
        System.debug('length: ' + body.length() + '; body1: ' + body);
        String jsonKey = 'memInfo';
        if (body != null && body.length() > 0 && body.contains(jsonKey)) {
            List<String> parts = body.split('=');
            if (parts[1] != null && parts[1] != '') {
                body = parts[1];
            }
        }
        if (body.length() == 0) {
            body = req.params.get('memInfo');
            System.debug('body = '+body);
        }
        System.debug('after ONBID: '+ONBID);
        String clearText = null;
        OnlineEncryption__c OBC = OnlineEncryption__c.getOrgDefaults();
        System.debug('length of OBC.key__c: ' + OBC.key__c.length());
        TF4SF__Product_Codes__c pc = new TF4SF__Product_Codes__c();
        TF4SF__Application_Configuration__c appConfig = TF4SF__Application_Configuration__c.getOrgDefaults();
        system.debug('the subProductCode is '+subProductCode);
        if (subProductCode != null) {
            pc = TF4SF__Product_Codes__c.getValues(subProductCode);
        } 
        if (pc == null) {
            pc = TF4SF__Product_Codes__c.getValues('ALL');
        }
        system.debug('the PC is '+pc);
        if (pc != null) {
            //if (String.isNotBlank(pc.TF4SF__Product_Code__c)) {
                productValue = pc.TF4SF__Product__c;
                system.debug('the PC is '+pc.TF4SF__Product__c);
            //}
            //if (String.isNotBlank(pc.TF4SF__Sub_Product_Code__c)) {
                subProductValue = pc.TF4SF__Sub_Product__c;
                system.debug('the PC is '+pc.TF4SF__Sub_Product__c);
                productTheme = pc.TF4SF__Product_Theme__c;
            //}                     
        }

        System.debug('productValue: ' + productValue + '; subProductValue: ' + subProductValue);
        System.debug('ONBID: '+ONBID);
        if (String.isNotBlank(ONBID)) {
            TF4SF__Application__c app = [SELECT Id, TF4SF__User_Token__c, TF4SF__Product__c, TF4SF__Sub_Product__c, TF4SF__Theme_URL__c, TF4SF__Special_Promo_Code__c, TF4SF__Created_Channel__c,
            TF4SF__Created_Branch_Name__c, TF4SF__Current_Channel__c, TF4SF__Created_Timestamp__c, TF4SF__Current_Branch_Name__c, TF4SF__Current_Person__c, TF4SF__Current_Timestamp__c, 
            TF4SF__Application_Status__c, TF4SF__Application_Page__c, TF4SF__Application_Version__c FROM TF4SF__Application__c WHERE Id =: ONBID];
            
                app.TF4SF__Product__c = productValue;
                app.TF4SF__Sub_Product__c = subProductValue;
                //TF4SF__Campaign_Id__c = campaignId,
                app.TF4SF__Theme_URL__c = productTheme;
                app.TF4SF__Special_Promo_Code__c = PromoCode; // MRM-4019
                //app.TF4SF__Created_Channel__c = 'Online Banking',
                //app.TF4SF__Created_Branch_Name__c = '',
                app.TF4SF__Current_Channel__c = 'Online Banking';
                //app.TF4SF__Created_Timestamp__c = system.now(),
                app.TF4SF__Current_Branch_Name__c = '';
                app.TF4SF__Current_Person__c = UserInfo.getUserId();
                app.TF4SF__Current_Timestamp__c = system.now();
                app.TF4SF__Application_Status__c = 'Open';
                app.TF4SF__Application_Version__c = appConfig.TF4SF__Application_Version__c;
                app.TF4SF__Application_Page__c = app.TF4SF__Application_Page__c;

            UserData ud = null;
            Boolean validPrefill = true;
            Account person;
            Set<Id> personIdSet = new Set<Id>();
            //Deserialize JSON here. Good place for a try/catch
            try {
                if (body != null && body != '') {
                    blob cryptoKey = EncodingUtil.base64Decode(OBC.key__c);
                    System.debug('The data is-->'+body);
                    clearText = OLBSimulator.decryptData(body);
                    System.debug('clearText =' + clearText);
                    ud = (UserData)JSON.deserialize(clearText, UserData.class);
                     System.debug('UD1------>' + ud);
                    app.TF4SF__First_Name__c = ud.FirstName;
                    app.TF4SF__Middle_Name__c = ud.MiddleName;
                    app.TF4SF__Last_Name__c = ud.LastName;
                    app.TF4SF__Suffix__c = ud.Suffix;
                    app.TF4SF__Street_Address_1__c = ud.Address1;
                    app.TF4SF__Street_Address_2__c = ud.Address2;
                    app.TF4SF__City__c = ud.City;
                    app.TF4SF__State__c = ud.State;
                    app.TF4SF__Zip_Code__c = ud.Zip;
                    app.TF4SF__Primary_Phone_Number__c = ud.PrimaryPhone;
                    app.TF4SF__Secondary_Phone_Number__c = ud.WorkPhone;
                    app.TF4SF__Email_Address__c = ud.EmailAddress;
                    app.TF4SF__IP_Address__c = ipaddress;
                    app.TF4SF__External_App_Stage__c = ud.MemberNumber;
                    
                    //List<String> dob_List = DateOfBirth.split('-');
                    //DateOfBirth = dob_List[1] + '/' + dob_List[2] + '/' + dob_List[0];
                    //app.Person_Number__c = ud.MemberNumber;
                    //app.Is_Employee_OLB__c = Boolean.valueOf(ud.IsEmployee);
                    
                }
            } catch(Exception e) {
                //Unable to parse encrypted data - fall through and create an application with no prefill data:
                system.debug('Decryption Error: ' + e.getMessage() + e.getStackTraceString());
                //res.addHeader('Refresh', '0; url=https://cs22.salesforce.com/ErrorPage' );
                //clearText = 'Decryption Error: ' + e.getMessage();
                clearText = null;
                validPrefill = false;
            }

            try {
                //DepositLandingPageController.setAppToken(app);
                if (TF4SF__Application__c.SObjectType.getDescribe().isUpdateable()) {
                    //insert app;
                    parseProductType(app);
                    update app;
                }

                //Datetime dt = Datetime.newInstance(person.Date_Of_Birth__c.year(), person.Date_Of_Birth__c.month(), person.Date_Of_Birth__c.day());
                //personDataMap.put('dob', String.valueOf(dt.format('MM/dd/yyyy')));
                TF4SF__Identity_Information__c iden = [SELECT Id, TF4SF__Application__c, TF4SF__Date_of_Birth__c, TF4SF__SSN_Prime__c, TF4SF__Id_Type__c, 
                TF4SF__Identity_Number_Primary__c, TF4SF__Issue_Date__c, TF4SF__Expiry_Date__c, TF4SF__State_Issued__c FROM TF4SF__Identity_Information__c WHERE TF4SF__Application__c =: app.Id];
                //TF4SF__Identity_Information__c iden = new TF4SF__Identity_Information__c(
                    //iden.TF4SF__Application__c = app.Id;
                    iden.TF4SF__Date_of_Birth__c = formatDate(ud.DOB);
                    iden.TF4SF__SSN_Prime__c = ud.TaxIdentifier;
                    iden.TF4SF__Id_Type__c = ud.IdType;
                    iden.TF4SF__Identity_Number_Primary__c = ud.IDNumber;
                    iden.TF4SF__Issue_Date__c = formatDate(ud.IDIssueDate);
                    iden.TF4SF__Expiry_Date__c = formatDate(ud.IDExpirationDate);
                    iden.TF4SF__State_Issued__c = ud.IDStateIssued;
                    //);
                
                if (TF4SF__Identity_Information__c.SObjectType.getDescribe().isUpdateable()) {
                    update iden;
                }

                // TF4SF__Employment_Information__c emp = new TF4SF__Employment_Information__c(
                //     TF4SF__Application__c = app.Id);
                
                // if (TF4SF__Employment_Information__c.SObjectType.getDescribe().isCreateable()) {
                //     insert emp;
                // }

                // TF4SF__About_Account__c acc = new TF4SF__About_Account__c(
                //     TF4SF__Application__c = app.Id
                // );

                // if (TF4SF__About_Account__c.SObjectType.getDescribe().isCreateable()) {
                //     insert acc;
                // }

                // TF4SF__Application2__c app2 = new TF4SF__Application2__c(
                //     TF4SF__Application__c = app.Id
                // );
                // if (TF4SF__Application2__c.SObjectType.getDescribe().isCreateable()) {
                //     insert app2;
                // }

                TF4SF__Application_Activity__c appAct = new TF4SF__Application_Activity__c(
                    TF4SF__Application__c = app.Id,
                    //Branch__c = app.Current_Branch_Name__c,
                    TF4SF__Channel__c = app.TF4SF__Current_Channel__c,
                    //Name__c = app.Current_Person__c,
                    TF4SF__Action__c = 'Redirected Application to Online Banking',
                    TF4SF__Activity_Time__c = system.now()
                );
                if (TF4SF__Application_Activity__c.SObjectType.getDescribe().isCreateable()) {
                    insert appAct;
                }
                String userToken = depositLandingPageController.decrypt(app.TF4SF__User_Token__c);
                String subpage = (validPrefill) ? '#/signup' : '#/signup';
                // if (app.TF4SF__Type_of_Checking__c != null || app.TF4SF__Type_of_Savings__c != null) {
                //     subpage = '#/product-select';
                // } else {
                //     subpage = '#/personal-info';
                // }
    
                System.debug('app.Id = ' + app.Id);
                PageReference p = new PageReference(TF4SF__SiteUrl__c.getOrgDefaults().TF4SF__Url__c+ 'tf4sf__dsp'+subpage);
                p.getParameters().put('id', app.Id);
                Cookie id = new Cookie('id', app.Id, null, -1, true);
                Cookie ut = new Cookie('ut', userToken, null, -1, true);
                // Set the new cookie for the page
                System.debug('p = ' + p);
                res.AddHeader('Content-Type', 'text/html; charset=UTF-8');
                res.AddHeader('Refresh', '0; url='+p.getUrl());
                String resBody = '{ "url": "'+p.getUrl()+'"}';
                //res.responseBody = Blob.valueOf(resBody);
                //res.AddBody(resBody);
                
            } catch(Exception e) {
                //Add debugging info!! - send to debug logs
                system.debug( 'Online Error: ' + e.getMessage() + e.getStackTraceString());
                res.addHeader('Refresh', '0; url='+TF4SF__SiteUrl__c.getOrgDefaults().TF4SF__Url__c+'TF4SF__InMaintanence' );
            }    

            
        } else if (String.isBlank(ONBID)) {
            TF4SF__Application__c app = new TF4SF__Application__c(
                TF4SF__Product__c = productValue,
                TF4SF__Sub_Product__c = subProductValue,
                //TF4SF__Campaign_Id__c = campaignId,
                TF4SF__Theme_URL__c = productTheme,
                TF4SF__Special_Promo_Code__c = PromoCode, // MRM-4019
                TF4SF__Created_Channel__c = 'Online Banking',
                TF4SF__Created_Branch_Name__c = '',
                TF4SF__Current_Channel__c = 'Online Banking',
                TF4SF__Created_Timestamp__c = system.now(),
                TF4SF__Current_Branch_Name__c = '',
                TF4SF__Current_Person__c = UserInfo.getUserId(),
                TF4SF__Current_Timestamp__c = system.now(),
                TF4SF__Application_Status__c = 'Open',
                TF4SF__Application_Version__c = appConfig.TF4SF__Application_Version__c,
                TF4SF__Application_Page__c = 'SignUpPage'
            );

            UserData ud = null;
            Boolean validPrefill = true;
            Account person;
            Set<Id> personIdSet = new Set<Id>();
            //Deserialize JSON here. Good place for a try/catch
            try {
                if (body != null && body != '') {
                    blob cryptoKey = EncodingUtil.base64Decode(OBC.key__c);
                    clearText = OLBSimulator.decryptData(body);
                    System.debug('clearText =' + clearText);
                    ud = (UserData)JSON.deserialize(clearText, UserData.class);
                    System.debug('UD1--->' + ud);
                    app.TF4SF__First_Name__c = ud.FirstName;
                    app.TF4SF__Middle_Name__c = ud.MiddleName;
                    app.TF4SF__Last_Name__c = ud.LastName;
                    app.TF4SF__Suffix__c = ud.Suffix;
                    app.TF4SF__Street_Address_1__c = ud.Address1;
                    app.TF4SF__Street_Address_2__c = ud.Address2;
                    app.TF4SF__City__c = ud.City;
                    app.TF4SF__State__c = ud.State;
                    app.TF4SF__Zip_Code__c = ud.Zip;
                    app.TF4SF__Primary_Phone_Number__c = ud.PrimaryPhone;
                    app.TF4SF__Secondary_Phone_Number__c = ud.WorkPhone;
                    app.TF4SF__Email_Address__c = ud.EmailAddress;
                    app.TF4SF__IP_Address__c = ipaddress;
                    //app.TF4SF__Member__c = person.Id;
                    
                    //List<String> dob_List = DateOfBirth.split('-');
                    //DateOfBirth = dob_List[1] + '/' + dob_List[2] + '/' + dob_List[0];
                    //app.Person_Number__c = ud.MemberNumber;
                    //app.Is_Employee_OLB__c = Boolean.valueOf(ud.IsEmployee);
                    
                }
            } catch(Exception e) {
                //Unable to parse encrypted data - fall through and create an application with no prefill data:
                system.debug('Decryption Error: ' + e.getMessage() + e.getStackTraceString());
                //res.addHeader('Refresh', '0; url=https://cs22.salesforce.com/ErrorPage' );
                //clearText = 'Decryption Error: ' + e.getMessage();
                clearText = null;
                validPrefill = false;
            }

            try {
                DepositLandingPageController.setAppToken(app);
                if (TF4SF__Application__c.SObjectType.getDescribe().isCreateable() && TF4SF__Application__c.SObjectType.getDescribe().isUpdateable()) {
                    insert app;
                    parseProductType(app);
                    update app;
                }

                //Datetime dt = Datetime.newInstance(person.Date_Of_Birth__c.year(), person.Date_Of_Birth__c.month(), person.Date_Of_Birth__c.day());
                //personDataMap.put('dob', String.valueOf(dt.format('MM/dd/yyyy')));
                TF4SF__Identity_Information__c iden = new TF4SF__Identity_Information__c(
                    TF4SF__Application__c = app.Id,
                    TF4SF__Date_of_Birth__c = formatDate(ud.DOB),
                    TF4SF__SSN_Prime__c = ud.TaxIdentifier,
                    TF4SF__Id_Type__c = ud.IdType,
                    TF4SF__Identity_Number_Primary__c = ud.IDNumber,
                    TF4SF__Issue_Date__c = formatDate(ud.IDIssueDate),
                    TF4SF__Expiry_Date__c = formatDate(ud.IDExpirationDate),
                    TF4SF__State_Issued__c = ud.IDStateIssued);
                
                if (TF4SF__Identity_Information__c.SObjectType.getDescribe().isCreateable()) {
                    insert iden;
                }

                TF4SF__Employment_Information__c emp = new TF4SF__Employment_Information__c(
                    TF4SF__Application__c = app.Id);
                
                if (TF4SF__Employment_Information__c.SObjectType.getDescribe().isCreateable()) {
                    insert emp;
                }

                TF4SF__About_Account__c acc = new TF4SF__About_Account__c(
                    TF4SF__Application__c = app.Id
                );

                if (TF4SF__About_Account__c.SObjectType.getDescribe().isCreateable()) {
                    insert acc;
                }

                TF4SF__Application2__c app2 = new TF4SF__Application2__c(
                    TF4SF__Application__c = app.Id
                );
                if (TF4SF__Application2__c.SObjectType.getDescribe().isCreateable()) {
                    insert app2;
                }

                TF4SF__Application_Activity__c appAct = new TF4SF__Application_Activity__c(
                    TF4SF__Application__c = app.Id,
                    //Branch__c = app.Current_Branch_Name__c,
                    TF4SF__Channel__c = app.TF4SF__Current_Channel__c,
                    //Name__c = app.Current_Person__c,
                    TF4SF__Action__c = 'Created the Application from Online Banking',
                    TF4SF__Activity_Time__c = system.now()
                );
                if (TF4SF__Application_Activity__c.SObjectType.getDescribe().isCreateable()) {
                    insert appAct;
                }
                String userToken = depositLandingPageController.decrypt(app.TF4SF__User_Token__c);
                String subpage = (validPrefill) ? '#/signup' : '#/signup'; 
                // if (app.TF4SF__Type_of_Checking__c != null || app.TF4SF__Type_of_Savings__c != null) {
                //     subpage = '#/product-select';
                // } else {
                //     subpage = '#/personal-info';
                // }
    
                System.debug('app.Id = ' + app.Id);
                PageReference p = new PageReference(TF4SF__SiteUrl__c.getOrgDefaults().TF4SF__Url__c+ 'tf4sf__dsp'+subpage);
                p.getParameters().put('id', app.Id);
                Cookie id = new Cookie('id', app.Id, null, -1, true);
                Cookie ut = new Cookie('ut', userToken, null, -1, true);
                // Set the new cookie for the page
                System.debug('p = ' + p);
                res.AddHeader('Content-Type', 'text/html; charset=UTF-8');
                res.AddHeader('Refresh', '0; url='+p.getUrl());
                String resBody = '{ "url": "'+p.getUrl()+'"}';
                //res.responseBody = Blob.valueOf(resBody);
                //res.AddBody(resBody);
                
            } catch(Exception e) {
                //Add debugging info!! - send to debug logs
                system.debug( 'Online Error: ' + e.getMessage() + e.getStackTraceString());
                res.addHeader('Refresh', '0; url='+TF4SF__SiteUrl__c.getOrgDefaults().TF4SF__Url__c+'TF4SF__InMaintanence' );
            }
        }   
    }

    global class UserData {
        //Application__c fields
        public String FirstName {get;set;}
        public String MiddleName {get;set;}
        public String LastName {get;set;}
        public String FullName {get;set;}
        public String Suffix {get;set;}
        public String Address1 {get;set;}
        public String Address2 {get;set;}
        public String City {get;set;}
        public String State {get;set;}
        public String Zip {get;set;}
        public String ZipPlusFour {get;set;}
        public String PrimaryPhone {get;set;}
        public String WorkPhone {get;set;}
        public String TaxIdentifier {get;set;}
        public String MemberNumber {get;set;}
        public String DOB {get;set;}
        public String EmailAddress {get;set;}
        public String IDType {get;set;}
        public String IDNumber {get;set;}
        public String IDStateIssued {get;set;}
        public String IDIssueDate {get;set;}
        public String IDExpirationDate {get;set;}
        public String IsEmployee {get;set;}

        public UserData() {}
    }

    public OnlineBankingController() {
        
    }

    global static void parseProductType(TF4SF__Application__c newApplication) {
        Integer i=0;

        // To update type of Checking product selected
        if (newApplication.TF4SF__Sub_Product__c!=null && newApplication.TF4SF__Sub_Product__c.contains('All')) {
            newApplication.TF4SF__Type_of_Checking__c=newApplication.TF4SF__Sub_Product__c;
        } else if (newApplication.TF4SF__Sub_Product__c!=null && newApplication.TF4SF__Sub_Product__c.contains('Checking')) {
            newApplication.TF4SF__Type_of_Checking__c=newApplication.TF4SF__Sub_Product__c;
        } else if (newApplication.TF4SF__Sub_Product__c!=null && newApplication.TF4SF__Sub_Product__c.contains('Checking')) {
            newApplication.TF4SF__Type_of_Checking__c=newApplication.TF4SF__Sub_Product__c;
        } else if (newApplication.TF4SF__Primary_Offer__c!=null && newApplication.TF4SF__Primary_Offer__c.contains('Checking')) {
            newApplication.TF4SF__Type_of_Checking__c=newApplication.TF4SF__Primary_Offer__c;
        } else if (newApplication.TF4SF__Second_Offer__c!=null && newApplication.TF4SF__Second_Offer__c.contains('Checking')) {
            newApplication.TF4SF__Type_of_Checking__c=newApplication.TF4SF__Second_Offer__c;
        } else if (newApplication.TF4SF__Third_Offer__c!=null && newApplication.TF4SF__Third_Offer__c.contains('Checking')) {
            newApplication.TF4SF__Type_of_Checking__c=newApplication.TF4SF__Third_Offer__c;
        } else {
            newApplication.TF4SF__Type_of_Checking__c=null;
        }
        if (newApplication.TF4SF__Type_of_Checking__c!=null) {
            i=i+1;
        }
        // To update Type of Savings Product selected
        if (newApplication.TF4SF__Sub_Product__c!=null && newApplication.TF4SF__Sub_Product__c.contains('Savings')) {
            newApplication.TF4SF__Type_of_Savings__c=newApplication.TF4SF__Sub_Product__c;
        } else if (newApplication.TF4SF__Primary_Offer__c!=null && newApplication.TF4SF__Primary_Offer__c.contains('Savings')) {
            newApplication.TF4SF__Type_of_Savings__c=newApplication.TF4SF__Primary_Offer__c;
        } else if (newApplication.TF4SF__Second_Offer__c!=null && newApplication.TF4SF__Second_Offer__c.contains('Savings')) {
            newApplication.TF4SF__Type_of_Savings__c=newApplication.TF4SF__Second_Offer__c;
        } else if (newApplication.TF4SF__Third_Offer__c!=null && newApplication.TF4SF__Third_Offer__c.contains('Savings')) {
            newApplication.TF4SF__Type_of_Savings__c=newApplication.TF4SF__Third_Offer__c;
        } else {
            newApplication.TF4SF__Type_of_Savings__c=null;
        }
        if (newApplication.TF4SF__Type_of_Savings__c!=null) {
            i=i+1;
        }

        // To update type of Certificates product selected
        if (newApplication.TF4SF__Sub_Product__c!=null && newApplication.TF4SF__Sub_Product__c.contains('Certificates')) {
            newApplication.TF4SF__Type_of_Certificates__c=newApplication.TF4SF__Sub_Product__c;
        } else if (newApplication.TF4SF__Primary_Offer__c!=null && newApplication.TF4SF__Primary_Offer__c.contains('Certificates')) {
            newApplication.TF4SF__Type_of_Certificates__c=newApplication.TF4SF__Primary_Offer__c;
        } else if (newApplication.TF4SF__Second_Offer__c!=null && newApplication.TF4SF__Second_Offer__c.contains('Certificates')) {
            newApplication.TF4SF__Type_of_Certificates__c=newApplication.TF4SF__Second_Offer__c;
        } else if (newApplication.TF4SF__Third_Offer__c!=null && newApplication.TF4SF__Third_Offer__c.contains('Certificates')) {
            newApplication.TF4SF__Type_of_Certificates__c=newApplication.TF4SF__Third_Offer__c;
        } else{
            newApplication.TF4SF__Type_of_Certificates__c=null;
        }
        if (newApplication.TF4SF__Type_of_Certificates__c!=null) {
            i=i+1;
        }

        // To update type of Credit Cards selected
        if (newApplication.TF4SF__Sub_Product__c!=null && newApplication.TF4SF__Sub_Product__c.contains('Credit Cards')) {
            newApplication.TF4SF__Type_of_Credit_Cards__c=newApplication.TF4SF__Sub_Product__c;
        } else if (newApplication.TF4SF__Primary_Offer__c!=null && newApplication.TF4SF__Primary_Offer__c.contains('Credit Cards')) {
            newApplication.TF4SF__Type_of_Credit_Cards__c=newApplication.TF4SF__Primary_Offer__c;
        } else if (newApplication.TF4SF__Second_Offer__c!=null && newApplication.TF4SF__Second_Offer__c.contains('Credit Cards')) {
            newApplication.TF4SF__Type_of_Credit_Cards__c=newApplication.TF4SF__Second_Offer__c;
        } else if (newApplication.TF4SF__Third_Offer__c!=null && newApplication.TF4SF__Third_Offer__c.contains('Credit Cards')) {
            newApplication.TF4SF__Type_of_Credit_Cards__c=newApplication.TF4SF__Third_Offer__c;
        } else {
            newApplication.TF4SF__Type_of_Credit_Cards__c=null;
        }
        if (newApplication.TF4SF__Type_of_Credit_Cards__c!=null) {
            i=i+1;
        }

        // To update type of Vehicle Loans selected
        if (newApplication.TF4SF__Sub_Product__c!=null && newApplication.TF4SF__Sub_Product__c.contains('Vehicle Loans')) {
            newApplication.TF4SF__Type_of_Vehicle_Loans__c=newApplication.TF4SF__Sub_Product__c;
        } else if (newApplication.TF4SF__Primary_Offer__c!=null && newApplication.TF4SF__Primary_Offer__c.contains('Vehicle Loans')) {
            newApplication.TF4SF__Type_of_Vehicle_Loans__c=newApplication.TF4SF__Primary_Offer__c;
        } else if (newApplication.TF4SF__Second_Offer__c!=null && newApplication.TF4SF__Second_Offer__c.contains('Vehicle Loans')) {
            newApplication.TF4SF__Type_of_Vehicle_Loans__c=newApplication.TF4SF__Second_Offer__c;
        } else if (newApplication.TF4SF__Third_Offer__c!=null && newApplication.TF4SF__Third_Offer__c.contains('Vehicle Loans')) {
            newApplication.TF4SF__Type_of_Vehicle_Loans__c=newApplication.TF4SF__Third_Offer__c;
        } else {
            newApplication.TF4SF__Type_of_Vehicle_Loans__c=null;
        }
        if (newApplication.TF4SF__Type_of_Vehicle_Loans__c!=null) {
            i=i+1;
        }

        // To update type of Personal Loan selected
        if (newApplication.TF4SF__Sub_Product__c!=null && newApplication.TF4SF__Sub_Product__c.contains('Personal Loans')) {
            newApplication.TF4SF__Type_of_Personal_Loans__c=newApplication.TF4SF__Sub_Product__c;
        } else if (newApplication.TF4SF__Primary_Offer__c!=null && newApplication.TF4SF__Primary_Offer__c.contains('Personal Loans')) {
            newApplication.TF4SF__Type_of_Personal_Loans__c=newApplication.TF4SF__Primary_Offer__c;
        } else if (newApplication.TF4SF__Second_Offer__c!=null && newApplication.TF4SF__Second_Offer__c.contains('Personal Loans')) {
            newApplication.TF4SF__Type_of_Personal_Loans__c=newApplication.TF4SF__Second_Offer__c;
        } else if (newApplication.TF4SF__Third_Offer__c!=null && newApplication.TF4SF__Third_Offer__c.contains('Personal Loans')) {
            newApplication.TF4SF__Type_of_Personal_Loans__c=newApplication.TF4SF__Third_Offer__c;
        } else {
            newApplication.TF4SF__Type_of_Personal_Loans__c=null;
        }
        if (newApplication.TF4SF__Type_of_Personal_Loans__c!=null) {
            i=i+1;
        }

        // To update type of Investment product selected
        if (newApplication.TF4SF__Sub_Product__c!=null && newApplication.TF4SF__Sub_Product__c.contains('Investments')) {
            newApplication.TF4SF__Type_of_Investments__c=newApplication.TF4SF__Sub_Product__c;
        } else if (newApplication.TF4SF__Primary_Offer__c!=null && newApplication.TF4SF__Primary_Offer__c.contains('Investments')) {
            newApplication.TF4SF__Type_of_Investments__c=newApplication.TF4SF__Primary_Offer__c;
        } else if (newApplication.TF4SF__Second_Offer__c!=null && newApplication.TF4SF__Second_Offer__c.contains('Investments')) {
            newApplication.TF4SF__Type_of_Investments__c=newApplication.TF4SF__Second_Offer__c;
        } else if (newApplication.TF4SF__Third_Offer__c!=null && newApplication.TF4SF__Third_Offer__c.contains('Investments')) {
            newApplication.TF4SF__Type_of_Investments__c=newApplication.TF4SF__Third_Offer__c;
        } else {
            newApplication.TF4SF__Type_of_Investments__c=null;
        }
        if (newApplication.TF4SF__Type_of_Investments__c!=null) {
            i=i+1;
        }

        // To update type of Home Loans product selected
        if (newApplication.TF4SF__Sub_Product__c!=null && newApplication.TF4SF__Sub_Product__c.contains('Home Loans')) {
            newApplication.TF4SF__Type_of_Mortgage_Loan__c=newApplication.TF4SF__Sub_Product__c;
        } else if (newApplication.TF4SF__Primary_Offer__c!=null && newApplication.TF4SF__Primary_Offer__c.contains('Home Loans')) {
            newApplication.TF4SF__Type_of_Mortgage_Loan__c=newApplication.TF4SF__Primary_Offer__c;
        } else if (newApplication.TF4SF__Second_Offer__c!=null && newApplication.TF4SF__Second_Offer__c.contains('Home Loans')) {
            newApplication.TF4SF__Type_of_Mortgage_Loan__c=newApplication.TF4SF__Second_Offer__c;
        } else if (newApplication.TF4SF__Third_Offer__c!=null && newApplication.TF4SF__Third_Offer__c.contains('Home Loans')) {
            newApplication.TF4SF__Type_of_Mortgage_Loan__c=newApplication.TF4SF__Third_Offer__c;
        } else {
            newApplication.TF4SF__Type_of_Mortgage_Loan__c=null;
        }
        if (newApplication.TF4SF__Type_of_Mortgage_Loan__c!=null) {
            i=i+1;
        }

        // To update type of Home Equity product selected
        if (newApplication.TF4SF__Sub_Product__c!=null && newApplication.TF4SF__Sub_Product__c.contains('Home Equity')) {
            newApplication.TF4SF__Type_Of_Home_Equity__c=newApplication.TF4SF__Sub_Product__c;
        } else if (newApplication.TF4SF__Primary_Offer__c!=null && newApplication.TF4SF__Primary_Offer__c.contains('Home Equity')) {
            newApplication.TF4SF__Type_Of_Home_Equity__c=newApplication.TF4SF__Primary_Offer__c;
        } else if (newApplication.TF4SF__Second_Offer__c!=null && newApplication.TF4SF__Second_Offer__c.contains('Home Equity')) {
            newApplication.TF4SF__Type_Of_Home_Equity__c=newApplication.TF4SF__Second_Offer__c;
        } else if (newApplication.TF4SF__Third_Offer__c!=null && newApplication.TF4SF__Third_Offer__c.contains('Home Equity')) {
            newApplication.TF4SF__Type_Of_Home_Equity__c=newApplication.TF4SF__Third_Offer__c;
        } else {
            newApplication.TF4SF__Type_Of_Home_Equity__c=null;
        }
        if (newApplication.TF4SF__Type_Of_Home_Equity__c!=null) {
            i=i+1;
        }

        // To update type of Mortgage short application selected
        if (newApplication.TF4SF__Sub_Product__c!=null && newApplication.TF4SF__Sub_Product__c.contains('Mortgage Short Application')) {
            newApplication.TF4SF__Type_of_Mortgage_Short_Application__c=newApplication.TF4SF__Sub_Product__c;
        } else if (newApplication.TF4SF__Primary_Offer__c!=null && newApplication.TF4SF__Primary_Offer__c.contains('Mortgage Short Application')) {
            newApplication.TF4SF__Type_of_Mortgage_Short_Application__c=newApplication.TF4SF__Primary_Offer__c;
        } else if (newApplication.TF4SF__Second_Offer__c!=null && newApplication.TF4SF__Second_Offer__c.contains('Mortgage Short Application')) {
            newApplication.TF4SF__Type_of_Mortgage_Short_Application__c=newApplication.TF4SF__Second_Offer__c;
        } else if (newApplication.TF4SF__Third_Offer__c!=null && newApplication.TF4SF__Third_Offer__c.contains('Mortgage Short Application')) {
            newApplication.TF4SF__Type_of_Mortgage_Short_Application__c=newApplication.TF4SF__Third_Offer__c;
        } else {
            newApplication.TF4SF__Type_of_Mortgage_Short_Application__c=null;
        }
        if (newApplication.TF4SF__Type_of_Mortgage_Short_Application__c!=null) {
            i=i+1;
        }
        //newApplication.Number_of_Products__c = 1;
        newApplication.TF4SF__Number_of_Products__c = i;
    }

    public static String formatDate(String incomingDate) {
        String outgoingDate = '';
        if (String.isNotBlank(incomingDate)) {
            List<String> SplitDate = new List<String>();
            SplitDate = incomingDate.split('T');
            List<String> properFormat = new List<String>();
            properFormat = SplitDate[0].split('-');
            outgoingDate = properFormat[1]+'/'+properFormat[2]+'/'+properFormat[0];
        }
        return outgoingDate;
    }
}