public class EmailNotificationDeposit_Utility {
 
   public  void sendEmail(List<TF4SF__Application__c> appList) { 

    List<TF4SF__Application__c> lstApp = new List<TF4SF__Application__c>();    
    List<TF4SF__Application_Configuration__c> appConfig = [SELECT Id, Deposit_Abandon_Email_Time__c FROM TF4SF__Application_Configuration__c];
    List<Messaging.SingleEmailMessage> mailsList =  new List<Messaging.SingleEmailMessage>();
    string appId='';

    try {
    //******Get EmailTemplate whose developer name Status_Portal_Member_and_Non_Member_Notification_new
    EmailTemplate templateId1H = [SELECT Id, Body, Subject, HtmlValue FROM EmailTemplate WHERE DeveloperName = 'Reminder_Abandoned_Email_Template'];
    List<Contact> con = [SELECT Id, LastName FROM Contact LIMIT 1];

    for (TF4SF__Application__c application : appList) {
      //******Set Emailbody
       appId=application.Id;
      if (application.TF4SF__Email_Address__c != null && (application.Time_Diff__c >= appConfig[0].Deposit_Abandon_Email_Time__c) ) {
        application.is_Processed_Abandoned_Mail_1__c = true ;
        lstApp.add(application);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail = mailMessage(templateId1H.id, application.TF4SF__Email_Address__c, application.id);  
        mailsList.add(mail);
      }

    }
    
     if ( lstApp.size() > 0 ) {
        Messaging.sendEmail(mailsList );       
        update lstApp;
      }
    } catch (Exception ex) {
       DebugLogger.InsertDebugLog(appId,ex.getMessage(),'Error in Line Number==>' + ex.getLineNumber()); 
    }
  }

  public  Messaging.SingleEmailMessage mailMessage(String TemplateId, String emailAddress, String appId) {
    List<Contact> con = [SELECT Id FROM Contact LIMIT 1];
    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    mail.setTemplateId(TemplateId);
    mail.toAddresses = new string[]{emailAddress} ;
    if(con.size () > 0) { 
        mail.setTargetObjectId(con[0].id);
        }else
        {
        Contact cons = new Contact();
        cons.Lastname= 'DummyTest';
        insert cons;
        mail.setTargetObjectId(cons.id);
        }
    mail.setWhatId(appId);   
    mail.setTreatTargetObjectAsRecipient(false);
    return mail;
  }
  
  public void sendResumeAppForDeposit(List<TF4SF__Application__c> listApp) {
        
        string appId='';    
        try{
            Boolean count = true;
            List<Contact> conList = new List<Contact>();
            List<TF4SF__Application__c> lstApp = new List<TF4SF__Application__c>();
            set<id> appIDSet = new set<id>();
            
            List<Messaging.SingleEmailMessage> mailsList =  new List<Messaging.SingleEmailMessage>();
            EmailTemplate templateId = [SELECT Id, Body, Subject, HtmlValue FROM EmailTemplate WHERE DeveloperName = 'Resume_Application'];
            OrgWideEmailAddress owa = [SELECT Id, Address, DisplayName FROM OrgWideEmailAddress where DisplayName ='talk2us@velobank.com' limit 1];
            List<Contact> con = [SELECT Id, LastName FROM Contact LIMIT 1];
            System.debug(listApp);
            for (TF4SF__Application__c app : listApp) {               
                    
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();               
                    appId=app.Id;
                    mail.setTemplateId(templateId.id);
                    if(owa != null) {
                        mail.setOrgWideEmailAddressId(owa.id); 
                    }
                    mail.toAddresses = new string[]{app.TF4SF__Email_Address__c} ;
                        if (con.size() > 0) {
                            mail.setTargetObjectId(con[0].id); 
                        }
                    else
                    {
                        count =false;
                        Contact conRec = new Contact();
                        conRec.lastName = app.Name;
                        conList.add(conRec);
                        insert conList;
                        mail.setTargetObjectId(conList[0].id); 
                    }
                    
                    mail.setWhatId(app.id);              
                    mail.setTreatTargetObjectAsRecipient(false);              
                    mailsList.add(mail);             
                    appIDSet.add(app.id);                
            }
            if (mailsList.size () > 0) {
                Messaging.sendEmail(mailsList );
                if(conList.size () > 0)
                    delete conList;              
            }
        }
        catch(Exception ex){
            DebugLogger.InsertDebugLog(appId,ex.getMessage(),'Error in Line Number==>' + ex.getLineNumber()); 
        }
    }  
}