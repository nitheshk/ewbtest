global with sharing class PreQualificationLetterController{

    public TF4SF__About_Account__c ac{get;set;}

    public PreQualificationLetterController(ApexPages.StandardController controller) {
        this.ac = (TF4SF__About_Account__c)controller.getRecord();
    }

    public String getChooserender() {
        if (ApexPages.currentPage().getParameters().get('p') != null)  {
            return 'pdf';
        }
        else {
            return null;
        }       
    }


    webservice static String generatePDF(Id appId) { 

        String res = 'False';
      if(String.isNotBlank(appId)) {
        //Get About account details and Application details
        
        List<TF4SF__About_Account__c> lstAcc = [SELECT Id, TF4SF__Purchase_P__c, TF4SF__Total_Loan_Amount__c,TF4SF__Application__c,                                            
                                        TF4SF__Application__r.TF4SF__Product__c, TF4SF__Application__r.TF4SF__Sub_Product__c , 
                                        TF4SF__Application__r.TF4SF__First_Name__c ,TF4SF__Application__r.TF4SF__Last_Name__c,
                                        TF4SF__Application__r.TF4SF__Email_Address__c
                                        FROM TF4SF__About_Account__c
                                        WHERE TF4SF__Application__c =: appId];

        res = PreQualificationLetterController.PreQualificationLetter(lstAcc);
      }
      return res;                             
    }

    
    public static String PreQualificationLetter(List<TF4SF__About_Account__c> lstAcc) {

        String strResult = 'false';    
        Map<Id,Attachment> mapAttach = new Map<Id,Attachment>();
        List<Attachment> lstAttach = new List<Attachment>();
        try{           
         
              for(TF4SF__About_Account__c obj : lstAcc) {

                 
                   if(obj.TF4SF__Application__r.TF4SF__Product__c == 'Home Loan' && obj.TF4SF__Application__r.TF4SF__Sub_Product__c == 'Home Loan - Short App') { 
                        
                        PageReference pdf =  Page.Prequalification_Letter;
                        pdf.getParameters().put('p','p');
                        pdf.getParameters().put('id', obj.Id);
                        pdf.setRedirect(true);

                        /*
                        //Check if attachment already exists
                        List<Attachment> lstAttach = [SELECT Id FROM Attachment WHERE Name='Pre-Qualification_Letter.pdf' 
                                                      AND  ParentId =: obj.TF4SF__Application__c];

                        if (lstAttach != null && lstAttach.size() > 0){
                            //Delete the existing attachment
                            delete lstAttach;
                        }
                        */
                        //Create attachment
                        Attachment attach = new Attachment();        
                        //attach.Body =  pdf.getContent();
                        if (Test.isRunningTest()) {
                           attach.Body = blob.valueOf('pdf1');
                            
                        } else {
                            attach.Body =  pdf.getContent();
                        }

                        String fileName = 'Pre-Qualification_Letter_' + String.valueOf(System.now()) + '.pdf';
                        attach.Name = fileName;
                        attach.ParentId = obj.TF4SF__Application__c;
                        mapAttach.put(obj.TF4SF__Application__c,attach);   
                        lstAttach.add(attach);                    

                        strResult = 'True';
                    }
                    else
                    {
                        strResult = 'False';
                    }
                }


                //Send the email to applciant with the above attachment
                if(lstAcc.size() > 0 && lstAttach.size() >0) {
                  insert lstAttach;
                  sendEmail(mapAttach,lstAcc);
                }
            
        } catch(exception ex) {
            System.debug('Exception in PreQualificationLetterController ' + ex.getMessage());
            System.debug('Exception in PreQualificationLetterController Line number' + ex.getLineNumber());
            strResult = 'False';
        } 
          return strResult;   
    }


    //This is used to send Prequalificaton letter to applicant
    public  static Void sendEmail(Map<Id,Attachment> mapAttach,List<TF4SF__About_Account__c> lstAcc) {
    
        String conId;
        Contact con = new Contact(); // Create a master list to hold the emails we'll send
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();

      try {
          //Get the application ID's from ACH Transfer list
                     
            if ( String.isNotBlank(lstAcc[0].TF4SF__Application__r.TF4SF__First_Name__c) ) {
              con.FirstName = lstAcc[0].TF4SF__Application__r.TF4SF__First_Name__c;
            } else {
              con.FirstName = 'FirstName';
            }

            if ( String.isNotBlank(lstAcc[0].TF4SF__Application__r.TF4SF__Last_Name__c)) {
              con.LastName = lstAcc[0].TF4SF__Application__r.TF4SF__Last_Name__c;
            } else {
              con.LastName = 'LastName';
            }

            if ( String.isNotBlank(lstAcc[0].TF4SF__Application__r.TF4SF__Email_Address__c)) {
              con.Email = lstAcc[0].TF4SF__Application__r.TF4SF__Email_Address__c;
            }
            else{
              con.Email = 'a@a.com';
            }
          

          //Insert Temp Contact
          insert con;
          conId = con.Id; 
          //Fetching Org Wide Email Address
          //OrgWideEmailAddress owea = [SELECT Id,Address FROM OrgWideEmailAddress LIMIT 1]; 
          OrgWideEmailAddress owa = [SELECT Id, Address, DisplayName FROM OrgWideEmailAddress where DisplayName ='talk2us@eastwestbank.com' limit 1];
                       
          //Fetching Email Template
          EmailTemplate et = [SELECT Id, Name,  DeveloperName FROM 
                            EmailTemplate WHERE DeveloperName = 'Mortgage_PreQualification_Letter'
                            LIMIT 1]; 

          for(TF4SF__About_Account__c obj : lstAcc ) {
            if ( String.isNotBlank(obj.TF4SF__Application__r.TF4SF__Email_Address__c) ) {

                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

               // Take the PDF content
               Attachment att = mapAttach.get(obj.TF4SF__Application__c);
               Blob b = att.Body;
               // Create the email attachment
               Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();            
               efa.setFileName('Pre-Qualification_Letter.pdf');
               efa.setBody(b);


                if (et != null) { mail.setTemplateId(et.Id); }
                mail.setTargetObjectId(con.Id);
                mail.setTreatTargetObjectAsRecipient(false);
                //mail.setOrgWideEmailAddressId(owea.Id);
                if(owa != null) {
                  mail.setOrgWideEmailAddressId(owa.id); 
                }
                mail.setToAddresses(new String[] {
                    obj.TF4SF__Application__r.TF4SF__Email_Address__c
                });
                mail.setWhatId(obj.TF4SF__Application__c);
                mail.setSaveAsActivity(false);
                mail.setBccSender(false);
                mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});

                mails.add(mail); 

            }
          }

          // Send all emails in the master list
          if (mails != null) { Messaging.sendEmail(mails); }         
        
      } catch (DMLException ex) {
          System.debug('An occured occured in Send Reminder email' + ex.getMessage());
          System.debug('An occured occured in Send Reminder email Line number' + ex.getLineNumber());
          
      } finally{
          //Deleting Inserted Dummy Contact in Catch block
          List<Contact> delConLst = null;
          delConLst = [SELECT Id FROM Contact WHERE Id = :conId];
          if (delConLst.size() > 0) { delete delConLst; }
      }
    }

}