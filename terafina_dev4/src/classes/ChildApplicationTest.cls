@isTest
public  class ChildApplicationTest{

@isTest static void Test_one(){

 Map<String,Trustee_Guarantor__c> mapTRGU =  new Map<String,Trustee_Guarantor__c>();
    List<TF4SF__Application__c> lstChildApplication = new List<TF4SF__Application__c>();
    List<Trustee_Guarantor__c> lstTRGUUpdate = new List<Trustee_Guarantor__c>();
    List<TF4SF__About_Account__c> lstAboutAccount = new List<TF4SF__About_Account__c>();
    List<TF4SF__Identity_Information__c> lstAIdentity = new List<TF4SF__Identity_Information__c>();
    List<TF4SF__Employment_Information__c> lstEmployment = new List<TF4SF__Employment_Information__c>();
    List<TF4SF__Application2__c> lstApplicationTwo = new List<TF4SF__Application2__c>();
    Map<Id,TF4SF__About_Account__c> mapac = new Map<Id,TF4SF__About_Account__c>();
   
   TF4SF__Application__c app= new TF4SF__Application__c();
   
          app.TF4SF__Application_Status__c = 'Save for later';
          app.TF4SF__Product__c = 'Home-Loan';
          app.TF4SF__Sub_Product__c =  'Home-Loan'; 
          app.TF4SF__Custom_Text2__c = 'Business Entity'; 
          app.TF4SF__Email_Address__c = 'Test@gmail.com';
          app.TF4SF__First_Name__c ='TestFirst';
          app.TF4SF__Middle_Name__c = 'TestMiddle';
          app.TF4SF__Last_Name__c = 'TestLast';
          app.TF4SF__Suffix__c = 'TestSuffix';
          app.Page_Flow_Completed__c ='PurchaseDetailsPage,PropertyDetailsPage';
          app.TF4SF__Primary_Phone_Number__c = '5896454344';  
 
          insert app;
          
          TF4SF__About_Account__c acc = new TF4SF__About_Account__c();
            acc.TF4SF__Application__c =app.id;
            acc.TF4SF__Mortgage_Applied_For__c ='test';
            acc.TF4SF__Purchase_P__c = 4.54;
            acc.TF4SF__Down_Payment__c =45.45;
            acc.Down_Payment_By_Percentage__c = '10.46';
            acc.TF4SF__Total_Loan_Amount__c = 67.6;
            acc.TF4SF__Street_Address_1_AboutAccount__c = 'teststreet1';
            acc.TF4SF__Street_Address_2_AboutAccount__c = 'teststreet2';
            acc.TF4SF__City_AboutAccount__c = 'testcity';
            acc.TF4SF__State_AboutAccount__c = 'teststate';
            acc.TF4SF__Zip_Code_Mrt__c = 'testzipcode';
            acc.TF4SF__County_AboutAccount__c = 'testcountry';
            acc.MRT_Built_Year__c = '1987';
            acc.TF4SF__Property_Type__c = 'test';
            acc.TF4SF__Occupancy__c ='testoccu';
            
            
            insert acc;
            mapac.put(app.id,acc);
          
          TF4SF__Identity_Information__c iden = new TF4SF__Identity_Information__c();
          Iden.TF4SF__Application__c = app.Id;
          insert iden;

          TF4SF__Employment_Information__c emp = new TF4SF__Employment_Information__c();
          Emp.TF4SF__Application__c = app.Id;
          insert emp;

          TF4SF__Application2__c app2= new TF4SF__Application2__c();
          app2.TF4SF__Application__c = app.Id;
          insert app2;
    TF4SF__Application__c app1= new TF4SF__Application__c();
   app1.Parent_Application__c = app.id;
          app1.TF4SF__Application_Status__c = 'Save for later';
          app1.TF4SF__Product__c = 'Home-Loan';
          app1.TF4SF__Sub_Product__c =  'Home-Loan'; 
          app1.TF4SF__Custom_Text2__c = 'Business Entity'; 
          app1.TF4SF__Email_Address__c = 'Test@gmail.com';
          app1.TF4SF__First_Name__c ='TestFirst';
          app1.TF4SF__Middle_Name__c = 'TestMiddle';
          app1.TF4SF__Last_Name__c = 'TestLast';
    
          app1.TF4SF__Suffix__c = 'TestSuffix';
          app1.Page_Flow_Completed__c ='PurchaseDetailsPage,PropertyDetailsPage';
          app1.TF4SF__Primary_Phone_Number__c = '5896454344';  
             app1.Trustee_Guarantor_Application__c=true;
 insert app1;
          
    
  
          Trustee_Guarantor__c objTG = new Trustee_Guarantor__c();
          objTG.Application__c=app.id;
          objTG.Email_Address__c='test@gmail.com';
          objTG.First_Name__c ='TestFirst';
           objTG.Child_Application__c=app1.id;
            
    
          objTG.Last_Name__c = 'TestLast';
          //objTG.Application__r.TF4SF__Email_Address__c='test@gmail.com';
          //objTG.Application__r.TF4SF__Custom_Text2__c='Business Entity';
          insert objTG; 
        
          lstTRGUUpdate.add(objTG);
          //lstTRGUUpdate[0].Application__c=app.id;
    
            OrgWideEmailAddress objemail = new OrgWideEmailAddress();
            objemail.Address = 'a@a.com';
    
         test.startTest();
        List<Trustee_Guarantor__c> lstTRGUUpdate1 = [select id,Email_Address__c,First_Name__c,Last_Name__c,Application__r.TF4SF__Custom_Text2__c ,Application__r.TF4SF__Email_Address__c from Trustee_Guarantor__c where Application__c =:app1.id];
         ChildApplication.createChildApplication(lstTRGUUpdate,mapac);
         ChildApplication.sendemail(lstTRGUUpdate1);
         ChildApplication.sendReminderEmail(lstTRGUUpdate,'First');
         test.stopTest();
          
          



}
}