@istest
class DataPurgingBatchJob_Sch_Test {
    static testmethod void test() {

        Test.StartTest();
        DataPurgingBatchJob_Sch sh1 = new DataPurgingBatchJob_Sch();
    
        String sch = '0 0 23 * * ?'; 
        system.schedule('DataPurgingBatchJobScheduler', sch, sh1); 
        Test.stopTest();
    }
}