//Controller to display the Applications on OTP validation by primary applicatnts or by the 
//joint applicants.
public with sharing class EW_ResumeApplication { 

    public EW_ResumeApplication() {       
                
    }

    @remoteaction
    //To search for applications based on the email ID
    public static String searchApplication (Map< String , String > searchParamMap ) {
        String applicantEmailId = searchParamMap.get('applicantEmailId');
        String phNumber = '';
        List<ApplicationWrapper> lstWrapper;

        try {    

            if (String.isNotBlank(applicantEmailId)) {

                List<TF4SF__Application__c> lstApplication = [SELECT Id, Name,TF4SF__SiteUrl__c,LastModifiedDate,CreatedDate,CreatedById,
                                                            TF4SF__Application_Status__c,TF4SF__Sub_Product__c , TF4SF__Email_Address_J2__c, 
                                                            TF4SF__Email_Address_J3__c, TF4SF__Email_Address_J__c, TF4SF__Email_Address__c, 
                                                            TF4SF__Primary_Phone_Number__c, TF4SF__Primary_Phone_Number_J__c, TF4SF__Primary_Phone_Number_J3__c, 
                                                            TF4SF__Primary_Phone_Number_J2__c 
                                                            FROM TF4SF__Application__c 
                                                            WHERE (TF4SF__Email_Address_J2__c=: applicantEmailId OR TF4SF__Email_Address_J3__c= : applicantEmailId 
                                                            OR TF4SF__Email_Address_J__c=: applicantEmailId OR  TF4SF__Email_Address__c=: applicantEmailId) 
                                                            AND (TF4SF__Application_Status__c='Abandoned' OR TF4SF__Application_Status__c='Save for Later')
                                                            AND TF4SF__Product__c ='Home Loan'
                                                            ORDER BY LastModifiedDate DESC];
               

                //Loop through the result
                lstWrapper = new  List<ApplicationWrapper>();
                for (TF4SF__Application__c obj : lstApplication){
                    //Encrypt the application ID before sending it to front end
                    String applicationId ;
                    if(!Test.isRunningTest()){
                        applicationId =  CryptoHelperCustom.encrypt(obj.Id);
                    }
                    if(Test.isRunningTest()){
                        applicationId = obj.id;
                    }
                    ApplicationWrapper objWrapper = new ApplicationWrapper( obj.Name,
                                                                            obj.TF4SF__Sub_Product__c,
                                                                            obj.TF4SF__Application_Status__c,
                                                                            applicationId,
                                                                            obj.CreatedById,
                                                                            obj.TF4SF__SiteUrl__c,
                                                                            obj.LastModifiedDate,
                                                                            '');

                    lstWrapper.add(objWrapper);
                }
            }

        } catch (Exception ex) {
            System.debug('Execption in searchApplication ==>' + ex.getMessage());
            System.debug('Execption in searchApplication Line number ==>' + ex.getLineNumber());
        }    

        if(lstWrapper != null && lstWrapper.size() > 0) {
            return JSON.serialize(lstWrapper);
        }
        else {
            return 'error';
        }
    }


     @remoteaction
    //To fetch the phone number and sent the OTP to appplicants phone number
    //public static String sendOTP (String applicationId,String applicantEmailId) {
    public static String sendOTP (Map<String , String> mapData) { 
        String applicationId = mapData.get('applicationId');   
        String applicantEmailId = mapData.get('applicantEmailId');
        String appId = '';
        String phNumber = '';
        String returnString = '';
        String usrId = '';
        String userId = '', factorId = '', smsId = '', oktaStatus = '';
        String responseFactorId = '' ,responseStatus = '';
        Integer status = 0;
        boolean sendOTP = false;
        Map<String, String> enrollMap = new Map<String, String>();
        List<OKTA__c> oktaList =  new List<OKTA__c>();
        List<ApplicationWrapper> lstWrapper;

        try {    
                if (String.isNotBlank(applicationId) && String.isNotBlank(applicantEmailId)){
                    
                    if (Test.isRunningTest()){
                        appId = applicationId;
                    }
                    if (!Test.isRunningTest()){
                        appId = CryptoHelperCustom.decrypt(applicationId);
                    }


                    List<TF4SF__Application__c> lstApplication = [SELECT Id, Name,TF4SF__SiteUrl__c,LastModifiedDate,CreatedDate,CreatedById,TF4SF__Application_Status__c,TF4SF__Sub_Product__c , TF4SF__Email_Address_J2__c, TF4SF__Email_Address_J3__c, TF4SF__Email_Address_J__c, TF4SF__Email_Address__c, TF4SF__Primary_Phone_Number__c, TF4SF__Primary_Phone_Number_J__c, TF4SF__Primary_Phone_Number_J3__c, TF4SF__Primary_Phone_Number_J2__c FROM TF4SF__Application__c where (Id =:appId) AND (TF4SF__Application_Status__c='Abandoned' OR TF4SF__Application_Status__c='Save for Later')];

                     //get phone number
                    if (lstApplication !=null && ! lstApplication.isEmpty() ) {

                        TF4SF__Application__c app = lstApplication[0];
                        System.debug('app.CreatedById ==>' + app.CreatedById);                    
                        if ( String.isNotBlank(app.TF4SF__Primary_Phone_Number__c) && String.isNotBlank(app.TF4SF__Email_Address__c) && app.TF4SF__Email_Address__c.equalsIgnoreCase(applicantEmailId) ) {
                            phNumber = String.valueOf(app.TF4SF__Primary_Phone_Number__c).remove('(').trim().remove(')').trim().remove('-').replace(' ', '');                        
                        }
                        else if (  String.isNotBlank(app.TF4SF__Primary_Phone_Number_J__c) &&  String.isNotBlank(app.TF4SF__Email_Address_J__c) && app.TF4SF__Email_Address_J__c.equalsIgnoreCase(applicantEmailId)) {
                            phNumber = String.valueOf(app.TF4SF__Primary_Phone_Number_J__c).remove('(').trim().remove(')').trim().remove('-').replace(' ', '');                       
                        }
                        else if (  String.isNotBlank(app.TF4SF__Primary_Phone_Number_J2__c) &&  String.isNotBlank(app.TF4SF__Email_Address_J2__c) && app.TF4SF__Email_Address_J2__c.equalsIgnoreCase(applicantEmailId)) {
                            phNumber = String.valueOf(app.TF4SF__Primary_Phone_Number_J2__c).remove('(').trim().remove(')').trim().remove('-').replace(' ', '');                        
                        }
                        else if (  String.isNotBlank(app.TF4SF__Primary_Phone_Number_J3__c) &&  String.isNotBlank(app.TF4SF__Email_Address_J3__c) && app.TF4SF__Email_Address_J3__c.equalsIgnoreCase(applicantEmailId)) {
                            phNumber = String.valueOf(app.TF4SF__Primary_Phone_Number_J3__c).remove('(').trim().remove(')').trim().remove('-').replace(' ', '');                        
                        }
                        
                            if (String.isNotBlank(applicantEmailId)) {
                                oktaList = [SELECT Id, Email_Address__c, Factor_Id__c, SMS_Id__c, Status__c, User_Id__c FROM OKTA__c WHERE Email_Address__c =: applicantEmailId.toLowerCase() LIMIT 1];
                                if (!oktaList.isempty()) {
                                    userId=oktaList[0].User_Id__c;
                                    factorId=oktaList[0].Factor_Id__c;
                                    smsId=oktaList[0].SMS_Id__c;
                                    oktaStatus=oktaList[0].Status__c;
                                }
                            }              
                 
                            if(String.isNotBlank(userId)) {  
                                if (String.isNotBlank(factorId) && oktaStatus == 'ACTIVE') {
                                    responseFactorId = EmailValidationExtension.challenge(appId, AuthToken.Auth(), userId, smsId);
                                    responseStatus = oktaList[0].Status__c;
                                    sendOTP = true;
                                    //data.put('Application__c.OKTA_FactorId__c',responseFactorId);
                                } else if (String.isNotBlank(phNumber)) {
                                    enrollMap = EmailValidationExtension.enrollSMS(appId, AuthToken.Auth(), userId, phNumber);
                                    responseFactorId = enrollMap.get('factorId');
                                    responseStatus = enrollMap.get('oktaStatus');
                                    sendOTP = true;
                                    //data.put('Application__c.OKTA_FactorId__c',responseFactorId);
                                }                   
                               
                                if (sendOTP) {
                                    status=200;
                                    if (status == 200){
                                        EmailValidationExtension.updateOkta(appId, applicantEmailId, userId, responseFactorId, responseStatus, smsId);
                                    }
                                } 
                            }                      
                                         
                        //Write function here to send OTP to the number
                        if(phNumber.length() >= 4 ){
                            returnString =  '{';
                            returnString +=     '\"phoneNumber\": \"' + phNumber.right(4) + '\",';
                            returnString +=     '\"appId\" : \"' + applicationId + '\",';
                            returnString +=     '\"randomId\":\"' + app.CreatedById + '\"'; 
                            returnString += '}';
                            //phNumber.right(4);
                        }


                    }                       
                }

             } catch (Exception ex) {
            System.debug('Execption in sendOTP ==>' + ex.getMessage());
            System.debug('Execption in sendOTP Line number ==>' + ex.getLineNumber());
        } 

        return returnString;
    }

    @remoteaction
    //To validate the otp and resume the application
    //public static String validateOTP (String otp,String applicationId, String userId) {
    public static String validateOTP (Map<String,String> mapData) {
        String otp = mapData.get('otp');
        String applicationId = mapData.get('applicationId');
        String userId = mapData.get('userId');
        String emailId = mapData.get('email');    
        String returnString = 'invalidotp';
        Boolean validOTP = false;
        String factorId = '', oktauserId = '', smsId = '', oktaStatus = '', factorResult = '',responsesmsId = '', responseStatus = '', valid = '', status = '';
        List<OKTA__c> oktaList =  new List<OKTA__c>();
        Map<String, String> verifyEnroll = new Map<String, String>();
        try {            

                //********************************************************************
                //Write function here to validate the OTP and return the resume application URL
                //********************************************************************
                if (String.isNotBlank(emailId)) {
                    oktaList = [SELECT Id, Email_Address__c, Factor_Id__c, SMS_Id__c, Status__c, User_Id__c FROM OKTA__c WHERE Email_Address__c =: emailId.toLowerCase() LIMIT 1];
                    if (!oktaList.isempty()) {
                        factorId = oktaList[0].factor_Id__c;
                        oktauserId = oktaList[0].User_Id__c;
                        smsId = oktaList[0].SMS_Id__c;
                        oktaStatus = oktaList[0].Status__c;
                    }
                    
                    //Get recent abandoned app based on email id
                        
                    if (String.isNotBlank(oktaStatus)) {
                        if (oktaStatus == 'ACTIVE') {
                            if (String.isNotBlank(smsId)) {
                                factorResult = ValidateOTPExtension.verify(CryptoHelperCustom.decrypt(applicationId), AuthToken.Auth(), oktauserId, otp, smsId);
                                responsesmsId = oktaList[0].SMS_Id__c;
                                responseStatus = oktaList[0].Status__c;
                                if (factorResult == 'SUCCESS') {
                                    valid='true';
                                    status='200';
                                    validOTP = true;    
                                }
                                
                            }
                        } else {
                           verifyEnroll = ValidateOTPExtension.verifySMSRegisteration(CryptoHelperCustom.decrypt(applicationId), AuthToken.Auth(), oktauserId, otp, factorId);
                           responsesmsId = verifyEnroll.get('smsId');
                           responseStatus = verifyEnroll.get('oktaStatus');
                           if(String.isNotBlank(responsesmsId)) {
                            valid='true';
                            status='200';
                            validOTP = true;
                           }
                        }
                    }
                    
                }

                 if ((validOTP && String.isNotBlank(applicationId) & String.isNotBlank(userId)) || Test.isRunningTest()){

                    TF4SF__Application_Configuration__c appConfig = TF4SF__Application_Configuration__c.getOrgDefaults();
                    TF4SF__SiteUrl__c siteConfig = TF4SF__SiteUrl__c.getOrgDefaults();

                    String appId = CryptoHelperCustom.decrypt(applicationId);                   

                    returnString = siteConfig.TF4SF__URL__c + 'DSPResumeApplication?id=' + appId + '&usr=' + userId;
                 }
            	
                 if (status == '200'){
                     EmailValidationExtension.updateOkta(CryptoHelperCustom.decrypt(applicationId), emailId, oktauserId, factorId, responseStatus, responsesmsId);
                 }

            } catch (Exception ex) {
            System.debug('Execption in validateOTP ==>' + ex.getMessage());
            System.debug('Execption in validateOTP Line number ==>' + ex.getLineNumber());
        } 

        return returnString;
    }

    public class ApplicationWrapper {
        public String name{get;set;}
        public String subProduct{get;set;}
        public String applicationStatus{get;set;}
        public String appId{get;set;}
        public String createdById{get;set;}
        public String siteUrl{get;set;}
        public String elapsedTime{get;set;}
        public String phoneNumber{get;set;}


        public ApplicationWrapper(  String name,
                                    String subProduct,
                                    String applicationStatus,
                                    String appId,
                                    String createdById,
                                    String siteUrl,
                                    Datetime elapsedTime,
                                    String phoneNumber){
            this.name = name;
            this.subProduct = subProduct;
            this.applicationStatus = applicationStatus;
            this.appId = appId;
            this.createdById = createdById;
            this.siteUrl = siteUrl ;
            this.elapsedTime = elapsedTime.format('MM-dd-yyyy h:mma', 'PST');
            this.phoneNumber = phoneNumber;
        }
    }
}