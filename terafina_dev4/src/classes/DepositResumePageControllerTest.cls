@isTest
public class DepositResumePageControllerTest{
    
    @isTest static void test_method_one() {
    
       String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
      EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US', ProfileId = p.Id,
      TimeZoneSidKey = 'America/Los_Angeles',
      UserName = uniqueUserName,TF4SF__Location__c='US',TF4SF__Channel__c='Online');

    System.runAs(u) {
        
        
        TF4SF__Application__c app=new TF4SF__Application__c();
        app.TF4SF__Application_Status__c='open';
        app.Ownerid=u.id;
        app.TF4SF__Current_Person__c=u.id;
        app.TF4SF__Application_Page__c='GetStartedPage';
        app.TF4SF__Current_Channel__c='online';
        app.TF4SF__Current_Branch_Name__c='US';
        app.TF4SF__Current_User_Email_Address__c='standarduser@testorg.com';
        app.TF4SF__User_Token__c='test';
        app.TF4SF__Application_Version__c='test2';
        app.TF4SF__User_Token_Expires__c=System.now();
        app.TF4SF__Current_timestamp__c=System.now(); 
        insert app;
        
        TF4SF__Application_Activity__c appact=new TF4SF__Application_Activity__c();
            appact.TF4SF__Application__c=app.Id;
            appact.TF4SF__Action__c='Resumed the Application';
            appact.TF4SF__Activity_Time__c = System.now();
            appact.TF4SF__Channel__c='online';
            appact.TF4SF__Branch__c='US';  
            appact.TF4SF__Name__c =u.id;
            insert appact;
            
        PageReference pageRef = Page.DepositResumePage;
        Test.setCurrentPage(pageRef);
 
        pageRef.getParameters().put('Id',app.id);   
        pageRef.getParameters().put('usr',u.id);   

        ApexPages.StandardController stc = new ApexPages.StandardController(app);

        test.startTest();
        
        DepositResumePageController dsp=new DepositResumePageController(stc);
        PageReference objPageRef = dsp.ResumeApp();
        test.stopTest();
        
    }
    }
  
}