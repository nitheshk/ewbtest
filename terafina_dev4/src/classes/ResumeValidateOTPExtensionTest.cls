@isTest
public class ResumeValidateOTPExtensionTest{

@isTest static void test_method_one() {


Map<String, String> tdata = new Map<String, String>();
Map<String, String> verifyEnroll = new Map<String, String>();
Map<String, String> data = new Map<String, String>();

     List <TF4SF__Application__c>  listCurrentApp=new List <TF4SF__Application__c>();
         TF4SF__Application__c app = new TF4SF__Application__c();
         app.TF4SF__Product__c = 'Checking';
         app.TF4SF__Sub_Product__c = 'Checking - Checking';
         app.TF4SF__Email_Address__c='test@gmail.com'; 
         app.TF4SF__First_Joint_Applicant__c = true;
         app.TF4SF__Second_Joint_Applicant__c = true;
         app.TF4SF__Third_Joint_Applicant__c = true;
         app.TF4SF__Application_Status__c='Abandoned';
         app.OKTA_UserId__c='test';
         app.OKTA_factorId__c='test';
         app.OKTA_SMSId__c='test';
         app.OKTA_Status__c='ACTIVE';
        
        insert app;
        
        List<OKTA__c> oktaList = new List<OKTA__c>();
        OKTA__c okta=new OKTA__c();
        okta.Factor_Id__c='test';
        okta.SMS_Id__c='test';
        okta.Status__c='ACTIVE';
        okta.User_Id__c='test';
        okta.Email_Address__c='test@gmail.com';
        oktaList.add(okta);
        insert oktaList;
        
        test.startTest();
        tdata.put('id',app.Id);
        data.put('otp','otp');        
        ResumeValidateOTPExtension.main(tdata);
        Test.stopTest();
}
}