@isTest
private class JasonApexTest {
    //test class for Following Parser classes
    //CustomerInfoFundsDetails
    //IDAuthAndVerifyMapping
    //IDAuthResponse
    //IdVerificationPayload
    //JsonParser
    
	private static testMethod void CustomerInfoFundsDetailsTest() {
        CustomerInfoFundsDetails obj=CustomerInfoFundsDetails.parse('{}');
	    system.assertNotEquals(null, obj);
	}
	
	private static testMethod void IDAuthAndVerifyMappingTest() {
        IDAuthAndVerifyMapping obj=IDAuthAndVerifyMapping.parse('{}');
        system.assertNotEquals(null, obj);
	}
    
    	
	private static testMethod void IDAuthResponseTest() {
        IDAuthResponse obj=IDAuthResponse.parse('{}');
        system.assertNotEquals(null, obj);
	}
	
	private static testMethod void IdVerificationPayloadTest() {
        IdVerificationPayload obj=IdVerificationPayload.parse('{}');
        system.assertNotEquals(null, obj);
	}
	
	private static testMethod void JsonParserTest() {
        JsonParser obj=new JsonParser();

        JsonParser.Status status=new JsonParser.Status();
        status.TransactionStatus='In progress';
        status.ActionType='Save for Later';
        status.RequestId='101';
        status.Reference='abc';
        status.ConversationId='12345';
        
        obj.Status=status;

        List<JsonParser.Products> products=new List<JsonParser.Products>();
        JsonParser.Products product=new JsonParser.Products();
        product.ExecutedStepName='';
        product.ProductStatus='Active';
        product.ProductConfigurationName='CD60';
        product.ProductType='Certificates';
        
        List<JsonParser.Items> items = new List<JsonParser.Items>();
        JsonParser.Items item=new JsonParser.Items();
        item.ItemName='60 months Certificate';
        item.ItemStatus='Active';

        items.add(item);

        product.Items=items;

        products.add(product);

        obj.Products=products;

       }
	
    
    
}