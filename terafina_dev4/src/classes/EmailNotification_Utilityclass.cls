//Utility class to send an email after an hour/3days/5days
public class EmailNotification_Utilityclass {
    
    //public  void sendEmail(set<string> appid) { //Sending the email 
    public  void sendEmail(List<TF4SF__Application__c> appList) { //Sending the email 
        
        List<TF4SF__Application__c> lstApp = new List<TF4SF__Application__c>();
        //List<TF4SF__Application__c> appList = [SELECT Id, TF4SF__First_Name__c, is_Processed_Abandoned_Mail_1__c , is_Processed_Abandoned_Mail_2__c , is_Processed_Abandoned_Mail_3__c, TF4SF__Email_Address__c, Time_Diff__c FROM TF4SF__Application__c WHERE Id IN :appId ];
        List<TF4SF__Application_Configuration__c> appConfig = [SELECT Id, Abandoned_Email_Time1__c, Abandoned_Email_Time2__c, Abandoned_Email_Time3__c FROM TF4SF__Application_Configuration__c];
        List<Messaging.SingleEmailMessage> mailsList =  new List<Messaging.SingleEmailMessage>();
        //System.debug(appList);
        
        //******Get EmailTemplate whose developer name Status_Portal_Member_and_Non_Member_Notification_new
        EmailTemplate templateId1H = [SELECT Id, Body, Subject, HtmlValue FROM EmailTemplate WHERE DeveloperName = 'Application_Abandoned_Email'];
        //EmailTemplate templateId3D = [SELECT Id, Body, Subject, HtmlValue FROM EmailTemplate WHERE DeveloperName = 'Application_Abandoned_Email_2Hour'];
        //EmailTemplate templateId5D = [SELECT Id, Body, Subject, HtmlValue FROM EmailTemplate WHERE DeveloperName = 'Application_Abandoned_Email_3Hour'];
        List<Contact> con = [SELECT Id FROM Contact LIMIT 1];
        
        for (TF4SF__Application__c application : appList) {
            //******Set Emailbody
            if (application.TF4SF__Product__c=='Home Loan' &&  application.TF4SF__Email_Address__c != null && application.is_Processed_Abandoned_Mail_1__c == false && (application.Time_Diff__c >= appConfig[0].Abandoned_Email_Time1__c) ) {
                application.is_Processed_Abandoned_Mail_1__c = true ;
                lstApp.add(application);
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail = mailMessage(templateId1H.id, application.TF4SF__Email_Address__c, application.id);
                //System.Debug('mail: ' + mail);
                //*****Add List of mail in mailsList
                mailsList.add(mail);
            }
           
            
        }
        
        try {
            if ( mailsList.size() > 0 ) {
                
                Messaging.sendEmail(mailsList );
                if( lstApp.size() > 0){
                    Set<TF4SF__Application__c> myset = new Set<TF4SF__Application__c>();
                    List<TF4SF__Application__c> result = new List<TF4SF__Application__c>();
                    myset.addAll(lstApp);
                    result.addAll(myset);
                    
                    update result;
                }
                //******Update docuRequestList
                
            }
            
        } catch (Exception ex) {
            //System.debug('=================' + ex.getlinenumber() + ex.getmessage());
        }
    }
    
    public  Messaging.SingleEmailMessage mailMessage(String TemplateId, String emailAddress, String appId) {
        List<Contact> con = [SELECT Id FROM Contact LIMIT 1];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setTemplateId(TemplateId);
        mail.toAddresses = new string[]{emailAddress} ;
            if(con.size () > 0) { 
                mail.setTargetObjectId(con[0].id);
            }else
            {
                Contact cons = new Contact();
                cons.Lastname= 'DummyTest';
                insert cons;
                mail.setTargetObjectId(cons.id);
            }
        mail.setWhatId(appId);
        // mail.setTargetObjectId(UserInfo.getUserId());
        mail.setTreatTargetObjectAsRecipient(false);
        
        return mail;
    }
}