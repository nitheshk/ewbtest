/*********************************************
@Author : Nag
@Date : 8/1/2018
@Description : This will drive  routing
**********************************************/
global with sharing class IDAuthenticationExtension implements TF4SF.DSP_Interface {
    global Map<String, String> main(Map<String, String> tdata) {
        Map<String, String> data = new Map<String, String>();
        data = tdata.clone();
        String appId = data.get('id');
        String idType=data.get('idType');
        String isNRA=data.get('isNra');
        String IS_NRA_NO_UI_VALUE = Label.NRA_NO_UI_VALUE;
        System.debug('id---->'+ ' '+ appId + ' ' + idType + ' '+ isNRA); 
        try {
                //For SSN Flow return data directly with existing design
                if(isNRA !=null && isNRA.equalsignorecase(IS_NRA_NO_UI_VALUE)){
                    DriverLicenseScan dlScan = new DriverLicenseScan();
                    data = dlScan.main(tdata);
                }else{
                    // NRA FLOW new design to handle any ID scan process
                    IDDocumentScan idScan = new IDDocumentScan();
                    data = idScan.main(tdata);
                }
                return data;
            } catch (Exception ex) {
                System.debug('NEVER COMES HERE :' + ex);
                return data;
            }
     }
}