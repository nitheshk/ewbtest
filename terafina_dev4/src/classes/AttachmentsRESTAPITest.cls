@isTest
public class AttachmentsRESTAPITest{
    
    static testmethod void validateStandardController(){
        
        Map<String, String> tdata = new Map<String, String>();
        Map<String, String> data = new Map<String, String>();
        TF4SF__Application__c app1 = new TF4SF__Application__c();
        app1.TF4SF__Product__c = 'Home Loan';
        app1.TF4SF__First_Name__c='test';
        app1.TF4SF__Last_Name__c = 'Test';
        app1.TF4SF__Email_Address__c= 'test@test.com';
        app1.TF4SF__Sub_Product__c = 'Home Loan - Short App';
        
        insert app1;
        
        TF4SF__Application_Configuration__c appConfig = new TF4SF__Application_Configuration__c();
        appConfig.TF4SF__Application_Code__c = 'dsp4_0';
        appConfig.TF4SF__Theme__c = 'dsp4_0';
        appConfig.TF4SF__Key__c = 'YMmKJt5QxS7KlAEASMsj4Q==';
        appConfig.TF4SF__DL_Endpoint_URL__c = 'https://app1.idware.net/DriverLicenseParserRest.svc/ParseImage';
        insert appConfig;  
        
        TF4SF__Identity_Information__c iden = new TF4SF__Identity_Information__c();
        iden.TF4SF__Application__c = app1.Id;
        insert iden;
        
        Blob b = Blob.valueOf('Test Data');
        Attachment att = new Attachment();
        att.name = 'testname';
        att.Body = b;
        att.Description = 'test';
        att.ParentId = app1.Id;
        att.ContentType = 'test';
        insert(att);
        
        test.startTest();
        
        Test.setMock(HttpCalloutMock.class, new AttachmentsRESTAPI_Mock ());
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = 'https://eastwestbank--dev.cs20.my.salesforce.com/services/apexrest/Attachments/v1/';  
       
        req.httpMethod = 'POST';
        
        
        
        TF4SF__Application__c app = new TF4SF__Application__c ();
        app.TF4SF__Product__c = 'Checking';
        app.TF4SF__Sub_Product__c = 'Checking - Checking';
        app.TF4SF__First_Name__c='firstname';
        app.TF4SF__Last_Name__c='lastname';
        app.TF4SF__City__c='testcity';
        app.TF4SF__Zip_Code__c='testzip';
        app.TF4SF__State__c='teststate';
        app.TF4SF__Street_Address_1__c='teststreet1';
        app.TF4SF__Street_Address_2__c='teststreet2';
        insert app;
        
        string JsonMsg='{"attachmentId ":att.id,"contentType ":"test"}';
        req.addparameter('attachmentId',att.id);
        req.addparameter('type','image/jpeg');
        req.addparameter('encoded','true');
        req.addparameter('parentId',app.id);
        req.addparameter('fileName','test');
        
        
        
        req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response = res;
        AttachmentsRESTAPI objclass = new AttachmentsRESTAPI ();
        AttachmentsRESTAPI.attachDoc(); 
        AttachmentsRESTAPI.getAttachment();
        AttachmentsRESTAPI.deleteAttachment();
        test.stopTest(); 
    }
}