@isTest
private class PersonalInfoExtension_Test {
    
    @isTest static void test_method_one() {
        // Implement test code
       
        TF4SF__Application__c app = new TF4SF__Application__c();
        app.TF4SF__Custom_Checkbox2__c = false;
        app.TF4SF__Custom_Checkbox3__c = true;
        app.TF4SF__Custom_Checkbox4__c = true;
        app.TF4SF__Custom_Text3__c ='800';
        app.TF4SF__Months__c  ='3';
        app.TF4SF__Custom_Text4__c='800';
        app.TF4SF__Months_J__c = '3';
        app.TF4SF__Custom_Text5__c = '800';
        app.TF4SF__Months_J2__c = '3';
        app.TF4SF__Custom_Text6__c = '800';
        app.TF4SF__Months_J3__c ='3';
        app.TF4SF__First_Joint_Applicant__c = true;
            app.TF4SF__Second_Joint_Applicant__c = true;
            app.TF4SF__Third_Joint_Applicant__c = true;
            app.TF4SF__Product__c = 'Home Loan';
        app.TF4SF__First_Name__c='test';
        app.TF4SF__Last_Name__c = 'Test';
        app.TF4SF__Email_Address__c= 'test@test.com';
        app.TF4SF__Sub_Product__c = 'Home Loan - Short App';
        
        insert app;
        Map<String, String> data = new Map<String, String> ();
            PersonalInfoExtension ah = new PersonalInfoExtension();
            Test.startTest();
           
            data.put('id',app.Id);
            PersonalInfoExtension.main(data);
        Test.stopTest();
    }
}