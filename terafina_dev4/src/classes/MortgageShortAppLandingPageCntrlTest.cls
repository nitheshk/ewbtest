@isTest(SeeAllData=true)
global with sharing class MortgageShortAppLandingPageCntrlTest {
    
@isTest static void test_method_one(){
    
String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
    Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
    User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
      EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US', ProfileId = p.Id,
      TimeZoneSidKey = 'America/Los_Angeles',
      UserName = uniqueUserName);
    System.runAs(u) {

       TF4SF__Application_Configuration__c appConfig = new TF4SF__Application_Configuration__c();
        appConfig.TF4SF__Application_Code__c = 'dsp4_0';
        appConfig.TF4SF__Theme__c = 'dsp4_0';
        appConfig.TF4SF__Key__c = 'YMmKJt5QxS7KlAEASMsj4Q==';
        appConfig.TF4SF__Popup_Seconds__c = 120;
        appConfig.TF4SF__Timeout_Seconds__c = 900;
        //insert appConfig;   
        
	  TF4SF__Application__c app = new TF4SF__Application__c ();
        app.TF4SF__Application_Status__c = 'Open';
        app.TF4SF__Product__c = 'checking';
        app.TF4SF__Sub_Product__c =  'checking';
        app.TF4SF__Theme_URL__c='Testurl';
        app.TF4SF__Person_Number__c='4358435454';
        app.TF4SF__Created_Channel__c='Online Banking';
        app.TF4SF__Current_Channel__c='Online Banking';

        insert app;
        
        TF4SF__Product_Codes__c pc1 = new TF4SF__Product_Codes__c (Name = 'Name', TF4SF__Product__c = 'checking',
        TF4SF__Sub_Product__c = 'checking', Product_Image__c = 'Test Image', Landing_Page_Description__c = ' Test Title',
        TF4SF__Product_Theme__c='Testurl');
        insert pc1;
        
        TF4SF__About_Account__c ab = new TF4SF__About_Account__c();
        ab.TF4SF__Application__c = app.id;
        insert ab;
        
        TF4SF__Application2__c ap2 = new TF4SF__Application2__c ();
        ap2.TF4SF__Application__c = app.id;
        insert ap2;
        
        TF4SF__Identity_Information__c iden = new TF4SF__Identity_Information__c ();
        iden.TF4SF__Application__c = app.id;
        insert iden;
        
        TF4SF__Employment_Information__c em = new TF4SF__Employment_Information__c();
        em.TF4SF__Application__c = app.id;
        insert em;
        
        TF4SF__Debug_Logs__c db = new TF4SF__Debug_Logs__c();
        db.TF4SF__Application__c = app.id;
        insert db;
        
        PageReference pageRef = Page.Mortgageshortapp;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id',app.id); 
        pageRef.getParameters().put('subPrdCode','subPrdCode'); 
        pageRef.getParameters().put('PromoCode','PromoCode'); 
        pageRef.getParameters().put('ZipCode','ZipCode'); 
        pageRef.getParameters().put('State','State'); 
        
        test.startTest();
        MortgageShortAppLandingPageCntrl objCtrl = new MortgageShortAppLandingPageCntrl();
        objCtrl.createApp();    
        test.stopTest();
}
}
}