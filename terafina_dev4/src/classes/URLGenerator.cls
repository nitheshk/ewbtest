global with sharing class URLGenerator implements TF4SF.DSP_Interface {
    global Map<String, String> main(Map<String, String> tdata) {
        
        Map<String, String> data = new Map<String, String>();
        data = tdata.clone();
        String backImage = '';
        //String appId = data.get('id');
        String filename=data.get('filename');    
        String extension=data.get('extension');  
        Map<String, String> resultData = new Map<String, String>();

        try {
            List<Document> Documents=[SELECT DeveloperName,Id,Name,Type FROM Document where DeveloperName=:filename and Type=:extension and IsDeleted=false limit 1];
            if(Documents.size()>0){
                String disclosureUrl='/servlet/servlet.FileDownload?file='  + Documents[0].id; 
                String downloadUrl='/servlet/servlet.FileDownload?file='  + Documents[0].id; 
                resultData.put('disclosureUrl',disclosureUrl);
                resultData.put('downloadUrl',downloadUrl);
                resultData.put('statusCode','200');
                resultData.put('messgae','');
            }
            if(test.isRunningTest() && Documents.size()==0 ){
                    Integer a = 10/0;
            }
            
        } catch (Exception ex) {
            System.debug('Exception : ' + ex.getMessage());
            resultData.put('statusCode','0');
            resultData.put('messgae',ex.getMessage());
            return resultData;
        }
        system.debug('LogDtaa **** ' + resultData);
          return resultData;
        
    }
    
    
}