global with sharing class W9TaxFormController{

  public String fullName {get; set;}
  public String scl {get; set;}  
  public String city {get; set;} 
  public String state {get; set;}
  public String zipcode {get; set;}
  public String streetAddress1 {get; set;}
  public String streetAddress2 {get; set;}
  public String currentDate {get; set;}  
  
    global void onload(){

    }

  public String getRenderAs()
  {
    string renderAs=ApexPages.currentPage().getParameters().get('p');
    if(renderAs!=null & renderAs!='')
       return renderAs;
       else
       return null; 
  }

    global W9TaxFormController() {  
   
    String appId = ApexPages.currentPage().getParameters().get('id');
    
    if(appId!=null && appId!='')
    {
    List<TF4SF__Application__c> appList;       
            
      if (!String.isBlank(appId)) {
      appList = [SELECT Id,TF4SF__Last_Name__c,TF4SF__First_Name__c, TF4SF__Full_Name_PA__c,
      TF4SF__City__c,TF4SF__Zip_Code__c,TF4SF__State__c,TF4SF__Street_Address_1__c,TF4SF__Street_Address_2__c
       ,(SELECT Id, TF4SF__SSN_Prime__c FROM TF4SF__Identity_Information__r) 
      FROM TF4SF__Application__c WHERE Id = :appId];
      if (appList.size() > 0) {
      
          System.debug('fullName'+appList[0].TF4SF__First_Name__c);
          
          fullName = appList[0].TF4SF__Full_Name_PA__c;
          city = appList[0].TF4SF__City__c;
          state = appList[0].TF4SF__State__c;
          zipcode = appList[0].TF4SF__Zip_Code__c;
          streetAddress1 = appList[0].TF4SF__Street_Address_1__c;
          streetAddress2 = appList[0].TF4SF__Street_Address_2__c;
          currentDate=string.valueof(System.Now());

          System.debug('fullName'+fullName);
          
         if (appList[0].TF4SF__Identity_Information__r != null) {
         
             TF4SF__Identity_Information__c ii = appList[0].TF4SF__Identity_Information__r;
             
             if (ii != null && String.isNotBlank(ii.TF4SF__SSN_Prime__c)) {
                
                System.debug('fullName'+ii.TF4SF__SSN_Prime__c);
                scl = ii.TF4SF__SSN_Prime__c;
                System.debug('fullName'+scl);        

             }
         }
      }
    }  
       
    }
    
   }   
   
    
}