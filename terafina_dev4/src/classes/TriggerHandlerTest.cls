@isTest
public class TriggerHandlerTest {
    
    @isTest static void test_method1() {
        
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
            EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US', ProfileId = p.Id,
            TimeZoneSidKey = 'America/Los_Angeles',
            UserName = uniqueUserName,TF4SF__Location__c='Yakima South 1st St');
       
        System.runAs(u) {

        List<TF4SF__Application__c> applst = new List<TF4SF__Application__c>();
        List<Id> UserList = new List<Id>(); 

        TF4SF__Application__c app = new TF4SF__Application__c();
        app.TF4SF__Custom_Checkbox2__c = false;
        app.TF4SF__Custom_Checkbox3__c = true;
        app.TF4SF__Custom_Checkbox4__c = true;
        app.TF4SF__Custom_Text3__c ='800';
        app.TF4SF__Months__c  ='3';
        app.TF4SF__Custom_Text4__c='800';
        app.TF4SF__Months_J__c = '3';
        app.TF4SF__Custom_Text5__c = '800';
        app.TF4SF__Months_J2__c = '3';
        app.TF4SF__Custom_Text6__c = '800';
        app.TF4SF__Months_J3__c ='3';
        app.Abandoned_Email__c= true;
        app.TF4SF__Email_Address__c ='test@test.com'; 
        app.TF4SF__Product__c='Home Loan';
        app.TF4SF__Application_Status__c='Open';
        app.TF4SF__Sub_Product__c = 'checking';
        app.TF4SF__Application_Page__c='GetStartedPage';
        app.Lead_Score__c='544';
        applst.add(app);
        insert applst;
        
       System.debug('app.TF4SF__Application_Status__c '+app.TF4SF__Application_Status__c); 
        
        Contact con1=new Contact();
        con1.lastname='Testing';
        con1.email='test@test.com';
        insert con1;
        
        app.TF4SF__Application_Status__c='Abandoned';
        update app;
        
       System.debug('app.TF4SF__Application_Status__c '+app.TF4SF__Application_Status__c);
        
         TF4SF__Application__c a = [SELECT Id, TF4SF__Application_Status__c FROM TF4SF__Application__c WHERE Id = :app.Id];
        
       Map<Id, TF4SF__Application__c> oldMap=new Map<Id, TF4SF__Application__c>();
        oldMap.put(app.id,a);
        
        TF4SF__Product_Codes__c pc=new TF4SF__Product_Codes__c(TF4SF__Product__c='Checking - Checking',TF4SF__Sub_Product__c='checking',Lead_Score_Completed__c='250',
       Lead_Score_Employment__c='466',Lead_Score_Identity__c='400',Lead_Score_Review__c='454',Lead_Score_Started__c='544',Name='testName');
       insert pc;
        
         Group RejectedQueue = new Group(Name='Test',TYPE = 'Queue',DeveloperName = 'Rejected_Leads');
        insert RejectedQueue;
        
        Group MortgageQueue =new Group (Name='Test',TYPE = 'Queue',DeveloperName = 'Mortgage_Leads');
        insert MortgageQueue; 
        
        Group BusinessQueue =new Group(Name='test',TYPE = 'Queue',DeveloperName = 'Business_Leads');
        insert BusinessQueue;
        
        Group AllOtherQueue =new Group(Name='test',TYPE = 'Queue',DeveloperName = 'SCCA_Leads');
        insert AllOtherQueue;
        
        List<Task> taskList=new List<Task>();
          Task tk=new Task();
          tk.Subject='An new application is assigned to you.';
          tk.WhatId=app.Id;
          tk.WhoId=u.Id;
          tk.Status='test';
          taskList.add(tk);
            
         Messaging.SingleEmailMessage et = new Messaging.SingleEmailMessage();   
            
         folder folderObj=[SELECT Id,Name FROM Folder WHERE Name = 'Email Folder' limit 1];         
         EmailTemplate ett = new EmailTemplate (developerName = 'AbandonEmail', FolderId = folderObj.Id,
         TemplateType= 'Text', Name = 'test');
   
      
        Test.startTest();
        TriggerHandler Objapp=new TriggerHandler (); 
        Objapp.sendSaveForLaterEmailForShares(applst,oldMap);
        Messaging.SingleEmailMessage e = new Messaging.SingleEmailMessage();
        e.setHtmlBody('<div id="startBody"></div>gsfdsdf<div id="endBody"></div>');
        Objapp.updateTask(e,app);
        Objapp.LeadScoreQueueAssignement(applst);
        Test.stopTest();
    }
 }
    
  @isTest static void test_method2() {

        List<TF4SF__Application__c> applst = new List<TF4SF__Application__c>();
        List<Id> UserList = new List<Id>(); 

        TF4SF__Application__c app = new TF4SF__Application__c();
        app.TF4SF__Custom_Checkbox2__c = false;
        app.TF4SF__Custom_Checkbox3__c = true;
        app.TF4SF__Custom_Checkbox4__c = true;
        app.TF4SF__Custom_Text3__c ='800';
        app.TF4SF__Months__c  ='3';
        app.TF4SF__Custom_Text4__c='800';
        app.TF4SF__Months_J__c = '3';
        app.TF4SF__Custom_Text5__c = '800';
        app.TF4SF__Months_J2__c = '3';
        app.TF4SF__Custom_Text6__c = '800';
        app.TF4SF__Months_J3__c ='3';
        app.Abandoned_Email__c= true;
        app.TF4SF__Email_Address__c ='test@test.com'; 
        app.TF4SF__Product__c='Home Equity';
        app.TF4SF__Application_Status__c='Open';
        app.TF4SF__Sub_Product__c = 'checking';
        app.TF4SF__Application_Page__c='GetStartedPage';
        app.Lead_Score__c='600';
         insert app;
        applst.add(app);
      
        
       System.debug('app.TF4SF__Application_Status__c '+app.TF4SF__Application_Status__c); 
        
        Contact con1=new Contact();
        con1.lastname='Testing';
        con1.email='test@test.com';
        insert con1;
        
        app.TF4SF__Application_Status__c='Abandoned';
        update app;
        
       System.debug('app.TF4SF__Application_Status__c '+app.TF4SF__Application_Status__c);
        
         TF4SF__Application__c a = [SELECT Id, TF4SF__Application_Status__c FROM TF4SF__Application__c WHERE Id = :app.Id];
        
       Map<Id, TF4SF__Application__c> oldMap=new Map<Id, TF4SF__Application__c>();
        oldMap.put(app.id,a);
        
        TF4SF__Product_Codes__c pc=new TF4SF__Product_Codes__c(TF4SF__Product__c='Checking - Checking',TF4SF__Sub_Product__c='checking',Lead_Score_Completed__c='500',
       Lead_Score_Employment__c='466',Lead_Score_Identity__c='521',Lead_Score_Review__c='454',Lead_Score_Started__c='900',Name='testName');
       insert pc;
      
        Group RejectedQueue = new Group(Name='Test',TYPE = 'Queue',DeveloperName = 'Rejected_Leads');
        insert RejectedQueue;
        
        Group MortgageQueue =new Group (Name='Test',TYPE = 'Queue',DeveloperName = 'Mortgage_Leads');
        insert MortgageQueue; 
        
        Group BusinessQueue =new Group(Name='test',TYPE = 'Queue',DeveloperName = 'Business_Leads');
        insert BusinessQueue;
        
        Group AllOtherQueue =new Group(Name='test',TYPE = 'Queue',DeveloperName = 'SCCA_Leads');
        insert AllOtherQueue;
        
         Test.startTest();
        TriggerHandler Objapp=new TriggerHandler (); 
        Objapp.sendSaveForLaterEmailForShares(applst,oldMap);
        Objapp.LeadScoring(applst);
        Objapp.LeadScoreQueueAssignement(applst);
        Test.stopTest();
    }   
    
 @isTest static void test_method3() {
      String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
    Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
    User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
              EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
              LocaleSidKey = 'en_US', ProfileId = p.Id,
              TimeZoneSidKey = 'America/Los_Angeles',
              UserName = uniqueUserName);
    System.runAs(u) {
            
     

        List<TF4SF__Application__c> applst = new List<TF4SF__Application__c>();
        List<Id> UserList = new List<Id>(); 

        TF4SF__Application__c app = new TF4SF__Application__c();
        app.TF4SF__Custom_Checkbox2__c = false;
        app.TF4SF__Custom_Checkbox3__c = true;
        app.TF4SF__Custom_Checkbox4__c = true;
        app.TF4SF__Custom_Text3__c ='800';
        app.TF4SF__Months__c  ='3';
        app.TF4SF__Custom_Text4__c='800';
        app.TF4SF__Months_J__c = '3';
        app.TF4SF__Custom_Text5__c = '800';
        app.TF4SF__Months_J2__c = '3';
        app.TF4SF__Custom_Text6__c = '800';
        app.TF4SF__Months_J3__c ='3';
        app.Abandoned_Email__c= true;
        app.TF4SF__Email_Address__c ='test@test.com'; 
        app.TF4SF__Product__c='Business Checking';
        app.TF4SF__Application_Status__c='Open';
        app.TF4SF__Sub_Product__c = 'checking';
        app.TF4SF__Application_Page__c='EmploymentPage';
        app.Lead_Score__c='600';
        app.OwnerId = u.id;
         insert app;
        applst.add(app);
      
        
       System.debug('app.TF4SF__Application_Status__c '+app.TF4SF__Application_Status__c); 
        
        Contact con1=new Contact();
        con1.lastname='Testing';
        con1.email='test@test.com';
        insert con1;
        
        app.TF4SF__Application_Status__c='Abandoned';
        update app;
        
       System.debug('app.TF4SF__Application_Status__c '+app.TF4SF__Application_Status__c);
        
         TF4SF__Application__c a = [SELECT Id, TF4SF__Application_Status__c FROM TF4SF__Application__c WHERE Id = :app.Id];
        
       Map<Id, TF4SF__Application__c> oldMap=new Map<Id, TF4SF__Application__c>();
        oldMap.put(app.id,a);
        
        TF4SF__Product_Codes__c pc=new TF4SF__Product_Codes__c(TF4SF__Product__c='Checking - Checking',TF4SF__Sub_Product__c='checking',Lead_Score_Completed__c='500',
       Lead_Score_Employment__c='600',Lead_Score_Identity__c='521',Lead_Score_Review__c='454',Lead_Score_Started__c='600',Name='testName');
       insert pc;
     
     Group RejectedQueue = new Group(Name='Test',TYPE = 'Queue',DeveloperName = 'Rejected_Leads');
        insert RejectedQueue;
        
        Group MortgageQueue =new Group (Name='Test',TYPE = 'Queue',DeveloperName = 'Mortgage_Leads');
        insert MortgageQueue; 
        
        Group BusinessQueue =new Group(Name='test',TYPE = 'Queue',DeveloperName = 'Business_Leads');
        insert BusinessQueue;

     Group AllOtherQueue =new Group(Name='test',TYPE = 'Queue',DeveloperName = 'SCCA_Leads');
        insert AllOtherQueue;
        
        Test.startTest();
        TriggerHandler Objapp=new TriggerHandler (); 
        Objapp.sendSaveForLaterEmailForShares(applst,oldMap);
        Objapp.LeadScoring(applst);
        Objapp.LeadScoreQueueAssignement(applst);
        Objapp.LeadScoreUserAssignement(applst);
        Test.stopTest();
    }    
 }
  @isTest static void test_method4() {

        List<TF4SF__Application__c> applst = new List<TF4SF__Application__c>();

        TF4SF__Application__c app = new TF4SF__Application__c();
        app.TF4SF__Custom_Checkbox2__c = false;
        app.TF4SF__Custom_Checkbox3__c = true;
        app.TF4SF__Custom_Checkbox4__c = true;
        app.TF4SF__Custom_Text3__c ='800';
        app.TF4SF__Months__c  ='3';
        app.TF4SF__Custom_Text4__c='800';
        app.TF4SF__Months_J__c = '3';
        app.TF4SF__Custom_Text5__c = '800';
        app.TF4SF__Months_J2__c = '3';
        app.TF4SF__Custom_Text6__c = '800';
        app.TF4SF__Months_J3__c ='3';
        app.Abandoned_Email__c= true;
        app.TF4SF__Email_Address__c ='test@test.com'; 
        app.TF4SF__Product__c='Vehicle Loans';
        app.TF4SF__Application_Status__c='Open';
        app.TF4SF__Sub_Product__c = 'checking';
        app.TF4SF__Application_Page__c='AccountDetailsPage';
        app.Lead_Score__c='600';
         insert app;
        applst.add(app);
      
        
       System.debug('app.TF4SF__Application_Status__c '+app.TF4SF__Application_Status__c); 
        
        Contact con1=new Contact();
        con1.lastname='Testing';
        con1.email='test@test.com';
        insert con1;
        
        app.TF4SF__Application_Status__c='Abandoned';
        update app;
        
       System.debug('app.TF4SF__Application_Status__c '+app.TF4SF__Application_Status__c);
        
         TF4SF__Application__c a = [SELECT Id, TF4SF__Application_Status__c FROM TF4SF__Application__c WHERE Id = :app.Id];
        
       Map<Id, TF4SF__Application__c> oldMap=new Map<Id, TF4SF__Application__c>();
        oldMap.put(app.id,a);
        
        TF4SF__Product_Codes__c pc=new TF4SF__Product_Codes__c(TF4SF__Product__c='Checking - Checking',TF4SF__Sub_Product__c='checking',Lead_Score_Completed__c='500',
       Lead_Score_Employment__c='466',Lead_Score_Identity__c='600',Lead_Score_Review__c='454',Lead_Score_Started__c='600',Name='testName');
       insert pc;
      
      Group RejectedQueue = new Group(Name='Test',TYPE = 'Queue',DeveloperName = 'Rejected_Leads');
        insert RejectedQueue;
        
        Group MortgageQueue =new Group (Name='Test',TYPE = 'Queue',DeveloperName = 'Mortgage_Leads');
        insert MortgageQueue; 
        
        Group BusinessQueue =new Group(Name='test',TYPE = 'Queue',DeveloperName = 'Business_Leads');
        insert BusinessQueue;
        
        Group AllOtherQueue =new Group(Name='test',TYPE = 'Queue',DeveloperName = 'SCCA_Leads');
        insert AllOtherQueue;
       
        List<Id> UserList = new List<Id>();
        GroupMember gem=new GroupMember();
         gem.UserOrGroupId=AllOtherQueue.id;
         UserList.add(gem.UserOrGroupId);
        
        Test.startTest();
        TriggerHandler Objapp=new TriggerHandler (); 
        Objapp.sendSaveForLaterEmailForShares(applst,oldMap);
        Objapp.LeadScoring(applst);
        Objapp.LeadScoreQueueAssignement(applst);

        Test.stopTest();
    }     
    @isTest static void test_method5() {

        List<TF4SF__Application__c> applst = new List<TF4SF__Application__c>();
        List<Id> UserList = new List<Id>(); 

        TF4SF__Application__c app = new TF4SF__Application__c();
        app.TF4SF__Custom_Checkbox2__c = false;
        app.TF4SF__Custom_Checkbox3__c = true;
        app.TF4SF__Custom_Checkbox4__c = true;
        app.TF4SF__Custom_Text3__c ='800';
        app.TF4SF__Months__c  ='3';
        app.TF4SF__Custom_Text4__c='800';
        app.TF4SF__Months_J__c = '3';
        app.TF4SF__Custom_Text5__c = '800';
        app.TF4SF__Months_J2__c = '3';
        app.TF4SF__Custom_Text6__c = '800';
        app.TF4SF__Months_J3__c ='3';
        app.Abandoned_Email__c= true;
        app.TF4SF__Email_Address__c ='test@test.com'; 
        app.TF4SF__Product__c='Personal Loans';
        app.TF4SF__Application_Status__c='Open';
        app.TF4SF__Sub_Product__c = 'checking';
        app.TF4SF__Application_Page__c='ReviewSubmitPage';
        app.Lead_Score__c='600';
         insert app;
        applst.add(app);
      
        
       System.debug('app.TF4SF__Application_Status__c '+app.TF4SF__Application_Status__c); 
        
        Contact con1=new Contact();
        con1.lastname='Testing';
        con1.email='test@test.com';
        insert con1;
        
        app.TF4SF__Application_Status__c='Abandoned';
        update app;
        
       System.debug('app.TF4SF__Application_Status__c '+app.TF4SF__Application_Status__c);
        
         TF4SF__Application__c a = [SELECT Id, TF4SF__Application_Status__c FROM TF4SF__Application__c WHERE Id = :app.Id];
        
       Map<Id, TF4SF__Application__c> oldMap=new Map<Id, TF4SF__Application__c>();
        oldMap.put(app.id,a);
        
        TF4SF__Product_Codes__c pc=new TF4SF__Product_Codes__c(TF4SF__Product__c='Checking - Checking',TF4SF__Sub_Product__c='checking',Lead_Score_Completed__c='500',
       Lead_Score_Employment__c='466',Lead_Score_Identity__c='521',Lead_Score_Review__c='600',Lead_Score_Started__c='600',Name='testName');
       insert pc;
        
        Group RejectedQueue = new Group(Name='Test',TYPE = 'Queue',DeveloperName = 'Rejected_Leads');
        insert RejectedQueue;
        
        Group MortgageQueue =new Group (Name='Test',TYPE = 'Queue',DeveloperName = 'Mortgage_Leads');
        insert MortgageQueue; 
        
        Group BusinessQueue =new Group(Name='test',TYPE = 'Queue',DeveloperName = 'Business_Leads');
        insert BusinessQueue;
        
        Group AllOtherQueue =new Group(Name='test',TYPE = 'Queue',DeveloperName = 'SCCA_Leads');
        insert AllOtherQueue;
        
        Test.startTest();
        TriggerHandler Objapp=new TriggerHandler (); 
        Objapp.sendSaveForLaterEmailForShares(applst,oldMap);
        Objapp.LeadScoring(applst);
        Objapp.LeadScoreQueueAssignement(applst);

        Test.stopTest();
    }      
      @isTest static void test_method6() {

        List<TF4SF__Application__c> applst = new List<TF4SF__Application__c>();
        List<Id> UserList = new List<Id>(); 

        TF4SF__Application__c app = new TF4SF__Application__c();
        app.TF4SF__Custom_Checkbox2__c = false;
        app.TF4SF__Custom_Checkbox3__c = true;
        app.TF4SF__Custom_Checkbox4__c = true;
        app.TF4SF__Custom_Text3__c ='800';
        app.TF4SF__Months__c  ='3';
        app.TF4SF__Custom_Text4__c='800';
        app.TF4SF__Months_J__c = '3';
        app.TF4SF__Custom_Text5__c = '800';
        app.TF4SF__Months_J2__c = '3';
        app.TF4SF__Custom_Text6__c = '800';
        app.TF4SF__Months_J3__c ='3';
        app.Abandoned_Email__c= true;
        app.TF4SF__Email_Address__c ='test@test.com'; 
        app.TF4SF__Product__c='Credit Cards';
        app.TF4SF__Application_Status__c='Open';
        app.TF4SF__Sub_Product__c = 'checking';
        app.TF4SF__Application_Page__c='ConfirmationPage';
        app.Lead_Score__c='600';
         insert app;
        applst.add(app);
      
       System.debug('app.TF4SF__Application_Status__c '+app.TF4SF__Application_Status__c); 
        
        Contact con1=new Contact();
        con1.lastname='Testing';
        con1.email='test@test.com';
        insert con1;
        
        app.TF4SF__Application_Status__c='Abandoned';
        update app;
        
       System.debug('app.TF4SF__Application_Status__c '+app.TF4SF__Application_Status__c);
        
         TF4SF__Application__c a = [SELECT Id, TF4SF__Application_Status__c FROM TF4SF__Application__c WHERE Id = :app.Id];
        
       Map<Id, TF4SF__Application__c> oldMap=new Map<Id, TF4SF__Application__c>();
        oldMap.put(app.id,a);
        
        TF4SF__Product_Codes__c pc=new TF4SF__Product_Codes__c(TF4SF__Product__c='Checking - Checking',TF4SF__Sub_Product__c='checking',Lead_Score_Completed__c='600',
       Lead_Score_Employment__c='466',Lead_Score_Identity__c='521',Lead_Score_Review__c='526',Lead_Score_Started__c='600',Name='testName');
       insert pc;
          Group RejectedQueue = new Group(Name='Test',TYPE = 'Queue',DeveloperName = 'Rejected_Leads');
        insert RejectedQueue;
        
        Group MortgageQueue =new Group (Name='Test',TYPE = 'Queue',DeveloperName = 'Mortgage_Leads');
        insert MortgageQueue; 
        
        Group BusinessQueue =new Group(Name='test',TYPE = 'Queue',DeveloperName = 'Business_Leads');
        insert BusinessQueue;
        
        Group AllOtherQueue =new Group(Name='test',TYPE = 'Queue',DeveloperName = 'SCCA_Leads');
        insert AllOtherQueue;
        
        Test.startTest();
        TriggerHandler Objapp=new TriggerHandler (); 
        Objapp.sendSaveForLaterEmailForShares(applst,oldMap);
        Objapp.LeadScoring(applst);
        Objapp.LeadScoreQueueAssignement(applst);
        Test.stopTest();
    }   
    
     @isTest static void test_method7() {

        List<TF4SF__Application__c> applst = new List<TF4SF__Application__c>();
        List<Id> UserList = new List<Id>(); 

        TF4SF__Application__c app = new TF4SF__Application__c();
        app.TF4SF__Custom_Checkbox2__c = false;
        app.TF4SF__Custom_Checkbox3__c = true;
        app.TF4SF__Custom_Checkbox4__c = true;
        app.TF4SF__Custom_Text3__c ='800';
        app.TF4SF__Months__c  ='3';
        app.TF4SF__Custom_Text4__c='800';
        app.TF4SF__Months_J__c = '3';
        app.TF4SF__Custom_Text5__c = '800';
        app.TF4SF__Months_J2__c = '3';
        app.TF4SF__Custom_Text6__c = '800';
        app.TF4SF__Months_J3__c ='3';
        app.Abandoned_Email__c= true;
        app.TF4SF__Email_Address__c ='test@test.com'; 
        app.TF4SF__Product__c='Checking';
        app.TF4SF__Application_Status__c='Open';
        app.TF4SF__Sub_Product__c = 'checking';
        app.TF4SF__Application_Page__c='ConfirmationPage';
        app.Lead_Score__c='600';
         insert app;
        applst.add(app);
      
        
       System.debug('app.TF4SF__Application_Status__c '+app.TF4SF__Application_Status__c); 
        
        Contact con1=new Contact();
        con1.lastname='Testing';
        con1.email='test@test.com';
        insert con1;
        
        app.TF4SF__Application_Status__c='Abandoned';
        update app;
        
       System.debug('app.TF4SF__Application_Status__c '+app.TF4SF__Application_Status__c);
        
         TF4SF__Application__c a = [SELECT Id, TF4SF__Application_Status__c FROM TF4SF__Application__c WHERE Id = :app.Id];
        
       Map<Id, TF4SF__Application__c> oldMap=new Map<Id, TF4SF__Application__c>();
        oldMap.put(app.id,a);
        
        TF4SF__Product_Codes__c pc=new TF4SF__Product_Codes__c(TF4SF__Product__c='Checking - Checking',TF4SF__Sub_Product__c='checking',Lead_Score_Completed__c='600',
       Lead_Score_Employment__c='466',Lead_Score_Identity__c='521',Lead_Score_Review__c='526',Lead_Score_Started__c='600',Name='testName');
       insert pc;
          Group RejectedQueue = new Group(Name='Test',TYPE = 'Queue',DeveloperName = 'Rejected_Leads');
        insert RejectedQueue;
        
        Group MortgageQueue =new Group (Name='Test',TYPE = 'Queue',DeveloperName = 'Mortgage_Leads');
        insert MortgageQueue; 
        
        Group BusinessQueue =new Group(Name='test',TYPE = 'Queue',DeveloperName = 'Business_Leads');
        insert BusinessQueue;
        
        Group AllOtherQueue =new Group(Name='test',TYPE = 'Queue',DeveloperName = 'SCCA_Leads');
        insert AllOtherQueue;
        
        Test.startTest();
        TriggerHandler Objapp=new TriggerHandler (); 
        Objapp.sendSaveForLaterEmailForShares(applst,oldMap);
        Objapp.LeadScoring(applst);
        Objapp.LeadScoreQueueAssignement(applst);
        Test.stopTest();
    }      

	
	 @isTest static void test_method8() {

        List<TF4SF__Application__c> applst = new List<TF4SF__Application__c>();
        List<Id> UserList = new List<Id>(); 

        TF4SF__Application__c app = new TF4SF__Application__c();
        app.TF4SF__Custom_Checkbox2__c = false;
        app.TF4SF__Custom_Checkbox3__c = true;
        app.TF4SF__Custom_Checkbox4__c = true;
        app.TF4SF__Custom_Text3__c ='800';
        app.TF4SF__Months__c  ='3';
        app.TF4SF__Custom_Text4__c='800';
        app.TF4SF__Months_J__c = '3';
        app.TF4SF__Custom_Text5__c = '800';
        app.TF4SF__Months_J2__c = '3';
        app.TF4SF__Custom_Text6__c = '800';
        app.TF4SF__Months_J3__c ='3';
        app.Abandoned_Email__c= true;
        app.TF4SF__Email_Address__c ='test@test.com'; 
        app.TF4SF__Product__c='Savings';
        app.TF4SF__Application_Status__c='Open';
        app.TF4SF__Sub_Product__c = 'checking';
        app.TF4SF__Application_Page__c='ConfirmationPage';
        app.Lead_Score__c='600';
         insert app;
        applst.add(app);
      
        
       System.debug('app.TF4SF__Application_Status__c '+app.TF4SF__Application_Status__c); 
        
        Contact con1=new Contact();
        con1.lastname='Testing';
        con1.email='test@test.com';
        insert con1;
        
        app.TF4SF__Application_Status__c='Abandoned';
        update app;
        
       System.debug('app.TF4SF__Application_Status__c '+app.TF4SF__Application_Status__c);
        
         TF4SF__Application__c a = [SELECT Id, TF4SF__Application_Status__c FROM TF4SF__Application__c WHERE Id = :app.Id];
        
       Map<Id, TF4SF__Application__c> oldMap=new Map<Id, TF4SF__Application__c>();
        oldMap.put(app.id,a);
        
        TF4SF__Product_Codes__c pc=new TF4SF__Product_Codes__c(TF4SF__Product__c='Checking - Checking',TF4SF__Sub_Product__c='checking',Lead_Score_Completed__c='600',
       Lead_Score_Employment__c='466',Lead_Score_Identity__c='521',Lead_Score_Review__c='526',Lead_Score_Started__c='600',Name='testName');
       insert pc;
          Group RejectedQueue = new Group(Name='Test',TYPE = 'Queue',DeveloperName = 'Rejected_Leads');
        insert RejectedQueue;
        
        Group MortgageQueue =new Group (Name='Test',TYPE = 'Queue',DeveloperName = 'Mortgage_Leads');
        insert MortgageQueue; 
        
        Group BusinessQueue =new Group(Name='test',TYPE = 'Queue',DeveloperName = 'Business_Leads');
        insert BusinessQueue;
        
        Group AllOtherQueue =new Group(Name='test',TYPE = 'Queue',DeveloperName = 'SCCA_Leads');
        insert AllOtherQueue;
        
        Test.startTest();
        TriggerHandler Objapp=new TriggerHandler (); 
        Objapp.sendSaveForLaterEmailForShares(applst,oldMap);
        Objapp.LeadScoring(applst);
        Objapp.LeadScoreQueueAssignement(applst);
        Test.stopTest();
    }       
	
	 @isTest static void test_method9() {

        List<TF4SF__Application__c> applst = new List<TF4SF__Application__c>();
        List<Id> UserList = new List<Id>(); 

        TF4SF__Application__c app = new TF4SF__Application__c();
        app.TF4SF__Custom_Checkbox2__c = false;
        app.TF4SF__Custom_Checkbox3__c = true;
        app.TF4SF__Custom_Checkbox4__c = true;
        app.TF4SF__Custom_Text3__c ='800';
        app.TF4SF__Months__c  ='3';
        app.TF4SF__Custom_Text4__c='800';
        app.TF4SF__Months_J__c = '3';
        app.TF4SF__Custom_Text5__c = '800';
        app.TF4SF__Months_J2__c = '3';
        app.TF4SF__Custom_Text6__c = '800';
        app.TF4SF__Months_J3__c ='3';
        app.Abandoned_Email__c= true;
        app.TF4SF__Email_Address__c ='test@test.com'; 
        app.TF4SF__Product__c='Certificates';
        app.TF4SF__Application_Status__c='Open';
        app.TF4SF__Sub_Product__c = 'checking';
        app.TF4SF__Application_Page__c='ConfirmationPage';
        app.Lead_Score__c='600';
         insert app;
        applst.add(app);
      
       System.debug('app.TF4SF__Application_Status__c '+app.TF4SF__Application_Status__c); 
        
        Contact con1=new Contact();
        con1.lastname='Testing';
        con1.email='test@test.com';
        insert con1;
        
        app.TF4SF__Application_Status__c='Abandoned';
        update app;
        
       System.debug('app.TF4SF__Application_Status__c '+app.TF4SF__Application_Status__c);
        
         TF4SF__Application__c a = [SELECT Id, TF4SF__Application_Status__c FROM TF4SF__Application__c WHERE Id = :app.Id];
        
       Map<Id, TF4SF__Application__c> oldMap=new Map<Id, TF4SF__Application__c>();
        oldMap.put(app.id,a);
        
        TF4SF__Product_Codes__c pc=new TF4SF__Product_Codes__c(TF4SF__Product__c='Checking - Checking',TF4SF__Sub_Product__c='checking',Lead_Score_Completed__c='600',
       Lead_Score_Employment__c='466',Lead_Score_Identity__c='521',Lead_Score_Review__c='526',Lead_Score_Started__c='600',Name='testName');
       insert pc;
          Group RejectedQueue = new Group(Name='Test',TYPE = 'Queue',DeveloperName = 'Rejected_Leads');
        insert RejectedQueue;
        
        Group MortgageQueue =new Group (Name='Test',TYPE = 'Queue',DeveloperName = 'Mortgage_Leads');
        insert MortgageQueue; 
        
        Group BusinessQueue =new Group(Name='test',TYPE = 'Queue',DeveloperName = 'Business_Leads');
        insert BusinessQueue;
        
        Group AllOtherQueue =new Group(Name='test',TYPE = 'Queue',DeveloperName = 'SCCA_Leads');
        insert AllOtherQueue;
        
        Test.startTest();
        TriggerHandler Objapp=new TriggerHandler (); 
        Objapp.sendSaveForLaterEmailForShares(applst,oldMap);
        Objapp.LeadScoring(applst);
        Objapp.LeadScoreQueueAssignement(applst);
        Test.stopTest();
    }   
	
	@isTest static void test_method10() {

        List<TF4SF__Application__c> applst = new List<TF4SF__Application__c>();
        List<Id> UserList = new List<Id>(); 

        TF4SF__Application__c app = new TF4SF__Application__c();
        app.TF4SF__Custom_Checkbox2__c = false;
        app.TF4SF__Custom_Checkbox3__c = true;
        app.TF4SF__Custom_Checkbox4__c = true;
        app.TF4SF__Custom_Text3__c ='800';
        app.TF4SF__Months__c  ='3';
        app.TF4SF__Custom_Text4__c='800';
        app.TF4SF__Months_J__c = '3';
        app.TF4SF__Custom_Text5__c = '800';
        app.TF4SF__Months_J2__c = '3';
        app.TF4SF__Custom_Text6__c = '800';
        app.TF4SF__Months_J3__c ='3';
        app.Abandoned_Email__c= true;
        app.TF4SF__Email_Address__c ='test@test.com'; 
        app.TF4SF__Product__c='Business Credit Cards';
        app.TF4SF__Application_Status__c='Open';
        app.TF4SF__Sub_Product__c = 'checking';
        app.TF4SF__Application_Page__c='ConfirmationPage';
        app.Lead_Score__c='600';
         insert app;
        applst.add(app);
      
        
       System.debug('app.TF4SF__Application_Status__c '+app.TF4SF__Application_Status__c); 
        
        Contact con1=new Contact();
        con1.lastname='Testing';
        con1.email='test@test.com';
        insert con1;
        
        app.TF4SF__Application_Status__c='Abandoned';
        update app;
        
       System.debug('app.TF4SF__Application_Status__c '+app.TF4SF__Application_Status__c);
        
         TF4SF__Application__c a = [SELECT Id, TF4SF__Application_Status__c FROM TF4SF__Application__c WHERE Id = :app.Id];
        
       Map<Id, TF4SF__Application__c> oldMap=new Map<Id, TF4SF__Application__c>();
        oldMap.put(app.id,a);
        
        TF4SF__Product_Codes__c pc=new TF4SF__Product_Codes__c(TF4SF__Product__c='Checking - Checking',TF4SF__Sub_Product__c='checking',Lead_Score_Completed__c='600',
       Lead_Score_Employment__c='466',Lead_Score_Identity__c='521',Lead_Score_Review__c='526',Lead_Score_Started__c='600',Name='testName');
       insert pc;
          Group RejectedQueue = new Group(Name='Test',TYPE = 'Queue',DeveloperName = 'Rejected_Leads');
        insert RejectedQueue;
        
        Group MortgageQueue =new Group (Name='Test',TYPE = 'Queue',DeveloperName = 'Mortgage_Leads');
        insert MortgageQueue; 
        
        Group BusinessQueue =new Group(Name='test',TYPE = 'Queue',DeveloperName = 'Business_Leads');
        insert BusinessQueue;
        
        Group AllOtherQueue =new Group(Name='test',TYPE = 'Queue',DeveloperName = 'SCCA_Leads');
        insert AllOtherQueue;
        
        Test.startTest();
        TriggerHandler Objapp=new TriggerHandler (); 
        Objapp.sendSaveForLaterEmailForShares(applst,oldMap);
        Objapp.LeadScoring(applst);
        Objapp.LeadScoreQueueAssignement(applst);
        Test.stopTest();
    }      
}