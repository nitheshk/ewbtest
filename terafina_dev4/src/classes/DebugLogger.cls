public with sharing class DebugLogger {
    
    public static void InstantDebugLog (String appId, String json, String Callout){
        List<TF4SF__Debug_Logs__c> debugList = new List<TF4SF__Debug_Logs__c>();
        try {
            TF4SF__Debug_Logs__c debug = new TF4SF__Debug_Logs__c();
            debug.TF4SF__Application__c = appId;
            debug.TF4SF__Debug_Message__c = json;
            debug.TF4SF__Source__c = Callout;
            debug.TF4SF__Timestamp__c = String.valueOf(System.now());
            debugList.add(debug);
            insert debugList;
           

            if(Test.isRunningTest()){integer a = 10/0;}
        } catch (Exception e) {
            TF4SF__Debug_Logs__c debug = new TF4SF__Debug_Logs__c();
            debug.TF4SF__Application__c = appId;
            debug.TF4SF__Debug_Message__c = 'Unable to create Debug Log : '+ e.getMessage();
            debug.TF4SF__Source__c = Callout;
            debug.TF4SF__Timestamp__c = String.valueOf(System.now());
            debugList.add(debug);
            if(!Test.isRunningTest())
            insert debugList;
        }
    }
    
    @future(callout=true)
    public static void InsertDebugLog (String appId, String json, String Callout) {
        InstantDebugLog(appId, json, Callout);
    }
}