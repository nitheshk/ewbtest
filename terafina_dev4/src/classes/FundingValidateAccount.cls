global with sharing class FundingValidateAccount implements TF4SF.DSP_Interface{

    global static Map<String, String> main (Map<String, String> tdata) {
        Map<String, String> data = new Map<String, String>();
        String appId = tdata.get('id');
        String fundingotp = tdata.get('otp');
        String Token = '',StatusCode='0';

       try {

        Token = AuthToken.Auth();                   
        String response1 = VerifyAccount(appId, Token, fundingotp);
        data.put('response1',response1 );
        String CFIID = '';
        String VerificationStatus = '';
        String ValidateVerificationStatus = '';
        Map<String, Object> verifyMap = (Map<String, Object>)JSON.deserializeUntyped(response1);        
        Map<String, Object> verifyDataMap = (Map<String, Object>)verifyMap.get('data');
        Map<String, Object> verifyAccRealTimeMap = (Map<String, Object>)verifyDataMap.get('VerifyAccRealTime');
        System.debug('verifyAccRealTimeMap ==>' +  verifyAccRealTimeMap);
        Map<String, Object> verifybodyMap = (Map<String, Object>)verifyAccRealTimeMap.get('body');
        Map<String, Object> verifyAccountRealTimeResponseMap = (Map<String, Object>)verifybodyMap.get('VerifyAccountRealTimeResponse');
        Map<String, Object> verifyreturnMap = (Map<String, Object>)verifyAccountRealTimeResponseMap.get('return');
        Map<String, Object> RealTimeAccountVerificationInfoStatusListMap = (Map<String, Object>)verifyreturnMap.get('RealTimeAccountVerificationInfoStatusList');
        List<Object> RealTimeAccountVerificationInfoStatusList = (List<Object>)RealTimeAccountVerificationInfoStatusListMap.get('RealTimeAccountVerificationInfoStatus');
        for (Object RealTimeAccountVerificationInfoStatus : RealTimeAccountVerificationInfoStatusList) {
            Map<String, Object> RealTimeAccountVerificationInfoStatusMap = (Map<String, Object>)RealTimeAccountVerificationInfoStatus;
            CFIID = String.valueOf(RealTimeAccountVerificationInfoStatusMap.get('CFIID'));
            VerificationStatus = String.valueOf(RealTimeAccountVerificationInfoStatusMap.get('VerificationStatus'));
            break;
        }
        if (VerificationStatus == 'Approved') {
            String response2 = ValidateAccount(appId, Token, CFIID); 
            Map<String, Object> validateMap = (Map<String, Object>)JSON.deserializeUntyped(response2);
            Map<String, Object> validateyDataMap = (Map<String, Object>)validateMap.get('data');
            Map<String, Object> validateAddHostMap = (Map<String, Object>)validateyDataMap.get('AddHost');
            Map<String, Object> validatebodyMap = (Map<String, Object>)validateAddHostMap.get('body');
            Map<String, Object> ValidateAddHostAccountsResponseMap = (Map<String, Object>)validatebodyMap.get('AddHostAccountsResponse');
            Map<String, Object> validatereturnMap = (Map<String, Object>)ValidateAddHostAccountsResponseMap.get('return');
            Map<String, Object> HostAccountVerificationInfoStatusListMap = (Map<String, Object>)validatereturnMap.get('HostAccountVerificationInfoStatusList');
            List<Object> HostAccountVerificationInfoStatusList = (List<Object>)HostAccountVerificationInfoStatusListMap.get('HostAccountVerificationInfoStatus');
            for (Object HostAccountVerificationInfoStatus : HostAccountVerificationInfoStatusList) {
                Map<String, Object> HostAccountVerificationInfoStatusMap = (Map<String, Object>)HostAccountVerificationInfoStatus;
                CFIID = String.valueOf(HostAccountVerificationInfoStatusMap.get('CFIID'));
                ValidateVerificationStatus = String.valueOf(HostAccountVerificationInfoStatusMap.get('VerificationStatus'));
                break;
            }
            if (ValidateVerificationStatus == 'Approved') {
                data.put('VerificationStatus', ValidateVerificationStatus);
                StatusCode='200';
            }
        }

        } catch (Exception e) {
            data.put('server-errors', 'Error encountered in FundingValidateAccount class: ' + e.getMessage() + '; line: ' + e.getLineNumber() + '; type: ' + e.getTypeName() + '; stack trace: ' + e.getStackTraceString());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error encountered in FundingValidateAccount class: ' + e.getMessage() + '; line: ' + e.getLineNumber() + '; type: ' + e.getTypeName() + '; stack trace: ' + e.getStackTraceString()));
            System.debug('exception' + e + ' ' + e.getLineNumber());
        }
        data.put('StatusCode',StatusCode);
        return data;
    } 

    //Third Method to call//
    public static String VerifyAccount(String appId, String Token, String fundingotp) {
        String result = '';
        VELOSETTINGS__C velo = VELOSETTINGS__C.getOrgDefaults();
        String endpoint = velo.FundingEndpoint__c+'api/process/verifyAccount';
        String applicationId = '\'' + appId + '\'';
        //System.debug('applicationId//productnumber:'+applicationId+'--'+productnumber);
        //select all fields in Application
        queryHelper.ObjectInfo appInfo = queryHelper.selectStar('TF4SF__Application__c');
        String queryString =  appInfo.query;
        queryString += 'WHERE ID = ' + applicationId ;
        TF4SF__Application__c app = Database.query(queryString);

        //this.Application = app;
        queryHelper.ObjectInfo app2Info = queryHelper.selectStar('TF4SF__Application2__c');
        String app2String =  app2Info.query;
        app2String += 'WHERE TF4SF__Application__c = ' + applicationId ;
        TF4SF__Application2__c app2 = Database.query(app2String);

        queryHelper.ObjectInfo empInfo = queryHelper.selectStar('TF4SF__Employment_Information__c');
        String employmentQuery =  empInfo.query;
        employmentQuery += 'WHERE TF4SF__Application__c = ' + applicationId;
        TF4SF__Employment_Information__c employment = Database.query(employmentQuery);
        //System.debug('employment:' + employment);

        queryHelper.ObjectInfo identityInfo = queryHelper.selectStar('TF4SF__Identity_Information__c');
        String identityQuery = identityInfo.query;
        identityQuery += 'WHERE TF4SF__Application__c = ' + applicationId;
        TF4SF__Identity_Information__c identity = Database.query(identityQuery);
        System.debug('identity:' + identity);

        queryHelper.ObjectInfo aboutAccountInfo = queryHelper.selectStar('TF4SF__About_Account__c');
        String aboutAccountQuery = aboutAccountInfo.query;
        aboutAccountQuery += 'WHERE TF4SF__Application__c = ' + applicationId;
        //System.debug('abtaccountquery:'+aboutAccountQuery);
        TF4SF__About_Account__c about = Database.query(aboutAccountQuery);
        //System.debug('about account:' + about); 

        DepositSubmit.Application application = new DepositSubmit.Application(app, app2, employment, identity, about);

        result += '{';
            if (application.app.TF4SF__Product__c == 'Checking') {
                result += '"ABA": "'+application.about.TF4SF__Routing_Number_CHK__c+'",';
                result += '"AccountNumber": "'+application.about.TF4SF__CHK_Account_Number__c+'",';
                result += '"AccountTypeCode": "'+application.about.TF4SF__Account_type_FI_CHK__c+'",';
            } else {
                result += '"ABA": "'+application.about.TF4SF__Routing_Number_SAV__c+'",';
                result += '"AccountNumber": "'+application.about.TF4SF__SAV_Account_Number__c+'",';
                result += '"AccountTypeCode": "'+application.about.TF4SF__Account_Type_FI_Sav__c+'",';
            }
            result += '"AccountTypeGroup": "Cash",';
            result += '"CFIID": "'+application.about.CFIID__c+'",';
            result += '"Email": "'+application.app.TF4SF__Email_Address__c+'",';
            result += '"FIID": "'+application.about.FIID__c+'",';
            result += '"OnlineParamList": [';
                result += '{';
                    result += '"CryptVal": "'+application.app.TF4SF__Login__c+'",';
                    result += '"ParamId": "'+application.about.userparam__c+'"';
                result += '},';
                result += '{';
                    result += '"CryptVal": "'+application.app.TF4SF__Password__c+'",';
                    result += '"ParamId": "'+application.about.passparam__c+'"';
                result += '},';
                result += '{';
                    result += '"ParamId": "'+application.about.Funding_MFA__c+'",';
                    result += '"CryptVal": "'+fundingotp+'"';
                result += '}';
            result += '],';
            result += '"RqUID": "VerifyAccountRealTimREQUEST",';
            result += '"RunID": "'+application.about.RUNId__c+'"';
        result += '}';

        return execute(appId, result, Token, Endpoint);

    }

    // forth method to call //
    public static String ValidateAccount(String appId, String Token, String CFIID) {
        String result = '';
        VELOSETTINGS__C velo = VELOSETTINGS__C.getOrgDefaults();
        String endpoint = velo.FundingEndpoint__c+'api/process/validateAccount';
        String applicationId = '\'' + appId + '\'';
        //System.debug('applicationId//productnumber:'+applicationId+'--'+productnumber);
        //select all fields in Application
        queryHelper.ObjectInfo appInfo = queryHelper.selectStar('TF4SF__Application__c');
        String queryString =  appInfo.query;
        queryString += 'WHERE ID = ' + applicationId ;
        TF4SF__Application__c app = Database.query(queryString);

        //this.Application = app;
        queryHelper.ObjectInfo app2Info = queryHelper.selectStar('TF4SF__Application2__c');
        String app2String =  app2Info.query;
        app2String += 'WHERE TF4SF__Application__c = ' + applicationId ;
        TF4SF__Application2__c app2 = Database.query(app2String);

        queryHelper.ObjectInfo empInfo = queryHelper.selectStar('TF4SF__Employment_Information__c');
        String employmentQuery =  empInfo.query;
        employmentQuery += 'WHERE TF4SF__Application__c = ' + applicationId;
        TF4SF__Employment_Information__c employment = Database.query(employmentQuery);
        //System.debug('employment:' + employment);

        queryHelper.ObjectInfo identityInfo = queryHelper.selectStar('TF4SF__Identity_Information__c');
        String identityQuery = identityInfo.query;
        identityQuery += 'WHERE TF4SF__Application__c = ' + applicationId;
        TF4SF__Identity_Information__c identity = Database.query(identityQuery);
        System.debug('identity:' + identity);

        queryHelper.ObjectInfo aboutAccountInfo = queryHelper.selectStar('TF4SF__About_Account__c');
        String aboutAccountQuery = aboutAccountInfo.query;
        aboutAccountQuery += 'WHERE TF4SF__Application__c = ' + applicationId;
        //System.debug('abtaccountquery:'+aboutAccountQuery);
        TF4SF__About_Account__c about = Database.query(aboutAccountQuery);
        //System.debug('about account:' + about); 

        DepositSubmit.Application application = new DepositSubmit.Application(app, app2, employment, identity, about);

        TF4SF__Product_Codes__c pc = [SELECT Id, TF4SF__Sub_Product__c, TF4SF__Product__c, TF4SF__Sub_Product_Code__c FROM TF4SF__Product_Codes__c WHERE TF4SF__Sub_Product__c =: application.app.TF4SF__Sub_Product__c];

        result += '{';
            result += '"RqUID": "'+ValidatePromoCodeExtension.getRandomString(25)+'",';
            result += '"AddHostAccounts": {';
                result += '"FIID": "'+velo.Funding_FIID__c+'",';
                result += '"AccountNumber": "'+application.app.TF4SF__External_App_ID__c+'",';
                result += '"ABA": "'+velo.Funding_ABA__c+'",';
                result += '"AccountTypeCode": "'+velo.Funding_Account_Type__c+'",';
                result += '"AccountTypeGroup": "'+velo.Funding_Account_Group__c+'",';
                result += '"OAOMigrateAcctToTN": "'+String.valueOf(velo.Funding_OAO__c)+'"';
            result += '},';
            result += '"AddUserProduct": {';
                result += '"ProductCode": "'+pc.TF4SF__Sub_Product_Code__c+'",';
                result += '"DestinationAccountId": "",';
                result += '"SourceAccountId": "'+CFIID+'",';
                if (application.app.TF4SF__Product__c == 'Checking') {
                    result += '"FundingAmount": "'+application.about.TF4SF__Dollar_Amount_External_CHK__c+'"';
                } else {
                    result += '"FundingAmount": "'+application.about.TF4SF__Dollar_Amount_External_SAV__c+'"';
                }
            result += '},';
            result += '"Email": "'+application.app.TF4SF__Email_Address__c+'"';
        result += '}';
        return execute(appId, result, Token, Endpoint);

    }

    private static String execute(String appId, String json, String Token, String Endpoint) {
        System.debug('appId, JSON: ' + appId  + '---' + json);
        String responseJson = '';
        String GUID = GUIDGenerator.getGUID();
        
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            VELOSETTINGS__C velo = VELOSETTINGS__C.getOrgDefaults();
            Integer timeOut = Integer.ValueOf(velo.ProcessAPI_Timeout__c);

            req.setTimeout(timeOut);
            req.setHeader(velo.Request_GUID_Name__c,GUID);
            req.setEndpoint(Endpoint);
            req.setHeader('Authorization','Bearer '+Token);
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('Accept','application/json');
            req.setMethod('POST');
            
            req.setBody(json);
            HttpResponse res = h.send(req);
            responseJson = res.getBody();
            system.debug('Res is '+res.getbody()); 
        try{
            DebugLogger.InsertDebugLog(appId, Endpoint + ', ' + GUID, 'FundingValidateAccount Request GUID'); 
        }catch(Exception ex){
            system.debug('Exception ex'+ex);
        }
        return responseJSON;
    }
}