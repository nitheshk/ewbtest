global class QueryHelper {
	global QueryHelper() {}

	global static String quote(String text) {
		return '\'' + text + '\'';
	}

	global static ObjectInfo selectStar(String objectName) {
		SObjectType objToken = Schema.getGlobalDescribe().get(objectName);
		DescribeSObjectResult objDef = objToken.getDescribe();
		Map<String, SObjectField> fields = objDef.fields.getMap(); 
		Set<String> fieldSet = fields.keySet();
		List<String> fieldsToQuery = new List<String>{};

		for (String fieldName : fieldSet) {
		   SObjectField fieldToken = fields.get(fieldName);
		   DescribeFieldResult selectedField = fieldToken.getDescribe();

		   //respect CRUD/FLS
			if (selectedField.isAccessible()) {
				fieldsToQuery.add(selectedField.getName());
			} else {
				System.debug('not accessible: ' + selectedField.getName());
			}
		}

		String queryString = 'SELECT ';
		queryString += String.join(fieldsToQuery, ', ');
		queryString += ' FROM ' + objectName + ' ';

		return new ObjectInfo(queryString, fieldsToQuery);
	}

	global class ObjectInfo {
		global String query {get; set;}
		global List<String> fieldNames {get; set;}
		global ObjectInfo() {}
		global ObjectInfo(String query, List<String>fieldNames) {
			this.query = query;
			this.fieldNames = fieldNames;
		}
	}
}