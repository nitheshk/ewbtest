public class DocusignTemplateMapper {

    public class Owner {
        public String userName;
        public String userId;
        public String email;
    }

    public List<EnvelopeTemplates> envelopeTemplates;
    public String resultSetSize;
    public String startPosition;
    public String endPosition;
    public String totalSetSize;

    public class EnvelopeTemplates {
        public String templateId;
        public String name;
        public String shared;
        public String password;
        public String description;
        public String lastModified;
        public String created;
        public Integer pageCount;
        public String uri;
        public String folderName;
        public String folderId;
        public String folderUri;
        public Owner owner;
        public String emailSubject;
        public String emailBlurb;
        public String signingLocation;
        public String authoritativeCopy;
        public String enforceSignerVisibility;
        public String enableWetSign;
        public String allowMarkup;
        public String allowReassign;
    }

    
    public static DocusignTemplateMapper parse(String json) {
        return (DocusignTemplateMapper) System.JSON.deserialize(json, DocusignTemplateMapper.class);
    }
}