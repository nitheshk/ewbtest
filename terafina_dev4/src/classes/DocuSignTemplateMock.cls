@isTest
global class DocuSignTemplateMock implements HttpCalloutMock {
  // Implement this interface method
  global HTTPResponse respond(HTTPRequest req) {
    // Optionally, only send a mock response for a specific endpoint
    // and method.
    // Create a fake response
    HttpResponse res = new HttpResponse();
    res.setHeader('Content-Type', 'application/json');
    res.setBody('<?xml version="1.0" encoding="UTF-8"?><Account>  <loginAccount>Test</loginAccount>  <userName>Test</userName></Account>');
    res.setStatusCode(200);
    return res;
  }
}