public class MortgageEmail {
    public void sendEmail(TF4SF__Application__c app){
        List<Messaging.Email> emailList = new List<Messaging.Email>();
        List<User> appOwner = new List<User>();
        System.debug('--application'+app);
        appOwner = [SELECT Id, Name, Email FROM User WHERE Id =:app.OwnerId];
        System.debug('appOwner'+appOwner);
        List<EmailTemplate> emailTemp = new List<EmailTemplate>();
        emailTemp = [SELECT Id, Name FROM EmailTemplate where Name = 'MortgageApp' ];
         System.debug('--emailTemp'+emailTemp);
        
        if ( app.TF4SF__Product__c == 'Home Loan'){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            
                if(!test.isRunningTest()){
                    String[] toAddresses = new String[] {appOwner[0].Email};
                    mail.setToAddresses(toAddresses);}
             if(test.isRunningTest()){
                 String[] toAddresses = new String[] {app.TF4SF__Email_Address__c};
                 mail.setToAddresses(toAddresses);}
                
            //mail.setTemplateId(emailTemp[0].Id);
            mail.setTargetObjectId(UserInfo.getUserId());
            mail.setUseSignature(false);
            mail.setSaveAsActivity(false);
            mail.setSubject('A New Application');
            mail.setHtmlBody('Hi '+appOwner[0].Name+', <br/><br/>A New Application '+app.TF4SF__Sub_Product__c+'  has been assigned to you <br/><br/> <a target="_blank" href="https://tfdemo2-dev-ed.my.salesforce.com/'+app.Id+'">Click here</a> ');
            System.debug('--mail after template Set'+mail);
            emailList.add(mail);          
        }
        Messaging.sendEmail(emailList);
    }
}