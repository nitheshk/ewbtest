global with sharing class ValidatePromoCodeEmailExtension implements TF4SF.DSP_Interface {
    
    global static Map<String, String> main(Map<String, String> tdata) {
        Map<String, String> data = new Map<String, String>();
        
        String Options = '',valid= 'false',str='',status='0',message='';        
        String appId = tdata.get('appId');     
        String emailid=tdata.get('EmailId');    
        String phoneNo=tdata.get('PhoneNo');  
        String apiPhoneNo='';             
         
        //Get mapped promo code email & contact no
        //Validate users email id & contact no 
        try{

         List <TF4SF__Application__c>  listCurrentApp = [SELECT Id, TF4SF__Application_Status__c,
         TF4SF__Sub_Product__c ,TF4SF__Email_Address__c,TF4SF__Primary_Phone_Number__c,
         Promo_Code_Email__c,Promo_Code_Phone_Number__c
         FROM TF4SF__Application__c 
         where Id=:appId];   

        if(!listCurrentApp.isempty() && String.isNotBlank(emailid) && String.isNotBlank(phoneNo)) 
         {         
            if(String.isNotBlank(listCurrentApp[0].Promo_Code_Email__c) && String.isNotBlank(listCurrentApp[0].Promo_Code_Phone_Number__c)) 
            {
                apiPhoneNo=listCurrentApp[0].Promo_Code_Phone_Number__c;

                if(emailid==listCurrentApp[0].Promo_Code_Email__c && PhoneCleanup(phoneNo)==PhoneCleanup(listCurrentApp[0].Promo_Code_Phone_Number__c))
                { 
                   valid='true';
                   status='200'; 
                } 
            }
          
         }            
        }
        catch(Exception ex){
            message=ex.getMessage();
        }
        
        Options += '{';
        Options += '"status": "'+status+'",'; 
        Options += '"statusMessage": "'+valid+'",';  
        Options += '"message": "'+message+'"';    
        Options += '}';

        data.put('status',Options);
        if (status == '200') {
            createShadowAccount(appId, AuthToken.Auth(), emailid.toLowerCase(), PhoneCleanup(phoneNo));
        }
        return data; 
    }

    public static string PhoneCleanup(String phoneNo)
    {
        return phoneNo.replace('(','').replace(')','').replace('-','').replace(' ','');
    }

    //@future(callout=true) // MVP2 : Nag, making direct call to avoid delay in shadowaccount creation.
    public static void createShadowAccount(String appId, String Token, String Email, String Phone) {
        List<OKTA__c> oktaList = [SELECT Id, Email_Address__c, Factor_Id__c, SMS_Id__c, Status__c, User_Id__c FROM OKTA__c WHERE Email_Address__c =: Email LIMIT 1];
        if (oktaList.size() > 0) {
            //Okta Account was created already with the same email, no new call required
            System.debug('Okta Account Already Exists with Email ##### : ' + Email);
            System.debug('Okta Account Already Exists ##### Values : ' + oktaList);
        } else {
            try{
                Http h = new Http();
                HttpRequest req = new HttpRequest();
                VELOSETTINGS__C velo = VELOSETTINGS__C.getOrgDefaults();
                Integer timeOut = Integer.ValueOf(velo.ProcessAPI_Timeout__c);
                String GUID = GUIDGenerator.getGUID();
                req.setTimeout(timeOut);
                req.setHeader(velo.Request_GUID_Name__c,GUID);
                req.setEndpoint(velo.Endpoint__c+'onb/api/process/iam/users');
                req.setHeader('Authorization','Bearer '+Token);
                req.setHeader('Accept','application/json');
                req.setHeader('Content-Type', 'application/json');
                req.setMethod('POST');

                String body = '{';
                body += '"profile": {';
                body += '"email": "'+Email+'",';
                body += '"login": "'+Email+'"';
                body += '}';
                body += '}';

                    
                req.setBody(body);
                HttpResponse res = h.send(req);
                system.debug('Res is '+res.getbody());

                try {
                    DebugLogger.InstantDebugLog(appId, GUID, 'ValidatePromoCodeEmailExtension CreateShadowAccount Request GUID');
                    DebugLogger.InstantDebugLog(appId, res.getBody(), 'ValidatePromoCodeEmailExtension CreateShadowAccount Response');
                    DebugLogger.InstantDebugLog(appId, req.getBody(), 'ValidatePromoCodeEmailExtension CreateShadowAccount Request');
                } catch (Exception e) {
                    system.debug('exception '+e.getMessage());
                }
                
                    if (res.getStatusCode() == 200) {
                        OKTA__c okta = new OKTA__c();
                        Map<String, Object> OktaMap = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
                        okta.User_Id__c = String.valueOf(OktaMap.get('id'));
                        okta.Status__c =  String.valueOf(OktaMap.get('status'));
                        okta.Email_Address__c = Email;
                        if (String.isNotBlank(okta.Status__c) && String.isNotBlank(okta.User_Id__c)) {
                            insert okta;
                        }
                    }
            } catch (Exception ex) {
                system.debug('exception '+ex.getMessage()+ ' at line number '+ex.getLineNumber());
            }
        }
        
    }

}