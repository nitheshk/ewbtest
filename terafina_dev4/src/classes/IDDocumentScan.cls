/*********************************************
@Author : Nag
@Date : 8/2/2018
@Description : This will process data returned 
               by delegate
**********************************************/
global with sharing class IDDocumentScan implements TF4SF.DSP_Interface {
    
    global Map<String, String> main(Map<String, String> tdata) {
        Map<String, String> data = new Map<String, String>();
        data = tdata.clone();
        String backImage = '';
        String appId = data.get('id');
        String idType=data.get('idType');       
        Blob imageBack;
        Blob imageFront;
        IDAuthResponse idAuthResponse = null;
        String ID_AUTH_DEFAULT_STATUS = Label.IDAuth_Default_Status;
        String ID_AUTH_RES_FIELD_DAY_OF_BIRTH = Label.ID_AUTH_RESPONSE_FIELD_DAYOFBIRTH;
        String ID_AUTH_RES_FIELD_MONTH_OF_BIRTH = Label.ID_AUTH_RESPONSE_FIELD_MONTHOFBIRTH;
        String ID_AUTH_RES_FIELD_YEAR_OF_BIRTH = Label.ID_AUTH_RESPONSE_FIELD_YEAROFBIRTH;
        
        ID_Auth_Config__mdt idAuthConfig;
        String DOB = '';

        try {
            Map<String, String> resultData = new Map<String, String>();
            data.put('RecordStatus' , '');
            
            String isNRA=data.get('isNra'); 
            System.debug('id---->'+ ' '+ appId + ' ' + idType + ' '+ isNRA); 
            
            for(ID_Auth_Config__mdt idAuth : [SELECT Configuration_Name__c,DeveloperName,Id,IDV_Fill_Fields__c,ID_Type__c,Label,Language,MasterLabel,Method__c,NamespacePrefix,QualifiedApiName,Service_Value__c,
                        Time_Out__c,UI_Prefill_Fields__c,URL__c,Use_Mock_Service__c,Valid_Criteria__c FROM ID_Auth_Config__mdt where ID_Type__c =: idType]){
                    idAuthConfig = idAuth;
            }
            
            idAuthResponse = IdScan(data,idAuthConfig);
            
            
            data.remove('attachmentId');
            data.remove('extensionClass');
           
            data.put('RecordStatus' , ID_AUTH_DEFAULT_STATUS);
             
             if(idAuthResponse != null && idAuthResponse.Data != null ){
                String recordStatusFromRes = idAuthResponse.Data.Record.RecordStatus != null ? idAuthResponse.Data.Record.RecordStatus.toLowerCase() : ID_AUTH_DEFAULT_STATUS;
                
                data.put('RecordStatus' , recordStatusFromRes);
                data.put('idType', idType);
                
                System.debug('Fields to map for UI $$$$$ : ' + idAuthConfig.UI_Prefill_Fields__c);
            
                IDAuthAndVerifyMapping fieldMapping = null;
                if(String.isNotBlank(idAuthConfig.UI_Prefill_Fields__c)){
                    fieldMapping = (IDAuthAndVerifyMapping)System.JSON.deserialize(idAuthConfig.UI_Prefill_Fields__c, IDAuthAndVerifyMapping.class);
                }
            
                if(fieldMapping != null){
                    Map<String, String> respMap = new Map<String,String>();
                    
                    //Populate Response Fields wiht FieldName and Value
                    
                    if(idAuthResponse.data.Record != null && idAuthResponse.data.Record.DatasourceResults != null){
                        for(IDAuthResponse.DatasourceResults ds : idAuthResponse.data.Record.DatasourceResults){
                            for(IDAuthResponse.AppendedFields appendedField : ds.AppendedFields){
                                system.debug('UI Map Data #### :'+appendedField.FieldName + ':' + appendedField.Data);
                                respMap.put(appendedField.FieldName, appendedField.Data);
                            }
                        }
                    }
                    //system.debug('Res Map after mapping @@@@@@:' + respMap);
                    //UIPrefilField set and Response RecordStatus matches with criteria or Fill UI Even not valid response flag
                    if(fieldMapping != null && (recordStatusFromRes.equalsIgnoreCase(idAuthConfig.Valid_Criteria__c) || fieldMapping.NotValidResponseFillUI)){
                        String dOfBirth = '';
                        String mOfBirth = '';
                        String yOfBirth = '';
                        for(IDAuthAndVerifyMapping.MappingDetails mapDetailRec : fieldMapping.MappingDetails ){
                            

                            if(mapDetailRec.ResFieldName == ID_AUTH_RES_FIELD_DAY_OF_BIRTH
                                            || mapDetailRec.ResFieldName == ID_AUTH_RES_FIELD_MONTH_OF_BIRTH 
                                            || mapDetailRec.ResFieldName == ID_AUTH_RES_FIELD_YEAR_OF_BIRTH){
                                
                                if(mapDetailRec.ResFieldName == ID_AUTH_RES_FIELD_DAY_OF_BIRTH){
                                    dOfBirth = respMap.get(mapDetailRec.ResFieldName);
                                }else  if(mapDetailRec.ResFieldName == ID_AUTH_RES_FIELD_MONTH_OF_BIRTH){
                                    mOfBirth = respMap.get(mapDetailRec.ResFieldName);
                                }else  if(mapDetailRec.ResFieldName == ID_AUTH_RES_FIELD_YEAR_OF_BIRTH){
                                    yOfBirth = respMap.get(mapDetailRec.ResFieldName);
                                }
                                // Format DOB
                                
                                if(String.isNotBlank(mOfBirth) && String.isNotBlank(dOfBirth) && String.isNotBlank(yOfBirth)){
                                   DOB = mOfBirth+'/'+dOfBirth+'/'+yOfBirth;
                                   data.put(mapDetailRec.PrefillFieldUI, DOB); 
                                }
                            }else{
                                data.put(mapDetailRec.PrefillFieldUI, respMap.get(mapDetailRec.ResFieldName));
                            }
                        } 
                    }
                }
                 
             } 
           
            system.debug(' before UI$$$$$$$ : ' + data);
            
            //FOR TEST COVERAGE
            if(test.isRunningTest()){Integer a = 10/0;}
            
            return data;
        } catch (Exception ex) {
            List<TF4SF__Field_Logic_New__c> lsitFieldLogic = [SELECT Id, TF4SF__Value__c FROM TF4SF__Field_Logic_New__c WHERE TF4SF__RecType__c = 'Page Labels' AND TF4SF__Page_Type__c = 'GetStartedPage' AND TF4SF__Label_Name__c = 'DL_Scan_Error_Message'];
            data.clear();
            // On exception clearing first name , last name and removing the attachment from the application.
            List<TF4SF__Application__c> appList = [SELECT Id, TF4SF__Last_Name__c, TF4SF__First_Name__c, TF4SF__Street_Address_1__c, TF4SF__Street_Address_2__c, TF4SF__City__c, TF4SF__State__c, TF4SF__Zip_Code__c, TF4SF__Suffix__c, TF4SF__Middle_Name__c FROM TF4SF__Application__c WHERE Id = :appId];

            if (appList.size() > 0) {
                appList[0].TF4SF__First_Name__c = '';
                appList[0].TF4SF__Last_Name__c = '';
                
                if (TF4SF__Application__c.SObjectType.getDescribe().isUpdateable()) { update appList; }

                List<TF4SF__Identity_Information__c> IdList = [SELECT Id, TF4SF__Country_Issued__c, TF4SF__State_Issued__c, TF4SF__Identity_Number_Primary__c, TF4SF__Issue_Date__c, TF4SF__Expiry_Date__c, TF4SF__ID_Type__c, TF4SF__Application__c FROM TF4SF__Identity_Information__c WHERE TF4SF__Application__c = :appId];
                System.debug('appid ####:' + appid);

                if (IdList.size() > 0) {
                   //ANYTHING TO RESET
                    if (TF4SF__Identity_Information__c.SObjectType.getDescribe().isUpdateable()) { update IdList; }
                }

                List<Attachment> attachments = [SELECT name FROM Attachment WHERE ParentID = :appList[0].Id];
                if (attachments.size() > 0) {
                    if (Attachment.sObjectType.getDescribe().isDeletable()) { delete attachments; }
                }
            }

            if (lsitFieldLogic != null && (!lsitFieldLogic.isEmpty()) ) {
                data.put('Exception', lsitFieldLogic[0].TF4SF__Value__c );
            } else {
                data.put('Exception', ex.getMessage());
            }

            System.debug('Exception : ' + ex.getMessage());
            return data;
        }
    }
    
    private IDAuthResponse IdScan (Map<String, String> tdata, ID_Auth_Config__mdt idAuthConfig) {
        
        String appId = tdata.get('id');
        String requestJson = '';
        String responseJson = '';
        String idType = tdata != null ? tdata.get('idType') : '' ;
        String NID_CUSTOM_LABEL = Label.IDAuth_NID_Service_Value;
        String PP_CUSTOM_LABEL = Label.IDAuth_PP_Service_Value;
        String PP_NRA_SCANNED_LABEL = Label.PP_Scanned_IMG_NRA_Label;
        String NID_NRA_SCANNED_FRONT_LABEL = Label.NID_Scanned_IMG_NRA_Front_Label;
        String NID_NRA_SCANNED_BACK_LABEL = Label.NID_Scanned_IMG_NRA_Back_Label;
        IDAuthResponse authRes;
        try {

            
            String backImage = null;
            String frontImage = null;
            String Token = '';
            TF4SF__Application__c app = [SELECT Id, Name, TF4SF__User_Token__c FROM TF4SF__Application__c WHERE Id =: appId];
            List<Attachment> attList = [SELECT Id, Name, Body, ContentType, ParentID FROM Attachment WHERE ParentID =: appId ORDER BY CreatedDate DESC LIMIT 2];
            system.debug('the atts are '+attList);
            for (Attachment att : attList) {
                system.debug('the att is :'+att.Id);
                //Stop looping if images processed
                if (idType == NID_CUSTOM_LABEL && String.isNotBlank(frontImage) && String.isNotBlank(backImage)) {
                    break;
                } 
                else if(idType == PP_CUSTOM_LABEL && String.isNotBlank(frontImage)) {
                     break;
                }
                if (!String.isBlank(att.Name)) {
                    String attachmentName = att.Name;
                    if (String.isNotBlank(IdType)) {
                        if( attachmentName == PP_NRA_SCANNED_LABEL ){
                            system.debug('the PP frontImage :' + att.Name);
                            frontImage = EncodingUtil.base64Encode(att.body);
                        }else if (attachmentName == NID_NRA_SCANNED_FRONT_LABEL ) {
                            system.debug('the frontImage :' + att.Name);
                            frontImage = EncodingUtil.base64Encode(att.body);
                        }else if (attachmentName == NID_NRA_SCANNED_BACK_LABEL ) {
                            system.debug('the backImage : ' + att.Name);
                            backImage = EncodingUtil.base64Encode(att.body);
                        }else{
                            //set to empty
                            backImage = '';
                            frontImage = '';
                        }
                    }
                }
                
            }
 
            
            IDAuthRequest req = new IDAuthRequest();
            req.applicationSfdcID = appId;
            if(idAuthConfig !=null && String.isNotBlank(idAuthConfig.Configuration_Name__c)){
                req.setConfigurationName(idAuthConfig.Configuration_Name__c);
            }
            //Intetionally left to decide for future release to come country code from UI
            req.setCountryCode(Label.IDV_REQ_DEFAULT_COUNTRY_CODE);
            req.setAcceptTruliooTermsAndConditions(true);
            IDAuthRequest.DataFields DataFields = new IDAuthRequest.DataFields();
            IDAuthRequest.Document Document = new IDAuthRequest.Document();
            Document.setDocumentType(idType);
            Document.setDocumentBackImage(backImage);
            Document.setDocumentFrontImage(frontImage);
            DataFields.setDocument(Document);
            req.setDataFields(DataFields);
            
            authRes = IDAuthenticationDelegate.processIDDocument(req); 
            //Set DocumentType for Presubmit for IDV
            if(authRes != null && authRes.Data != null ){
                authRes.Data.DocumentType = idType;
            }
            System.debug('authRes #####:' + authRes);
            
            
            requestJson = JSON.serialize(req);
            responseJson = JSON.serialize(authRes);
            
            DebugLogger.InsertDebugLog(appId, requestJson, idType + ' '+ 'Request');
            DebugLogger.InsertDebugLog(appId, responseJson, idType +' '+ 'Response');
        } catch (Exception e) {
            system.debug('exception '+e.getMessage());
            DebugLogger.InsertDebugLog(appId, requestJson, idType + ' '+ 'Request');
            DebugLogger.InsertDebugLog(appId, responseJson, idType + ' '+ 'Response');           
        }
        
        
        return authRes;
    }
}