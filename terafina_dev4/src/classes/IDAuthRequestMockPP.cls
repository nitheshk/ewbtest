/*************************************************
@Author : Nag
@Date : 8/3/2018
@Description : This responds with mockresponse for Passport
****************************************************/

public class IDAuthRequestMockPP implements HttpCalloutMock {

    public HTTPResponse respond(HTTPRequest req) {
    
        HttpResponse resp = new HttpResponse();
        resp.setStatusCode(200);
        resp.setStatus('success');
        resp.setBody(returnMockResponse(Label.IDAuth_PP_Service_Value));
        return resp;
    }

    private static String returnMockResponse(String resReq){
        String fileName = resReq+'MockJson';
        String body;
        try{
        StaticResource sr = [SELECT Id,NamespacePrefix,SystemModstamp FROM StaticResource WHERE Name =: fileName LIMIT 1];
        String prefix = sr.NamespacePrefix;
        if( String.isEmpty(prefix) ) {
            prefix = '';
        } else {
            //If has NamespacePrefix
            prefix += '__';
        }
        String srPath = '/resource/' + sr.SystemModstamp.getTime() + '/' + prefix + fileName; 
        PageReference pg = new PageReference( srPath );
        body = pg.getContent().toString();
        
        }catch(Exception ex){
            system.debug('Exception ex :' + ex);
        }
        return body;
    }
    
}