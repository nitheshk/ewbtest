@isTest(SeeAllData=true)
public class CryptoHelperCustomTest {

  @isTest static void test_method_one() {
    String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
    Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
    User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
      EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US', ProfileId = p.Id,
      TimeZoneSidKey = 'America/Los_Angeles',
      UserName = uniqueUserName);

    System.runAs(u) {
      TF4SF__Application__c app = new TF4SF__Application__c(
                TF4SF__External_App_Stage__c = '1',TF4SF__External_AppStage_CrossSell1__c = 'Pending',
                TF4SF__External_AppStage_CrossSell2__c = 'Pending',TF4SF__External_AppStage_CrossSell3__c = 'Pending',
                TF4SF__Primary_Offer__c = 'Credit Cards - Visa Platinum',TF4SF__Second_Offer__c = 'Personal Loans - Certificate Secured',
                TF4SF__Third_Offer__c = 'Vehicle Loans - Auto',
                TF4SF__Current_Person__c = u.Id,TF4SF__Custom_Checkbox4__c=true,TF4SF__Current_Branch_Name__c='Personal-Loans',
                TF4SF__Mailing_Zip_Code__c='57423',TF4SF__Zip_Code_Prev__c='12345',TF4SF__Preferred_Contact_Method__c='Work',
                TF4SF__Membership_Qualification_Type__c = 'Qualifying Member or Roommate', TF4SF__Custom_Text5__c='test',
                TF4SF__Tertiary_Phone_Number__c='',  OwnerId  = u.Id,

                //Applicant
                TF4SF__First_Name__c = 'TestFirst',TF4SF__Last_Name__c = 'TestLast', TF4SF__Email_Address__c = 'test@test.com',
                TF4SF__Product__c = 'Checking',TF4SF__Sub_Product__c = 'Checking - Checking',TF4SF__City__c = 'There',
                TF4SF__Primary_Phone_Number__c = '8889557212',TF4SF__State__c = 'AZ',TF4SF__Street_Address_1__c = '123 That Street', 
                TF4SF__Street_Address_2__c = '',TF4SF__Zip_Code__c = '89898',TF4SF__Months__c = '5', TF4SF__Middle_Name__c = 'TestMiddle',
                TF4SF__Housing_Status__c = 'Own', TF4SF__Monthly_Payment__c = 3000.00,TF4SF__Years__c = 2.0,TF4SF__Secondary_Phone_Number__c = '8885551313',
            TF4SF__User_Token_Expires__c=System.now());
            insert app;
            
            TF4SF__Identity_Information__c ident = new TF4SF__Identity_Information__c(
                TF4SF__Application__c = app.Id,TF4SF__SSN_Prime__c = '999999999',TF4SF__Date_of_Birth__c = '01/01/1970',
                TF4SF__SSN_J1__c = '999999991',TF4SF__Date_of_Birth_J1__c = '01/01/1970',
                TF4SF__SSN_J2__c = '999999992',TF4SF__Date_of_Birth_J2__c = '01/01/1970',
                TF4SF__SSN_J3__c = '999999993',TF4SF__Date_of_Birth_J3__c = '01/01/1970',
                TF4SF__ID_Type__c = 'Drivers License', TF4SF__Identity_Number_Primary__c = '99999999991', TF4SF__State_Issued__c = 'AZ',
                TF4SF__ID_Type_J1__c = 'Drivers License', TF4SF__Identity_Number_J1__c = '99999999992', TF4SF__State_Issued_J1__c = 'AZ',
                TF4SF__ID_Type_J2__c = 'Drivers License', TF4SF__Identity_Number_J2__c = '99999999993', TF4SF__State_Issued_J2__c = 'AZ',
                TF4SF__Country_Issued__c='CA'
            );
            insert ident;

       List<TF4SF__Application__c> appList = new List<TF4SF__Application__c>();
       appList.add(app);

          Test.StartTest();
          CryptoHelperCustom.genMasterKey();
          CryptoHelperCustom.refreshTokens(appList);
          CryptoHelperCustom.expireToken(app);
          CryptoHelperCustom.setAppToken(app);
          CryptoHelperCustom.userTokenIsValid(app.id,'AnxzH/+Kmj85t2Wzkj5qETFWtwltJXsC5dcjGEgtGa89TTX4OIYGRky9UDclje65');
          Test.StopTest();
    }
    }
}