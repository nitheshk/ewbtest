@isTest
public class DepositPreSubmitTest{ 

 @isTest static void test_method_one() {
    String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
    Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
    User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
      EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US', ProfileId = p.Id,
      TimeZoneSidKey = 'America/Los_Angeles',
      UserName = uniqueUserName);

    System.runAs(u) {

Map<String, String> tdata = new Map<String, String>();
    
TF4SF__Application__c app = new TF4SF__Application__c ();
        app.TF4SF__Product__c = 'Checking';
        app.TF4SF__Last_Name__c='TestFirst';
        app.TF4SF__First_Name__c='TestLast';
        app.TF4SF__Sub_Product__c = 'Checking - Checking';
        insert app;
    
    /*TF4SF__Application__c app1 = new TF4SF__Application__c ();
    
        app1.Primary_Product_Case_Id__c ='CaseNumber';
        app1.Primary_Product_Case_Status__c='Declined';
        app1.TF4SF__Primary_Product_Status__c='Approved';
        app1.TF4SF__Application_Status__c = 'open';
        app1.TF4SF__Current_Channel__c = 'Online';
        app1.TF4SF__Email_Address__c = 'abc@gmail.com';
        app1.TF4SF__External_App_ID__c='AccNumber';
        app1.TF4SF__External_App_Stage__c='CustomerNumber';
        app1.TF4SF__Application_Page__c='StartFundingPage';
        insert app1;*/
        
        VELOSETTINGS__C vo = new VELOSETTINGS__C();
        vo.Name='test';
        vo.Request_GUID_Name__c = 'RequestUUID';
        vo.ProcessAPI_Timeout__c = 120000;
        vo.TokenEndPoint__c = 'AnxzH/+Kmj85t2Wzkj5qETFWtwltJXsC5dcjGEgtGa89TTX4OIYGRky9UDclje65';
        insert vo;
        
        tdata.put('id',(String)app.id);
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new DepositPreSubmitMock()); 
        DepositPreSubmit.main(tdata);
        test.stopTest();
    }
 }

 @isTest static void test_method_two() {
    String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
    Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
    User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
      EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US', ProfileId = p.Id,
      TimeZoneSidKey = 'America/Los_Angeles',
      UserName = uniqueUserName);

    System.runAs(u) {

    Map<String, String> tdata = new Map<String, String>();
        
    TF4SF__Application__c app = new TF4SF__Application__c ();
        app.TF4SF__Product__c = 'Checking';
        app.TF4SF__Last_Name__c='TestFirst';
        app.TF4SF__First_Name__c='TestLast';
        app.TF4SF__Sub_Product__c = 'Checking - Checking';
        insert app;
    
    /*TF4SF__Application__c app1 = new TF4SF__Application__c ();
    
        app1.Primary_Product_Case_Id__c ='CaseNumber';
        app1.Primary_Product_Case_Status__c='Declined';
        app1.TF4SF__Primary_Product_Status__c='Approved';
        app1.TF4SF__Application_Status__c = 'open';
        app1.TF4SF__Current_Channel__c = 'Online';
        app1.TF4SF__Email_Address__c = 'abc@gmail.com';
        app1.TF4SF__External_App_ID__c='AccNumber';
        app1.TF4SF__External_App_Stage__c='CustomerNumber';
        app1.TF4SF__Application_Page__c='StartFundingPage';
        insert app1;*/
        
        VELOSETTINGS__C vo = new VELOSETTINGS__C();
        vo.Name='test';
        vo.Request_GUID_Name__c = 'RequestUUID';
        vo.ProcessAPI_Timeout__c = 120000;
        vo.ClientId__c = '8er2cx5d37h96GtJk9b9';
        vo.ClientSecret__c = '8er2cx5d37h96GtJk9b9';
        vo.EndPoint__c = 'https://devopenapi.velobank.com/';
        vo.FundingEndpoint__c ='https://devopenapi.velobank.com/';
        vo.TokenEndPoint__c = 'https://openapiservices.eastwestbank.com/AuthorizationServer/api/token/client/jwt';
        insert vo;
        
        tdata.put('id',(String)app.id);
        tdata.put('Token', 'AnxzH/+Kmj85t2Wzkj5qETFWtwltJXsC5dcjGEgtGa89TTX4OIYGRky9UDclje65');
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new DepositSubmitMock()); 
        DepositPreSubmit.applyNowProductASync((String)app.id,app.TF4SF__Sub_Product__c,tdata);
        //String Response = '{ "data": {"ApplicationId": "APP-M-05585", "ApplicationStatus": "Pending", "InstantIdResponse": { "Status": { "ConversationId": "31000105268575", "RequestId": "300475735", "TransactionStatus": "pending", "ActionType": "answers", "Reference": "Reference1" }, "Products": [{ "ProductType": "InstantID", "ExecutedStepName": "InstantID Step", "ProductConfigurationName": "ewb.onlineaccountopening_instantid", "ProductStatus": "pass" }, { "ProductType": "Velocity", "ExecutedStepName": "PostID Step", "ProductConfigurationName": "ewb.testing.onlineaccountopening_velocity_postid", "ProductStatus": "pass", "Items": [{ "ItemName": "FREQUENCY", "ItemStatus": "pass" }, { "ItemName": "QUIZ", "ItemStatus": "pass" } ] }, { "ProductType": "IdentityEvent", "ExecutedStepName": "Identity_Events Step", "ProductConfigurationName": "ewb.onlineaccountopening_identity_events", "ProductStatus": "pass" }, { "ProductType": "IIDQA", "ExecutedStepName": "Authentication Step", "ProductConfigurationName": "ewb.testing.redherrings_authentication_open", "ProductStatus": "pending", "QuestionSet": { "QuestionSetId": 96344445, "Questions": [{ "QuestionId": 331817715, "Key": 5401, "Type": "singlechoice", "Text": { "Statement": "Which of the following colleges have you attended?" }, "HelpText": { "Statement": "Select the college you have attended." }, "Choices": [{ "ChoiceId": 1632692475, "Text": { "Statement": "Black Hawk College" } }, { "ChoiceId": 1632692485, "Text": { "Statement": "Hussian School Of Art" } }, { "ChoiceId": 1632692495, "Text": { "Statement": "Northern State University" } }, { "ChoiceId": 1632692505, "Text": { "Statement": "Schenectady County Community College" } }, { "ChoiceId": 1632692515, "Text": { "Statement": "None of the above" } } ] }, { "QuestionId": 331817725, "Key": 6015, "Type": "singlechoice", "Text": { "Statement": "Which of the following boats or watercrafts have you owned?" }, "HelpText": { "Statement": "Select the watercraft you have been associated with." }, "Choices": [{ "ChoiceId": 1632692525, "Text": { "Statement": "15 ft. Skeeter Products,inc. 1988" } }, { "ChoiceId": 1632692535, "Text": { "Statement": "16 ft. Dixie 172" } }, { "ChoiceId": 1632692545, "Text": { "Statement": "19 ft. Crestliner Phantom" } }, { "ChoiceId": 1632692555, "Text": { "Statement": "21 ft. Micro Boats Mfg (gilleys 2009" } }, { "ChoiceId": 1632692565, "Text": { "Statement": "None of the above" } } ] }, { "QuestionId": 331817735, "Key": 30131, "Type": "singlechoice", "Text": { "Statement": "In which of the following cities does Roni Moss currently live or own property?" }, "HelpText": { "Statement": "Names may be listed as last-name first-name, include maiden names or contain slight misspellings." }, "Choices": [{ "ChoiceId": 1632692575, "Text": { "Statement": "Bellmawr, New Jersey" } }, { "ChoiceId": 1632692585, "Text": { "Statement": "Edison, New Jersey" } }, { "ChoiceId": 1632692595, "Text": { "Statement": "Highland Park, New Jersey" } }, { "ChoiceId": 1632692605, "Text": { "Statement": "Jersey City, New Jersey" } }, { "ChoiceId": 1632692615, "Text": { "Statement": "None of the above or I am not familiar with this person" } } ] } ] } } ], "PassThroughs": [{ "Type": "INSTANT_ID", "Data": "{\"InstantIDResponseEx\":{\"response\":{\"Header\":{\"Status\":0,\"TransactionId\":\"41878\"},\"Result\":{\"InputEcho\":{\"Name\":{\"First\":\"IMANI\",\"Last\":\"MOSS\"},\"Address\":{\"StreetAddress1\":\"1501 E Century Blvd Apt 2\",\"City\":\"Los Angeles\",\"State\":\"CA\",\"Zip5\":\"90002-2801\"},\"DOB\":{\"Year\":1961,\"Month\":12,\"Day\":23},\"SSN\":\"545159043\",\"HomePhone\":\"9413487575\",\"UseDOBFilter\":false,\"DOBRadius\":2,\"Passport\":{\"Number\":\"\",\"ExpirationDate\":{},\"Country\":\"\",\"MachineReadableLine1\":\"\",\"MachineReadableLine2\":\"\"},\"Channel\":\"\",\"OwnOrRent\":\"\"},\"UniqueId\":\"385133830\",\"VerifiedInput\":{\"Name\":{\"First\":\"IMANI\",\"Last\":\"MOSS\"},\"Address\":{\"StreetNumber\":\"1501\",\"StreetPreDirection\":\"E\",\"StreetName\":\"CENTURY\",\"StreetSuffix\":\"BLVD\",\"UnitDesignation\":\"APT\",\"UnitNumber\":\"2\",\"StreetAddress1\":\"1501 E CENTURY BLVD APT 2\",\"City\":\"LOS ANGELES\",\"State\":\"CA\",\"Zip5\":\"90002\",\"Zip4\":\"8821\",\"County\":\"YUMA\"},\"SSN\":\"545159043\",\"HomePhone\":\"9413487575\",\"DOB\":{\"Year\":\"1961\",\"Month\":\"12\",\"Day\":\"23\"}},\"DOBVerified\":true,\"NameAddressSSNSummary\":10,\"NameAddressPhone\":{\"Summary\":\"7\",\"Type\":\"P\"},\"ComprehensiveVerification\":{\"ComprehensiveVerificationIndex\":20,\"RiskIndicators\":{\"RiskIndicator\":[{\"RiskCode\":\"51\",\"Description\":\"The input last name is not associated with the input SSN\",\"Sequence\":1},{\"RiskCode\":\"66\",\"Description\":\"The input SSN is associated with a different last name same first name\",\"Sequence\":2},{\"RiskCode\":\"SD\",\"Description\":\"The input address State is different than LN best address State for the input identity\",\"Sequence\":3},{\"RiskCode\":\"10\",\"Description\":\"The input phone number is a mobile number\",\"Sequence\":4},{\"RiskCode\":\"49\",\"Description\":\"The input phone and address are geographically distant (>10 miles)\",\"Sequence\":5}]},\"PotentialFollowupActions\":{\"FollowupAction\":[{\"RiskCode\":\"B\",\"Description\":\"Verify name with Social (via SSN card DL if applicable paycheck stub or other Government Issued I\"},{\"RiskCode\":\"C\",\"Description\":\"Verify name with Address (via DL utility bill Directory Assistance paycheck stub or other Govern\"},{\"RiskCode\":\"D\",\"Description\":\"Verify phone (Directory Assistance utility bill)\"}]}},\"ReversePhone\":{\"Name\":{\"First\":\"IMANI\",\"Last\":\"MOSS\"},\"Address\":{\"StreetNumber\":\"1501\",\"StreetPreDirection\":\"E\",\"StreetName\":\"CENTURY\",\"StreetSuffix\":\"BLVD\",\"UnitDesignation\":\"APT\",\"UnitNumber\":\"2\",\"StreetAddress1\":\"1501 E CENTURY BLVD APT 2\",\"City\":\"LOS ANGELES\",\"State\":\"CA\",\"Zip5\":\"90002\"}},\"SSNInfo\":{\"Valid\":\"G\",\"IssuedLocation\":\"NORTH CAROLINA\",\"IssuedStartDate\":{\"Year\":1989,\"Month\":1},\"IssuedEndDate\":{\"Year\":1990,\"Month\":12}},\"ChronologyHistories\":{\"ChronologyHistory\":[{\"Address\":{\"StreetNumber\":\"1501\",\"StreetPreDirection\":\"E\",\"StreetName\":\"CENTURY\",\"StreetSuffix\":\"BLVD\",\"UnitDesignation\":\"APT\",\"UnitNumber\":\"2\",\"StreetAddress1\":\"1501 E CENTURY BLVD APT 2\",\"City\":\"YUMA\",\"State\":\"AZ\",\"Zip5\":\"85364\",\"Zip4\":\"8821\"},\"DateFirstSeen\":{\"Year\":2013,\"Month\":6},\"DateLastSeen\":{\"Year\":2013,\"Month\":9}},{\"Address\":{\"StreetAddress1\":\"27 HICKORY GLN\",\"City\":\"VILLA RICA\",\"State\":\"GA\",\"Zip5\":\"30180\",\"Zip4\":\"4801\"},\"DateFirstSeen\":{\"Year\":2004,\"Month\":10},\"DateLastSeen\":{\"Year\":2013,\"Month\":9},\"IsBestAddress\":true},{\"Address\":{\"StreetAddress1\":\"115 RUTH DR\",\"City\":\"NEWNAN\",\"State\":\"GA\",\"Zip5\":\"30265\",\"Zip4\":\"2152\"},\"DateFirstSeen\":{\"Year\":2004,\"Month\":9},\"DateLastSeen\":{\"Year\":2013,\"Month\":9}}]},\"AdditionalScore1\":\"0\",\"AdditionalScore2\":\"0\",\"CurrentName\":{\"First\":\"IMANI\",\"Last\":\"MOSS\"},\"AdditionalLastNames\":{\"AdditionalLastName\":[{\"DateLastSeen\":{\"Year\":2013,\"Month\":9},\"LastName\":\"CUTLER\"}]},\"PassportValidated\":false,\"DOBMatchLevel\":8,\"SSNFoundForLexID\":true,\"AddressPOBox\":false,\"AddressCMRA\":false,\"InstantIDVersion\":\"1\",\"EmergingId\":false,\"AddressStandardized\":false,\"StandardizedInputAddress\":{\"StreetNumber\":\"1501\",\"StreetPreDirection\":\"E\",\"StreetName\":\"CENTURY\",\"StreetSuffix\":\"BLVD\",\"UnitDesignation\":\"APT\",\"UnitNumber\":\"2\",\"City\":\"YUMA\",\"State\":\"AZ\",\"Zip5\":\"90002\",\"Zip4\":\"8821\",\"Latitude\":\"32.68572\",\"Longitude\":\"-114.65043\"}}}}}" }] }, "PinpointResponse": { "status": "ok", "message": { "infection_data": { "malware": { "malware_ids": { }, "info": { }, "malware": { }, "timestamp": "2018-07-02 16:00:49", "risk_score": 0, "customer_session_id": "ox6JkZKgTCs2ojo44B8qotMh3" } }, "device_data": { "device_key": "N/A", "is_new": null, "v3": { "screen_width": "1366", "activeX": "0", "dma_code": 0, "ip_class": "C", "continent_code": "NA", "remote_addr": "209.63.147.116", "city": "San Francisco", "plugins": "429867076", "country_code": "US", "counter": "1", "last_access": "2018-07-02 16:00:50", "screen_touch": "0", "agent_key": "N/A", "accept": "*/*", "screen_dpi": "24", "client_language": "en-US", "post_code": "94115", "screen_height": "768", "mimes": "13481324", "longitude": -122.4371, "latitude": 37.7862, "first_time": "2018-07-02 16:00:50", "cpu": "Win32", "browser": "Chrome", "machine_id": "N/A", "digest": "b2a539fa71396ab4856deed4791d991d6a2ce413", "js": "1", "country_name": "United States", "file_upload_indicator": "0", "platform": "Win32", "history_length": "2", "file_upload": "1", "client_time_zone": "420", "navigator_props": "1469268959", "doc_location": "https%3A//dev-eastwestbank.cs20.force.com/DepositLandingPage%3FsubPrdCode%3DALL", "browser_version": "67.0.3396", "client_charset": "UTF-8", "isp": "Integra Telecom", "x_forwarded_for": "209.63.147.116, 10.81.145.125", "accept_language": "en-US,en;q=0.9", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36", "org": "Integra Telecom", "region": "CA", "first_access": "2018-07-02 16:00:50", "charset": 1, "country": "USA", "id": 2528773531, "pinpoint_session": "00041190698871700280", "cookie": "0.0973339698972", "hashed_user_id": "N/A", "application": "222916", "os": "Windows 10", "business": "eastwestbank", "fonts": "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8", "area_code": "0", "ip_time_zone": "-480", "accept_encoding": "gzip, deflate, br", "country_code3": "USA" } }, "recommendation": { "risk_score": 0, "reason_id": 0, "recommendation": "allow", "reason": "" }, "customer_session_id": "ox6JkZKgTCs2ojo44B8qotMh3" } } }, "result": { "success": true, "code": "200", "message": "Success" } }';
        //PreSubmitWrapper wrap = PreSubmitWrapper.parse(Response);
        //DepositPreSubmit.savekycquestions((String)app.id, wrap);
        test.stopTest();
    }
 }
}