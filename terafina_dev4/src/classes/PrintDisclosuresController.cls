global with sharing class PrintDisclosuresController{

  public String SiteUrl {get; set;}
  public String ProdCode {get; set;}
  public String IsSaving {get; set;}
  public String IsChecking {get; set;}
  public List<TF4SF__Disclosure__c> discList {get; set;}
  public Map<String, List<Attachment>> AttMap {get; set;}
  public List<Attachment> attList {get; set;} 

  global void onload(){
     
  }
    
  global PrintDisclosuresController() { 
       IsSaving='none';
       IsChecking='none';
       AttMap = new Map<String, List<Attachment>>();
       SiteUrl= TF4SF__Application_Configuration__c.getOrgDefaults().TF4SF__Instance_URL__c;
       ProdCode=ApexPages.currentPage().getParameters().get('PC');
       
  	   discList = new List<TF4SF__Disclosure__c>();
  	   List<TF4SF__Disclosure_Names__c> dnList = [SELECT Id, TF4SF__Disclosure_Label__c, Product_Code__c FROM TF4SF__Disclosure_Names__c WHERE Product_Code__c =: ProdCode];
  	   List<String> discName = new List<String>();
  	   for (TF4SF__Disclosure_Names__c dn : dnList) {
  	   		discName.add(dn.TF4SF__Disclosure_Label__c);	
  	   }
  	   if (discName.size() > 0) {
  	   	attList = new List<Attachment>();
	  	   for (String dName : discName) {
	  	   		discList = [SELECT Id, Name, (SELECT Id FROM ATTACHMENTS ORDER by CreatedDate DESC LIMIT 1) FROM TF4SF__Disclosure__c WHERE Name =: dName];
	  	   		attList = discList[0].ATTACHMENTS;
	  	   		AttMap.put(dName, attList);
	  	   }
  	   		//discList = [SELECT Id, Name, (SELECT Id FROM ATTACHMENTS ORDER by CreatedDate DESC LIMIT 1) FROM TF4SF__Disclosure__c WHERE Name IN : discName];
  	   }
       
       if(ProdCode=='Savings')
          IsSaving='block';
          else
          IsChecking='block'; 

    }   
}