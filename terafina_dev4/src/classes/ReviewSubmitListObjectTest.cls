@isTest
public class ReviewSubmitListObjectTest{

    @isTest public static void method_one(){

    Map<String, String> data= new Map<String, String>();
    Map<String, String> tdata= new Map<String, String>();
     
        
    List<TF4SF__Application__c> lstApp=new List<TF4SF__Application__c>();
        
    TF4SF__Application__c app1 = new TF4SF__Application__c(TF4SF__First_Name__c='firstname',TF4SF__Product__c = 'Checking', 
    TF4SF__Sub_Product__c = 'Checking - Checking',TF4SF__First_Joint_Applicant__c=true,TF4SF__Second_Joint_Applicant__c=true,TF4SF__Third_Joint_Applicant__c =true);
    
   /* TF4SF__Application__c app2 = new TF4SF__Application__c(TF4SF__First_Name__c='firstname',TF4SF__Product__c = 'Checking', 
    TF4SF__Sub_Product__c = 'Checking - Checking',TF4SF__First_Joint_Applicant__c=true,TF4SF__Second_Joint_Applicant__c=true,TF4SF__Third_Joint_Applicant__c =true);
    insert app2;*/
    
    insert app1;     
    lstApp.add(app1);
   
      Trustee_Guarantor__c objTG = new Trustee_Guarantor__c(Applicant_Type__c='J1',Application__c=app1.id);
        insert objTG;
        
        List<TF4SF__Debug_Logs__c> debugList = new List<TF4SF__Debug_Logs__c>();
          TF4SF__Debug_Logs__c debug = new TF4SF__Debug_Logs__c();
            debug.TF4SF__Application__c = app1.id;
            debug.TF4SF__Debug_Message__c = 'Unable to create Debug Log';
            debug.TF4SF__Source__c = 'Callout';
            debug.TF4SF__Timestamp__c = String.valueOf(System.now());
            debugList.add(debug);
            insert debugList;
        
    DebugLogger.InsertDebugLog(app1.id,app1.Id,'ReviewSubmitListObject main called');
    tdata.put('Id',(String)app1.id);
    tdata.put('objectList','Trustee_Guarantor__c');
    
    test.startTest();
    //Test.setMock(HttpCalloutMock.class, new ReviewSubmitListObjectMock());
    ReviewSubmitListObject ldelete=new ReviewSubmitListObject();
    ReviewSubmitListObject.getData(app1.id, lstApp,'Trustee_Guarantor__c');    
    ReviewSubmitListObject.main(tdata);
    test.stopTest();

}
@isTest public static void method_two(){

    Map<String, String> data= new Map<String, String>();
    Map<String, String> tdata= new Map<String, String>();
     
        
    List<TF4SF__Application__c> lstApp=new List<TF4SF__Application__c>();
        
    TF4SF__Application__c app1 = new TF4SF__Application__c(TF4SF__First_Name__c='firstname',TF4SF__Product__c = 'Checking', 
    TF4SF__Sub_Product__c = 'Checking - Checking',TF4SF__First_Joint_Applicant__c=true,TF4SF__Second_Joint_Applicant__c=true,TF4SF__Third_Joint_Applicant__c =true);
    
   /* TF4SF__Application__c app2 = new TF4SF__Application__c(TF4SF__First_Name__c='firstname',TF4SF__Product__c = 'Checking', 
    TF4SF__Sub_Product__c = 'Checking - Checking',TF4SF__First_Joint_Applicant__c=true,TF4SF__Second_Joint_Applicant__c=true,TF4SF__Third_Joint_Applicant__c =true);
    insert app2;*/
    
    insert app1;     
    lstApp.add(app1);
   
      Trustee_Guarantor__c objTG = new Trustee_Guarantor__c(Applicant_Type__c='J2',Application__c=app1.id);
        insert objTG;
        
        List<TF4SF__Debug_Logs__c> debugList = new List<TF4SF__Debug_Logs__c>();
          TF4SF__Debug_Logs__c debug = new TF4SF__Debug_Logs__c();
            debug.TF4SF__Application__c = app1.id;
            debug.TF4SF__Debug_Message__c = 'Unable to create Debug Log';
            debug.TF4SF__Source__c = 'Callout';
            debug.TF4SF__Timestamp__c = String.valueOf(System.now());
            debugList.add(debug);
            insert debugList;
        
    DebugLogger.InsertDebugLog(app1.id,app1.Id,'ReviewSubmitListObject main called');
    tdata.put('Id',(String)app1.id);
    tdata.put('objectList','Trustee_Guarantor__c');
    
    test.startTest();
    //Test.setMock(HttpCalloutMock.class, new ReviewSubmitListObjectMock());
    ReviewSubmitListObject ldelete=new ReviewSubmitListObject();
    ReviewSubmitListObject.getData(app1.id, lstApp,'Trustee_Guarantor__c');    
    ReviewSubmitListObject.main(tdata);
    test.stopTest();

}
    @isTest public static void method_three(){

    Map<String, String> data= new Map<String, String>();
    Map<String, String> tdata= new Map<String, String>();
     
        
    List<TF4SF__Application__c> lstApp=new List<TF4SF__Application__c>();
        
    TF4SF__Application__c app1 = new TF4SF__Application__c(TF4SF__First_Name__c='firstname',TF4SF__Product__c = 'Checking', 
    TF4SF__Sub_Product__c = 'Checking - Checking',TF4SF__First_Joint_Applicant__c=true,TF4SF__Second_Joint_Applicant__c=true,TF4SF__Third_Joint_Applicant__c =true);
    
   /* TF4SF__Application__c app2 = new TF4SF__Application__c(TF4SF__First_Name__c='firstname',TF4SF__Product__c = 'Checking', 
    TF4SF__Sub_Product__c = 'Checking - Checking',TF4SF__First_Joint_Applicant__c=true,TF4SF__Second_Joint_Applicant__c=true,TF4SF__Third_Joint_Applicant__c =true);
    insert app2;*/
    
    insert app1;     
    lstApp.add(app1);
   
      Trustee_Guarantor__c objTG = new Trustee_Guarantor__c(Applicant_Type__c='J3',Application__c=app1.id);
        insert objTG;
        
        List<TF4SF__Debug_Logs__c> debugList = new List<TF4SF__Debug_Logs__c>();
          TF4SF__Debug_Logs__c debug = new TF4SF__Debug_Logs__c();
            debug.TF4SF__Application__c = app1.id;
            debug.TF4SF__Debug_Message__c = 'Unable to create Debug Log';
            debug.TF4SF__Source__c = 'Callout';
            debug.TF4SF__Timestamp__c = String.valueOf(System.now());
            debugList.add(debug);
            insert debugList;
        
    DebugLogger.InsertDebugLog(app1.id,app1.Id,'ReviewSubmitListObject main called');
    tdata.put('Id',(String)app1.id);
    tdata.put('objectList','Trustee_Guarantor__c');
    
    test.startTest();
    //Test.setMock(HttpCalloutMock.class, new ReviewSubmitListObjectMock());
    ReviewSubmitListObject ldelete=new ReviewSubmitListObject();
    ReviewSubmitListObject.getData(app1.id, lstApp,'Trustee_Guarantor__c');    
    ReviewSubmitListObject.main(tdata);
    test.stopTest();

}
}