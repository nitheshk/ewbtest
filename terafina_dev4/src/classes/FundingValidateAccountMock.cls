@isTest
global class FundingValidateAccountMock implements HttpCalloutMock {
    
    	// Implement this interface method
	global HTTPResponse respond(HTTPRequest req) {
		// Optionally, only send a mock response for a specific endpoint
		// and method.
		// Create a fake response
		HttpResponse res = new HttpResponse();
		if (req.getEndpoint().endsWith('jwt')) {
			res.setHeader('Content-Type', 'application/json');
	        res.setBody('{"access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IkFkYjBMTVdKUHhuRzNOb2EwTk5STk52UVYxdyJ9.eyJ1bmlxdWVfbmFtZSI6IjhlcjJjeDVkMzdoOTZHdEprOWI5Iiwicm9sZSI6InBhcnRuZXIiLCJpc3MiOiJodHRwOi8vYWRmcy5lYXN0d2VzdGJhbmsuY29tL2FkZnMvc2VydmljZXMvdHJ1c3QiLCJhdWQiOiJodHRwczovL29wZW5hcGlzZXJ2aWNlcy5lYXN0d2VzdGJhbmsuY29tL2FwaSIsImV4cCI6MTUzNDcyMDAxNSwibmJmIjoxNTMxMTIwMDE1fQ.Wt8BAvli5eXXZVEfiHYQoQT37FTTfVopkd-8oGb5iGTshlGH1S90CYPebcEXRzI-U3LZ5K9F2UMrv8x-sOekcqsCBh_p44naoVi1Tf1Pewab1uET-zk_ekTEW9ic8mwxyWih8mnIwNy1xmDM5LWpqg6O6Kusu5b_ki2yr84VMYntSFYGD5iVPktSeIHmsR95VygdxoDVJyIueZuBYNVH2Fis-w9Vilg3g4bUrBSmx4mgAL39wiCV7Nx44eag_VikMuJgjJs_x-q3uTOJH1eq2RqStY6ar635lUgIAHecsyAZwdJxqq9lQ1mQ3CiwCV4B3wOFqmOXJr90id6CgvzplA","token_type":"bearer","expires_in":3599999,"refresh_token":null}');
		} else if (req.getEndpoint().endsWith('verifyAccount')) {
			res.setHeader('Content-Type', 'application/json');
	        res.setBody('{"data":{"VerifyAccRealTime":{"body":{"VerifyAccountRealTimeResponse":{"return":{"RqUID":"VerifyAccountRealTimREQUEST","Status":{"StatusCode":"0","Severity":"Info","StatusDesc":"Success","StatusDetail":{"RequestStatus":null,"Severity":null,"MessageData":null}},"RealTimeAccountVerificationInfoStatusList":{"RealTimeAccountVerificationInfoStatus":[{"Status":{"StatusCode":"0","Severity":"Info","StatusDesc":"Success","StatusDetail":{"RequestStatus":null,"Severity":null,"MessageData":null}},"CFIID":"13356754","AccountNumber":"2121312QWAQ","AccountTypeId":"177816","FIID":"110500","ABA":"999999994","CreditABA":"999999994","VerificationMode":"online","VerificationStatus":"Pending Approval","FailureReasonCode":null,"FailureReasonDesc":null,"AccountTypeCode":null,"RunId":null,"TwoFARealtimeAcctVerificationParam":{"Param":null},"BatchId":null,"FailureCategory":"-1","MatchType":null}]}}}}}},"result":{"success":true,"code":"200","message":"Success"}}');
		} else if (req.getEndpoint().endsWith('validateAccount')) {
			res.setHeader('Content-Type', 'application/json');
	        res.setBody('{"data":{"AddHost":{"body":{"AddHostAccountsResponse":{"return":{"RqUID":"9d0505cd-4dcd-423c-adad-d229911bbe4b","Status":{"StatusCode":"0","Severity":"Info","StatusDesc":"Success","StatusDetail":{"RequestStatus":null,"Severity":null,"MessageData":null}},"HostAccountVerificationInfoStatusList":{"HostAccountVerificationInfoStatus":[{"Status":{"StatusCode":"0","Severity":"Info","StatusDesc":"Success","StatusDetail":{"RequestStatus":null,"Severity":null,"MessageData":null}},"CFIID":"13356755","AccountNumber":"EWBHA2110","AccountTypeId":"40599","FIID":"14596","ABA":"322070381","CreditABA":null,"VerificationMode":"partner-approved","VerificationStatus":"Approved","FailureReasonCode":null,"FailureReasonDesc":null}]}}}}},"AddUserProduct":{"body":{"AddUserProductResponse":{"return":{"RqUID":"9d0505cd-4dcd-423c-adad-d229911bbe4b","Status":{"StatusCode":"0","Severity":"Info","StatusDesc":"Success","StatusDetail":{"RequestStatus":null,"Severity":null,"MessageData":null}}}}}}},"result":{"success":true,"code":"200","message":"Success"}}');
    	}
		res.setStatusCode(200);
		return res;
	}
}