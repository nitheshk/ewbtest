@isTest
public class Geolocation_Mock implements HttpCalloutMock {

  public HTTPResponse respond(HTTPRequest req) {
    HttpResponse res = new HttpResponse();
    res.setBody('{"__deprecation_message__":"This API endpoint is deprecated and will stop working on July 1st, 2018. For more information please visit: https://github.com/apilayer/freegeoip#readme","ip":"1.186.48.106","country_code":"IN","country_name":"India","region_code":"KA","region_name":"Karnataka","city":"Mangalore","zip_code":"575001","time_zone":"Asia/Kolkata","latitude":12.8639,"longitude":74.8353,"metro_code":0}');
    res.setStatusCode(200);
    return res;
  }
}