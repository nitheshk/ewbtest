@isTest
global class ValidatePromoCodeExtension_Mock implements HttpCalloutMock {

  // Implement this interface method
  global HTTPResponse respond(HTTPRequest req) {
    // Optionally, only send a mock response for a specific endpoint
    // and method.
    // Create a fake response
    HttpResponse res = new HttpResponse();
    res.setHeader('Content-Type', 'application/json');
    res.setBody('{ "data": { "AddHost": { "body": { "AddHostAccountsResponse": { "return": { "RqUID": "9d0505cd-4dcd-423c-adad-d229911bbe4b", "Status": { "StatusCode": "0", "Severity": "Info", "StatusDesc": "Success", "StatusDetail": { "RequestStatus": null, "Severity": null, "MessageData": null } }, "HostAccountVerificationInfoStatusList": { "HostAccountVerificationInfoStatus": [ { "Status": { "StatusCode": "4530", "Severity": "Error", "StatusDesc": "Add account error", "StatusDetail": { "RequestStatus": "False", "Severity": "Error", "MessageData": [ { "MessageCode": "FTE01", "MessgaeDesc": "You have already added this account. If you have more than one account at this institution, you may want to double-check the account number and other information requested.", "MessageSeverity": null }, { "MessageCode": "4454", "MessgaeDesc": "Partner Account Verification Processing Error", "MessageSeverity": null } ] } }, "CFIID": "13305053", "AccountNumber": "EWBHA2121312", "AccountTypeId": "40599", "FIID": "14596", "ABA": "322070381", "CreditABA": null, "VerificationMode": "partner-approved", "VerificationStatus": "Approved", "FailureReasonCode": null, "FailureReasonDesc": null } ] } } } } }, "AddUserProduct": { "body": { "AddUserProductResponse": { "return": { "RqUID": "9d0505cd-4dcd-423c-adad-d229911bbe4b", "Status": { "StatusCode": "100", "Severity": "Error", "StatusDesc": "General error", "StatusDetail": { "RequestStatus": null, "Severity": null, "MessageData": null } } } } } } }, "result": { "success": true, "code": "200", "message": "Success" } }');
    res.setStatusCode(200);
    return res;
}
}