@isTest 
public class MortgageShortAppController_Test { 
        @isTest static void test_method_one() {
       
        TF4SF__Application__c app = new TF4SF__Application__c(TF4SF__Custom_Checkbox2__c = false,TF4SF__Custom_Checkbox3__c = true,
        TF4SF__Custom_Checkbox4__c = true,TF4SF__Custom_Text3__c ='800',TF4SF__Custom_Text4__c='800',
        TF4SF__Months_J__c = '3',TF4SF__Custom_Text5__c = '800',TF4SF__Months_J2__c = '3',TF4SF__Custom_Text6__c = '800',TF4SF__Months_J3__c ='3',
        TF4SF__User_Token_Expires__c=System.now().addMinutes(100),TF4SF__User_Token__c= 'AnxzH/+Kmj85t2Wzkj5qETFWtwltJXsC5dcjGEgtGa89TTX4OIYGRky9UDclje65',
        Abandoned_Email__c= false,TF4SF__Product__c='Home Loan',TF4SF__Application_Status__c = 'Submitted',TF4SF__Sub_Product__c='checking',
        
        TF4SF__First_Name__c = 'TestFirst',TF4SF__Last_Name__c = 'TestLast', TF4SF__Email_Address__c = 'test@test.com',
        TF4SF__City__c = 'There',TF4SF__Primary_Phone_Number__c = '8889557212',TF4SF__State__c = 'AZ',TF4SF__Street_Address_1__c = '123 That Street', 
        TF4SF__Street_Address_2__c = '',TF4SF__Zip_Code__c = '89898',TF4SF__Months__c = '5', TF4SF__Middle_Name__c = 'TestMiddle',
        TF4SF__Housing_Status__c = 'Own', TF4SF__Monthly_Payment__c = 3000.00,TF4SF__Years__c = 2.0,TF4SF__Secondary_Phone_Number__c = '8885551313');
        insert app;
        
        TF4SF__About_Account__c obj1 = new TF4SF__About_Account__c();
        obj1.TF4SF__Application__c=app.id;
        obj1.TF4SF__Property_Type__c='testhome';
        obj1.TF4SF__Occupancy__c='testengg';
        obj1.TF4SF__Purchase_P__c=23.7;
        obj1.TF4SF__Total_Loan_Amount__c=55.6;
        obj1.TF4SF__Down_Payment__c=654.55;
        insert obj1;
        
        TF4SF__Application_Configuration__c custSetting = new TF4SF__Application_Configuration__c();
        
         PageReference p = Page.DSPResumeApplication;    
         Test.setCurrentPage(p);
            ApexPages.currentPage().getParameters().put('id', app.Id);
            ApexPages.currentPage().getParameters().put('ut', 'AnxzH/+Kmj85t2Wzkj5qETFWtwltJXsC5dcjGEgtGa89TTX4OIYGRky9UDclje65');    
            
        Test.startTest();
        MortgageShortAppController.ApplicationWrapper msac = new MortgageShortAppController.ApplicationWrapper();
        String jsonDataGet = '{"id":"'+ app.Id +'","ut":"AnxzH/+Kmj85t2Wzkj5qETFWtwltJXsC5dcjGEgtGa89TTX4OIYGRky9UDclje65","pageType":"budget","propertyType":"","occupancy":"","purchasePrice":"","loanAmount":"","downPayment":"","firstName":"sdss","lastName":"adad","email":"a@a.com","phoneNumber":"(231) 213-1231"}';
        String jsondata = '{"id":"'+ app.Id +'","ut":"AnxzH/+Kmj85t2Wzkj5qETFWtwltJXsC5dcjGEgtGa89TTX4OIYGRky9UDclje65","pageType":"budget","propertyType":"Land","occupancy":"Primary","purchasePrice":"100","loanAmount":"12","downPayment":"80", "firstName":"tets","lastName":"taet","email":"test@tets.vom","phoneNumber":"9902189392"';
        MortgageShortAppController.getData(jsonDataGet);
        MortgageShortAppController.saveData(jsonDataGet); 
        MortgageShortAppController.expireSession(jsonDataGet);
        MortgageShortAppController.isValidZipcode('43534');
        //MortgageShortAppController.extendExpiration(jsonDataGet);
        Test.stopTest();
    }
}