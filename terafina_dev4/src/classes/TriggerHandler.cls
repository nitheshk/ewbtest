public with sharing class TriggerHandler {
    public TriggerHandler() {}
   
 public void sendSaveForLaterEmailForShares(List<TF4SF__Application__c> triggerNew, Map<Id, TF4SF__Application__c> oldMap) {
        //List<Contact> delContact = new List<Contact>();
        try {
            if (triggerNew.size() > 0 && oldMap.size() > 0) {
                List<Contact> conList = new List<Contact>();
                Set<Id> appIdSet = new Set<Id>();
                List<String> emailAddressList = new List<String>();
                List<Messaging.SingleEmailMessage> mailsList =  new List<Messaging.SingleEmailMessage>(); 
                Contact con = new Contact();
                
                System.debug('Getting email list'+mailsList);                            
                                                 
                        for (TF4SF__Application__c app : triggerNew) {
                        
                            //Check if Member did not provide an email address is Not TRUE
                            if (app.TF4SF__Email_Address__c != null) {
                            //if (app.TF4SF__Email_Address__c != null && app.Abandoned_Email__c==false) { 
                                  System.debug('email add'+mailsList);
                                  
                                  System.debug('app.TF4SF__Application_Status__c '+app.TF4SF__Application_Status__c);
                                  System.debug('oldMap.get(app.Id).TF4SF__Application_Status__c '+oldMap.get(app.Id).TF4SF__Application_Status__c);
                                  
                                //Check if the Product is only Shares
                                if (app.TF4SF__Application_Status__c != oldMap.get(app.Id).TF4SF__Application_Status__c && app.TF4SF__Application_Status__c == 'Abandoned' || Test.isRunningTest()) {
                                    
                                    System.debug('send email'+mailsList);
                                   
                                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                                    EmailTemplate eTemplate;
                                    List<EmailTemplate> etList;                           
                                    
                                    etList = [SELECT Id, Name, DeveloperName, Subject, Body, HtmlValue, Markup FROM EmailTemplate WHERE DeveloperName = 'AbandonEmail' LIMIT 1];
                                    
                               
                                    System.debug('email templates'+etList);
                                    
                                    if (etList.size() > 0) { eTemplate = etList[0]; }
                                    emailAddressList.add(app.TF4SF__Email_Address__c);
                                    
                                    
                                       if (Schema.sObjectType.Contact.fields.lastName.isCreateable())
                                       { con.lastName = String.isBlank(app.TF4SF__Last_Name__c) ? 'Applicant' : app.TF4SF__Last_Name__c; }
                                    if (Schema.sObjectType.Contact.fields.Email.isCreateable()) {
                                    
                                     con.Email =  app.TF4SF__Email_Address__c ; }
                                     
                                    //con.Email = String.isBlank(app.TF4SF__Email_Address__c) ? 'test@test.com' : app.TF4SF__Email_Address__c;
                                    if (Contact.sObjectType.getDescribe().isCreateable()) { 
                                        upsert con; 
                                        conList.add(con);
                                    
                                    }
                                     mail.setSaveAsActivity(false);  
                                          for(OrgWideEmailAddress owa : [select id, Address, DisplayName from OrgWideEmailAddress]) 
                                          {
                                           if(owa.DisplayName.contains('System Admin'))
                                           { 
                                            mail.setOrgWideEmailAddressId(owa.id); 
                                           } 
                                          }
                                   
                                   

                                    if (eTemplate != null) { 
                                                                      
                                        mail.setReplyTo('no-reply@eastwest.org');
                                        mail.setSaveAsActivity(false);
                                        mail.setBccSender(false);
                                        mail.setCcAddresses(null);
                                        mail.setTemplateID(eTemplate.Id);
                                        mail.setWhatId(app.Id);                                       
                                       
                                        
                                        mail.settoAddresses(emailAddressList);                                       
                                        mail.settargetObjectId(con.id);                                       
                                       
                                        mailsList.add(mail);  
                                        app.Abandoned_Email__c=true;                                        
                                        
                                    }
                                
                            
                        }
                    }

                    if (mailsList.size() > 0 && !mailsList.isEmpty() && CheckRecursive.looprun) {
                        try {
                            Messaging.sendEmail(mailsList);
                            CheckRecursive.looprun = false;
                           
                            //if (con != null && Contact.sObjectType.getDescribe().isDeletable()) { delete con; }                         
                        } catch (Exception e) {
                            System.debug('Exception occured in TriggerHandler class sendSaveForLaterEmailForShares method with error msg :::: ' + e.getMessage() + ' &&&&& at line ##### ' + e.getLineNumber());
                     
                        }
                    }
                    if(conList.size() > 0) {
                        if(Contact.sObjectType.getDescribe().isDeletable()) {delete conList;}
                    }
               
            }
            }
        }catch (Exception ex) {
            System.debug('Exception occured in TriggerHandler class sendSaveForLaterEmailForShares method with error msg :::: ' + ex.getMessage() + ' &&&&& at line ##### ' + ex.getLineNumber());
         
        }       
        
        
 }


    public void LeadScoring(List<TF4SF__Application__c> appList) {
        for (TF4SF__Application__c app : appList) {
            try{
                TF4SF__Product_Codes__c pcList = [SELECT ID, TF4SF__Product__c, TF4SF__Sub_Product__c, Lead_Score_Completed__c, Lead_Score_Employment__c, Lead_Score_Identity__c, Lead_Score_Review__c, Lead_Score_Started__c FROM TF4SF__Product_Codes__c WHERE TF4SF__Sub_Product__c =: app.TF4SF__Sub_Product__c LIMIT 1];
                if (app.TF4SF__Application_Page__c == 'GetStartedPage' || app.TF4SF__Application_Page__c == 'CrossSellPage' || app.TF4SF__Application_Page__c == 'PersonalInfoPage') {
                    if (pcList.Lead_Score_Started__c != null) {
                        app.Lead_Score__c = pcList.Lead_Score_Started__c;
                    }
                } else if (app.TF4SF__Application_Page__c == 'EmploymentPage' || app.TF4SF__Application_Page__c == 'IdentityPage') {
                    if (pcList.Lead_Score_Employment__c != null) {
                        app.Lead_Score__c = pcList.Lead_Score_Employment__c;
                    }
                } else if (app.TF4SF__Application_Page__c == 'AccountDetailsPage' || app.TF4SF__Application_Page__c == 'PurchaseDetailsPage' || app.TF4SF__Application_Page__c == 'PropertyDetailsPage' || app.TF4SF__Application_Page__c == 'DeclerationPage' || app.TF4SF__Application_Page__c == 'MortgagedetailsPage') {
                     if (pcList.Lead_Score_Identity__c != null) {
                         app.Lead_Score__c = pcList.Lead_Score_Identity__c;
                     }
                } else if (app.TF4SF__Application_Page__c == 'ReviewSubmitPage') {
                     if (pcList.Lead_Score_Review__c != null) {
                         app.Lead_Score__c = pcList.Lead_Score_Review__c;
                     }
                } else if (app.TF4SF__Application_Page__c == 'ConfirmationPage') {
                     if (pcList.Lead_Score_Completed__c != null) {
                         app.Lead_Score__c = pcList.Lead_Score_Completed__c;
                     }
                }
                
            } catch(Exception e) {
                
            }
        }
    }

    public void LeadScoreQueueAssignement(List<TF4SF__Application__c> appList) {
        Group RejectedQueue = [SELECT Id FROM Group WHERE TYPE = 'Queue' AND DeveloperName = 'Rejected_Leads'];
        Group MortgageQueue = [SELECT Id FROM Group WHERE TYPE = 'Queue' AND DeveloperName = 'Mortgage_Leads'];
        Group BusinessQueue = [SELECT Id FROM Group WHERE TYPE = 'Queue' AND DeveloperName = 'Business_Leads'];
        Group AllOtherQueue = [SELECT Id, (SELECT Id, UserOrGroupId FROM GroupMembers) FROM Group WHERE TYPE = 'Queue' AND DeveloperName = 'SCCA_Leads'];
        List<String> mailToAddresses = new List<String>();
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        List<GroupMember> GroupMem = AllOtherQueue.GroupMembers;
        List<Id> UserList = new List<Id>();
        Map<Id, Integer> appcountMap = new Map<Id, Integer>();
        for (GroupMember gm : GroupMem) {
            UserList.add(gm.UserOrGroupId);
        }
        User[] usr = [SELECT Name, Email FROM User WHERE Id IN: UserList];
        for (User u : usr) { mailToAddresses.add(u.Email); }
        for (TF4SF__Application__c app : appList) {
            if (app.TF4SF__Application_Status__c != 'Submitted') {
                try {
                    TF4SF__Product_Codes__c pc = [SELECT ID, TF4SF__Product__c, TF4SF__Sub_Product__c, Lead_Score_Completed__c, Lead_Score_Employment__c, Lead_Score_Identity__c, Lead_Score_Review__c, Lead_Score_Started__c FROM TF4SF__Product_Codes__c WHERE TF4SF__Sub_Product__c =: app.TF4SF__Sub_Product__c LIMIT 1];
                    if (app.TF4SF__Product__c != NULL && app.Lead_Score__c != NULL) {
                        if (app.TF4SF__Product__c == 'Home Loan') {
                            //if (app.Lead_Score__c == pc.Lead_Score_Started__c) {
                                //app.OwnerId = RejectedQueue.Id;
                            //} else {
                                //app.OwnerId = MortgageQueue.Id;
                                app.OwnerId = '0056A000001Mx0O';
                                MortgageEmail me = new MortgageEmail();
                                me.sendEmail(app);
                            //}
                        } else if (app.TF4SF__Product__c == 'Home Equity') { 
                            if (app.Lead_Score__c == pc.Lead_Score_Started__c) {    
                                app.OwnerId = RejectedQueue.Id;
                            } else {
                                app.OwnerId = MortgageQueue.Id;
                            }
                        } else if (app.TF4SF__Product__c == 'Business Checking' || app.TF4SF__Product__c == 'Business Savings' || app.TF4SF__Product__c == 'Business CDs') {
                            if (app.Lead_Score__c == pc.Lead_Score_Started__c) {   
                                app.OwnerId = RejectedQueue.Id;
                            } else {
                                app.OwnerId = BusinessQueue.Id;
                            }
                        } else if (app.TF4SF__Product__c == 'Vehicle Loans') {
                            if (app.Lead_Score__c == pc.Lead_Score_Started__c || app.Lead_Score__c == pc.Lead_Score_Employment__c) {   
                                app.OwnerId = RejectedQueue.Id;
                            } else {
                                app.OwnerId = AllOtherQueue.Id;
                            }
                        } else if (app.TF4SF__Product__c == 'Personal Loans') { 
                            if (app.Lead_Score__c == pc.Lead_Score_Started__c || app.Lead_Score__c == pc.Lead_Score_Employment__c) {   
                                app.OwnerId = RejectedQueue.Id;
                            } else {
                                app.OwnerId = AllOtherQueue.Id;
                            }
                        } else if (app.TF4SF__Product__c == 'Credit Cards') {
                            if (app.Lead_Score__c == pc.Lead_Score_Started__c || app.Lead_Score__c == pc.Lead_Score_Employment__c) {
                                app.OwnerId = RejectedQueue.Id;
                            } else {
                                app.OwnerId = AllOtherQueue.Id;
                            }
                        } else if (app.TF4SF__Product__c == 'Checking') {
                            if (app.Lead_Score__c == pc.Lead_Score_Started__c || app.Lead_Score__c == pc.Lead_Score_Employment__c || app.Lead_Score__c == pc.Lead_Score_Identity__c) {
                                app.OwnerId = RejectedQueue.Id;
                            } else {
                                app.OwnerId = AllOtherQueue.Id;
                            }
                        } else if (app.TF4SF__Product__c == 'Savings') {
                            if (app.Lead_Score__c == pc.Lead_Score_Started__c || app.Lead_Score__c == pc.Lead_Score_Employment__c || app.Lead_Score__c == pc.Lead_Score_Identity__c) {
                                app.OwnerId = RejectedQueue.Id;
                            } else {
                                app.OwnerId = AllOtherQueue.Id;
                            }
                        } else if (app.TF4SF__Product__c == 'Certificates') {
                            if (app.Lead_Score__c == pc.Lead_Score_Started__c || app.Lead_Score__c == pc.Lead_Score_Employment__c || app.Lead_Score__c == pc.Lead_Score_Identity__c) {
                                app.OwnerId = RejectedQueue.Id;
                            } else {
                                app.OwnerId = AllOtherQueue.Id;
                            }
                        } else if (app.TF4SF__Product__c == 'Business Credit Cards' || app.TF4SF__Product__c == 'Business Loans') {
                            if (app.Lead_Score__c == pc.Lead_Score_Started__c || app.Lead_Score__c == pc.Lead_Score_Employment__c || app.Lead_Score__c == pc.Lead_Score_Identity__c) {
                                app.OwnerId = RejectedQueue.Id;
                            } else {
                                app.OwnerId = AllOtherQueue.Id;
                            }
                        }

                        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                        mail.setToAddresses(mailToAddresses);
                        mail.setReplyTo('Support@terafinainc.com');
                        mail.setSenderDisplayName('Support');

                        mail.setSubject('Application waiting for review in the queue');
                        String body = 'Team, \n\n';
                        body += 'Please review the application in the queue <a href=\'https://tfdemo2-dev-ed.my.salesforce.com/'+app.Id+'\'>Click Here</a>';

                        mail.setHtmlBody(body);
                        mails.add(mail);
        
                    }
                } catch (Exception e) {
                    System.debug('Execption in validateOTP ==>' + e.getMessage());
            System.debug('Execption in validateOTP Line number ==>' + e.getLineNumber());

                }
            } else {
               //app.OwnerId = app.TF4SF__Current_Person__c; 
            }
        }
        try{ if (mails.size() > 0) {
            Messaging.sendEmail(mails);
        }
        }catch(Exception e){}
    } 
    
    public void LeadScoreUserAssignement(List<TF4SF__Application__c> appList) {
        try {
            String GroupName;
            List<Task> taskList = new List<Task>();
            List<String> mailToAddresses = new List<String>();
            List<String> AppIdList = new List<String>(); 
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
            for (TF4SF__Application__c app : appList) {
                if (app.TF4SF__Product__c == 'Business Checking' || app.TF4SF__Product__c == 'Business Savings' || app.TF4SF__Product__c == 'Business CDs') {
                    GroupName = 'Business_Leads';
                }
                // else if (app.TF4SF__Product__c == 'Home Loan') {
                //    GroupName = 'Mortgage_Leads';
                //}
                AppIdList.add(app.Id);
            }
            if (String.isNotBlank(GroupName)) {
                Group AllOtherQueue = [SELECT Id, DeveloperName, (SELECT Id, UserOrGroupId FROM GroupMembers) FROM Group WHERE TYPE = 'Queue' AND DeveloperName =: groupName];
                List<GroupMember> GroupMem = AllOtherQueue.GroupMembers;
                List<Id> UserList = new List<Id>();
                Map<Id, Integer> appcountMap = new Map<Id, Integer>();
                for (GroupMember gm : GroupMem) {
                 UserList.add(gm.UserOrGroupId);
                }

                List<TF4SF__Application__c> allSCCAApps = [SELECT Id, OwnerId FROM TF4SF__Application__c WHERE OwnerId IN: UserList];
                if (allSCCAApps.size() > 0) {
                    for (TF4SF__Application__c apps : allSCCAApps) {
                        if(!appcountMap.containsKey(apps.OwnerId)) {
                            appcountMap.put(apps.OwnerId, 1);
                        } else {
                            appcountMap.put(apps.OwnerId, appcountMap.get(apps.OwnerId)+1);
                        }
                    }
                    if (appcountMap.keySet().size() > 0) {
                        List<Id> oidList = new List<Id>();
                        String lowCtOwn;
                        oidlist.addAll(appcountMap.keyset());
                        if (oidlist.size() > 0) {
                            Integer p = appcountMap.get(oidList[0]);
                            Integer q = 0;
                            for (String i : oidList) {
                                q = appcountMap.get(i);
                                if (p > q) {
                                    p = appcountMap.get(i);
                                    lowCtOwn = i;
                                }
                            }    
                        }
                        if (String.isNotBlank(lowCtOwn)) {
                            User usr = [SELECT Id, Name, Email FROM User WHERE Id =:lowCtOwn];
                            mailToAddresses.add(usr.Email);
                            List<Task> tList = [SELECT Id, WhatId, Subject FROM Task WHERE WhatId IN: AppIdList];
                            Map<String, Boolean> tMap = new Map<String, Boolean>();
                            for (Task t : tList) {
                                if (t.Subject == 'An new application is assigned to you.') {
                                    tMap.put(t.WhatId, true);
                                } else {
                                    tMap.put(t.WhatId, false);
                                }
                            }
                            for (TF4SF__Application__c app : appList) {
                                try {
                                    if (app.TF4SF__Application_Status__c != 'Submitted') {
                                        app.OwnerId = lowCtOwn;
                                        if ((tMap.containsKey(app.Id) && tMap.get(app.Id) == false) || !tMap.containsKey(app.Id)) {
                                            Task tsk = new Task(WhatId = app.Id, OwnerId = app.OwnerId, Subject = 'An new application is assigned to you.');
                                            taskList.add(tsk);    
                                            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                                            mail.setToAddresses(mailToAddresses);
                                            mail.setReplyTo('support@terafinainc.com');
                                            mail.setSenderDisplayName('Support');
                                            mail.setSubject('An new application is assigned to you');
                                            String body = 'Dear ' + usr.Name + ', \n\n';
                                            body += 'Please find the <a href=\'https://tfdemo2-dev-ed.my.salesforce.com/'+app.Id+'\'>lead<a>';

                                            mail.setHtmlBody(body);
                                            mails.add(mail);
                                            //taskList.add(updateTask(mail, app));
                                        }    
                                    } else {
                                        //app.OwnerId = app.TF4SF__Current_Person__c; 
                                    }
                                    
                                } catch (Exception e) {

                                }
                            }    
                        }
                        if (taskList.size() > 0) {
                            if (Task.sObjectType.getDescribe().isCreateable()) {insert taskList;}
                        }
                        if (mails.size() > 0) {
                            Messaging.sendEmail(mails);
                        }
                    }
                }
            }
        } catch (Exception e) {

        }
    }

    public Task updateTask(Messaging.SingleEmailMessage email, TF4SF__Application__c app ) {

        // creating a a task for the email that is sent out
        String mailTextBody = email.getPlainTextBody();
        String mailHtmlBody = email.getHTMLBody();
        String mailSubject = email.getSubject();

        String strippedHtmlBody;
        //checking if the mailHtmlBody contains startbody and endbody div tags
        if (mailHtmlBody.contains('<div id="startBody"></div>') && mailHtmlBody.contains('<div id="endBody"></div>')) {
            String str = mailHtmlBody.substringBetween('<div id="startBody"></div>','<div id="endBody"></div>');
            strippedHtmlBody = str.stripHtmlTags();
        } else {
            strippedHtmlBody = mailHtmlBody.stripHtmlTags();
        }
        //Sundar Code//Date: 08-24-2017//Discription: add Createable field condition befor update   
        Task task = new Task();
        if (Schema.sObjectType.Task.fields.WhatId.isCreateable()) {
            task.WhatId = app.Id;
        }
        if (Schema.sObjectType.Task.fields.OwnerId.isCreateable()) {
            task.OwnerId = app.OwnerId;
        }
        if (Schema.sObjectType.Task.fields.Subject.isCreateable()) {
            task.Subject = mailSubject;
        }
        //if (Schema.sObjectType.Task.fields.Description.isCreateable()) {
        //    task.Description = 'To: \r\n\r\n' +strippedHtmlBody;
        //}
        if (Schema.sObjectType.Task.fields.Priority.isCreateable()) {
            task.Priority = 'Normal';
        }
        if (Schema.sObjectType.Task.fields.Status.isCreateable()) {
            task.Status = 'Completed';
        }
        if (Schema.sObjectType.Task.fields.Type.isCreateable()) {
            task.Type = 'Email';
        }
        return task;
    }
}