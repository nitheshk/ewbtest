public class AssetsJson {

    public class PA {
        public String Id;
        public String Applicant_Type_c;
        public String Account_Balance_c;
        public String AssetsType_c;
        public String Financial_Institution_c;
        public String Address_c;
        public String Address2_c;
        public String Amount_c;
        public String City_c;                
        public String Market_Value_c;
        public String Mortgage_Amount_c;
        public String Net_Rental_Income_c;
        public String Real_Estate_Other_c;
        public String Rental_Income_c;
        public String State_c;        
        public String Type_of_Property_c;
        public String Zip_Code_c;
    }


    public class J1 {
        public String Id;
        public String Applicant_Type_c;
        public String Account_Balance_c;
        public String AssetsType_c;
        public String Financial_Institution_c;
        public String Address_c;
        public String Address2_c;
        public String Amount_c;
        public String City_c;                
        public String Market_Value_c;
        public String Mortgage_Amount_c;
        public String Net_Rental_Income_c;
        public String Real_Estate_Other_c;
        public String Rental_Income_c;
        public String State_c;        
        public String Type_of_Property_c;
        public String Zip_Code_c;
    }
    
    public class J2 {
        public String Id;
        public String Applicant_Type_c;
        public String Account_Balance_c;
        public String AssetsType_c;
        public String Financial_Institution_c;
        public String Address_c;
        public String Address2_c;
        public String Amount_c;
        public String City_c;                
        public String Market_Value_c;
        public String Mortgage_Amount_c;
        public String Net_Rental_Income_c;
        public String Real_Estate_Other_c;
        public String Rental_Income_c;
        public String State_c;        
        public String Type_of_Property_c;
        public String Zip_Code_c;
            }
    
    public class J3 {
        public String Id;
        public String Applicant_Type_c;
        public String Account_Balance_c;
        public String AssetsType_c;
        public String Financial_Institution_c;
        public String Address_c;
        public String Address2_c;
        public String Amount_c;
        public String City_c;                
        public String Market_Value_c;
        public String Mortgage_Amount_c;
        public String Net_Rental_Income_c;
        public String Real_Estate_Other_c;
        public String Rental_Income_c;
        public String State_c;        
        public String Type_of_Property_c;
        public String Zip_Code_c;
    }
    
    
    
     public PA PA;
     public J1 J1;
     public J2 J2;
     public J3 J3;

        
    public static List<AssetsJson> parse(String json) {
        return (List<AssetsJson>) System.JSON.deserialize(json, List<AssetsJson>.class);
    }
}