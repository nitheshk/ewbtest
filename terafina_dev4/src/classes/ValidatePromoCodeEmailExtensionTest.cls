@isTest
public class ValidatePromoCodeEmailExtensionTest{
     
    static testmethod void validatemyTest(){
     Map<String, String> data = new Map<String, String>();
     Map<String, String> tdata = new Map<String, String>();
        
        
        VELOSETTINGS__C vo = new VELOSETTINGS__C();
        vo.Name='test';
        vo.TokenEndPoint__c = 'rsgsdgsdf';
        vo.Request_GUID_Name__c = 'RequestUUID';
        vo.ProcessAPI_Timeout__c = 120000;
        insert vo; 
      
        List <TF4SF__Application__c>  listCurrentApp=new List <TF4SF__Application__c>();
        TF4SF__Application__c ap = new TF4SF__Application__c();
        ap.TF4SF__Product__c = 'Home Loan';
        ap.TF4SF__First_Name__c='test';
        ap.TF4SF__Last_Name__c = 'Test';
        ap.TF4SF__Application_Status__c='Open';
        ap.TF4SF__Email_Address__c= 'test@test.com';
        ap.TF4SF__Primary_Phone_Number__c='1234567890';
        ap.TF4SF__Sub_Product__c = 'Home Loan - Short App';
        ap.Promo_Code_Email__c='test@gmail.com';
        ap.Promo_Code_Phone_Number__c='435354354';
        listCurrentApp.add(ap);
        insert listCurrentApp;
    
      tdata.put('appId',(String)ap.id);
      tdata.put('EmailId','emailId');
      tdata.put('PhoneNo','PhoneNo');

  
    
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new ValidatePromoCodeEmailExtension_Mock());
        ValidatePromoCodeEmailExtension ob = new ValidatePromoCodeEmailExtension(); 
        ValidatePromoCodeEmailExtension.createShadowAccount(ap.id,'Token',ap.Promo_Code_Email__c,ap.Promo_Code_Phone_Number__c);
        ValidatePromoCodeEmailExtension.PhoneCleanup(ap.Promo_Code_Phone_Number__c);
        ValidatePromoCodeEmailExtension.main(tdata);
        test.stopTest();
    }

}