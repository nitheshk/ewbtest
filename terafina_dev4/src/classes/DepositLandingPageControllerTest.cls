@isTest
public class DepositLandingPageControllerTest{
    static testMethod void testMethod1() {
    Test.StartTest();
     TF4SF__Application_Configuration__c appConfig = new TF4SF__Application_Configuration__c();
       appConfig.TF4SF__key__c ='YMmKJt5QxS7KlAEASMsj4Q==';
       appConfig.TF4SF__Timeout_Seconds__c=900;
       appConfig.Name = 'key';
       appConfig.TF4SF__Application_Code__c='DS';
       appConfig.TF4SF__Theme__c='DSP';
       insert appConfig;
       
     TF4SF__Application__c app1 = new TF4SF__Application__c();
        app1.TF4SF__First_Name__c = 'Test Account';
        app1.TF4SF__Sub_Product__c = 'Credit';
        app1.TF4SF__Primary_Offer__c = 'Credit';
        app1.TF4SF__Second_Offer__c ='Credit';
        app1.TF4SF__Third_Offer__c ='Credit';
        insert app1;
        
     Test.setCurrentPageReference(new PageReference('Page.DepositLandingPage')); 
     System.currentPageReference().getParameters().put('id', app1.id);
     System.currentPageReference().getParameters().put('prdCode','prdCode');

     DepositLandingPageController obj = new DepositLandingPageController();
     DepositLandingPageController.setAppToken(app1);
     obj.beginApp();
     Test.StopTest();
    }
}