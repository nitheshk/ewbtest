@isTest
global class DepositPreSubmitMock implements HttpCalloutMock {
   // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IkFkYjBMTVdKUHhuRzNOb2EwTk5STk52UVYxdyJ9.eyJ1bmlxdWVfbmFtZSI6IjhlcjJjeDVkMzdoOTZHdEprOWI5Iiwicm9sZSI6InBhcnRuZXIiLCJpc3MiOiJodHRwOi8vYWRmcy5lYXN0d2VzdGJhbmsuY29tL2FkZnMvc2VydmljZXMvdHJ1c3QiLCJhdWQiOiJodHRwczovL29wZW5hcGlzZXJ2aWNlcy5lYXN0d2VzdGJhbmsuY29tL2FwaSIsImV4cCI6MTUzNDcyMDAxNSwibmJmIjoxNTMxMTIwMDE1fQ.Wt8BAvli5eXXZVEfiHYQoQT37FTTfVopkd-8oGb5iGTshlGH1S90CYPebcEXRzI-U3LZ5K9F2UMrv8x-sOekcqsCBh_p44naoVi1Tf1Pewab1uET-zk_ekTEW9ic8mwxyWih8mnIwNy1xmDM5LWpqg6O6Kusu5b_ki2yr84VMYntSFYGD5iVPktSeIHmsR95VygdxoDVJyIueZuBYNVH2Fis-w9Vilg3g4bUrBSmx4mgAL39wiCV7Nx44eag_VikMuJgjJs_x-q3uTOJH1eq2RqStY6ar635lUgIAHecsyAZwdJxqq9lQ1mQ3CiwCV4B3wOFqmOXJr90id6CgvzplA","token_type":"bearer","expires_in":3599999,"refresh_token":null}');
        res.setStatusCode(200);
        return res;
    }
}