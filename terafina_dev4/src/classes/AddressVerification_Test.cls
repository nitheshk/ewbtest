@isTest
public class AddressVerification_Test {
 static testmethod void validateStandardController(){
     
    Map<String, String> tdata = new Map<String, String>();
    Map<String, String> data = new Map<String, String>();
     
     
    TF4SF__Application__c app1 = new TF4SF__Application__c();
        app1.TF4SF__Product__c = 'Home Loan';
        app1.TF4SF__First_Name__c='test';
        app1.TF4SF__Last_Name__c = 'Test';
        app1.TF4SF__Email_Address__c= 'test@test.com';
        app1.TF4SF__Sub_Product__c = 'Home Loan - Short App';
    
    insert app1;
     TF4SF__Identity_Information__c iden = new TF4SF__Identity_Information__c();
    iden.TF4SF__Application__c = app1.Id;
    insert iden;
     
     VELOSETTINGS__C vo = new VELOSETTINGS__C();
        vo.Name='test';
        vo.TokenEndPoint__c = 'rsgsdgsdf';
        vo.Request_GUID_Name__c = 'RequestUUID';
        vo.ProcessAPI_Timeout__c = 120000;
        insert vo; 
     
     tdata.put('id', app1.Id);
     tdata.put('id', app1.Id);
     tdata.put('Street_Address_1__c', 'Street_Address_1__c');
     tdata.put('Street_Address_2__c', 'Street_Address_2__c');
     tdata.put('State__c', 'CA');
     tdata.put('City__c', 'City__c');
     tdata.put('Zip_Code__c', 'Zip_Code__c');
     tdata.put('Country__c', 'Country__c');

     
     Test.StartTest();
    Test.setMock(HttpCalloutMock.class, new AddressVerification_Mock ());
     AddressVerification.main(tdata);
     test.stopTest();
 }
}