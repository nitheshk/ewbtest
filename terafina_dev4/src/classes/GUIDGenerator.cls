/************************************************************************************
 * @Author: Nag
 * @Date : 8/16/2018
 * @Description: This is UUID generator. It will be used to send to APIs  
 * @Name: GUIDGenerator
 *************************************************************************************/
public class GUIDGenerator {
    public static String getGUID(){
        String uuidToReturn = '13219ec0-3a81-44c5-a300-de14b7d0235f';
        try{
                /*
                Blob b = Crypto.GenerateAESKey(256);
                String h = EncodingUtil.ConvertTohex(b);
                String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' 
                            + h.SubString(12,16) + '-' + h.SubString(16,20) 
                            + '-' + h.substring(20,32);
                */
                Uuid4Generator uuid4Gen = new Uuid4Generator();
                uuidToReturn = uuid4Gen.getValue();
           
        }catch(Exception ex){
            system.debug('GUID Generation failed' + ex);
        }
        return uuidToReturn;
    }
}