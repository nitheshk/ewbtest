public class PreSubmitWrapper {
	public class Status {
		public String ConversationId {get;set;} 
		public String RequestId {get;set;} 
		public String TransactionStatus {get;set;} 
		public String ActionType {get;set;} 
		public String Reference {get;set;} 

		public Status(System.JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'ConversationId') {
							ConversationId = parser.getText();
						} else if (text == 'RequestId') {
							RequestId = parser.getText();
						} else if (text == 'TransactionStatus') {
							TransactionStatus = parser.getText();
						} else if (text == 'ActionType') {
							ActionType = parser.getText();
						} else if (text == 'Reference') {
							Reference = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Status consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}

	public class Risk_engine_recommendation {
		public Integer risk_score {get;set;} 
		public Integer reason_id {get;set;} 
		public String recommendation {get;set;} 
		public String reason {get;set;} 

		public Risk_engine_recommendation(System.JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'risk_score') {
							risk_score = parser.getIntegerValue();
						} else if (text == 'reason_id') {
							reason_id = parser.getIntegerValue();
						} else if (text == 'recommendation') {
							recommendation = parser.getText();
						} else if (text == 'reason') {
							reason = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Risk_engine_recommendation consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}

	public class Products {
		public String ProductType {get;set;} 
		public String ExecutedStepName {get;set;} 
		public String ProductConfigurationName {get;set;} 
		public String ProductStatus {get;set;} 
		public List<Items> Items {get;set;} 
		public QuestionSet QuestionSet {get;set;} 

		public Products(System.JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'ProductType') {
							ProductType = parser.getText();
						} else if (text == 'ExecutedStepName') {
							ExecutedStepName = parser.getText();
						} else if (text == 'ProductConfigurationName') {
							ProductConfigurationName = parser.getText();
						} else if (text == 'ProductStatus') {
							ProductStatus = parser.getText();
						} else if (text == 'Items') {
							Items = arrayOfItems(parser);
						} else if (text == 'QuestionSet') {
							QuestionSet = new QuestionSet(parser);
						} else {
							System.debug(LoggingLevel.WARN, 'Products consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}

	public class Message {
		public Infection_data infection_data {get;set;} 
		public Device_data device_data {get;set;} 
		public Recommendation recommendation {get;set;} 
		public Risk_engine_recommendation risk_engine_recommendation {get;set;} 
		public String customer_session_id {get;set;} 

		public Message(System.JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'infection_data') {
							infection_data = new Infection_data(parser);
						} else if (text == 'device_data') {
							device_data = new Device_data(parser);
						} else if (text == 'recommendation') {
							recommendation = new Recommendation(parser);
						} else if (text == 'risk_engine_recommendation') {
							risk_engine_recommendation = new Risk_engine_recommendation(parser);
						} else if (text == 'customer_session_id') {
							customer_session_id = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Message consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}

	public class Malware_ids {
		public abc fifty {get;set;} 
		public String fifty3 {get;set;} 

		public Malware_ids(System.JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					   // if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						//text='50';
						// if (text == '50') {
							// fifty = new "50"(parser);
						// } else if (text == '53') {
							// fifty3 = new "50"(parser);
						// } else {
							// System.debug(LoggingLevel.WARN, 'Malware_ids consuming unrecognized property: '+text);
							// consumeObject(parser);
						// }
					//}
					//}
				}
			}
		}
	}

	public class Text {
		public String Statement {get;set;} 

		public Text(System.JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String reason = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (reason == 'Statement') {
							Statement = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Text consuming unrecognized property: '+reason);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}

	public class Data {
		public String ApplicationId {get;set;} 
		public String ApplicationStatus {get;set;} 
		public InstantIdResponse InstantIdResponse {get;set;} 
		public PinpointResponse PinpointResponse {get;set;} 
		public String CaseNumber {get;set;} 
		public String AccountNumber {get;set;} 
		public String CustomerNumber {get; set;}
		public String Error {get;set;} 

		public Data(System.JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'ApplicationId') {
							ApplicationId = parser.getText();
						} else if (text == 'ApplicationStatus') {
							ApplicationStatus = parser.getText();
						} else if (text == 'InstantIdResponse') {
							InstantIdResponse = new InstantIdResponse(parser);
						} else if (text == 'PinpointResponse') {
							//PinpointResponse = new PinpointResponse(parser);
						} else if (text == 'CaseNumber') {
							CaseNumber = parser.getText();
						} else if (text == 'AccountNumber') {
							AccountNumber = parser.getText();
						} else if (text == 'CustomerNumber') {
							CustomerNumber = parser.getText();
						} else if (text == 'Error') {
							Error = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Data consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}

	public class Info {
		public Info(System.JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						{
							System.debug(LoggingLevel.WARN, 'Info consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}

	public Data data {get;set;} 
	public Result result {get;set;} 

	public PreSubmitWrapper(System.JSONParser parser) {
		while (parser.nextToken() != System.JSONToken.END_OBJECT) {
			if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
				String text = parser.getText();
				if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
					if (text == 'data') {
						data = new Data(parser);
					} else if (text == 'result') {
						result = new Result(parser);
					} else {
						System.debug(LoggingLevel.WARN, 'PreSubmitWrapper consuming unrecognized property: '+text);
						consumeObject(parser);
					}
				}
			}
		}
	}

	public class Recommendation {
		public String recommendation {get;set;} 
		public String resolution_id {get;set;} 
		public Integer reason_id {get;set;} 
		public String reason {get;set;} 
		public Integer risk_score {get;set;} 

		public Recommendation(System.JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'recommendation') {
							recommendation = parser.getText();
						} else if (text == 'resolution_id') {
							resolution_id = parser.getText();
						} else if (text == 'reason_id') {
							reason_id = parser.getIntegerValue();
						} else if (text == 'reason') {
							reason = parser.getText();
						} else if (text == 'risk_score') {
							risk_score = parser.getIntegerValue();
						} else {
							System.debug(LoggingLevel.WARN, 'Recommendation consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}

	public class PassThroughs {
		public String Type_Z {get;set;} // in json: Type
		public String Data {get;set;} 

		public PassThroughs(System.JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'Type') {
							Type_Z = parser.getText();
						} else if (text == 'Data') {
							Data = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'PassThroughs consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}

	public class Result {
		public Boolean success {get;set;} 
		public String code {get;set;} 
		public String message {get;set;} 

		public Result(System.JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'success') {
							success = parser.getBooleanValue();
						} else if (text == 'code') {
							code = parser.getText();
						} else if (text == 'message') {
							message = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Result consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}

	public class Infection_data {
		public Malware_Z malware {get;set;} 

		public Infection_data(System.JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'malware') {
							malware = new Malware_Z(parser);
						} else {
							System.debug(LoggingLevel.WARN, 'Infection_data consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}

	public class Choices {
		public Long ChoiceId {get;set;} 
		public Text Text {get;set;} 

		public Choices(System.JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String option = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (option == 'ChoiceId') {
							ChoiceId = parser.getLongValue();
						} else if (option == 'Text') {
							Text = new Text(parser);
						} else {
							System.debug(LoggingLevel.WARN, 'Choices consuming unrecognized property: '+option);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}

	public class Malware_Z {
		public String customer_session_id {get;set;} 
		public Info info {get;set;} 
		public List<Malware> malware {get;set;} 
		public Malware_ids malware_ids {get;set;} 
		public Integer risk_score {get;set;} 
		public String timestamp {get;set;} 
		public String encrypted_user_id {get;set;} 
		public String encryption_key_id {get;set;} 

		public Malware_Z(System.JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'customer_session_id') {
							customer_session_id = parser.getText();
						} else if (text == 'info') {
							info = new Info(parser);
						} else if (text == 'malware') {
							malware = arrayOfMalware(parser);
						} else if (text == 'malware_ids') {
							malware_ids = new Malware_ids(parser);
						} else if (text == 'risk_score') {
							risk_score = parser.getIntegerValue();
						} else if (text == 'timestamp') {
							timestamp = parser.getText();
						} else if (text == 'encrypted_user_id') {
							encrypted_user_id = parser.getText();
						} else if (text == 'encryption_key_id') {
							encryption_key_id = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Malware_Z consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}

	public class PinpointResponse {
		public String status {get;set;} 
		public Message message {get;set;} 

		public PinpointResponse(System.JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'status') {
							status = parser.getText();
						} else if (text == 'message') {
							message = new Message(parser);
						} else {
							System.debug(LoggingLevel.WARN, 'PinpointResponse consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}

	public class Device_data {
		public String device_key {get;set;} 
		public String gd_id {get;set;} 
		public Boolean is_new {get;set;} 
		public Double is_new_device_confidence_level {get;set;} 
		public Boolean is_new_global_device {get;set;} 
		public V3 v3 {get;set;} 

		public Device_data(System.JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'device_key') {
							device_key = parser.getText();
						} else if (text == 'gd_id') {
							gd_id = parser.getText();
						} else if (text == 'is_new') {
							is_new = parser.getBooleanValue();
						} else if (text == 'is_new_device_confidence_level') {
							is_new_device_confidence_level = parser.getDoubleValue();
						} else if (text == 'is_new_global_device') {
							is_new_global_device = parser.getBooleanValue();
						} else if (text == 'v3') {
							v3 = new V3(parser);
						} else {
							System.debug(LoggingLevel.WARN, 'Device_data consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}

	public class Items {
		public String ItemName {get;set;} 
		public String ItemStatus {get;set;} 

		public Items(System.JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'ItemName') {
							ItemName = parser.getText();
						} else if (text == 'ItemStatus') {
							ItemStatus = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Items consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}

	public class Questions {
		public Integer QuestionId {get;set;} 
		public Integer Key {get;set;} 
		public String Type_Z {get;set;} // in json: Type
		public Text Text {get;set;} 
		public Text HelpText {get;set;} 
		public List<Choices> Choices {get;set;} 

		public Questions(System.JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String option = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (option == 'QuestionId') {
							QuestionId = parser.getIntegerValue();
						} else if (option == 'Key') {
							Key = parser.getIntegerValue();
						} else if (option == 'Type') {
							Type_Z = parser.getText();
						} else if (option == 'Text') {
							Text = new Text(parser);
						} else if (option == 'HelpText') {
							HelpText = new Text(parser);
						} else if (option == 'Choices') {
							Choices = arrayOfChoices(parser);
						} else {
							System.debug(LoggingLevel.WARN, 'Questions consuming unrecognized property: '+option);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}

	public class InstantIdResponse {
		public Status Status {get;set;} 
		public List<Products> Products {get;set;} 
		public List<PassThroughs> PassThroughs {get;set;}

		public InstantIdResponse(System.JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'Status') {
							Status = new Status(parser);
						} else if (text == 'Products') {
							Products = arrayOfProducts(parser);
						} else if (text == 'PassThroughs') {
							PassThroughs = arrayOfPassThroughs(parser);
						} else {
							System.debug(LoggingLevel.WARN, 'InstantIdResponse consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}

	public class Malware {
		public Integer risk_score {get;set;} 
		public String name {get;set;} 
		public String key {get;set;} 
		public String id {get;set;} 
		public String signature {get;set;} 
		public String targeted {get;set;} 
		public String collector {get;set;} 
		public String pattern {get;set;} 

		public Malware(System.JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'risk_score') {
							risk_score = parser.getIntegerValue();
						} else if (text == 'name') {
							name = parser.getText();
						} else if (text == 'key') {
							key = parser.getText();
						} else if (text == 'id') {
							id = parser.getText();
						} else if (text == 'signature') {
							signature = parser.getText();
						} else if (text == 'targeted') {
							targeted = parser.getText();
						} else if (text == 'collector') {
							collector = parser.getText();
						} else if (text == 'pattern') {
							pattern = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Malware consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}

	public class abc {
		public Integer risk_score {get;set;} 
		public String targeted {get;set;} 

		public abc(System.JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'risk_score') {
							risk_score = parser.getIntegerValue();
						} else if (text == 'targeted') {
							targeted = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, '50 consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}

	public class V3 {
		public Integer screen_width {get;set;} 
		public String doc_location {get;set;} 
		public Integer activeX {get;set;} 
		public Integer dma_code {get;set;} 
		public String ip_class {get;set;} 
		public Integer client_time_zone {get;set;} 
		public String remote_addr {get;set;} 
		public Integer history_length {get;set;} 
		public Integer js {get;set;} 
		public String accept_encoding {get;set;} 
		public Integer counter {get;set;} 
		public String machine_id {get;set;} 
		public String region {get;set;} 
		public Integer screen_dpi {get;set;} 
		public String client_language {get;set;} 
		public String isp {get;set;} 
		public Integer screen_height {get;set;} 
		public String first_time {get;set;} 
		public String agent_key {get;set;} 
		public String platform {get;set;} 
		public String cpu {get;set;} 
		public Integer ip_time_zone {get;set;} 
		public String digest {get;set;} 
		public Integer screen_touch {get;set;} 
		public String fonts {get;set;} 
		public String global_cookie {get;set;} 
		public Integer file_upload_indicator {get;set;} 
		public String mimes {get;set;} 
		public Integer file_upload {get;set;} 
		public Double longitude {get;set;} 
		public Long navigator_props {get;set;} 
		public String browser_version {get;set;} 
		public String client_charset {get;set;} 
		public String last_access {get;set;} 
		public String browser {get;set;} 
		public Integer charset {get;set;} 
		public String accept_language {get;set;} 
		public Double latitude {get;set;} 
		public String org {get;set;} 
		public String city {get;set;} 
		public String first_access {get;set;} 
		public String country {get;set;} 
		public String continent_code {get;set;} 
		public String country_name {get;set;} 
		public String country_code {get;set;} 
		public String country_code3 {get;set;} 
		public String post_code {get;set;} 
		public String x_forwarded_for {get;set;} 
		public Integer id {get;set;} 
		public String pinpoint_session {get;set;} 
		public String hashed_user_id {get;set;} 
		public String os {get;set;} 
		public String user_agent {get;set;} 
		public String application {get;set;} 
		public String cookie {get;set;} 
		public String plugins {get;set;} 
		public Integer area_code {get;set;} 
		public String accept {get;set;} 
		public String business {get;set;} 

		public V3(System.JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'screen_width') {
							screen_width = parser.getIntegerValue();
						} else if (text == 'doc_location') {
							doc_location = parser.getText();
						} else if (text == 'activeX') {
							activeX = parser.getIntegerValue();
						} else if (text == 'dma_code') {
							dma_code = parser.getIntegerValue();
						} else if (text == 'ip_class') {
							ip_class = parser.getText();
						} else if (text == 'client_time_zone') {
							client_time_zone = parser.getIntegerValue();
						} else if (text == 'remote_addr') {
							remote_addr = parser.getText();
						} else if (text == 'history_length') {
							history_length = parser.getIntegerValue();
						} else if (text == 'js') {
							js = parser.getIntegerValue();
						} else if (text == 'accept_encoding') {
							accept_encoding = parser.getText();
						} else if (text == 'counter') {
							counter = parser.getIntegerValue();
						} else if (text == 'machine_id') {
							machine_id = parser.getText();
						} else if (text == 'region') {
							region = parser.getText();
						} else if (text == 'screen_dpi') {
							screen_dpi = parser.getIntegerValue();
						} else if (text == 'client_language') {
							client_language = parser.getText();
						} else if (text == 'isp') {
							isp = parser.getText();
						} else if (text == 'screen_height') {
							screen_height = parser.getIntegerValue();
						} else if (text == 'first_time') {
							first_time = parser.getText();
						} else if (text == 'agent_key') {
							agent_key = parser.getText();
						} else if (text == 'platform') {
							platform = parser.getText();
						} else if (text == 'cpu') {
							cpu = parser.getText();
						} else if (text == 'ip_time_zone') {
							ip_time_zone = parser.getIntegerValue();
						} else if (text == 'digest') {
							digest = parser.getText();
						} else if (text == 'screen_touch') {
							screen_touch = parser.getIntegerValue();
						} else if (text == 'fonts') {
							fonts = parser.getText();
						} else if (text == 'global_cookie') {
							global_cookie = parser.getText();
						} else if (text == 'file_upload_indicator') {
							file_upload_indicator = parser.getIntegerValue();
						} else if (text == 'mimes') {
							mimes = parser.getText();
						} else if (text == 'file_upload') {
							file_upload = parser.getIntegerValue();
						} else if (text == 'longitude') {
							longitude = parser.getDoubleValue();
						} else if (text == 'navigator_props') {
							navigator_props = parser.getLongValue();
						} else if (text == 'browser_version') {
							browser_version = parser.getText();
						} else if (text == 'client_charset') {
							client_charset = parser.getText();
						} else if (text == 'last_access') {
							last_access = parser.getText();
						} else if (text == 'browser') {
							browser = parser.getText();
						} else if (text == 'charset') {
							charset = parser.getIntegerValue();
						} else if (text == 'accept_language') {
							accept_language = parser.getText();
						} else if (text == 'latitude') {
							latitude = parser.getDoubleValue();
						} else if (text == 'org') {
							org = parser.getText();
						} else if (text == 'city') {
							city = parser.getText();
						} else if (text == 'first_access') {
							first_access = parser.getText();
						} else if (text == 'country') {
							country = parser.getText();
						} else if (text == 'continent_code') {
							continent_code = parser.getText();
						} else if (text == 'country_name') {
							country_name = parser.getText();
						} else if (text == 'country_code') {
							country_code = parser.getText();
						} else if (text == 'country_code3') {
							country_code3 = parser.getText();
						} else if (text == 'post_code') {
							post_code = parser.getText();
						} else if (text == 'x_forwarded_for') {
							x_forwarded_for = parser.getText();
						} else if (text == 'id') {
							id = parser.getIntegerValue();
						} else if (text == 'pinpoint_session') {
							pinpoint_session = parser.getText();
						} else if (text == 'hashed_user_id') {
							hashed_user_id = parser.getText();
						} else if (text == 'os') {
							os = parser.getText();
						} else if (text == 'user_agent') {
							user_agent = parser.getText();
						} else if (text == 'application') {
							application = parser.getText();
						} else if (text == 'cookie') {
							cookie = parser.getText();
						} else if (text == 'plugins') {
							plugins = parser.getText();
						} else if (text == 'area_code') {
							area_code = parser.getIntegerValue();
						} else if (text == 'accept') {
							accept = parser.getText();
						} else if (text == 'business') {
							business = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'V3 consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}

	public class QuestionSet {
		public Integer QuestionSetId {get;set;} 
		public List<Questions> Questions {get;set;} 

		public QuestionSet(System.JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'QuestionSetId') {
							QuestionSetId = parser.getIntegerValue();
						} else if (text == 'Questions') {
							Questions = arrayOfQuestions(parser);
						} else {
							System.debug(LoggingLevel.WARN, 'QuestionSet consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}

	public static PreSubmitWrapper parse(String json) {
		System.JSONParser parser = System.JSON.createParser(json);
		return new PreSubmitWrapper(parser);
	}

	public static void consumeObject(System.JSONParser parser) {
		Integer depth = 0;
		do {
			System.JSONToken curr = parser.getCurrentToken();
			if (curr == System.JSONToken.START_OBJECT || 
				curr == System.JSONToken.START_ARRAY) {
				depth++;
			} else if (curr == System.JSONToken.END_OBJECT ||
				curr == System.JSONToken.END_ARRAY) {
				depth--;
			}
		} while (depth > 0 && parser.nextToken() != null);
	}

	private static List<PassThroughs> arrayOfPassThroughs(System.JSONParser p) {
		List<PassThroughs> res = new List<PassThroughs>();
		if (p.getCurrentToken() == null) p.nextToken();
		while (p.nextToken() != System.JSONToken.END_ARRAY) {
			res.add(new PassThroughs(p));
		}

		return res;
	}

	private static List<Questions> arrayOfQuestions(System.JSONParser p) {
		List<Questions> res = new List<Questions>();
		if (p.getCurrentToken() == null) p.nextToken();
		while (p.nextToken() != System.JSONToken.END_ARRAY) {
			res.add(new Questions(p));
		}

		return res;
	}

	private static List<Items> arrayOfItems(System.JSONParser p) {
		List<Items> res = new List<Items>();
		if (p.getCurrentToken() == null) p.nextToken();
		while (p.nextToken() != System.JSONToken.END_ARRAY) {
			res.add(new Items(p));
		}

		return res;
	}

	private static List<Choices> arrayOfChoices(System.JSONParser p) {
		List<Choices> res = new List<Choices>();
		if (p.getCurrentToken() == null) p.nextToken();
		while (p.nextToken() != System.JSONToken.END_ARRAY) {
			res.add(new Choices(p));
		}

		return res;
	}

	private static List<Products> arrayOfProducts(System.JSONParser p) {
		List<Products> res = new List<Products>();
		if (p.getCurrentToken() == null) p.nextToken();
		while (p.nextToken() != System.JSONToken.END_ARRAY) {
			res.add(new Products(p));
		}

		return res;
	}

	private static List<Malware> arrayOfMalware(System.JSONParser p) {
		List<Malware> res = new List<Malware>();
		if (p.getCurrentToken() == null) p.nextToken();
		while (p.nextToken() != System.JSONToken.END_ARRAY) {
			res.add(new Malware(p));
		}

		return res;
	}
}