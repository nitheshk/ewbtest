global class SchBatchToSendDocuments implements Schedulable {

  
   public static String sched = '0 0 0 * * ?';
   global static String scheduleMe() {
       SchBatchToSendDocuments SC = new SchBatchToSendDocuments(); 
       return System.schedule('PayverisProcessACHApplications', sched, SC);
   }

   global void execute(SchedulableContext sc) {
       BatchToSendDocuments b1 = new BatchToSendDocuments();
       ID batchprocessid = Database.executeBatch(b1,1);           
   }
}