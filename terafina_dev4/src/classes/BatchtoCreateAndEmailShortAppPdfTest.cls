@isTest(SeeAllData=true)
global class BatchtoCreateAndEmailShortAppPdfTest{ 

@isTest static void method_one(){

 String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
    Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
    User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
      EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US', ProfileId = p.Id,
      TimeZoneSidKey = 'America/Los_Angeles',
      UserName = uniqueUserName);
    System.runAs(u) {
    
        TF4SF__Application__c app = new TF4SF__Application__c ();
        app.TF4SF__Application_Status__c = 'Open';
        app.TF4SF__Product__c = 'Home Loan';
        app.TF4SF__Sub_Product__c =  'Home Loan - Short App';
        
        insert app;
         List<TF4SF__About_Account__c> abblst=new  List<TF4SF__About_Account__c> ();
        
        TF4SF__About_Account__c ab = new TF4SF__About_Account__c();
        ab.TF4SF__Application__c = app.id;
        insert ab;
        
    
        Database.QueryLocator QL;
        Database.BatchableContext BC;
        BatchtoCreateAndEmailShortAppPdf instance1 = new BatchtoCreateAndEmailShortAppPdf();
        instance1.start(BC);
        instance1.execute(BC,abblst);
        instance1.finish(BC);
}
}
}