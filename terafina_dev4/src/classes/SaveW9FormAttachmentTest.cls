@isTest
public class SaveW9FormAttachmentTest{

     static testmethod void validateStandardController(){
    
    Map<String, String> tdata = new Map<String, String>();
    Map<String, String> data = new Map<String, String>();
    TF4SF__Application__c app1 = new TF4SF__Application__c();
    app1.TF4SF__Product__c = 'Home Loan';
        app1.TF4SF__First_Name__c='test';
        app1.TF4SF__Last_Name__c = 'Test';
        app1.TF4SF__Email_Address__c= 'test@test.com';
        app1.TF4SF__Sub_Product__c = 'Home Loan - Short App';
    
    insert app1;
    
    TF4SF__Application_Configuration__c appConfig = new TF4SF__Application_Configuration__c();
        appConfig.TF4SF__Application_Code__c = 'dsp4_0';
        appConfig.TF4SF__Theme__c = 'dsp4_0';
        appConfig.TF4SF__Key__c = 'YMmKJt5QxS7KlAEASMsj4Q==';
        appConfig.TF4SF__DL_Endpoint_URL__c = 'https://app1.idware.net/DriverLicenseParserRest.svc/ParseImage';
        insert appConfig;  

    
    TF4SF__Identity_Information__c iden = new TF4SF__Identity_Information__c();
    iden.TF4SF__Application__c = app1.Id;
    insert iden;
         
         
    Blob b = Blob.valueOf('Test Data');
    Attachment att = new Attachment();
        att.name = 'w9TaxForm.pdf';
         
         
             att.Body = b;
        
         
        att.Description = 'w9TaxForm';
        att.ParentId = app1.Id;
        att.ContentType = 'application/pdf';
    	insert(att);

     tdata.put('id',app1.Id);
     PageReference pageRef = Page.w9TaxForm;
	 Test.setCurrentPage(pageRef);
     pageRef.getParameters().put('Id',app1.Id); 
   
    test.startTest();
    SaveW9FormAttachment objclass = new SaveW9FormAttachment();
    SaveW9FormAttachment.main(tdata);
    test.stopTest(); 
    
     }

}