global class SchBatchToMarkTrstBusiParentAppComplete implements Schedulable{

  
    public static String sched = '0 0 0,2,8,12,16,20 ? * *';
   global static String scheduleMe() {
       SchBatchToMarkTrstBusiParentAppComplete SC = new SchBatchToMarkTrstBusiParentAppComplete(); 
       return System.schedule('PayverisProcessACHApplications', sched, SC);
   }

   global void execute(SchedulableContext sc) {
       BatchToMarkTrstBusiParentAppComplete b1 = new BatchToMarkTrstBusiParentAppComplete();
       ID batchprocessid = Database.executeBatch(b1,1);           
   }
}