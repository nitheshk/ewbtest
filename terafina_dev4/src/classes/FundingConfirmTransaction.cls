global with sharing class FundingConfirmTransaction implements TF4SF.DSP_Interface {

    global static Map<String, String> main (Map<String, String> tdata) {
        Map<String, String> data = tdata.clone();
        String appId = tdata.get('id');
        String Token = '';
        String statusCode='0'; 
        String Status='',response='';

        try{
        VELOSETTINGS__C velo = VELOSETTINGS__C.getOrgDefaults();
        
        Token = AuthToken.Auth();
        response = ConfirmTransaction(appId, Token);
        Map<String, Object> ConfirmMap = (Map<String, Object>)JSON.deserializeUntyped(response);
        Map<String, Object> dataMap = (Map<String, Object>)ConfirmMap.get('data');
        Map<String, Object> bodyMap = (Map<String, Object>)dataMap.get('body');
        Map<String, Object> UpdateUserMilestoneResponseMap = (Map<String, Object>)bodyMap.get('UpdateUserMilestoneResponse');
        Map<String, Object> returnMap = (Map<String, Object>)UpdateUserMilestoneResponseMap.get('return');
        Map<String, Object> StatusMap = (Map<String, Object>)returnMap.get('Status');
        Status = String.valueOf(StatusMap.get('StatusDesc'));
        statusCode='200';
        }
        catch(Exception ex){            
            data.put('ErrorMessage', 'An error occured in execute with error ####### ' + ex.getMessage() + ' ' + 'In Line ::::::::: ' + ex.getLineNumber());
        }

        data.put('StatusCode', StatusCode);
        data.put('Response', response);
        data.put('Status', Status);
        if (StatusCode == '200') {
            updateApp(appId);
        }
        return data;
    }
    // Fifth method to call
    public static String ConfirmTransaction(String appId, String Token) {
        String result = '';

        String applicationId = '\'' + appId + '\'';
        //System.debug('applicationId//productnumber:'+applicationId+'--'+productnumber);
        //select all fields in Application
        queryHelper.ObjectInfo appInfo = queryHelper.selectStar('TF4SF__Application__c');
        String queryString =  appInfo.query;
        queryString += 'WHERE ID = ' + applicationId ;
        TF4SF__Application__c app = Database.query(queryString);

        //this.Application = app;
        queryHelper.ObjectInfo app2Info = queryHelper.selectStar('TF4SF__Application2__c');
        String app2String =  app2Info.query;
        app2String += 'WHERE TF4SF__Application__c = ' + applicationId ;
        TF4SF__Application2__c app2 = Database.query(app2String);

        queryHelper.ObjectInfo empInfo = queryHelper.selectStar('TF4SF__Employment_Information__c');
        String employmentQuery =  empInfo.query;
        employmentQuery += 'WHERE TF4SF__Application__c = ' + applicationId;
        TF4SF__Employment_Information__c employment = Database.query(employmentQuery);
        //System.debug('employment:' + employment);

        queryHelper.ObjectInfo identityInfo = queryHelper.selectStar('TF4SF__Identity_Information__c');
        String identityQuery = identityInfo.query;
        identityQuery += 'WHERE TF4SF__Application__c = ' + applicationId;
        TF4SF__Identity_Information__c identity = Database.query(identityQuery);
        System.debug('identity:' + identity);

        queryHelper.ObjectInfo aboutAccountInfo = queryHelper.selectStar('TF4SF__About_Account__c');
        String aboutAccountQuery = aboutAccountInfo.query;
        aboutAccountQuery += 'WHERE TF4SF__Application__c = ' + applicationId;
        //System.debug('abtaccountquery:'+aboutAccountQuery);
        TF4SF__About_Account__c about = Database.query(aboutAccountQuery);
        //System.debug('about account:' + about); 

        DepositSubmit.Application application = new DepositSubmit.Application(app, app2, employment, identity, about);

        result += '{';
            result += '"Email": "'+application.app.TF4SF__Email_Address__c+'",';
            result += '"UpdateUserMilestone": {';
                result += '"CEUserID": "'+application.about.CEUserID__c+'",';
                result += '"MileStoneType": "OAO_QUEUE_STATUS",';
                result += '"MileStoneValue": "Pending",';
                result += '"RqUID": "UpdateUserMilestone",';
                result += '"UserID": "'+application.app.TF4SF__Email_Address__c+'"';
            result += '}';
        result += '}';
        return execute(appId, result, Token);


    }

    public static void updateApp(String appId) {
        TF4SF__Application__c app = [SELECT ID, Primary_Product_Sub_Status__c FROM TF4SF__Application__c WHERE Id =: appId];
        app.Primary_Product_Sub_Status__c = 'Funding Initiated';
        update app;
    }

    private static String execute(String appId, String json, String Token) {
        System.debug('appId, JSON: ' + appId  + '---' + json);
        String responseJson = '';
        String GUID = GUIDGenerator.getGUID();

        Http h = new Http();
        HttpRequest req = new HttpRequest();
        VELOSETTINGS__C velo = VELOSETTINGS__C.getOrgDefaults();
        Integer timeOut = Integer.ValueOf(velo.ProcessAPI_Timeout__c);
        req.setTimeout(timeOut);
        req.setEndpoint(velo.FundingEndpoint__c+'api/process/confirmTransaction');
        req.setHeader('Authorization','Bearer '+Token);
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Accept','application/json');
        req.setMethod('POST');
        req.setHeader(velo.Request_GUID_Name__c,GUID);

        req.setBody(json);
        HttpResponse res = h.send(req);
        responseJson = res.getBody();
        system.debug('Res is '+res.getbody());  
        DebugLogger.InsertDebugLog(appId, GUID, 'Confirmation Request GUID');
        DebugLogger.InsertDebugLog(appId, req.getBody(), 'Confirmation Request');
        DebugLogger.InsertDebugLog(appId, res.getBody(), 'Confirmation Response'); 
        return responseJSON;
    }

    public static string formatDate (String DateOfBirth) {
        string properDate = '';
        properDate = DateOfBirth.replaceAll('/','-');
        return properDate;
    }

}