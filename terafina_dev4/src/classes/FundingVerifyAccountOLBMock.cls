@isTest
global class FundingVerifyAccountOLBMock implements HttpCalloutMock {
    
 
	// Implement this interface method
	global HTTPResponse respond(HTTPRequest req) {
		// Optionally, only send a mock response for a specific endpoint
		// and method.
		// Create a fake response
		HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/json');
		res.setBody('{"CreateUser": {"RqUID": "ouFH22AeFPDNBZSMAx60HBzJK","AgreementAccepted": "true","UserInfo": {"LastName": "LUNSFORD","FirstName": "CARLA","DateOfBirth": "06-02-1976","SSN": "563932401","DrvLicNumber": "","DrvLicState": "","DrvLicAddressType": ""},"ContactInfo": {"PhoneType": "DAYTIMEPHONE","PhoneNumber": "510-896-0273","AddressType": "CURRENT-ADDRESS","Line1": "2111 DE HOOP SW AVE","City": "WYOMING","State": "MI","Zip": "49509","Email": "test3062@velo.com","EmailType": "1"},"FundingAccountInfo": {"RoutingNumber": "999999994"}}}');
		res.setStatusCode(200);
		return res;
	}
}