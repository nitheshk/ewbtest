global class BatchToMarkTrstBusiParentAppComplete implements
Database.Batchable <sObject> , Database.Stateful, Database.AllowsCallouts { 


  global Database.QueryLocator start(Database.BatchableContext bc) {
   
     String query =  'SELECT Id,Child_Applications_Completed__c ,TF4SF__Product__c,TF4SF__Sub_Product__c,';
            query += ' TF4SF__Custom_Text2__c';
            query += ' FROM TF4SF__Application__c';
            query += ' WHERE  (TF4SF__Custom_Text2__c =\'Business Entity\' OR TF4SF__Custom_Text2__c =\'Trust\' OR TF4SF__Custom_Text2__c =\'Joint Application\')';
            query += ' AND TF4SF__Product__c =\'Home Loan\' AND Child_Applications_Completed__c = False';
          
        return Database.getQueryLocator(query);      
  } 

  global void execute(Database.BatchableContext bc, List<TF4SF__Application__c> scope) {
    List<Id> lstAppId = new  List<Id>();     
    List<TF4SF__Application__c> lstUpdate = new List<TF4SF__Application__c>();  
    try { 

          System.debug('1 ==>');
          for(TF4SF__Application__c obj : scope) {
           lstAppId.add(obj.Id);
          }
          Map<Id,Trustee_Guarantor__c> mapTrusteeGuarentor = new Map<Id,Trustee_Guarantor__c>();

          //Get all the Trustee/Guarentor/Co-borrower recoreds
          List<Trustee_Guarantor__c> lstTrust = [SELECT Id,Child_Application_Completed__c,Application__c FROM Trustee_Guarantor__c
                                                WHERE Child_Application_Completed__c = False AND Application__c IN : lstAppId ];

          System.debug('2 ==>');                                    
          //Add Trustee records to Map
          for(Trustee_Guarantor__c obj : lstTrust) {
            mapTrusteeGuarentor.put(obj.Application__c,obj);
          }

          System.debug('3 ==>');
          //Loop through the Application to check for completed child applications
          for(TF4SF__Application__c obj : scope) {
            Trustee_Guarantor__c objTrust = new Trustee_Guarantor__c();
            objTrust  = mapTrusteeGuarentor.get(obj.Id);
             //Check if all the child applications are commpleted.Set the Child_Applications_Completed__c flag to True in Parent application
            if(objTrust == null){
              obj.Child_Applications_Completed__c = True;
              lstUpdate.add(obj);
            }
          }

         

       
      
    } catch (exception e) {
        System.debug('Exception ==>' + e.getMessage());
    } finally {
        if(lstUpdate.size() > 0) {
          update lstUpdate;
        }
    }
  }

  global void finish(Database.BatchableContext bc) {
  }

 
   
  }