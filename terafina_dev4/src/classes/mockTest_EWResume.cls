@isTest 
global class mockTest_EWResume implements HttpCalloutMock {
	// Implement this interface method
	global HTTPResponse respond(HTTPRequest req) {
		// Optionally, only send a mock response for a specific endpoint
		// and method.
		// Create a fake response
		HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/json');
		res.setBody('{"success":true,"message":"SMS token was sent","cellphone":"+91-XXX-XXX-XXX-XX92"}');
		res.setStatusCode(200);
		return res;
	}
}