public class TrusteeGuarantorJSON { 

    public class PA {
        public String Id;
        public String Applicant_Type_c;
        public String First_Name_c;
        public String Last_Name_c;
        public String Middle_Name_c;
        public String Suffix_c;
        public String Type_c;
        public String Phone_Number_c;
        public String Email_Address_c;
        public String Address_c;
        public String Address2_c;
        public String City_c;
        public String State_c;
        public String Zip_Code_c;
    }


    public class J1 {
       public String Id;
        public String Applicant_Type_c;
        public String First_Name_c;
        public String Last_Name_c;
        public String Middle_Name_c;
        public String Suffix_c;
        public String Type_c;
        public String Phone_Number_c;
        public String Email_Address_c;
        public String Address_c;
        public String Address2_c;
        public String City_c;
        public String State_c;
        public String Zip_Code_c;
        
    }
    
    public class J2 {
        public String Id;
        public String Applicant_Type_c;
        public String First_Name_c;
        public String Last_Name_c;
        public String Middle_Name_c;
        public String Suffix_c;
        public String Type_c;
        public String Phone_Number_c;
        public String Email_Address_c;
        public String Address_c;
        public String Address2_c;
        public String City_c;
        public String State_c;
        public String Zip_Code_c;
       
      }
    
    public class J3 {
        public String Id;
        public String Applicant_Type_c;
        public String First_Name_c;
        public String Last_Name_c;
        public String Middle_Name_c;
        public String Suffix_c;
        public String Type_c;
        public String Phone_Number_c;
        public String Email_Address_c;
        public String Address_c;
        public String Address2_c;
        public String City_c;
        public String State_c;
        public String Zip_Code_c;
      
    }
    
    
    
     public PA PA;
     public J1 J1;
     public J2 J2;
     public J3 J3;

        
    public static List<TrusteeGuarantorJSON> parse(String json) {
        return (List<TrusteeGuarantorJSON>) System.JSON.deserialize(json, List<TrusteeGuarantorJSON>.class);
    }
}