global with sharing class DocuSignTemplate {
    global static void updateTemplateId() {
        
        String backImage = '';
       
        String username = '';
        String password = '';
        String accountId = ''; 
        String integratorKey = '';
         Docusign_Config__c cred = Docusign_Config__c.getOrgDefaults();
         
           if (cred != NULL) {
            accountId = cred.AccountID__c;
            username = cred.Username__c;
            password = cred.Password__c;
            integratorKey = cred.IntegratorKey__c ;
         }

        
           String authenticationHeader =
            '<DocuSignCredentials>' +
            '<Username>' + username + '</Username>' +
            '<Password>' + password + '</Password>' +
            '<IntegratorKey>' + integratorKey + '</IntegratorKey>' +
            '</DocuSignCredentials>';
        
            HttpResponse res = null;
     
         
               String url = 'https://demo.docusign.net/restapi/v2/login_information';
               res = InitializeRequest(url, 'GET', '', authenticationHeader,'application/xml');
               
                Integer status = res.getStatusCode();
                String resBody = res.getBody();
                 
                if (status != 200) { // 200 = OK
                    
                    
                }
                 String baseURL = parseXMLBody(resBody, 'baseUrl');
                
                 url = baseURL + '/templates'; System.debug('status envelopes url ' +url);
                 res = InitializeRequest(url, 'GET', '', authenticationHeader,'application/json');
                 status = res.getStatusCode();
                 resBody = res.getBody();

                if (status != 200) { // 200 = OK
                   
                    
                }
                /*
                String Statusres = parseXMLBody(resBody, 'status');
                 System.debug('Statusres : ' +Statusres);
                data.put('Status', Statusres);
                */
            
           
            
        }
        
        global static String parseXMLBody(String body, String searchToken) {
      
        String value = '';

        if (body!= '' && body!= NULL) {
            Dom.Document doc1 = new Dom.Document();
            doc1.load(body);
            Dom.XMLNode xroot1 = doc1.getrootelement();
            Dom.XMLNode[] xrec1 = xroot1.getchildelements(); //Get all Record Elements

            for (Dom.XMLNode firstInnerChild : xrec1) { //Loop Through Records
                if (firstInnerChild.getname() == searchToken) {
                    value = firstInnerChild.gettext();
                    break;
                } else {
                    Dom.XMLNode[] xrec2 = NULL;
                    xrec2 = firstInnerChild.getchildelements();

                    if (xrec2 != NULL) {
                        for (Dom.XMLNode secondInnerChild : xrec2) {
                            if (secondInnerChild.getname() == 'loginAccount') {
                                for (Dom.XMLNode thirdInnerChild : secondInnerChild.getchildren()) {
                                    if (thirdInnerChild.getname() == searchToken) {
                                        System.debug('values is: ' +  thirdInnerChild.gettext());
                                        value = thirdInnerChild.gettext();
                                    }

                                    if (value != '') { break; }
                                }
                            }

                            if (value != '') { break; }
                        }
                    }
                }

                if (value != '') { break; }
            }
        }

        return value;
    }
   
        
        
        global static HttpResponse InitializeRequest(String url, String method, String body, String httpAuthHeader,String ContentType) {
        HttpResponse res = null;
        Map<String,String> mapTemplate = new Map<String,String>();
        try {
            Http http = new Http();
            HttpRequest req = new HttpRequest();
          
            req.setEndpoint(url);
            req.setHeader('X-DocuSign-Authentication', httpAuthHeader);
            req.setHeader('Content-Type', ContentType);
            //req.setHeader('Accept', 'application/xml');
            req.setMethod(method);
            req.setTimeout(120000);

            if (method == 'POST') {
                req.setHeader('Content-Length', String.valueOf(body.length()));
                req.setbody(body);
            }

            res = http.send(req);
           
                
            DocusignTemplateMapper  objDocuSign = DocusignTemplateMapper.parse(res.getBody());
            
            //Parse the response and access the template name and template Id,Store it a map
            for(DocusignTemplateMapper.envelopeTemplates template : objDocuSign.envelopeTemplates){                             
                if(mapTemplate.containsKey(template.name)){ 
                    System.debug('Duplicate Template Name found ' + template.name + ' Old Template ID will be replaced with Template Id ' + template.templateId);
                } 
                mapTemplate.put(template.name,template.templateId); 
            }
            
            //Get the disclosure name and template ID from current org
            List<TF4SF__Disclosure__c> lstDisclosure = [Select Id, Name,Template_ID__c from TF4SF__Disclosure__c LIMIT 5000];
            
            //Loop the disclosure from current org ,get the corresponding Template ID of each template from Docusign and update the template ID in current org
            
            for(TF4SF__Disclosure__c obj : lstDisclosure){
                String templateId = null;
                if( String.isNotBlank(obj.Name)) {
                    templateId = mapTemplate.get(obj.Name);
                    //obj.TF4SF__Template_ID__c = templateId;
                    obj.Template_ID__c = templateId;
                }
            }
            
            update lstDisclosure;
            
             
             
        } catch (Exception e) {
            System.debug(e); // simple exception handling, please review it
        }

        return res;
    }
 }