@isTest
global class ApplicationWrapperTest{

@isTest static void Test_metho_one(){

 String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
      Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
      User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
      EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US', ProfileId = p.Id,
      TimeZoneSidKey = 'America/Los_Angeles',
      UserName = uniqueUserName);
      System.runAs(u) {

 TF4SF__Application__c app = new TF4SF__Application__c ();
        app.TF4SF__Product__c = 'Checking';
        app.TF4SF__Sub_Product__c = 'Checking - Checking';
        
        insert app;
        TF4SF__About_Account__c ab = new TF4SF__About_Account__c();
        ab.TF4SF__Application__c = app.id;
      
        insert ab;
        
        TF4SF__Application2__c ap2 = new TF4SF__Application2__c ();
        ap2.TF4SF__Application__c = app.id;
        insert ap2;
        
        TF4SF__Identity_Information__c iden = new TF4SF__Identity_Information__c ();
        iden.TF4SF__Application__c = app.id;
        insert iden;
        
        TF4SF__Employment_Information__c em = new TF4SF__Employment_Information__c();
        em.TF4SF__Application__c = app.id;
        insert em;
        
        TF4SF__Debug_Logs__c db = new TF4SF__Debug_Logs__c();
        db.TF4SF__Application__c = app.id;
        insert db;
        
        test.startTest();
        ApplicationWrapper  wrap=new ApplicationWrapper(app,ap2,em,iden,ab);
        test.stopTest();
    
}
}
}