/**********************************************************
* Name : AppDetailsViewerRedirect
* Desc : Customized page to display the application details
* Date : 12 Sep 2018
* Author : Nithesh K
* *******************************************************/
public class AppDetailsViewerRedirect {
        //Default constructor
        public AppDetailsViewerRedirect (){ }
        //Default Standard constructor
        public AppDetailsViewerRedirect (ApexPages.StandardController stdController){ }
        
      /**
      *  @Desc: Open custom page in edit mode 
      *  @name: validateAndRedirect -  Perform an action on page load
      *  @return: PageReference - Redirect to AppDetailsViewer by adding additional parameter "Editmode" to "true"
      */
        public PageReference validateAndRedirect(){
          PageReference retURL =  Page.AppDetailsViewer;
          retURL.getParameters().put('id', ApexPages.currentPage().getParameters().get('id') );
          retURL.getParameters().put('editmode', 'true' );
          retURL.setRedirect(true);
          return retURL;
        }
}