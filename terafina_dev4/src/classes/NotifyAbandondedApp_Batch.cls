//Batch class to notify to the abandoned apps 
global class NotifyAbandondedApp_Batch implements Database.Batchable<sObject>{
    global Database.QueryLocator start(Database.BatchableContext BC) {
        //**********Get records from object Application 
        System.debug('SELECT Id, TF4SF__Email_Address__c,TF4SF__Product__c, is_Processed_Abandoned_Mail_1__c  ,is_Processed_Abandoned_Mail_2__c  ,is_Processed_Abandoned_Mail_3__c   , Time_Diff__c FROM TF4SF__Application__c where  TF4SF__Product__c=\'Home Loan\' and ( is_Processed_Abandoned_Mail_1__c  =false OR is_Processed_Abandoned_Mail_2__c  =false OR is_Processed_Abandoned_Mail_3__c   =false) AND (TF4SF__Application_Status__c =\'Save for Later\' OR TF4SF__Application_Status__c=\'Abandoned\')');
        return Database.getQueryLocator('SELECT Id, TF4SF__Product__c,TF4SF__Email_Address__c, is_Processed_Abandoned_Mail_1__c  ,is_Processed_Abandoned_Mail_2__c  , Time_Diff__c FROM TF4SF__Application__c where TF4SF__Product__c=\'Home Loan\' and ( is_Processed_Abandoned_Mail_1__c  =false OR is_Processed_Abandoned_Mail_2__c  =false ) AND (TF4SF__Application_Status__c =\'Save for Later\' OR TF4SF__Application_Status__c=\'Abandoned\')');}
    
    global void execute(Database.BatchableContext BC, List<TF4SF__Application__c> listApp) {
        
        //*****call documentStipulationRequest_Utilityclass
        EmailNotification_Utilityclass utility = new EmailNotification_Utilityclass();
        utility.sendEmail(listApp);
    }
    
    global void finish(Database.BatchableContext BC) {
        //method sends the email
        ScheduleFollowupEmailBatchApex.SchedulerMethod();
    }
}