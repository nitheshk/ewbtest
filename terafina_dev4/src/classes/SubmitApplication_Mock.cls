@isTest
global class SubmitApplication_Mock implements HttpCalloutMock {
  // Implement this interface method
  global HTTPResponse respond(HTTPRequest req) {
    // Optionally, only send a mock response for a specific endpoint
    // and method.
    // Create a fake response
    HttpResponse res = new HttpResponse();
    res.setHeader('Content-Type', 'application/json');
    res.setBody('{ "data": { "ApplicationId": "APP-M-01238", "ApplicationStatus": "Approved", "CaseNumber": "CAS-01021-C4B5M8", "CustomerNumber": "00000060669", "AccountNumber": 178759935, "RequestUUID": null, "ErrorMessageList": [ ] }, "result": { "success": true, "code": "200", "message": "Success" ,"token":"sadsdddsd"} }');
    res.setStatusCode(200);
    return res;
  }
}