public class AssetDelete {

    //This is to delete the joint applicants Asset details if Joints are added to the application and then removed
    public static void deleteJointAssets(String appId,
                                         List<Asset_Details__c> lstDelete,
                                         Map<String,String> appJointData) {

        List<Asset_Details__c> lstResult = new List<Asset_Details__c>();
        List<Asset_Details__c> lstAssetDeleteJ1 = new List<Asset_Details__c>();
        List<Asset_Details__c> lstAssetDeleteJ2 = new List<Asset_Details__c>();
        List<Asset_Details__c> lstAssetDeleteJ3 = new  List<Asset_Details__c>();

        String jointOne = ( String.isNotBlank(appJointData.get('J1')) ? appJointData.get('J1') : 'True');
        String jointTwo= ( String.isNotBlank(appJointData.get('J2')) ? appJointData.get('J2') : 'True');
        String jointThree = ( String.isNotBlank(appJointData.get('J3')) ? appJointData.get('J3') : 'True');

        try{
                //Get all the Assets of the application
                if (String.isNotBlank(appId)) {
                    lstResult = [SELECT Id,Applicant_Type__c FROM Asset_Details__c WHERE Application__c =: appId LIMIT 5000];                  
                }
                else if (lstDelete.size() > 0)
                {
                    lstResult = lstDelete.clone();
                }

                if (lstResult.size() > 0) {

                    for( Asset_Details__c objAsset : lstResult ) {    

                            //Store the J1 Asset in a list                             
                            if ( objAsset.Applicant_Type__c == 'J1') {
                                lstAssetDeleteJ1.add(objAsset);
                            }

                            //Store the J2 Asset in a list                             
                            if ( objAsset.Applicant_Type__c == 'J2') {
                                lstAssetDeleteJ2.add(objAsset);
                            }

                            //Store the J3 Asset in a list                             
                            if ( objAsset.Applicant_Type__c == 'J3') {
                                lstAssetDeleteJ3.add(objAsset);
                            }
                    }

                    System.debug('lstAssetDeleteJ2 ==>' + lstAssetDeleteJ2);

                    //**********************************  Joint 1 ***************************************************
                    //If Joint 1 is removed, delete Joint 1 Asset from Asset details objects
                    if (jointOne == 'false' ) { 
                        
                        if (lstAssetDeleteJ1.size() > 0) { 
                            delete lstAssetDeleteJ1;
                        }
                                                                    
                    }

                    //**********************************  Joint 1 End ***************************************************


                    //**********************************  Joint 2 ***************************************************
                    //If Joint 2 is removed, delete Joint 2 Asset from Asset details objects              
                    if (jointTwo  == 'false') {
                        System.debug('Inside joint 2 delete ==>' + lstAssetDeleteJ2);
                        if (lstAssetDeleteJ2.size() > 0) { 
                            delete lstAssetDeleteJ2;
                        }
                    }
                    

                    //**********************************  Joint 2 End  ***************************************************


                    //**********************************  Joint 3 ***************************************************

                    //If Joint 3 is removed, delete Joint 3 Asset from Asset details objects  
                    if (jointThree  == 'false') {       
                        
                        if (lstAssetDeleteJ3.size() > 0) { 
                            delete lstAssetDeleteJ3;
                        }
                        
                    }

                    //**********************************  Joint 3 End  ***************************************************

                }



            }catch(exception ex) {
                System.debug('Excpetion in deleteJointAsset ' + ex.getMessage());
                System.debug('Excpetion in deleteJointAsset line number' + ex.getLineNumber());                
            }

    }
    
  
}