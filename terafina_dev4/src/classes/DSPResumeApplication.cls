public with sharing class DSPResumeApplication { 
    
    public TF4SF__Application__c app{get; set;}
    public String appId{get;set;}
    public User loggedInUser{get; set;}
    public String usr{get; set;}
    public String ut{get; set;}

    public DSPResumeApplication(ApexPages.StandardController stdController) {
        appId = ApexPages.currentPage().getParameters().get('id');
        usr = ApexPages.currentPage().getParameters().get('usr');
        app = [SELECT Id, TF4SF__Login__c, TF4SF__Password__c, TF4SF__Application_Status__c, TF4SF__Application_Page__c, TF4SF__Current_Channel__c, TF4SF__Current_timestamp__c, TF4SF__Current_Person__c, TF4SF__Current_Branch_Name__c, TF4SF__Current_User_Email_Address__c, TF4SF__User_Token__c, TF4SF__User_Token_Expires__c, TF4SF__Application_Version__c FROM TF4SF__Application__c WHERE Id =: appId AND CreatedDate != NULL];
        System.debug('app.TF4SF__Application_Page__c in constructor ==== ' + app.TF4SF__Application_Page__c);
        if (usr != null) { loggedInUser = [SELECT Id, Email, TF4SF__Channel__c, TF4SF__Location__c, Name FROM User WHERE Id = :usr AND CreatedDate != NULL]; }
        if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Current_timestamp__c.isCreateable() && Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Current_timestamp__c.isUpdateable()) { app.TF4SF__Current_timestamp__c = System.now(); }
        if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Current_Person__c.isCreateable() && Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Current_Person__c.isUpdateable()) { app.TF4SF__Current_Person__c = loggedInUser.Id; }
        if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Current_Branch_Name__c.isCreateable() && Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Current_Branch_Name__c.isUpdateable()) { app.TF4SF__Current_Branch_Name__c = loggedInUser.TF4SF__Location__c; }
        if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Current_User_Email_Address__c.isCreateable() && Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Current_User_Email_Address__c.isUpdateable()) { app.TF4SF__Current_User_Email_Address__c = loggedInUser.Email; }

        if (loggedInUser != null && loggedInUser.TF4SF__Channel__c != null) {
            if (Schema.sObjectType.TF4SF__Application__c.fields.Ownerid.isCreateable() && Schema.sObjectType.TF4SF__Application__c.fields.Ownerid.isUpdateable()) { app.Ownerid = loggedInUser.Id; }
            if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Current_Channel__c.isCreateable() && Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Current_Channel__c.isUpdateable()) { app.TF4SF__Current_Channel__c = loggedInUser.TF4SF__Channel__c; }
        } else {
            if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Current_Channel__c.isCreateable() && Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Current_Channel__c.isUpdateable()) { app.TF4SF__Current_Channel__c = 'Online'; }
        }
    }

    public PageReference ResumeApp() {

        PageReference pg = null;
        String url = 'tf4sf__dsp';
        System.debug('appId ==== ' + appId);
        System.debug('app.TF4SF__Application_Page__c ==== ' + app.TF4SF__Application_Page__c);
        if (!String.isBlank(appId)) {
            if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Application_Status__c.isUpdateable()) {
                if (app.TF4SF__Application_Status__c == 'Submitted') {
                } else { 
                    app.TF4SF__Application_Status__c = 'Open';
                }
            }
            if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Login__c.isUpdateable()) { app.TF4SF__Login__c = ''; }
            if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Password__c.isUpdateable()) { app.TF4SF__Password__c = ''; }
            if (Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Application_Page__c.isCreateable() && Schema.sObjectType.TF4SF__Application__c.fields.TF4SF__Application_Page__c.isUpdateable()) { app.TF4SF__Application_Page__c = app.TF4SF__Application_Page__c; }
            CryptoHelperCustom.setAppToken(app);
            if (TF4SF__Application__c.SObjectType.getDescribe().isUpdateable()) { update app; }

            TF4SF__About_Account__c AboutAccount = [SELECT Id, TF4SF__Routing_Number_CHK__c, TF4SF__CHK_Account_Number__c, TF4SF__Account_type_FI_CHK__c, 
            TF4SF__Custom_Picklist1__c, TF4SF__Custom_Text4__c, TF4SF__Custom_Text5__c, TF4SF__Dollar_Amount_External_CHK__c, TF4SF__Routing_Number_Sav__c, 
            TF4SF__SAV_Account_Number__c, TF4SF__Account_Type_FI_Sav__c, TF4SF__Custom_Picklist2__c, TF4SF__Custom_Text6__c, 
            TF4SF__Custom_Text7__c, TF4SF__Dollar_Amount_External_SAV__c, TF4SF__Application__c FROM TF4SF__About_Account__c WHERE TF4SF__Application__c =: ApexPages.currentPage().getParameters().get('id')];

            if (Schema.sObjectType.TF4SF__About_Account__c.fields.TF4SF__Routing_Number_CHK__c.isUpdateable()) { AboutAccount.TF4SF__Routing_Number_CHK__c = ''; }
            if (Schema.sObjectType.TF4SF__About_Account__c.fields.TF4SF__CHK_Account_Number__c.isUpdateable()) { AboutAccount.TF4SF__CHK_Account_Number__c = ''; }
            if (Schema.sObjectType.TF4SF__About_Account__c.fields.TF4SF__Account_type_FI_CHK__c.isUpdateable()) { AboutAccount.TF4SF__Account_type_FI_CHK__c = ''; }
            if (Schema.sObjectType.TF4SF__About_Account__c.fields.TF4SF__Custom_Picklist1__c.isUpdateable()) { AboutAccount.TF4SF__Custom_Picklist1__c = ''; }
            if (Schema.sObjectType.TF4SF__About_Account__c.fields.TF4SF__Custom_Text4__c.isUpdateable()) { AboutAccount.TF4SF__Custom_Text4__c = ''; }
            if (Schema.sObjectType.TF4SF__About_Account__c.fields.TF4SF__Custom_Text5__c.isUpdateable()) { AboutAccount.TF4SF__Custom_Text5__c = ''; }
            if (Schema.sObjectType.TF4SF__About_Account__c.fields.TF4SF__Dollar_Amount_External_CHK__c.isUpdateable()) { AboutAccount.TF4SF__Dollar_Amount_External_CHK__c = null; }
            if (Schema.sObjectType.TF4SF__About_Account__c.fields.TF4SF__Routing_Number_Sav__c.isUpdateable()) { AboutAccount.TF4SF__Routing_Number_Sav__c = ''; }
            if (Schema.sObjectType.TF4SF__About_Account__c.fields.TF4SF__SAV_Account_Number__c.isUpdateable()) { AboutAccount.TF4SF__SAV_Account_Number__c = ''; }
            if (Schema.sObjectType.TF4SF__About_Account__c.fields.TF4SF__Account_Type_FI_Sav__c.isUpdateable()) { AboutAccount.TF4SF__Account_Type_FI_Sav__c = ''; }
            if (Schema.sObjectType.TF4SF__About_Account__c.fields.TF4SF__Custom_Picklist2__c.isUpdateable()) { AboutAccount.TF4SF__Custom_Picklist2__c = ''; }
            if (Schema.sObjectType.TF4SF__About_Account__c.fields.TF4SF__Custom_Text6__c.isUpdateable()) { AboutAccount.TF4SF__Custom_Text6__c = ''; }
            if (Schema.sObjectType.TF4SF__About_Account__c.fields.TF4SF__Custom_Text7__c.isUpdateable()) { AboutAccount.TF4SF__Custom_Text7__c = ''; }
            if (Schema.sObjectType.TF4SF__About_Account__c.fields.TF4SF__Dollar_Amount_External_SAV__c.isUpdateable()) { AboutAccount.TF4SF__Dollar_Amount_External_SAV__c = null; }

            if (TF4SF__About_Account__c.SObjectType.getDescribe().isUpdateable()) { update AboutAccount; }


            System.debug('app = ' + app);
            System.debug('app status after update =====' + app.TF4SF__Application_Status__c);
            System.debug('app.TF4SF__Application_Page__c after update =====' + app.TF4SF__Application_Page__c);

            TF4SF__Application__c latestApp = [SELECT Id, TF4SF__User_Token__c, TF4SF__Application_Page__c FROM TF4SF__Application__c WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
            System.debug('latestApp.TF4SF__Application_Page__c ==== ' + latestApp.TF4SF__Application_Page__c);
            System.debug('Encrypted usertoken from app = ' + app.TF4SF__User_Token__c);
            System.debug('Encrypted usertoken from latestApp = ' + latestApp.TF4SF__User_Token__c);
            String userToken = CryptoHelperCustom.decrypt(latestApp.TF4SF__User_Token__c);
            System.debug('Decrypted usertoken from latestApp = '+ userToken);

            TF4SF__Application_Activity__c appact = new TF4SF__Application_Activity__c ();
            if (Schema.sObjectType.TF4SF__Application_Activity__c.fields.TF4SF__Application__c.isCreateable()) { appact.TF4SF__Application__c = app.Id; }
            if (Schema.sObjectType.TF4SF__Application_Activity__c.fields.TF4SF__Branch__c.isCreateable()) { appact.TF4SF__Branch__c = app.TF4SF__Current_Branch_Name__c; }
            if (Schema.sObjectType.TF4SF__Application_Activity__c.fields.TF4SF__Channel__c.isCreateable()) { appact.TF4SF__Channel__c = app.TF4SF__Current_Channel__c; }
            if (Schema.sObjectType.TF4SF__Application_Activity__c.fields.TF4SF__Name__c.isCreateable()) { appact.TF4SF__Name__c = app.TF4SF__Current_Person__c; }
            if (Schema.sObjectType.TF4SF__Application_Activity__c.fields.TF4SF__Action__c.isCreateable()) { appact.TF4SF__Action__c = 'Resumed the Application'; }
            if (Schema.sObjectType.TF4SF__Application_Activity__c.fields.TF4SF__Activity_Time__c.isCreateable()) { appact.TF4SF__Activity_Time__c = System.now(); }
            if (TF4SF__Application_Activity__c.SObjectType.getDescribe().isCreateable()) { insert appact; }

            TF4SF.Logger.inputSource('ResumeApplication - Resumeapp', String.escapeSingleQuotes(appId));
            TF4SF.Logger.addMessage('Redirecting to dsp page', System.now().format());
            if (!String.isBlank(app.TF4SF__Application_Page__c)) {
                if (app.TF4SF__Application_Page__c == 'GetStartedPage') { //GetStartedPage
                    pg = new PageReference(TF4SF__SiteUrl__c.getOrgDefaults().TF4SF__Url__c + url + '#get-started');

                } else if (app.TF4SF__Application_Page__c == 'SignupPage') { //SignupPage
                    pg = new PageReference(TF4SF__SiteUrl__c.getOrgDefaults().TF4SF__Url__c + url + '#signup');

                } else if (app.TF4SF__Application_Page__c == 'ProductSelectPage') { //ProductSelectPage
                    pg = new PageReference(TF4SF__SiteUrl__c.getOrgDefaults().TF4SF__Url__c + url + '#product-select');

                } else if (app.TF4SF__Application_Page__c == 'PersonalInfoPage') { //PersonalInfoPage
                    pg = new PageReference(TF4SF__SiteUrl__c.getOrgDefaults().TF4SF__Url__c + url + '#personal-info');

                } else if (app.TF4SF__Application_Page__c == 'EmploymentPage') { //EmploymentPage
                    pg = new PageReference(TF4SF__SiteUrl__c.getOrgDefaults().TF4SF__Url__c + url + '#employment');

                } else if (app.TF4SF__Application_Page__c == 'ReviewSubmitPage') { //ReviewSubmitPage
                    pg = new PageReference(TF4SF__SiteUrl__c.getOrgDefaults().TF4SF__Url__c + url + '#review-submit');

                } else if (app.TF4SF__Application_Page__c == 'DeclarationsPage' || app.TF4SF__Application_Page__c == 'KYCPage') { //DeclarationsPage & KYCPage
                    pg = new PageReference(TF4SF__SiteUrl__c.getOrgDefaults().TF4SF__Url__c + url + '#declarations');
                } else if (app.TF4SF__Application_Page__c == 'IdScanPage') { //IdScanPage
                    pg = new PageReference(TF4SF__SiteUrl__c.getOrgDefaults().TF4SF__Url__c + url + '#idscan');
                } else if (app.TF4SF__Application_Page__c == 'EmailVerificationPage') { //EmailVerificationPage
                    pg = new PageReference(TF4SF__SiteUrl__c.getOrgDefaults().TF4SF__Url__c + url + '#email-verification');

                } else if (app.TF4SF__Application_Page__c == 'AddressInfoPage') { //AddressInfoPage
                    pg = new PageReference(TF4SF__SiteUrl__c.getOrgDefaults().TF4SF__Url__c + url + '#address-info');

                } else if (app.TF4SF__Application_Page__c == 'PrevAddInfoPage') { //PrevAddInfoPage
                    pg = new PageReference(TF4SF__SiteUrl__c.getOrgDefaults().TF4SF__Url__c + url + '#prev-add-info');

                } else if (app.TF4SF__Application_Page__c == 'MaritalStatusPage') { //MaritalStatusPage
                    pg = new PageReference(TF4SF__SiteUrl__c.getOrgDefaults().TF4SF__Url__c + url + '#marital-status');

                } else if (app.TF4SF__Application_Page__c == 'DependentsPage') { //DependentsPage
                    pg = new PageReference(TF4SF__SiteUrl__c.getOrgDefaults().TF4SF__Url__c + url + '#dependents');

                } else if (app.TF4SF__Application_Page__c == 'IncomeSummaryPage') { //IncomeSummaryPage
                    pg = new PageReference(TF4SF__SiteUrl__c.getOrgDefaults().TF4SF__Url__c + url + '#income-summary');

                } else if (app.TF4SF__Application_Page__c == 'AssetSummaryPage') { //AssetSummaryPage
                    pg = new PageReference(TF4SF__SiteUrl__c.getOrgDefaults().TF4SF__Url__c + url + '#asset-summary');

                } else if (app.TF4SF__Application_Page__c == 'LiabilitiesSummaryPage') { //LiabilitiesSummaryPage
                    pg = new PageReference(TF4SF__SiteUrl__c.getOrgDefaults().TF4SF__Url__c + url + '#liabilities-summary');

                } else if (app.TF4SF__Application_Page__c == 'PurchaseDetailsPage') { //PurchaseDetailsPage
                    pg = new PageReference(TF4SF__SiteUrl__c.getOrgDefaults().TF4SF__Url__c + url + '#purchase-details');

                } else if (app.TF4SF__Application_Page__c == 'PropertyDetailsPage') { //PropertyDetailsPage
                    pg = new PageReference(TF4SF__SiteUrl__c.getOrgDefaults().TF4SF__Url__c + url + '#property-details');

                } else if (app.TF4SF__Application_Page__c == 'GMIQuestionPage') { //GMIQuestionPage
                    pg = new PageReference(TF4SF__SiteUrl__c.getOrgDefaults().TF4SF__Url__c + url + '#gmi-question');

                } else if (app.TF4SF__Application_Page__c == 'StartFundingPage' || app.TF4SF__Application_Page__c == 'AccountDetailsPage' || app.TF4SF__Application_Page__c == 'OnlineBankingPage' || app.TF4SF__Application_Page__c == 'FundingOtpPage' || app.TF4SF__Application_Page__c == 'FundingConfirmation' ) { //StartFundingPage
                    pg = new PageReference(TF4SF__SiteUrl__c.getOrgDefaults().TF4SF__Url__c + url + '#start-funding');
                } else if (app.TF4SF__Application_Page__c == 'NraPage') { //NraPage
                    pg = new PageReference(TF4SF__SiteUrl__c.getOrgDefaults().TF4SF__Url__c + url + '#nra');
                } else if (app.TF4SF__Application_Page__c == 'NraPassportPage') { //NraPassportPage
                    pg = new PageReference(TF4SF__SiteUrl__c.getOrgDefaults().TF4SF__Url__c + url + '#nra-passport');
                } else if (app.TF4SF__Application_Page__c == 'NraIdScanPage') { //NraIdScanPage
                    pg = new PageReference(TF4SF__SiteUrl__c.getOrgDefaults().TF4SF__Url__c + url + '#nra-idscan');
                } else { //Default Personal-Info Page Redirect
                    pg = new PageReference(TF4SF__SiteUrl__c.getOrgDefaults().TF4SF__Url__c + url + '#personal-info');
                } 
            } else { //Default Personal-Info Page Redirect if Application Page is Null
                pg = new PageReference(TF4SF__SiteUrl__c.getOrgDefaults().TF4SF__Url__c + url + '#personal-info');
            }
            Cookie id = ApexPages.currentPage().getCookies().get('id');
            Cookie ut = ApexPages.currentPage().getCookies().get('ut');
            id = new Cookie('id', app.Id, null, -1, true);
            ut = new Cookie('ut', userToken, null, -1, true);

            // Set the new cookie for the page
            ApexPages.currentPage().setCookies(new Cookie[]{id, ut});
        }
        pg.setRedirect(false);
        TF4SF.Logger.writeAllLogs();
        System.debug('PageReference before return ======== ' + pg);
        return pg;
    }
}