global with sharing class ValidatePromoCodeExtension implements TF4SF.DSP_Interface {
    
    global static Map<String, String> main(Map<String, String> tdata) {
        Map<String, String> data = tdata.clone();
        
        String Options = '';
        String valid= 'false',status='0',message='';
        String appId = tdata.get('id');
        String promoCode= tdata.get('promocode');    
        String UpdatePromo = '';   
        String Token = '';
        String GUID = '';
        String promoFirstName = '';
        String promoLastName = '';
        String promoEmail = ''; 
        String promoPhone = '';
        HttpResponse res2 = null;
        HttpRequest req2 = null;

        try{

        VELOSETTINGS__C velo = VELOSETTINGS__C.getOrgDefaults();
        Token = AuthToken.Auth();

        Http h2 = new Http();
        req2 = new HttpRequest();
        Integer timeOut = Integer.ValueOf(velo.ProcessAPI_Timeout__c);
        GUID = GUIDGenerator.getGUID();
        req2.setEndpoint(velo.Endpoint__c+'api/process/promotions/'+promoCode+'/verify');
        req2.setTimeout(timeOut);
        req2.setHeader(velo.Request_GUID_Name__c,GUID);
        req2.setHeader('Authorization','Bearer '+Token);
        req2.setHeader('Accept','application/json');
        req2.setMethod('GET');
        res2 = h2.send(req2);
        UpdatePromo = res2.getBody();
        system.debug('Res is '+res2.getbody());

        Map<String, Object> promoMap = (Map<String, Object>) JSON.deserializeUntyped(res2.getBody());
        system.debug('the PromoCodeMap is '+promoMap);
        Map<String, Object> dataMap = (Map<String, Object>)promoMap.get('data');
        Map<String, Object> promotionMap = (Map<String, Object>)dataMap.get('Promotion');
        system.debug('the Promotion is '+promotionMap);
        Map<String, Object> onBoardingMap = (Map<String, Object>)dataMap.get('OnboardingInvitation');
        system.debug('the OnboardingInvitation is '+onBoardingMap);
        String promoCodeValid = String.valueOf(promotionMap.get('PromoCode'));
        String promoCodeStatus = String.valueOf(promotionMap.get('Status'));
        promoFirstName = String.valueOf(onBoardingMap.get('FirstName'));
        promoLastName = String.valueOf(onBoardingMap.get('LastName'));
        promoEmail = String.valueOf(onBoardingMap.get('EmailAddress'));
        promoPhone = String.valueOf(onBoardingMap.get('PhoneNumber'));

        system.debug('the PromoCodeValid is '+promoCodeValid);
        system.debug('the PromoCodeStatus is '+promoCodeStatus);
        system.debug('the PromoEmail is '+promoEmail);
        system.debug('the PromoPhone is '+promoPhone);    
             
        data.put('Application__c.Promo_Code_Email__c',promoEmail);
        data.put('Application__c.Promo_Code_Phone_Number__c',formatPhone(promoPhone));       

        //Validate promo code  
        if(String.isNotBlank(promoCode) && promoCode == promoCodeValid)
        {
            if (promoCodeStatus == 'New') {
                valid='true';
                status='200';
            } else if (promoCodeStatus == 'InProgress') {
                valid = 'false';
                status = '201';
            } 
        } 
        
       }catch(Exception ex){
        message=ex.getMessage()+' Line: '+ex.getLineNumber();
        system.debug('$$$$$$$$$:'+message);
       }      
      
        Options += '{';
        Options += '"status": "'+status+'",'; 
        Options += '"statusMessage": "'+valid+'",';  
        Options += '"message": "'+message+'"';    
        Options += '}';
        system.debug('Options :' + Options);
        data.put('status',Options);
        updatePromoStatus(appId, promoCode, promoFirstName, promoLastName, promoEmail, promoPhone, Token);
        //createShadowAccount(appId, Token, phoneEmail, formatPhone(promoPhone));
        
        DebugLogger.InsertDebugLog(appId, GUID, 'ValidatePromoCode Request GUID');
        //DebugLogger.InsertDebugLog(appId, req2.getBody(), 'ValidatePromoCode Request');
        DebugLogger.InsertDebugLog(appId, UpdatePromo, 'ValidatePromoCode Response');
        
        return data; 
    }

    //@future(callout=true) //Nag: This call has to be fired fist to fire createShadow account in synchronous way
    public static void updatePromoStatus(String appId, String PromoCode, String FirstName, String LastName, String Email, String PhoneNumber, String Token) {
        system.debug('the token '+Token);
        system.debug('the procoCode '+PromoCode);
        try{
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            VELOSETTINGS__C velo = VELOSETTINGS__C.getOrgDefaults();
            Integer timeOut = Integer.ValueOf(velo.ProcessAPI_Timeout__c);
            String GUID = GUIDGenerator.getGUID();
            req.setTimeout(timeOut);
            req.setHeader(velo.Request_GUID_Name__c,GUID);
            req.setEndpoint(velo.Endpoint__c+'api/process/promotions/'+PromoCode);
            req.setHeader('Authorization','Bearer '+Token);
            req.setHeader('Accept','application/json');
            req.setHeader('Content-Type', 'application/json');
            String body = '{';
                    body += '"Promotion": {';
                        body += '"Status": "InProgress"';
                    body += '},';
                    body += '"OnboardingInvitation": {';
                        body += '"FirstName": "'+FirstName+'",';
                        body += '"LastName": "'+LastName+'",';
                        body += '"EmailAddress": "'+Email+'",';
                        body += '"PhoneNumber": "'+PhoneNumber+'"';
                    body += '},';
                    body += '"RequestUUID": "'+getRandomString(25)+'"';
                    body += '}';
            req.setBody(body);
            req.setMethod('PUT');
            HttpResponse res = h.send(req);
            system.debug('Req is '+req.getbody());
            system.debug('Res is '+res.getbody());

            DebugLogger.InsertDebugLog(appId, GUID, 'UpdatePromoStatus Request GUID');
            DebugLogger.InsertDebugLog(appId, req.getBody(), 'UpdatePromoStatus Request'); 
            DebugLogger.InsertDebugLog(appId, res.getBody(), 'UpdatePromoStatus Response');
        }catch(Exception ex){
            system.debug('exception :' + ex);
        }
    }


    public static string formatPhone(String phNum) {
        String phoneNumber = '';
        string nondigits = '[^0-9]';
        string PhoneDigits;
          
          // remove all non numeric
          PhoneDigits = phNum.replaceAll(nondigits,'');
          
          // 10 digit: reformat with dashes
          if (PhoneDigits.length() == 10){ 
            phoneNumber = '('+PhoneDigits.substring(0,3)+') ' + PhoneDigits.substring(3,6) + '-' + PhoneDigits.substring(6,10);
          }else{
            phoneNumber =  PhoneDigits;
          }
          // 11 digit: if starts with 1, format as 10 digit 
          //if (PhoneDigits.length() == 11) {
          //  if (PhoneDigits.substring(0,1) == '1') {
          //    phoneNumber =  PhoneDigits.substring(1,4) + '-' +
          //            PhoneDigits.substring(4,7) + '-' +
          //            PhoneDigits.substring(7,11);
          //  }
          //}
        return phoneNumber;
    }

    private static final String RANDOM_CHARS = 
      'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    
    public static String getRandomString(Integer len) {
        String mode = String.valueOf(RANDOM_CHARS.length() - 1);
        String retVal = '';
        if (len != null && len >= 1) {
            Integer chars = 0;
            Integer random;
            do {
                random = Math.round(Math.random() * Integer.valueOf(mode));
                retVal += RANDOM_CHARS.substring(random, random + 1);
                chars++;
            } while (chars < len);
        }

        return retVal;
    }

}