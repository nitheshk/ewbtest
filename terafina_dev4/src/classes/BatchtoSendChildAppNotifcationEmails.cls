global class BatchtoSendChildAppNotifcationEmails implements 
Database.Batchable <sObject> , Database.Stateful, Database.AllowsCallouts {


  global Database.QueryLocator start(Database.BatchableContext bc) {
    
    String query =  'SELECT Id, Email_Address__c, First_Name__c,Last_Name__c,';
           query += ' Email_Sent__c,Email_Sent_Date__c';           
           query += ' FROM Trustee_Guarantor__c WHERE Email_Sent__c= False AND Child_Application__c != Null';
          
        return Database.getQueryLocator(query);      
  }

  global void execute(Database.BatchableContext bc, List<Trustee_Guarantor__c> scope) {
    
    //process each batch of records
    try {        
        
          //Call Send reminder email function
          ChildApplication.sendEmail(scope);
      
    } catch (exception e) {
        System.debug('Exception in BatchtoSendChildAppReminderEmails ==>' + e.getMessage());
    } finally {
     
    }
  }

  global void finish(Database.BatchableContext bc) {

  }

 
   
}