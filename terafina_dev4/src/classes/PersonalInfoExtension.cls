global with sharing class PersonalInfoExtension implements TF4SF.DSP_Interface {
    
    global static Map<String, String> main(Map<String, String> tdata) {
        System.debug('In PersonalInfoExtension ==>');
        Map<String, String> data = tdata.clone();
        List<Income_Details__c> lstIncome= new List<Income_Details__c>();
        List<Asset_Details__c> lstAsset= new List<Asset_Details__c>();
        List<Liability_Details__c> lstLiability= new List<Liability_Details__c>();

        String appId = data.get('id');

        String jointOne = data.get('Application__c.First_Joint_Applicant__c') ;
        String jointTwo =  data.get('Application__c.Second_Joint_Applicant__c') ;
        String jointThree = data.get('Application__c.Third_Joint_Applicant__c') ;

        try {
            if (String.isNotBlank(appId)) {
                //TF4SF__Application__c app = [SELECT Id, Name, TF4SF__Product__c, TF4SF__Sub_Product__c,TF4SF__First_Joint_Applicant__c, TF4SF__Second_Joint_Applicant__c, TF4SF__Third_Joint_Applicant__c FROM TF4SF__Application__c WHERE Id =: appId];
                //System.debug('app ==>' + app);              
          
				//Put the Joint  application flag to a map           
	            Map<String,String> appJointData = new Map<String,String>();
	            appJointData.put('J1' ,jointOne);
	            appJointData.put('J2' ,jointTwo);
	            appJointData.put('J3' ,jointThree);

	            //********************************** Income delete start **************************************
	            //Delete joints income details if Joints are added and then removed from the application
	            IncomeDelete.deleteJointIncome(appId,lstIncome,appJointData);
	            
	            //********************************** Income delete end *****************************************

	            //********************************** Assets delete start **************************************
	            //Delete joints income details if Joints are added and then removed from the application
	            AssetDelete.deleteJointAssets(appId,lstAsset,appJointData);
	            
	            //********************************** Assets delete end *****************************************

	            //********************************** Liability delete start **************************************
	            //Delete joints income details if Joints are added and then removed from the application
	            LiabilityDelete.deleteJointLiabilities(appId,lstLiability,appJointData);
	            
	            //********************************** Liability delete end *****************************************
        	}

        } catch(exception ex) {
            System.debug('Excpetion in PersonalInfoExtension ' + ex.getMessage());
            System.debug('Excpetion in PersonalInfoExtension line number' + ex.getLineNumber());
        }

        return data;
    }
}