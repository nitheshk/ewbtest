@isTest
global class W8TaxFormControllerTest{

@isTest static void test_one(){

String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
      Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
      User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
      EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US', ProfileId = p.Id,
      TimeZoneSidKey = 'America/Los_Angeles',
      UserName = uniqueUserName);
      
      System.runAs(u) {
        List<TF4SF__Application__c> appList =new List<TF4SF__Application__c>();       

        TF4SF__Application__c app = new TF4SF__Application__c ();
        app.TF4SF__Product__c = 'Checking';
        app.TF4SF__Sub_Product__c = 'Checking - Checking';
        //app.TF4SF__Full_Name_PA__c='testfullname';  
        app.TF4SF__First_Name__c='firstname';
        app.TF4SF__Last_Name__c='lastname';
        app.TF4SF__City__c='testcity';
        app.TF4SF__Zip_Code__c='testzip';
        app.TF4SF__State__c='teststate';
        app.TF4SF__Street_Address_1__c='teststreet1';
        app.TF4SF__Street_Address_2__c='teststreet2';
        appList.add(app);
        insert appList;
        
        TF4SF__Identity_Information__c iden = new TF4SF__Identity_Information__c ();
        iden.TF4SF__SSN_Prime__c='43566465'; 
        iden.TF4SF__Application__c=app.Id;
        insert iden;
        
        PageReference pageRef = Page.w8TaxFormPdf;
        Test.setCurrentPage(pageRef);     
        pageRef.getParameters().put('Id',app.id); 
        pageRef.getParameters().put('p','p');       
                   
        test.startTest();
        W8TaxFormController w8Formtax=new  W8TaxFormController();
        w8Formtax.getRenderAs();
        test.stopTest();
}
}
}