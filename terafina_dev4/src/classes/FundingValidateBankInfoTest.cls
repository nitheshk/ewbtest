@isTest
public class FundingValidateBankInfoTest {

@isTest static void Test_one(){
Map<String, String> tdata = new Map<String, String>();

 String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
      Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
      User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
      EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US', ProfileId = p.Id,
      TimeZoneSidKey = 'America/Los_Angeles',
      UserName = uniqueUserName);
      System.runAs(u) {

     TF4SF__Application__c app = new TF4SF__Application__c ();
        app.TF4SF__Product__c = 'Checking';
        app.TF4SF__Last_Name__c='TestFirst';
        app.TF4SF__First_Name__c='TestLast';
        app.TF4SF__Sub_Product__c = 'Checking - Checking';
        app.TF4SF__Primary_Phone_Number__c='5464655559';
        app.TF4SF__Street_Address_1__c='TestStreet1';
        app.TF4SF__City__c='TestCity';
        app.TF4SF__State__c='TestState';
        app.TF4SF__Zip_Code__c='43564';
        app.TF4SF__Email_Address__c='Test@gmail.com';  
        insert app;
          
     TF4SF__Application2__c app2=new TF4SF__Application2__c();
          app2.TF4SF__Application__c=app.id;
          insert app2;
          
        TF4SF__Identity_Information__c iden=new TF4SF__Identity_Information__c();
        iden.TF4SF__Application__c=app.id; 
        iden.TF4SF__Date_Of_Birth__c='12/34/190';
          iden.TF4SF__SSN_Prime__c='test';
          insert iden;
          
        TF4SF__Employment_Information__c emp=new TF4SF__Employment_Information__c();
         emp.TF4SF__Application__c=app.id; 
          insert emp;
          
        TF4SF__About_Account__c ab = new TF4SF__About_Account__c();
        ab.TF4SF__Application__c = app.id;
        ab.TF4SF__Routing_Number_CHK__c='23';
        ab.TF4SF__Routing_Number_SAV__c='45';  
        insert ab;
          
        
        VELOSETTINGS__C vo = new VELOSETTINGS__C();
        vo.Name='test';
        vo.TokenEndPoint__c = 'rsgsdgsdf';
        vo.Request_GUID_Name__c = 'RequestUUID';
        vo.ProcessAPI_Timeout__c = 120000;
        insert vo;
        
        tdata.put('id',(String)app.id);
        tdata.put('Application__c.Product__c','Checking');
        tdata.put('About_Account__c.Routing_Number_CHK__c','999999998');
        tdata.put('About_Account__c.Routing_Number_SAV__c','999999998');
       
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new FundingValidateBankInfoMock());                          
        FundingValidateBankInfo fv=new FundingValidateBankInfo(); 
        FundingValidateBankInfo.main(tdata);
        FundingValidateBankInfo.ValidateBankInformation(app.Id,'Token','999999998');
        test.stopTest();

}
}
}