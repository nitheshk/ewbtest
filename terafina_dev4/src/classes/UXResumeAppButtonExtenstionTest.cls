@isTest(seeAlldata=true)
public with sharing class UXResumeAppButtonExtenstionTest {
@isTest
public static void test1(){

	String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
    Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
    User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
    EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
    LocaleSidKey = 'en_US', ProfileId = p.Id,
    TimeZoneSidKey = 'America/Los_Angeles',
    UserName = uniqueUserName);
    System.runAs(u) {
    
    TF4SF__Application__c app1=new TF4SF__Application__c ();
    app1.TF4SF__Application_Status__c='Submitted';
    app1.TF4SF__Online_Banking_Enrollment__c=false; 
    app1.TF4SF__Check_Order__c=false;
    app1.TF4SF__ATM_Card__c=true;
    app1.TF4SF__Custom_DateTime2__c = System.now();
    insert app1;

    Group q = new Group(Name = 'Check Order Enrollment Complete', type = 'Queue');
    insert q;
            
    QueueSObject qs = new QueueSObject();
    qs.SobjectType='Lead';
    qs.QueueId = q.Id;
    insert qs;
        
    PageReference pageRef = Page.UX_ResumeApp_Button;
    Test.setCurrentPage(pageRef);
    pageRef.getParameters().put('Id',app1.id); 
    ApexPages.StandardController stc = new ApexPages.StandardController(app1);
    
    test.startTest();
    UXResumeAppButtonExtenstion uxapp=new UXResumeAppButtonExtenstion (stc);
    test.stopTest();
 
}
}
 @isTest
public static void test2(){
    
    String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
    Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
    User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
    EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
    LocaleSidKey = 'en_US', ProfileId = p.Id,
    TimeZoneSidKey = 'America/Los_Angeles',
    UserName = uniqueUserName);
    System.runAs(u) {
    
    TF4SF__Application__c app1=new TF4SF__Application__c ();
    app1.TF4SF__Application_Status__c='Submitted';
    app1.TF4SF__Online_Banking_Enrollment__c=false; 
    app1.TF4SF__Check_Order__c=false;
    app1.TF4SF__ATM_Card__c=true;
    app1.TF4SF__Custom_DateTime2__c = System.now();
    
    insert app1;

    Group q = new Group(Name = 'Debit Card Enrollment Complete', type = 'Queue');
    insert q;
        
     QueueSObject qs=new QueueSObject();
     qs.SobjectType='Lead';
     qs.QueueId=q.Id;
     insert qs;
        
    
    
    PageReference pageRef = Page.UX_ResumeApp_Button;
    Test.setCurrentPage(pageRef);
    pageRef.getParameters().put('Id',app1.id); 
    ApexPages.StandardController stc = new ApexPages.StandardController(app1);
        
    test.startTest();
    UXResumeAppButtonExtenstion uxapp=new UXResumeAppButtonExtenstion (stc);
    test.stopTest();                        
}  
}
    @isTest
public static void test3(){
    
    String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
    Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
    User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
    EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
    LocaleSidKey = 'en_US', ProfileId = p.Id,
    TimeZoneSidKey = 'America/Los_Angeles',
    UserName = uniqueUserName);
    System.runAs(u) {
    
    TF4SF__Application__c app1=new TF4SF__Application__c ();
    app1.TF4SF__Application_Status__c='Submitted';
    app1.TF4SF__Online_Banking_Enrollment__c=true; 
    app1.TF4SF__Check_Order__c=false;
    app1.TF4SF__ATM_Card__c=true;
    app1.TF4SF__Custom_DateTime2__c = System.now();
    insert app1;

    Group q = new Group(Name = 'Debit Card Enrollment', type = 'Queue');
    insert q;
    
    QueueSObject qs=new QueueSObject();
    qs.SobjectType='Lead';
    qs.QueueId = q.Id;
    insert qs;
        
   
    PageReference pageRef = Page.UX_ResumeApp_Button;
    Test.setCurrentPage(pageRef);
    pageRef.getParameters().put('Id',app1.id); 
    ApexPages.StandardController stc = new ApexPages.StandardController(app1);
        
    test.startTest();
    UXResumeAppButtonExtenstion uxapp=new UXResumeAppButtonExtenstion (stc);
    test.stopTest();                        
}  
}

 @isTest
public static void test4(){
    
    String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
    Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
    User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
    EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
    LocaleSidKey = 'en_US', ProfileId = p.Id,
    TimeZoneSidKey = 'America/Los_Angeles',
    UserName = uniqueUserName);
    System.runAs(u) {
    
    TF4SF__Application__c app1=new TF4SF__Application__c ();
    app1.TF4SF__Application_Status__c='Submitted';
    app1.TF4SF__Online_Banking_Enrollment__c=true; 
    app1.TF4SF__Check_Order__c=false;
    app1.TF4SF__ATM_Card__c=true;
    app1.TF4SF__Custom_DateTime2__c = System.now();
    insert app1;

    Group q = new Group(Name = 'Check Order Enrollment Complete', type = 'Queue');
    insert q;
        
    QueueSObject qs=new QueueSObject();
    qs.SobjectType='Lead';
    qs.QueueId=q.ID;
    insert qs;

        
   
    PageReference pageRef = Page.UX_ResumeApp_Button;
    Test.setCurrentPage(pageRef);
    pageRef.getParameters().put('Id',app1.id); 
    ApexPages.StandardController stc = new ApexPages.StandardController(app1);
        
    test.startTest();
    UXResumeAppButtonExtenstion uxapp=new UXResumeAppButtonExtenstion (stc);
    test.stopTest();                        
}  
}
    @isTest public static void test5(){
    
    String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
    Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
    User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
    EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
    LocaleSidKey = 'en_US', ProfileId = p.Id,
    TimeZoneSidKey = 'America/Los_Angeles',
    UserName = uniqueUserName);
    System.runAs(u) {
    
    TF4SF__Application__c app1=new TF4SF__Application__c ();
    app1.TF4SF__Application_Status__c='Submitted';
    app1.TF4SF__Online_Banking_Enrollment__c=false; 
    app1.TF4SF__Check_Order__c=true;
    app1.TF4SF__ATM_Card__c=false;
    app1.TF4SF__Custom_DateTime2__c = null;
    app1.TF4SF__Custom_DateTime3__c = System.now();
    insert app1;

    Group q = new Group(Name = 'Debit Card Enrollment Complete', type = 'Queue');
    insert q;
     
    QueueSObject qs=new QueueSObject();
    qs.SobjectType='Lead';
    qs.QueueId=q.Id;
    insert qs;

    PageReference pageRef = Page.UX_ResumeApp_Button;
    Test.setCurrentPage(pageRef);
    pageRef.getParameters().put('Id',app1.id); 
    ApexPages.StandardController stc = new ApexPages.StandardController(app1);
        
    test.startTest();
    UXResumeAppButtonExtenstion uxapp=new UXResumeAppButtonExtenstion (stc);
    test.stopTest();                        
}  
}

@isTest public static void test6(){
    
    String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
    Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
    User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
    EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
    LocaleSidKey = 'en_US', ProfileId = p.Id,
    TimeZoneSidKey = 'America/Los_Angeles',
    UserName = uniqueUserName);
    System.runAs(u) {
    
    TF4SF__Application__c app1=new TF4SF__Application__c ();
    app1.TF4SF__Application_Status__c='Submitted';
    app1.TF4SF__Online_Banking_Enrollment__c=false; 
    app1.TF4SF__Check_Order__c=false;
    app1.TF4SF__ATM_Card__c=true;
    app1.TF4SF__Custom_DateTime2__c = System.now();
    app1.TF4SF__Custom_DateTime3__c = null;
    insert app1;

    Group q = new Group(Name = 'Debit Card Enrollment Complete', type = 'Queue');
    insert q;
         
    QueueSObject qs=new QueueSObject();
    qs.SobjectType='Lead';
    qs.QueueId=q.Id;
    insert qs;
   
    PageReference pageRef = Page.UX_ResumeApp_Button;
    Test.setCurrentPage(pageRef);
    pageRef.getParameters().put('Id',app1.id); 
    ApexPages.StandardController stc = new ApexPages.StandardController(app1);
        
    test.startTest();
    UXResumeAppButtonExtenstion uxapp=new UXResumeAppButtonExtenstion (stc);
    test.stopTest();                        
}  
}

    @isTest public static void test7(){
    
    String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
    Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
    User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
    EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
    LocaleSidKey = 'en_US', ProfileId = p.Id,
    TimeZoneSidKey = 'America/Los_Angeles',
    UserName = uniqueUserName);
    System.runAs(u) {
    
    TF4SF__Application__c app1=new TF4SF__Application__c ();
    app1.TF4SF__Application_Status__c='Submitted';
    app1.TF4SF__Online_Banking_Enrollment__c=true; 
    app1.TF4SF__Check_Order__c=true;
    app1.TF4SF__ATM_Card__c=false;
    app1.TF4SF__Custom_DateTime2__c = System.now();
    app1.TF4SF__Custom_DateTime3__c = System.now();
    insert app1;

    Group q = new Group(Name = 'Debit Card Enrollment Complete', type = 'Queue');
    insert q;
     
    QueueSObject qs=new QueueSObject();
    qs.SobjectType='Lead';
    qs.QueueId=q.Id;
    insert qs;

    PageReference pageRef = Page.UX_ResumeApp_Button;
    Test.setCurrentPage(pageRef);
    pageRef.getParameters().put('Id',app1.id); 
    ApexPages.StandardController stc = new ApexPages.StandardController(app1);
        
    test.startTest();
    UXResumeAppButtonExtenstion uxapp=new UXResumeAppButtonExtenstion (stc);
    test.stopTest();                        
}  
}
}