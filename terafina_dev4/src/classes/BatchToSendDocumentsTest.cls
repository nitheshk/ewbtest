@isTest
public class BatchToSendDocumentsTest{
    
    @isTest static void method_one(){
        
        Set<String> Codes = new Set<String>();
        List<String> disclosureNames = new List<String>();
        
        List<TF4SF__Application__c> appLst = new List<TF4SF__Application__c> ();
        TF4SF__Application__c app = new TF4SF__Application__c ();
        app.TF4SF__Application_Status__c = 'Open';
        app.TF4SF__Product__c = 'Checking';
        app.TF4SF__Sub_Product__c =  'Home Loan - Short App';
        app.Documents_Posted__c=false;
        app.Documents_Posted_Timestamp__c=System.now();
        insert app;
        
		disclosureNames.add('Premier Checking');  
        
        VELOSETTINGS__C vo = new VELOSETTINGS__C();
        vo.Name='test';
        vo.EndPoint__c = 'api/process/products';
        vo.Request_GUID_Name__c = 'RequestUUID';
        vo.ProcessAPI_Timeout__c = 120000;
        insert vo;

        appLst.add(app);
        
        Blob b = Blob.valueOf('Test Data');
        Attachment att = new Attachment();
        att.name = 'Front';
        att.Body = b;
        att.isPrivate = false;
        att.Description = 'W9TaxForm';
        att.ParentId = app.id;
        att.ContentType = 'application/pdf';
        insert(att);
        
        Test.setMock(HttpCalloutMock.class, new BatchToSendDocumentsMock()); 
        
        Database.QueryLocator QL;
        Database.BatchableContext BC;
        BatchToSendDocuments instance1 = new BatchToSendDocuments ();
        //BatchToSendDocuments.sendDocument(att);
        instance1.start(BC);
        instance1.execute(BC,appLst);
        instance1.finish(BC);
    }
}