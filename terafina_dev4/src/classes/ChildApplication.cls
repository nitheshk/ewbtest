public  class ChildApplication {

    public static void createChildApplication(List<Trustee_Guarantor__c> listTRGU,                                           
                                               Map<Id,TF4SF__About_Account__c> mapAboutAccount) {

        Map<String,Trustee_Guarantor__c> mapTRGU =  new Map<String,Trustee_Guarantor__c>();
        List<TF4SF__Application__c> lstChildApplication = new List<TF4SF__Application__c>();
        List<Trustee_Guarantor__c> lstTRGUUpdate = new List<Trustee_Guarantor__c>();
        List<TF4SF__About_Account__c> lstAboutAccount = new List<TF4SF__About_Account__c>();
        List<TF4SF__Identity_Information__c> lstAIdentity = new List<TF4SF__Identity_Information__c>();
        List<TF4SF__Employment_Information__c> lstEmployment = new List<TF4SF__Employment_Information__c>();
        List<TF4SF__Application2__c> lstApplicationTwo = new List<TF4SF__Application2__c>();

        try{

            
                for(Trustee_Guarantor__c obj : listTRGU) {
                    TF4SF__Application__c objChildApp = new TF4SF__Application__c();


                    objChildApp.Parent_Application__c = obj.Application__c; 
                    objChildApp.TF4SF__Application_Status__c = 'Save for later';
                    objChildApp.TF4SF__Product__c = obj.Application__r.TF4SF__Product__c;//'Home Loan';
                    objChildApp.TF4SF__Sub_Product__c =  obj.Application__r.TF4SF__Sub_Product__c; //'Home Loan - 15 Year Fixed';
                    objChildApp.TF4SF__Custom_Text2__c = 'Single Application'; //Application Type
                    objChildApp.TF4SF__Email_Address__c = obj.Email_Address__c;
                    objChildApp.TF4SF__First_Name__c = obj.First_Name__c;
                    objChildApp.TF4SF__Middle_Name__c = obj.Middle_Name__c;
                    objChildApp.TF4SF__Last_Name__c = obj.Last_Name__c;
                    objChildApp.TF4SF__Suffix__c = obj.Suffix__c;
                    objChildApp.TF4SF__Primary_Phone_Number__c = obj.Phone_Number__c;   

                    //If parent application is Trustee/Guarentor set the flag in child application to true
                    if (obj.Application__r.TF4SF__Custom_Text2__c == 'Business Entity' || obj.Application__r.TF4SF__Custom_Text2__c == 'Trust') {
                        objChildApp.Trustee_Guarantor_Application__c = True;
                    }   
                    
                    //Set the page flow so that PurchaseDetailsPage,PropertyDetailsPage shows as completed
                    //by default
                    objChildApp.Page_Flow_Completed__c = 'PurchaseDetailsPage,PropertyDetailsPage';
                    

                    lstChildApplication.add(objChildApp);

                    //Add each Trustee/Guarantor to a map in order to update the row with child Application ID later
                    String strKey = obj.First_Name__c + obj.Email_Address__c + obj.Phone_Number__c;
                    mapTRGU.put(strKey,obj);
                }

                //Create Child Application
                if ( lstChildApplication.size() > 0) {
                    insert  lstChildApplication;
                }


                //Create records in other necessary objects

                System.debug('lstApplication ==> ' + lstChildApplication);

                //Loop through the newly created child applications to get the application ID
                //Update the child application ID of each Trustee/Guarantor
                for (TF4SF__Application__c  obj :lstChildApplication) {
                    Trustee_Guarantor__c objTRGU = new Trustee_Guarantor__c();

                    String strKey = obj.TF4SF__First_Name__c + obj.TF4SF__Email_Address__c + obj.TF4SF__Primary_Phone_Number__c;
                    objTRGU = mapTRGU.get(strKey);

                    if( objTRGU != NULL) {
                        //Get the newly created child application ID and assign it to Child Application ID field of 
                        //Trustee/Guarantor 
                        objTRGU.Child_Application__c = obj.Id;
                        lstTRGUUpdate.add(objTRGU);
                    } 

                    //Create records in other necessary objects
                    //About Account                 
                    TF4SF__About_Account__c objAc = new TF4SF__About_Account__c();

                    //Get About account details from the map and assign the values to child application
                    TF4SF__About_Account__c objACfromMap = mapAboutAccount.get(obj.Parent_Application__c);
                    if(objAC != Null || !test.isRunningTest()) {
                        objAc.TF4SF__Application__c = obj.Id;
                        //Purchase Details
                        objAc.TF4SF__Mortgage_Applied_For__c = objACfromMap.TF4SF__Mortgage_Applied_For__c;
                        objAc.TF4SF__Purchase_P__c = objACfromMap.TF4SF__Purchase_P__c;
                        objAc.TF4SF__Down_Payment__c = objACfromMap.TF4SF__Down_Payment__c;
                        objAc.Down_Payment_By_Percentage__c = objACfromMap.Down_Payment_By_Percentage__c;
                        objAc.TF4SF__Total_Loan_Amount__c = objACfromMap.TF4SF__Total_Loan_Amount__c;

                        //Property Details
                        objAc.TF4SF__Street_Address_1_AboutAccount__c = objACfromMap.TF4SF__Street_Address_1_AboutAccount__c;
                        objAc.TF4SF__Street_Address_2_AboutAccount__c = objACfromMap.TF4SF__Street_Address_2_AboutAccount__c;
                        objAc.TF4SF__City_AboutAccount__c = objACfromMap.TF4SF__City_AboutAccount__c;
                        objAc.TF4SF__State_AboutAccount__c = objACfromMap.TF4SF__State_AboutAccount__c;
                        objAc.TF4SF__Zip_Code_Mrt__c = objACfromMap.TF4SF__Zip_Code_Mrt__c;
                        objAc.TF4SF__County_AboutAccount__c = objACfromMap.TF4SF__County_AboutAccount__c;
                        objAc.MRT_Built_Year__c = objACfromMap.MRT_Built_Year__c;
                        objAc.TF4SF__Property_Type__c = objACfromMap.TF4SF__Property_Type__c;
                        objAc.TF4SF__Occupancy__c = objACfromMap.TF4SF__Occupancy__c;

                    }

                    //Identity information
                    TF4SF__Identity_Information__c objIden = new TF4SF__Identity_Information__c();
                    objIden.TF4SF__Application__c = obj.Id;

                    //Employment Inforation
                    TF4SF__Employment_Information__c objEmp = new TF4SF__Employment_Information__c();
                    objEmp.TF4SF__Application__c = obj.Id;

                    //Application 2
                    TF4SF__Application2__c objAppTwo = new TF4SF__Application2__c();
                    objAppTwo.TF4SF__Application__c = obj.Id;

                    
                    lstAboutAccount.add(objAc);
                    lstAIdentity.add(objIden);
                    lstEmployment.add(objEmp);
                    lstApplicationTwo.add(objAppTwo);

                }



                //Update Truste/Guarantor object
                if ( lstTRGUUpdate.size() > 0) {
                    update lstTRGUUpdate;
                }


                //Insert other necessary object
                if (lstAboutAccount.size() > 0) {
                    insert lstAboutAccount;
                }

                if (lstAIdentity.size() > 0) {
                    insert lstAIdentity;
                }

                if (lstEmployment.size() > 0) {
                    insert lstEmployment;
                }

                if (lstApplicationTwo.size() > 0) {
                    insert lstApplicationTwo;
                }

                //Send email notification
                sendEmail(listTRGU);
                
            
        } catch (Exception ex) {
            System.debug('Exception in createChildApplication ' + ex.getMessage());
            System.debug('Exception in createChildApplication Line number' + ex.getLineNumber());
        }
    }



    
    //This method is used to send out Email notifications to Trustee/Guarentor after child applications are created
    // Called from <>
    public static void sendEmail(List<Trustee_Guarantor__c> lstTRGR) {


        //List<Trustee_Guarantor__c> lstResult = [SELECT Id, Child_Application__c , Child_Application_Completed__c, Email_Address__c, First_Name__c,Last_Name__c, Phone_Number__c,Email_Sent__c,Email_Sent_Date__c FROM Trustee_Guarantor__c WHERE Email_Sent__c= False];   

        String conId;
        Contact con = new Contact(); // Create a master list to hold the emails we'll send
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        List<Trustee_Guarantor__c> lstTRGUUpdate = new  List<Trustee_Guarantor__c>();

      try {

            //Get the email templates 
            List<EmailTemplate> etTrustee = [SELECT Id, Name,  DeveloperName 
                                            FROM EmailTemplate 
                                            WHERE DeveloperName = 'Mrtgage_Child_Application_Email_Trustee' LIMIT 1];

            List<EmailTemplate> etBusiness = [SELECT Id, Name,  DeveloperName 
                                             FROM EmailTemplate 
                                             WHERE DeveloperName = 'Mrtgage_Child_Application_Email_Business' LIMIT 1];

            List<EmailTemplate> etCoBorrower = [SELECT Id, Name,  DeveloperName 
                                              FROM EmailTemplate 
                                              WHERE DeveloperName = 'Mrtgage_Child_Application_Email_Co_brwr' LIMIT 1]; 



          //Get the application ID's from ACH Transfer list
         
          if (lstTRGR.size() > 0) {
              if (Schema.sObjectType.Contact.fields.FirstName.isCreateable()){
                  if ( String.isNotBlank(lstTRGR[0].First_Name__c) ) {
                      con.FirstName = lstTRGR[0].First_Name__c;
                  } else {
                      con.FirstName = 'FirstName';
                  }
              }
              
              if (Schema.sObjectType.Contact.fields.LastName.isCreateable()){
                  if ( String.isNotBlank(lstTRGR[0].Last_Name__c)) {
                      con.LastName = lstTRGR[0].Last_Name__c;
                  } else {
                      con.LastName = 'LastName';
                  }
              }
              
              if (Schema.sObjectType.Contact.fields.Email.isCreateable()){
                  if ( String.isNotBlank(lstTRGR[0].Email_Address__c)) {
                      con.Email = lstTRGR[0].Email_Address__c;
                  }
              }
          } 

          //Insert Temp Contact
          insert con;
          conId = con.Id; 
          //Fetching Org Wide Email Address
          OrgWideEmailAddress owea = [SELECT Id,Address FROM OrgWideEmailAddress LIMIT 1]; 
        

            if(etTrustee.size() > 0 && etBusiness.size() > 0 && etCoBorrower.size() > 0) {
                //Loop through the applications and build email
                for (Trustee_Guarantor__c obj: lstTRGR) {

                    if (String.isNotBlank(obj.Email_Address__c)) {

						
                        if(String.isNotBlank(obj.Application__r.TF4SF__Custom_Text2__c) || Test.isRunningTest()) {
                            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

                            //Check the type of application and select the appropriate email template
                            if(obj.Application__r.TF4SF__Custom_Text2__c=='Trust') {
                                if (etTrustee[0] != null) { mail.setTemplateId(etTrustee[0].Id); }
                            }

                            if(obj.Application__r.TF4SF__Custom_Text2__c=='Business Entity') {
                                if (etBusiness[0] != null) { mail.setTemplateId(etBusiness[0].Id); }
                            }
                              
                            if(obj.Application__r.TF4SF__Custom_Text2__c=='Joint Application') {
                                if (etCoBorrower[0] != null) { mail.setTemplateId(etCoBorrower[0].Id); }
                            }  
                              
                            mail.setTargetObjectId(con.Id);
                            mail.setTreatTargetObjectAsRecipient(false);
                            mail.setOrgWideEmailAddressId(owea.Id);
                            mail.setToAddresses(new String[] {
                              obj.Email_Address__c
                            });

                            if(String.isNotBlank(obj.Application__r.TF4SF__Email_Address__c)){
                                mail.setCCAddresses(new String[] {
                                  obj.Application__r.TF4SF__Email_Address__c
                                });
                            }
                            mail.setWhatId(obj.Id);
                            mail.setSaveAsActivity(false);
                            mail.setBccSender(false);
                            mails.add(mail); 


                            //Update Email sent flag to True
                            obj.Email_Sent__c = True;
                            obj.Email_Sent_Date__c = System.now();
                            lstTRGUUpdate.add(obj); 
                        }              
                    }
                }
            }

          // Send all emails in the master list
          if (mails != null) { Messaging.sendEmail(mails); }

          //Update object
          if ( lstTRGUUpdate.size() > 0 ) {
            update lstTRGUUpdate;
          }
          
      } catch (DMLException ex) {
          System.debug('An occured occured in Send email' + ex.getMessage());
          System.debug('An occured occured in Send email Line number' + ex.getLineNumber());          
      } 
      finally {
        //Deleting Inserted Dummy Contact in Catch block
          List<Contact> delConLst = null;
          delConLst = [SELECT Id FROM Contact WHERE Id = :conId];
          if (delConLst.size() > 0) { delete delConLst; }
      }
    }

    //This method is used to send out Reminder Email notifications to Trustee/Guarentor if their applications are not completed yet
    //Called from <>
    public static void sendReminderEmail(List<Trustee_Guarantor__c> lstTRGR,
                                        String reminderEmailNumber) {


        //List<Trustee_Guarantor__c> lstResult = [SELECT Id, Child_Application__c , Child_Application_Completed__c, Email_Address__c, First_Name__c,Last_Name__c, Phone_Number__c,Email_Sent__c,Reminder_Email_1_Sent__c FROM Trustee_Guarantor__c WHERE Email_Sent__c= True AND Reminder_Email_1_Sent__c = False]; 

        String conId;
        Contact con = new Contact(); // Create a master list to hold the emails we'll send
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        List<Trustee_Guarantor__c> lstTRGUUpdate = new  List<Trustee_Guarantor__c>();

        try {
                //Get the application ID's from ACH Transfer list

                if (lstTRGR.size() > 0) {
                    if ( String.isNotBlank(lstTRGR[0].First_Name__c) ) {
                      con.FirstName = lstTRGR[0].First_Name__c;
                    } else {
                      con.FirstName = 'FirstName';
                    }

                    if ( String.isNotBlank(lstTRGR[0].Last_Name__c)) {
                      con.LastName = lstTRGR[0].Last_Name__c;
                    } else {
                      con.LastName = 'LastName';
                    }

                    if ( String.isNotBlank(lstTRGR[0].Email_Address__c)) {
                      con.Email = lstTRGR[0].Email_Address__c;
                    }
                } 

                //Insert Temp Contact
                insert con;
                conId = con.Id; 


                //Get the email templates 
                List<EmailTemplate> etTrustee = [SELECT Id, Name,  DeveloperName 
                                                FROM EmailTemplate 
                                                WHERE DeveloperName = 'Mrtgage_Child_Application_Email_Trustee' LIMIT 1];

                List<EmailTemplate> etBusiness = [SELECT Id, Name,  DeveloperName 
                                                 FROM EmailTemplate 
                                                 WHERE DeveloperName = 'Mrtgage_Child_Application_Email_Business' LIMIT 1];

                List<EmailTemplate> etCoBorrower = [SELECT Id, Name,  DeveloperName 
                                                  FROM EmailTemplate 
                                                  WHERE DeveloperName = 'Mrtgage_Child_Application_Email_Co_brwr' LIMIT 1];

                //Fetching Org Wide Email Address
                OrgWideEmailAddress owea = [SELECT Id,Address FROM OrgWideEmailAddress LIMIT 1]; 

                if(etTrustee.size() > 0 && etBusiness.size() > 0 && etCoBorrower.size() > 0) {
                    //Loop through the applications and build email
                    for (Trustee_Guarantor__c obj: lstTRGR) {
                        if ( String.isNotBlank(obj.Email_Address__c) ) {                      
						System.debug('----'+obj.Application__r.TF4SF__Custom_Text2__c);
                            System.debug('----'+ obj.Application__r.TF4SF__Email_Address__c);
                            if((String.isNotBlank(obj.Application__r.TF4SF__Custom_Text2__c) && String.isNotBlank( obj.Application__r.TF4SF__Email_Address__c) ) ) {
                                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

                                //Check the type of application and select the appropriate email template

                                if(obj.Application__r.TF4SF__Custom_Text2__c=='Trust') {
                                    if (etTrustee[0] != null) { mail.setTemplateId(etTrustee[0].Id); }
                                }

                                if(obj.Application__r.TF4SF__Custom_Text2__c=='Business Entity') {
                                    if (etBusiness[0] != null) { mail.setTemplateId(etBusiness[0].Id); }
                                }

                                if(obj.Application__r.TF4SF__Custom_Text2__c=='Joint Application') {
                                    if (etCoBorrower[0] != null) { mail.setTemplateId(etCoBorrower[0].Id); }
                                }    

                                mail.setTargetObjectId(con.Id);
                                mail.setTreatTargetObjectAsRecipient(false);
                                mail.setOrgWideEmailAddressId(owea.Id);
                                mail.setToAddresses(new String[] {
                                  obj.Email_Address__c
                                });
                                mail.setCCAddresses(new String[] {
                                  obj.Application__r.TF4SF__Email_Address__c
                                });
                                mail.setWhatId(obj.Id);
                                mail.setSaveAsActivity(false);
                                mail.setBccSender(false);
                                mails.add(mail); 


                                //Update Reminder Email sent flag to True
                                if( reminderEmailNumber == 'First') {
                                    obj.Reminder_Email_1_Sent__c = True;
                                  }

                                lstTRGUUpdate.add(obj); 
                            }              
                        }
                    }
                }

                // Send all emails in the master list
                if (mails != null) { Messaging.sendEmail(mails); }

                //Update object
                if ( lstTRGUUpdate.size() > 0 ) {
                    update lstTRGUUpdate;
                }               
        } catch (DMLException ex) {
            System.debug('An occured occured in Send Reminder email' + ex.getMessage());
            System.debug('An occured occured in Send Reminder email Line number' + ex.getLineNumber());      
      } finally {
            //Deleting Inserted Dummy Contact in Catch block
            List<Contact> delConLst = null;
            delConLst = [SELECT Id FROM Contact WHERE Id = :conId];
            if (delConLst.size() > 0) { delete delConLst; }
      } 
    }
}