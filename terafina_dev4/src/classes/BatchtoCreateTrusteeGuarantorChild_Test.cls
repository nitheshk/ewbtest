@isTest
global class BatchtoCreateTrusteeGuarantorChild_Test{ 

@isTest static void method_one(){

String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
    Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
    User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
      EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US', ProfileId = p.Id,
      TimeZoneSidKey = 'America/Los_Angeles',
      UserName = uniqueUserName);
    System.runAs(u) {
    
        TF4SF__Application__c app = new TF4SF__Application__c ();
        app.TF4SF__Application_Status__c = 'Open';
        app.TF4SF__Product__c = 'Home Loan';
        app.TF4SF__Sub_Product__c =  'Home Loan - Short App';
        
        insert app;
    
  List<Trustee_Guarantor__c> appList = new List<Trustee_Guarantor__c>();    
  TF4SF__About_Account__c ab = new TF4SF__About_Account__c();
     ab.TF4SF__Application__c = app.id;    
     /*ab.TF4SF__Mortgage_Applied_For__c='test3';
     ab.TF4SF__Down_Payment__c=34.0;
     ab.Down_Payment_By_Percentage__c=10.0;
     ab.TF4SF__Total_Loan_Amount__c='5566';
     ab.TF4SF__Street_Address_1_AboutAccount__c='street1';
     ab.TF4SF__Street_Address_2_AboutAccount__c='street2';
     ab.TF4SF__County_AboutAccount__c='test1';
     ab.TF4SF__Year_Built__c=2018;
     ab.TF4SF__Property_Type__c=testhome;
     ab.TF4SF__Occupancy__c=testengg;*/
     insert ab;
     
    
        Database.QueryLocator QL;
        Database.BatchableContext BC;
        BatchtoCreateTrusteeGuarantorChildApps  instance1 = new BatchtoCreateTrusteeGuarantorChildApps();
        instance1.start(BC);
        instance1.execute(BC,appList);
        instance1.finish(BC);
}
}
}