@isTest
public class EmailNotificationDeposit_UtilityTest {
    
    @isTest static void test_method_one() {
        
        User us = [Select id from User where Id = :UserInfo.getUserId()];
        
      
        
        
        TF4SF__Application__c app1 = new TF4SF__Application__c (TF4SF__Product__c = 'Home Loan',
             TF4SF__Email_Address__c ='Test@test.com',is_Processed_Abandoned_Mail_1__c= false,is_Processed_Abandoned_Mail_2__c= false,
           is_Processed_Abandoned_Mail_3__c=false, Open_To_Abandoned_DateTime__c=system.today().adddays(-10));
        insert app1;
        
        List<TF4SF__Application__c> testAfterInsert = [SELECT Id,TF4SF__Product__c,TF4SF__Email_Address__c,
                                                       Time_Diff__c,is_Processed_Abandoned_Mail_1__c,is_Processed_Abandoned_Mail_2__c,
                                                       is_Processed_Abandoned_Mail_3__c,
                                                       Open_To_Abandoned_DateTime__c FROM TF4SF__Application__c WHERE Id = :app1.Id]; 
        System.debug('New Diff' + testAfterInsert[0].Time_Diff__c);
        
        
        
      
        List <TF4SF__Application_Configuration__c> appConfig = new List<TF4SF__Application_Configuration__c>();
        TF4SF__Application_Configuration__c t = new TF4SF__Application_Configuration__c();
        t.TF4SF__Application_Code__c ='dsp4_0';
        t.Deposit_Abandon_Email_Time__c=1.40;
        t.TF4SF__Theme__c='test1';
        appConfig.add(t);
        
        insert appConfig;
        
        System.runAs(us){
            
            test.startTest();
            EmailNotificationDeposit_Utility em = new EmailNotificationDeposit_Utility();
            em.sendEmail(testAfterInsert);
            em.sendResumeAppForDeposit(testAfterInsert);
            test.stopTest();
        }
    }
}