global with sharing class ResumeValidateOTPExtension implements TF4SF.DSP_Interface {

global static Map<String, String> main(Map<String, String> tdata) {

        Map<String, String> data = new Map<String, String>();
        Map<String, String> verifyEnroll = new Map<String, String>();
        String Options = '';
        String appId = tdata.get('id');
        String otpCode= tdata.get('otp');
        String phoneNo='', emailId='',valid= 'false', isOther='0',message='',status='0', factorId='', oktauserId='', smsId='',oktaStatus='';
        String abandonDspId='',userId='', responsesmsId='', responseStatus='';
        String factorResult = '';
        List<OKTA__c> oktaList = new List<OKTA__c>();
         //Based on current DSP Id get the Email & retrive recent Abandon App
         //Current DSP Id Email will have retrieval Email Id(Abandon App)
        try
        {
         List <TF4SF__Application__c>  listCurrentApp = [SELECT Id, TF4SF__Application_Status__c,
         TF4SF__Sub_Product__c ,TF4SF__Email_Address__c,TF4SF__Primary_Phone_Number__c, OKTA_UserId__c, OKTA_factorId__c, OKTA_SMSId__c, OKTA_Status__c
         FROM TF4SF__Application__c 
         where Id=:appId];
        
        if (!listCurrentApp .isempty()) {
        
            emailId = listCurrentApp[0].TF4SF__Email_Address__c;
        if (emailId!='') {    
            
            oktaList = [SELECT Id, Email_Address__c, Factor_Id__c, SMS_Id__c, Status__c, User_Id__c FROM OKTA__c WHERE Email_Address__c =: emailId.toLowerCase() LIMIT 1];
            if (!oktaList.isempty()) {
                factorId = oktaList[0].factor_Id__c;
                oktauserId = oktaList[0].User_Id__c;
                smsId = oktaList[0].SMS_Id__c;
                oktaStatus = oktaList[0].Status__c;
            }
            
            //Get recent abandoned app based on email id                      
                
                if (String.isNotBlank(oktaStatus)) {
                    if (oktaStatus == 'ACTIVE') {
                        if (String.isNotBlank(smsId)) {
                            factorResult = ValidateOTPExtension.verify(appId, AuthToken.Auth(), oktauserId, otpCode, smsId);
                            responsesmsId = oktaList[0].SMS_Id__c;
                            responseStatus = oktaList[0].Status__c;
                            if (factorResult == 'SUCCESS') {
                                valid='true';
                                status='200';    
                            }
                            
                        }
                    } else {
                       verifyEnroll = ValidateOTPExtension.verifySMSRegisteration(appId, AuthToken.Auth(), oktauserId, otpCode, factorId);
                       responsesmsId = verifyEnroll.get('smsId');
                       responseStatus = verifyEnroll.get('oktaStatus');
                       if(String.isNotBlank(responsesmsId)) {
                        valid='true';
                        status='200';
                       }
                    }
                }

                phoneNo=listCurrentApp[0].TF4SF__Primary_Phone_Number__c;                
                abandonDspId=listCurrentApp[0].Id;  
                
                } 
           
        }   
              
        }
        catch(Exception ex)
        {
            message=ex.getMessage();
        }
        message=emailId;
        userId=UserInfo.getUserId(); 

        Options += '{';
        Options += '"status": "'+status+'",';        
        Options += '"statusMessage": "'+valid+'",'; 
        Options += '"AbandonDspId": "'+abandonDspId+'",'; 
        Options += '"UserId": "'+userId+'",';
        Options += '"message": "'+message+'"'; 
        Options += '}';

        data.put('status',Options);
        EmailValidationExtension.updateOkta(abandonDspId, emailId, oktauserId, factorId, responseStatus, responsesmsId);
        return data;
    }


}