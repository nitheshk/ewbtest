global with sharing class DocuSignStatus implements TF4SF.DSP_Interface {
    global Map<String, String> main(Map<String, String> tdata) {
        Map<String, String> data = new Map<String, String>();
        data = tdata.clone();
        String backImage = '';
        String appId = data.get('id');
        String username = '';
        String password = '';
        String accountId = ''; 
        String integratorKey = '';
         Docusign_Config__c cred = Docusign_Config__c.getOrgDefaults();
         
           if (cred != NULL) {
            accountId = cred.AccountID__c;
            username = cred.Username__c;
            password = cred.Password__c;
            integratorKey = cred.IntegratorKey__c ;
         }

        
           String authenticationHeader =
            '<DocuSignCredentials>' +
            '<Username>' + username + '</Username>' +
            '<Password>' + password + '</Password>' +
            '<IntegratorKey>' + integratorKey + '</IntegratorKey>' +
            '</DocuSignCredentials>';
         List<TF4SF__Application__c> DocuSign=[select Docusign_EnvelopeID__c from TF4SF__Application__c where id=:appId];
            HttpResponse res = null;
         String envelopeId=DocuSign[0].Docusign_EnvelopeID__c;
          System.debug('envelopeId ' +envelopeId);
            if(!String.isBlank(envelopeId))
            {
               String url = 'https://demo.docusign.net/restapi/v2/login_information';
               res = InitializeRequest(url, 'GET', '', authenticationHeader);
               System.debug('authenticationHeader ' +authenticationHeader);
                Integer status = res.getStatusCode();
                String resBody = res.getBody();
                 System.debug('status ' +status);
                if (status != 200) { // 200 = OK
                    data.put('Docusing Embedded Signing URL', 'Failure at 200');
                    return data;
                }
                 String baseURL = parseXMLBody(resBody, 'baseUrl');
                 System.debug('baseURL ' +baseURL);
                 url = baseURL + '/envelopes/'+envelopeId; 
                 System.debug('status envelopes url ' +url);
                 res = InitializeRequest(url, 'GET', '', authenticationHeader);
                 status = res.getStatusCode();
                 resBody = res.getBody();

                if (status != 200) { // 200 = OK
                    data.put('Docusing Embedded Signing URL', 'Failure at 200');
                    return data;
                }
                String Statusres = parseXMLBody(resBody, 'status');
                 System.debug('Statusres : ' +Statusres);
                data.put('Status', Statusres);
            
            } else {
                data.put('Status', '');
            }
            return data;
        }
        
        global static String parseXMLBody(String body, String searchToken) {
        System.debug('Body : ' + body + 'Token : ' + searchToken);
        String value = '';

        if (body!= '' && body!= NULL) {
            Dom.Document doc1 = new Dom.Document();
            doc1.load(body);
            Dom.XMLNode xroot1 = doc1.getrootelement();
            Dom.XMLNode[] xrec1 = xroot1.getchildelements(); //Get all Record Elements

            for (Dom.XMLNode firstInnerChild : xrec1) { //Loop Through Records
                if (firstInnerChild.getname() == searchToken) {
                    value = firstInnerChild.gettext();
                    break;
                } else {
                    Dom.XMLNode[] xrec2 = NULL;
                    xrec2 = firstInnerChild.getchildelements();

                    if (xrec2 != NULL) {
                        for (Dom.XMLNode secondInnerChild : xrec2) {
                            if (secondInnerChild.getname() == 'loginAccount') {
                                for (Dom.XMLNode thirdInnerChild : secondInnerChild.getchildren()) {
                                    if (thirdInnerChild.getname() == searchToken) {
                                        System.debug('values is: ' +  thirdInnerChild.gettext());
                                        value = thirdInnerChild.gettext();
                                    }

                                    if (value != '') { break; }
                                }
                            }

                            if (value != '') { break; }
                        }
                    }
                }

                if (value != '') { break; }
            }
        }

        return value;
    }
   
        
        
        global static HttpResponse InitializeRequest(String url, String method, String body, String httpAuthHeader) {
        HttpResponse res = null;

        try {
            Http http = new Http();
            HttpRequest req = new HttpRequest();
            System.debug('url===='+url);
            System.debug('httpAuthHeader===='+httpAuthHeader);
            System.debug('body===='+body);
            req.setEndpoint(url);
            req.setHeader('X-DocuSign-Authentication', httpAuthHeader);
            req.setHeader('Content-Type', 'application/xml');
            req.setHeader('Accept', 'application/xml');
            req.setMethod(method);
            req.setTimeout(120000);

            if (method == 'POST') {
                req.setHeader('Content-Length', String.valueOf(body.length()));
                req.setbody(body);
            }

            res = http.send(req);
            System.debug('the response is:' + res);
            System.debug('the body of the response is:' + res.getBody());
        } catch (Exception e) {
            System.debug(e); // simple exception handling, please review it
        }

        return res;
    }
 }