@isTest
public class LiabilityDetails_Test {

    static testMethod void test1() {
    
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
            EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US', ProfileId = p.Id,
            TimeZoneSidKey = 'America/Los_Angeles',
            UserName = uniqueUserName,TF4SF__Location__c='Yakima South 1st St');
            
        
        TF4SF__Application_Configuration__c appConfig = new TF4SF__Application_Configuration__c();
        appConfig.TF4SF__Application_Code__c = 'dsp4_0';
        appConfig.TF4SF__Theme__c = 'dsp4_0';
        appConfig.TF4SF__Key__c = 'YMmKJt5QxS7KlAEASMsj4Q==';
        appConfig.TF4SF__Popup_Seconds__c = 120;
        appConfig.TF4SF__Timeout_Seconds__c = 900;
       insert appConfig;   

        System.runAs(u) {
            
       TF4SF__Customer__c cust = new TF4SF__Customer__c();
        cust.TF4SF__Username__c = 'TestingDeclined';
        insert cust;
        
            TF4SF__Application__c app1 = new TF4SF__Application__c(TF4SF__Customer__c=cust.id,TF4SF__Primary_Phone_Number__c='(509)491-8025',TF4SF__Email_Address__c='charo509@yahoo.com',TF4SF__First_Name__c='TestFirst',TF4SF__Middle_Name__c='TestMiddel',TF4SF__Last_Name__c='TestLast',TF4SF__Product__c = 'Checking', TF4SF__Sub_Product__c = 'Test Sub Product',
                OwnerId = u.Id, TF4SF__Current_Person__c = u.Id, TF4SF__Created_Channel__c = 'Branch', TF4SF__Current_Channel__c = 'Branch',
                TF4SF__Created_Branch_Name__c = 'Walla Walla Tietan', TF4SF__Created_Person__c = u.Id, TF4SF__Current_Branch_Name__c = 'WallaWa',
                TF4SF__Application_Page__c = 'CrossSellPage', TF4SF__Type_of_Checking__c = 'Test',
                                                                   
                TF4SF__Type_of_Certificates__c = 'Certificates - 12 months', TF4SF__Type_of_Credit_Cards__c = 'Credit Cards - Advanced',
                TF4SF__Type_of_Personal_Loans__c = 'Personal Loans - Personal Loan', TF4SF__Type_of_Savings__c = 'Savings - Smart Savings',
                TF4SF__Type_of_Vehicle_Loans__c = 'Vehicle Loans -Vehicle Loan', TF4SF__User_Token__c = 'AnxzH/+Kmj85t2Wzkj5qETFWtwltJXsC5dcjGEgtGa89TTX4OIYGRky9UDclje65',
                TF4SF__Street_Address_1__c='828 S 8th Ave',TF4SF__Street_Address_2__c='828 S 8th Ave',TF4SF__City__c='Pasco',TF4SF__State__c='WA',TF4SF__Zip_Code__c='99301-5730',
                TF4SF__Created_User_Email_Address__c='athul.kakathkar@terafinainc.com',TF4SF__Current_User_Email_Address__c='athul.kakathkar@terafinainc.com',
                TF4SF__Application_Status__c='Open',TF4SF__Sub_Product_Description__c='testdesc');
            insert app1;

           
            
           TF4SF__Identity_Information__c ident = new TF4SF__Identity_Information__c(
             TF4SF__SSN_Prime__c = '999999999',TF4SF__Date_of_Birth__c = '07/26/1973',
             TF4SF__Country_Issued__c='CA',TF4SF__ID_Type__c='TestId',TF4SF__Application__c=app1.id,TF4SF__Identity_Number_Primary__c='test123',TF4SF__State_Issued__c='US',TF4SF__Expiry_Date__c='07/26/1996',TF4SF__Issue_Date__c='07/26/1976',TF4SF__Citizenship__c='WA'
             );
            insert ident;
            
            TF4SF__Application_Activity__c appact=new TF4SF__Application_Activity__c();
            appact.TF4SF__Application__c=app1.Id;
            appact.TF4SF__Action__c='Created the Application';
            appact.TF4SF__Activity_Time__c = System.now();
            insert appact;
            
            Test.StartTest();
            
			LiabilityDetails obj = new LiabilityDetails();
			Map<String, String> tdata = new Map<String, String>();
			tdata.put('Id',app1.Id);
			tdata.put('Action','Upsert');
			
			tdata.put('Liability_PA_Data','[{"PA":{"Name_of_Creditor__c":"A","Nature_of_Liability__c":"Automobile Loans","Monthly_Pledge__c":3000,"Monthly_Payment__c":3212323,"Paid_Off__c":"Yes"}}]');
			tdata.put('Liability_J1_Data','[{"J1":{"Name_of_Creditor__c":"A","Nature_of_Liability__c":"Automobile Loans","Monthly_Pledge__c":3000,"Monthly_Payment__c":3212323,"Paid_Off__c":"Yes"}}]');
			tdata.put('Liability_J2_Data','[{"J2":{"Name_of_Creditor__c":"A","Nature_of_Liability__c":"Automobile Loans","Monthly_Pledge__c":3000,"Monthly_Payment__c":3212323,"Paid_Off__c":"Yes"}}]');
			tdata.put('Liability_J3_Data','[{"J3":{"Name_of_Creditor__c":"A","Nature_of_Liability__c":"Automobile Loans","Monthly_Pledge__c":3000,"Monthly_Payment__c":3212323,"Paid_Off__c":"Yes"}}]');
			
			LiabilityDetails.main(tdata);
            
           test.stopTest();
        }
    }
    
     static testMethod void test2() {
    
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
            EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US', ProfileId = p.Id,
            TimeZoneSidKey = 'America/Los_Angeles',
            UserName = uniqueUserName,TF4SF__Location__c='Yakima South 1st St');
            
        
        TF4SF__Application_Configuration__c appConfig = new TF4SF__Application_Configuration__c();
        appConfig.TF4SF__Application_Code__c = 'dsp4_0';
        appConfig.TF4SF__Theme__c = 'dsp4_0';
        appConfig.TF4SF__Key__c = 'YMmKJt5QxS7KlAEASMsj4Q==';
        appConfig.TF4SF__Popup_Seconds__c = 120;
        appConfig.TF4SF__Timeout_Seconds__c = 900;
       insert appConfig;   

        System.runAs(u) {
            
       TF4SF__Customer__c cust = new TF4SF__Customer__c();
        cust.TF4SF__Username__c = 'TestingDeclined';
        insert cust;
        
            TF4SF__Application__c app1 = new TF4SF__Application__c(TF4SF__Customer__c=cust.id,TF4SF__Primary_Phone_Number__c='(509)491-8025',TF4SF__Email_Address__c='charo509@yahoo.com',TF4SF__First_Name__c='TestFirst',TF4SF__Middle_Name__c='TestMiddel',TF4SF__Last_Name__c='TestLast',TF4SF__Product__c = 'Checking', TF4SF__Sub_Product__c = 'Test Sub Product',
                OwnerId = u.Id, TF4SF__Current_Person__c = u.Id, TF4SF__Created_Channel__c = 'Branch', TF4SF__Current_Channel__c = 'Branch',
                TF4SF__Created_Branch_Name__c = 'Walla Walla Tietan', TF4SF__Created_Person__c = u.Id, TF4SF__Current_Branch_Name__c = 'WallaWa',
                TF4SF__Application_Page__c = 'CrossSellPage', TF4SF__Type_of_Checking__c = 'Test',
                                                                   
                TF4SF__Type_of_Certificates__c = 'Certificates - 12 months', TF4SF__Type_of_Credit_Cards__c = 'Credit Cards - Advanced',
                TF4SF__Type_of_Personal_Loans__c = 'Personal Loans - Personal Loan', TF4SF__Type_of_Savings__c = 'Savings - Smart Savings',
                TF4SF__Type_of_Vehicle_Loans__c = 'Vehicle Loans -Vehicle Loan', TF4SF__User_Token__c = 'AnxzH/+Kmj85t2Wzkj5qETFWtwltJXsC5dcjGEgtGa89TTX4OIYGRky9UDclje65',
                TF4SF__Street_Address_1__c='828 S 8th Ave',TF4SF__Street_Address_2__c='828 S 8th Ave',TF4SF__City__c='Pasco',TF4SF__State__c='WA',TF4SF__Zip_Code__c='99301-5730',
                TF4SF__Created_User_Email_Address__c='athul.kakathkar@terafinainc.com',TF4SF__Current_User_Email_Address__c='athul.kakathkar@terafinainc.com',
                TF4SF__Application_Status__c='Open',TF4SF__Sub_Product_Description__c='testdesc');
            insert app1;

           
            
           TF4SF__Identity_Information__c ident = new TF4SF__Identity_Information__c(
             TF4SF__SSN_Prime__c = '999999999',TF4SF__Date_of_Birth__c = '07/26/1973',
             TF4SF__Country_Issued__c='CA',TF4SF__ID_Type__c='TestId',TF4SF__Application__c=app1.id,TF4SF__Identity_Number_Primary__c='test123',TF4SF__State_Issued__c='US',TF4SF__Expiry_Date__c='07/26/1996',TF4SF__Issue_Date__c='07/26/1976',TF4SF__Citizenship__c='WA'
             );
            insert ident;
            
            TF4SF__Application_Activity__c appact=new TF4SF__Application_Activity__c();
            appact.TF4SF__Application__c=app1.Id;
            appact.TF4SF__Action__c='Created the Application';
            appact.TF4SF__Activity_Time__c = System.now();
            insert appact;
            
            Test.StartTest();
            
			LiabilityDetails obj = new LiabilityDetails();
			Map<String, String> tdata = new Map<String, String>();
			tdata.put('Id',app1.Id);
			tdata.put('Action','Upsert');
			
			tdata.put('Liability_PA_Data','[{"PA":{"Name_of_Creditor__c":"","Nature_of_Liability__c":" ","Monthly_Pledge__c":3000,"Monthly_Payment__c":3212323,"Paid_Off__c":""}}]');
			tdata.put('Liability_J1_Data','[{"J1":{"Name_of_Creditor__c":"","Nature_of_Liability__c":" ","Monthly_Pledge__c":3000,"Monthly_Payment__c":3212323,"Paid_Off__c":""}}]');
			tdata.put('Liability_J2_Data','[{"J2":{"Name_of_Creditor__c":"","Nature_of_Liability__c":" ","Monthly_Pledge__c":3000,"Monthly_Payment__c":3212323,"Paid_Off__c":""}}]');
			tdata.put('Liability_J3_Data','[{"J3":{"Name_of_Creditor__c":"","Nature_of_Liability__c":" ","Monthly_Pledge__c":3000,"Monthly_Payment__c":3212323,"Paid_Off__c":""}}]');
			
			LiabilityDetails.main(tdata);
            
           test.stopTest();
        }
    }
    
     static testMethod void test3() {
         
         	String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
            EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US', ProfileId = p.Id,
            TimeZoneSidKey = 'America/Los_Angeles',
            UserName = uniqueUserName,TF4SF__Location__c='Yakima South 1st St');
            
        
        TF4SF__Application_Configuration__c appConfig = new TF4SF__Application_Configuration__c();
        appConfig.TF4SF__Application_Code__c = 'dsp4_0';
        appConfig.TF4SF__Theme__c = 'dsp4_0';
        appConfig.TF4SF__Key__c = 'YMmKJt5QxS7KlAEASMsj4Q==';
        appConfig.TF4SF__Popup_Seconds__c = 120;
        appConfig.TF4SF__Timeout_Seconds__c = 900;
       insert appConfig;   

        System.runAs(u) {
            
               TF4SF__Customer__c cust = new TF4SF__Customer__c();
                cust.TF4SF__Username__c = 'TestingDeclined';
                insert cust;
                 
                    TF4SF__Application__c app1 = new TF4SF__Application__c(TF4SF__Customer__c=cust.id,TF4SF__Primary_Phone_Number__c='(509)491-8025',TF4SF__Email_Address__c='charo509@yahoo.com',TF4SF__First_Name__c='TestFirst',TF4SF__Middle_Name__c='TestMiddel',TF4SF__Last_Name__c='TestLast',TF4SF__Product__c = 'Checking', TF4SF__Sub_Product__c = 'Test Sub Product',
                        OwnerId = u.Id, TF4SF__Current_Person__c = u.Id, TF4SF__Created_Channel__c = 'Branch', TF4SF__Current_Channel__c = 'Branch',
                        TF4SF__Created_Branch_Name__c = 'Walla Walla Tietan', TF4SF__Created_Person__c = u.Id, TF4SF__Current_Branch_Name__c = 'WallaWa',
                        TF4SF__Application_Page__c = 'CrossSellPage', TF4SF__Type_of_Checking__c = 'Test',
                                                                           
                        TF4SF__Type_of_Certificates__c = 'Certificates - 12 months', TF4SF__Type_of_Credit_Cards__c = 'Credit Cards - Advanced',
                        TF4SF__Type_of_Personal_Loans__c = 'Personal Loans - Personal Loan', TF4SF__Type_of_Savings__c = 'Savings - Smart Savings',
                        TF4SF__Type_of_Vehicle_Loans__c = 'Vehicle Loans -Vehicle Loan', TF4SF__User_Token__c = 'AnxzH/+Kmj85t2Wzkj5qETFWtwltJXsC5dcjGEgtGa89TTX4OIYGRky9UDclje65',
                        TF4SF__Street_Address_1__c='828 S 8th Ave',TF4SF__Street_Address_2__c='828 S 8th Ave',TF4SF__City__c='Pasco',TF4SF__State__c='WA',TF4SF__Zip_Code__c='99301-5730',
                        TF4SF__Created_User_Email_Address__c='athul.kakathkar@terafinainc.com',TF4SF__Current_User_Email_Address__c='athul.kakathkar@terafinainc.com',
                        TF4SF__Application_Status__c='Open',TF4SF__Sub_Product_Description__c='testdesc',TF4SF__First_Joint_Applicant__c =true ,TF4SF__Second_Joint_Applicant__c =true, TF4SF__Third_Joint_Applicant__c =true );
                    insert app1;
                 
                    TF4SF__Field_Logic_New__c objFl= new TF4SF__Field_Logic_New__c(
                        TF4SF__hideExpression_Fields__c = 'a',
                        TF4SF__IsDisplayed__c = true,
                        TF4SF__IsRequired__c = true,
                        TF4SF__Label__c = 'a',
                        TF4SF__DSP_Field_Name__c ='Name_of_Creditor__c',
                        TF4SF__sObject__c ='Liability_Details__c',
                        TF4SF__expressionProperties_Fields__c = 'a',
                        TF4SF__Datatype_Form__c ='a',
                        TF4SF__Data_OptionsTypes__c ='a',
                        TF4SF__Picklist_Values__c ='a',
                        TF4SF__className_Field__c = 'a'                                
                    );
                 
            	insert objFl;
                 test.startTest();             
                    LiabilityDetails obj = new LiabilityDetails();
                    Map<String, String> tdata = new Map<String, String>();
                     tdata.put('Id',app1.Id);
                    tdata.put('Action','Load');
                    
                    
                    LiabilityDetails.main(tdata);
                    
                   test.stopTest();
        	}
            
        }
    
}