/*********************************************
@Author : Nag
@Date : 9/24/2018
@Description : This will process data to be deleted based
               Data_Retention_Settings custom metadata.
**********************************************/
global class DataPurgingBatchJob implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {
    String query;
    String objectToDelete;
    Data_Retention_Settings__mdt dataRetention;

     global DataPurgingBatchJob(String objectType, Data_Retention_Settings__mdt dataRetention) {
         //sObject Name from custom metadata 
         this.objectToDelete = objectType;
         this.dataRetention = dataRetention;
     }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        
        if(dataRetention != null){
            query = dataRetention.Related_Query__c;
        } 
        system.debug('query ### :' + query);
        return Database.getQueryLocator(query);
    }
     global void execute(Database.BatchableContext BC, List<sObject> recordList) {
        try {
            system.debug('recordList #####:' + recordList);
            delete recordList;
        }catch(Exception ex){
        
        }
     }

    global void finish(Database.BatchableContext BC) {
        
    }  
}