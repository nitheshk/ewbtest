@isTest
public class EmailValidationExtensionTest{
     
    static testmethod void validatemyTest(){
     Map<String, String> data = new Map<String, String>();
     Map<String, String> tdata = new Map<String, String>();
        
     List<TF4SF__Application__c> listApp = new List<TF4SF__Application__c>();

      TF4SF__Application__c ap = new TF4SF__Application__c();
        ap.TF4SF__Product__c = 'Home Loan';
        ap.TF4SF__First_Name__c='test';
        ap.TF4SF__Last_Name__c = 'Test';
        ap.TF4SF__Application_Status__c='Open';
        ap.TF4SF__Email_Address__c= 'test@test.com';
        ap.TF4SF__Application_Status__c='Abandoned';
        ap.TF4SF__Primary_Phone_Number__c='1234567890';
        ap.TF4SF__Sub_Product__c = 'Home Loan - Short App';
        listApp.add(ap);
        insert listApp;
        
        List<OKTA__c> oktaList = new List<OKTA__c>();
        OKTA__c okta=new OKTA__c();
        okta.Factor_Id__c='';
        okta.SMS_Id__c='';
        okta.Status__c='';
        okta.User_Id__c='';
        okta.Email_Address__c='test@gmail.com';
        oktaList.add(okta);
        insert oktaList;
        
        VELOSETTINGS__C vo = new VELOSETTINGS__C();
        vo.Name='test';
        vo.TokenEndPoint__c = 'rsgsdgsdf';
        vo.Request_GUID_Name__c = 'RequestUUID';
        vo.ProcessAPI_Timeout__c = 120000;
        insert vo;
    
      tdata.put('id',(String)ap.id);
      tdata.put('emailId',ap.TF4SF__Email_Address__c);
      

        test.startTest();
        Test.setMock(HttpCalloutMock.class, new EmailValidationExtension_Mock());
        EmailValidationExtension ob = new EmailValidationExtension(); 
        EmailValidationExtension.updateOkta(ap.id, ap.TF4SF__Email_Address__c,'userId', 'factorId', 'Status', 'smsId');
        EmailValidationExtension.enrollSMS(ap.id,'Token','userId', 'Phone');
        EmailValidationExtension.challenge(ap.id, 'Token', 'userId', 'factorId');
        EmailValidationExtension.main(tdata);
        test.stopTest();
    }

}