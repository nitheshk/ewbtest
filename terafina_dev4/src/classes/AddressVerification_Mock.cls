@isTest
public class AddressVerification_Mock implements HttpCalloutMock {

  public HTTPResponse respond(HTTPRequest req) {
    HttpResponse res = new HttpResponse();
    res.setBody('{"data":{"FullAddress":"test;","Latitude":"","City":"","Longitude":"","ErrorMessageList":null,"ResultCodes":[{"LongDescription":"The address could not be verified at least up to the postal code level.","Code":"AE01","ShortDescription":"Postal Code Error/General Error"}],"AddressLine2":"","AddressLine1":"","State":"","Zipcode":"","Country":"United States of America","RecordID":"84cb93b3-7160-4cd0-b39a-63a33a247706","RequestUUID":null},"result":{"success":true,"code":"200","message":"Success"}}');
    res.setStatusCode(200);
    return res;
  }
}