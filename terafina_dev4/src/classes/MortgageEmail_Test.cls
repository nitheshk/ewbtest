@isTest
public class MortgageEmail_Test {
    static testmethod void validatemyTest(){        
    
    
    String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
            EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US', ProfileId = p.Id,
            TimeZoneSidKey = 'America/Los_Angeles',
            UserName = uniqueUserName,TF4SF__Location__c='Yakima South 1st St');
            
        insert u;    
        
        TF4SF__Application_Configuration__c appConfig = new TF4SF__Application_Configuration__c();
        appConfig.TF4SF__Application_Code__c = 'dsp4_0';
        appConfig.TF4SF__Theme__c = 'dsp4_0';
        appConfig.TF4SF__Key__c = 'YMmKJt5QxS7KlAEASMsj4Q==';
        appConfig.TF4SF__Popup_Seconds__c = 120;
        appConfig.TF4SF__Timeout_Seconds__c = 900;
       insert appConfig;   
       
       TF4SF__Application__c app = new TF4SF__Application__c(TF4SF__Primary_Phone_Number__c='(509)491-8025',TF4SF__Email_Address__c='charo509@yahoo.com',TF4SF__First_Name__c='TestFirst',TF4SF__Middle_Name__c='TestMiddel',TF4SF__Last_Name__c='TestLast',TF4SF__Product__c = 'Home Loan', TF4SF__Sub_Product__c = 'Test Sub Product',
                OwnerId = u.Id, TF4SF__Current_Person__c = u.Id, TF4SF__Created_Channel__c = 'Branch', TF4SF__Current_Channel__c = 'Branch',
                TF4SF__Created_Branch_Name__c = 'Walla Walla Tietan', TF4SF__Created_Person__c = u.Id, TF4SF__Current_Branch_Name__c = 'WallaWa',
                TF4SF__Application_Page__c = 'CrossSellPage', TF4SF__Type_of_Checking__c = 'Test',
                                                                   
                TF4SF__Type_of_Certificates__c = 'Certificates - 12 months', TF4SF__Type_of_Credit_Cards__c = 'Credit Cards - Advanced',
                TF4SF__Type_of_Personal_Loans__c = 'Personal Loans - Personal Loan', TF4SF__Type_of_Savings__c = 'Savings - Smart Savings',
                TF4SF__Type_of_Vehicle_Loans__c = 'Vehicle Loans -Vehicle Loan', TF4SF__User_Token__c = 'AnxzH/+Kmj85t2Wzkj5qETFWtwltJXsC5dcjGEgtGa89TTX4OIYGRky9UDclje65',
                TF4SF__Street_Address_1__c='828 S 8th Ave',TF4SF__Street_Address_2__c='828 S 8th Ave',TF4SF__City__c='Pasco',TF4SF__State__c='WA',TF4SF__Zip_Code__c='99301-5730',
                TF4SF__Created_User_Email_Address__c='athul.kakathkar@terafinainc.com',TF4SF__Current_User_Email_Address__c='athul.kakathkar@terafinainc.com',
                TF4SF__Application_Status__c='Open',TF4SF__Sub_Product_Description__c='testdesc');
            insert app;
       

        System.runAs(u) {
    
            Test.startTest();
            MortgageEmail ob = new MortgageEmail ();
                
            ob.sendEmail(app);
            
            Test.stopTest();
        }
    }

}