global with sharing class FundingVerifyAccountOLB implements TF4SF.DSP_Interface{

    global static Map<String, String> main (Map<String, String> tdata) {
        Long time1 = DateTime.now().getTime();
        Boolean infoDebug = false;
        infoDebug = (tdata.get('infoDebug') == 'true');
        Map<String, String> data = new Map<String, String>();
        String appId = tdata.get('id');
        String Token = '';
        String fiid =  tdata.get('fiid');
        String userparam = tdata.get('userparam');
        String passparam = tdata.get('passparam');
        system.debug(' the fiid is '+fiid);
        system.debug(' the userparam is '+userparam);
        system.debug(' the fpassparamiid is '+passparam);
        String response1 = '',response2 = '', response3 = '';
        String runId = null;
        String cfiId = null;
        String userMFA = null;
        String answerMFA = null;
        String valueMFA = null;
        VELOSETTINGS__C velo = VELOSETTINGS__C.getOrgDefaults();
        Map<String, String> userValueMFAMap = new Map<String, String>();
        List<String> valueMFAList = new List<String>();
        List<String> paramList = new List<String>();
        String jsonReqMFA = '';
        
        try {
            Token = AuthToken.Auth();    
                
            response1 = VerifyAccount(appId, Token, fiid, userparam, passparam);
            JSON2ApexVerifyFundingRes1 res1=JSON2ApexVerifyFundingRes1.parse(response1);

            //String runId='',cfiId='';
            runId=res1.data.VerifyAccRealTime.body.VerifyAccountRealTimeResponse.return_Z.RealTimeAccountVerificationInfoStatusList.RealTimeAccountVerificationInfoStatus[0].RunID;
            cfiId=res1.data.VerifyAccRealTime.body.VerifyAccountRealTimeResponse.return_Z.RealTimeAccountVerificationInfoStatusList.RealTimeAccountVerificationInfoStatus[0].CFIID;
            String Res1VerificationStatus = res1.data.VerifyAccRealTime.body.VerifyAccountRealTimeResponse.return_Z.RealTimeAccountVerificationInfoStatusList.RealTimeAccountVerificationInfoStatus[0].VerificationStatus;
            data.put('response1',response1);

            if (Res1VerificationStatus == 'Approved') {
                data.put('response2',response1);
                data.put('StatusCode','200');
            } else {
                Object TwoFA = res1.data.VerifyAccRealTime.body.VerifyAccountRealTimeResponse.return_Z.RealTimeAccountVerificationInfoStatusList.RealTimeAccountVerificationInfoStatus[0].TwoFARealtimeAcctVerificationParam;
                System.debug('params: ### : '+TwoFA);
                String ResponseChoiceFormat = '';
                String Type = '', jsonRes = '';
                List<String> resList = new List<String>();
                List<String> ChoiceList = new List<String>();
                Map<String, String> ChoiceMap = new Map<String, String>();
                
                if (TwoFA != null) {
                    List<Object> params = res1.data.VerifyAccRealTime.body.VerifyAccountRealTimeResponse.return_Z.RealTimeAccountVerificationInfoStatusList.RealTimeAccountVerificationInfoStatus[0].TwoFARealtimeAcctVerificationParam.Param;
                    if (!params.isEmpty()) {
                        system.debug('the param size is '+params.size());
                        jsonReqMFA += '[';
                        jsonReqMFA += '{';
                        jsonReqMFA += '"Questions": [';
                        for (Integer ival = 0; ival < params.size(); ival++) {
                            userMFA = res1.data.VerifyAccRealTime.body.VerifyAccountRealTimeResponse.return_Z.RealTimeAccountVerificationInfoStatusList.RealTimeAccountVerificationInfoStatus[0].TwoFARealtimeAcctVerificationParam.Param[ival].Name;
                            valueMFA = res1.data.VerifyAccRealTime.body.VerifyAccountRealTimeResponse.return_Z.RealTimeAccountVerificationInfoStatusList.RealTimeAccountVerificationInfoStatus[0].TwoFARealtimeAcctVerificationParam.Param[ival].Value;
                            userValueMFAMap.put(userMFA, valueMFA);
                            paramList.add(userMFA);
                            if (ival >= 2) {
                                //ValueMFAList.add(valueMFA);
                                Integer i = 1;
                                if (String.isNotBlank(valueMFA)) {
                                    if (valueMFA.contains('<br>')) { 
                                        resList = valueMFA.split('<br>');
                                        system.debug('the choice format of valueMFA is '+resList);
                                        system.debug('the Question is '+resList[0]);
                                        resList.remove(0);
                                        system.debug('the new list is '+resList);
                                        for (String choiceId : resList) {
                                            ChoiceList = choiceId.split('for');
                                            ChoiceMap.put(ChoiceList[0].trim(),ChoiceList[1].trim());
                                            ResponseChoiceFormat += '{"ChoiceId": '+ChoiceList[0].trim()+',"Text": {"Statement": "'+ChoiceList[1].trim()+'"}},';
                                        }
                                        ResponseChoiceFormat = ResponseChoiceFormat.removeEnd(',');
                                        ResponseChoiceFormat = '['+ResponseChoiceFormat+']';
                                        Type = 'singlechoice';
                                        system.debug('the Choice Options json is '+ResponseChoiceFormat);
                                        system.debug('the new map is '+ChoiceMap);
                                        jsonRes = 'Please select the option';            
                                    } else {
                                        ResponseChoiceFormat = null;
                                        Type = 'Question';
                                        jsonRes = valueMFA;
                                    }
                                }
                                jsonReqMFA += '{';
                                    jsonReqMFA += '"QuestionId": '+i+',';
                                    jsonReqMFA += '"ParamId": "'+userMFA+'",';
                                    jsonReqMFA += '"Key": '+i+',';
                                    jsonReqMFA += '"Type": "'+Type+'",';
                                    jsonReqMFA += '"Text": {';
                                        jsonReqMFA += '"Statement": "'+jsonRes+'"';
                                    jsonReqMFA += '},';
                                    jsonReqMFA += '"Choices": '+ResponseChoiceFormat+'';
                                jsonReqMFA += '},';    
                                i++;    
                            }
                        }
                        jsonReqMFA = jsonReqMFA.removeEnd(',');
                        jsonReqMFA += ']';
                        jsonReqMFA += '}';
                        jsonReqMFA += ']';
                    }
                }
                //if (valueMFAList.size() > 0) {
                //    valueMFAList.remove(0);
                //    valueMFAList.remove(1);
                //}
                system.debug('MFA Token: '+valueMFA);
                system.debug('MFA Map: '+jsonReqMFA);
                if (String.isNotBlank(jsonReqMFA)) {
                    response2 = jsonReqMFA; //VerifyAccountOLB(ValueMFAList);    
                    response3 = VerifyAccountRequest(userValueMFAMap, paramList);    
                }
                
                // JSON2ApexVerifyFundingRes1 res2=JSON2ApexVerifyFundingRes1.parse(response2);
                // Object answerFA = res2.data.VerifyAccRealTime.body.VerifyAccountRealTimeResponse.return_Z.RealTimeAccountVerificationInfoStatusList.RealTimeAccountVerificationInfoStatus[0].TwoFARealtimeAcctVerificationParam;
                // System.debug('params: ### : '+answerFA);
                
                // if (answerFA != null) {
                //     List<Object> params = res2.data.VerifyAccRealTime.body.VerifyAccountRealTimeResponse.return_Z.RealTimeAccountVerificationInfoStatusList.RealTimeAccountVerificationInfoStatus[0].TwoFARealtimeAcctVerificationParam.Param;
                //     if (!params.isEmpty()) {
                //         if (params.size() == 3) {
                //             answerMFA = res2.data.VerifyAccRealTime.body.VerifyAccountRealTimeResponse.return_Z.RealTimeAccountVerificationInfoStatusList.RealTimeAccountVerificationInfoStatus[0].TwoFARealtimeAcctVerificationParam.Param[2].Name;
                //         }
                //     }
                // }
                data.put('response2',response2);
                data.put('response3',response3);
                data.put('StatusCode','200');
            }

            data.put('About_Account__c.CFIID__c',cfiId);
            data.put('About_Account__c.RUNID__c',runId);
        } catch (Exception e) {
            data.put('server-errors', 'Error encountered in FundingVerifyAccountOLB class: ' + e.getMessage() + '; line: ' + e.getLineNumber() + '; type: ' + e.getTypeName() + '; stack trace: ' + e.getStackTraceString());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error encountered in FundingVerifyAccountOLB class: ' + e.getMessage() + '; line: ' + e.getLineNumber() + '; type: ' + e.getTypeName() + '; stack trace: ' + e.getStackTraceString()));
            System.debug('exception' + e + ' ' + e.getLineNumber());
        } finally {
             if (String.isNotBlank(appId)) {
                List<TF4SF__About_Account__c> acc = [SELECT TF4SF__Application__c, Id, CFIID__c, RUNID__c FROM TF4SF__About_Account__c WHERE TF4SF__Application__c=:appId];
                if (acc.size() > 0) {
                    acc[0].CFIID__c = String.isNotBlank(cfiId)? cfiId:null;
                    acc[0].RUNID__c = String.isNotBlank(runId)? runId:null;
                    acc[0].Funding_MFA__c = String.isNotBlank(userMFA)? userMFA:null;
                    update acc;
                }
                // DebugLogger.InsertDebugLog(appId, response1, 'VerifyAccount 1 Response');
                //if (String.isNotBlank(response2)) {
                    //  DebugLogger.InsertDebugLog(appId, response2, 'VerifyAccount 2 Response');
                //}
            }
        }

        return data;
    } 

    // First Method to Call //
    public static String VerifyAccount(String appId, String Token,String fiid,String userparam,String passparam) {
        String result = '';
        
        String applicationId = '\'' + appId + '\'';
        //System.debug('applicationId//productnumber:'+applicationId+'--'+productnumber);
        //select all fields in Application
        queryHelper.ObjectInfo appInfo = queryHelper.selectStar('TF4SF__Application__c');
        String queryString =  appInfo.query;
        queryString += 'WHERE ID = ' + applicationId ;
        TF4SF__Application__c app = Database.query(queryString);

        //this.Application = app;
        queryHelper.ObjectInfo app2Info = queryHelper.selectStar('TF4SF__Application2__c');
        String app2String =  app2Info.query;
        app2String += 'WHERE TF4SF__Application__c = ' + applicationId ;
        TF4SF__Application2__c app2 = Database.query(app2String);

        queryHelper.ObjectInfo empInfo = queryHelper.selectStar('TF4SF__Employment_Information__c');
        String employmentQuery =  empInfo.query;
        employmentQuery += 'WHERE TF4SF__Application__c = ' + applicationId;
        TF4SF__Employment_Information__c employment = Database.query(employmentQuery);
        //System.debug('employment:' + employment);

        queryHelper.ObjectInfo identityInfo = queryHelper.selectStar('TF4SF__Identity_Information__c');
        String identityQuery = identityInfo.query;
        identityQuery += 'WHERE TF4SF__Application__c = ' + applicationId;
        TF4SF__Identity_Information__c identity = Database.query(identityQuery);
        System.debug('identity:' + identity);

        queryHelper.ObjectInfo aboutAccountInfo = queryHelper.selectStar('TF4SF__About_Account__c');
        String aboutAccountQuery = aboutAccountInfo.query;
        aboutAccountQuery += 'WHERE TF4SF__Application__c = ' + applicationId;
        //System.debug('abtaccountquery:'+aboutAccountQuery);
        TF4SF__About_Account__c about = Database.query(aboutAccountQuery);
        //System.debug('about account:' + about); 
        try {
        DepositSubmit.Application application = new DepositSubmit.Application(app, app2, employment, identity, about);

        result += '{';
            if (application.app.TF4SF__Product__c == 'Checking') {
                result += '"ABA": "'+application.about.TF4SF__Routing_Number_CHK__c+'",';
                result += '"AccountNumber": "'+application.about.TF4SF__CHK_Account_Number__c+'",';
                result += '"AccountTypeCode": "'+application.about.TF4SF__Account_type_FI_CHK__c+'",';
            } else {
                result += '"ABA": "'+application.about.TF4SF__Routing_Number_SAV__c+'",';
                result += '"AccountNumber": "'+application.about.TF4SF__SAV_Account_Number__c+'",';
                result += '"AccountTypeCode": "'+application.about.TF4SF__Account_Type_FI_Sav__c+'",';
            }

            result += '"AccountTypeGroup": "Cash",';
            result += '"CFIID": "",';
            result += '"Email": "'+application.app.TF4SF__Email_Address__c+'",';
            result += '"FIID": "'+fiid+'",';
            result += '"OnlineParamList": [';
                result += '{';
                    result += '"CryptVal": "'+application.app.TF4SF__Login__c+'",';
                    result += '"ParamId": "'+userparam+'"';
                result += '},';
                result += '{';
                    result += '"CryptVal": "'+application.app.TF4SF__Password__c+'",';
                    result += '"ParamId": "'+passparam+'"';
                result += '}';
            result += '],';
            result += '"RqUID": "VerifyAccountRealTimREQUEST",';
            result += '"RunID": ""';
        result += '}';

        
        } catch (Exception e) {
            system.debug('exception'+e+' '+e.getLineNumber());
        }

        return execute(appId, result, Token, 'Verify Account 1');
    }

    //Second Method to call//
    public static String VerifyAccountOLB(List<String> mfaList) {
        String result = '';
        try {
            Integer i = 1;
            String ResponseChoiceFormat = '';
            String Type = '';
            List<String> resList = new List<String>();
            List<String> ChoiceList = new List<String>();
            Map<String, String> ChoiceMap = new Map<String, String>();
            
            result += '[';
            result += '{';
                result += '"Questions": [';
                for (String res : mfaList) {
                    if (String.isNotBlank(res)) {
                        if (res.contains('<br>')) { 
                            resList = res.split('<br>');
                            system.debug('the choice format of res is '+resList);
                            system.debug('the Question is '+resList[0]);
                            resList.remove(0);
                            system.debug('the new list is '+resList);
                            for (String choiceId : resList) {
                                ChoiceList = choiceId.split('for');
                                ChoiceMap.put(ChoiceList[0].trim(),ChoiceList[1].trim());
                                ResponseChoiceFormat += '{"ChoiceId": '+ChoiceList[0].trim()+',"Text": {"Statement": "'+ChoiceList[1].trim()+'"}},';
                            }
                            ResponseChoiceFormat = ResponseChoiceFormat.removeEnd(',');
                            ResponseChoiceFormat = '['+ResponseChoiceFormat+']';
                            Type = 'singlechoice';
                            system.debug('the Choice Options json is '+ResponseChoiceFormat);
                            system.debug('the new map is '+ChoiceMap);
                            res = 'Please select the option';            
                        } else {
                            ResponseChoiceFormat = null;
                            Type = 'Question';
                        }
                    }
                    result += '{';
                        result += '"QuestionId": '+i+',';
                        result += '"ParamId": '+i+',';
                        result += '"Key": '+i+',';
                        result += '"Type": "'+Type+'",';
                        result += '"Text": {';
                            result += '"Statement": "'+res+'"';
                        result += '},';
                        result += '"Choices": '+ResponseChoiceFormat+'';
                    result += '},';    
                    i++;    
                }
                result = result.removeEnd(',');
                result += ']';
            result += '}]';    
            
            //String res = 'Please select/Enter the option<br>1 for Phone - Text :xxx-xxx-3863<br>3 for Phone - Voice :408-567-3864<br>4 for Phone - Text :408-567-3864<br>6 for Email - E Mail :n...e@hotmail.com<br>7 for Email - E Mail :m...n@gmail.com<br>8 for Email - E Mail :money@rediff.com<br>9 for if you already have an Identification Code';
            
            //result += '[';
            //result += '{';
            //    result += '"Questions": [';
            //    result += '{';
            //        result += '"QuestionId": '+i+',';
            //        result += '"Key": '+i+',';
            //        result += '"Type": "singlechoice",';
            //        result += '"Text": {';
            //        result += '"Statement": "Please select the option"';
            //        result += '},';
            //        result += '"Choices": ['+ResponseChoiceFormat+']';
            //    result += '}';
            //    result += ']';
            //result += '}]';    
        
        } catch (Exception e) {
            system.debug('exception'+e+' '+e.getLineNumber());
        }

        return result;
    }

    public static String VerifyAccountRequest(Map<String, String> mfaMap, List<String> paramList) {
        String result = '';
        for (String paramId : paramList) {
            result += '[';
            result += '{';
                result += '"CryptVal": "'+mfaMap.get(paramId)+'",';
                result += '"ParamId": "'+paramId+'"';
            result += '},';    
        }
        result = result.removeEnd(',');
        result += ']';
        //result += '{';
        //    result += '"CryptVal": "'+application.app.TF4SF__Login__c+'",';
        //    result += '"ParamId": "'+userparam+'"';
        //result += '},';
        //result += '{';
        //    result += '"CryptVal": "'+application.app.TF4SF__Password__c+'",';
        //    result += '"ParamId": "'+passparam+'"';
        //result += '},';
        return result;
    }

    private static String execute(String appId, String json, String Token, String CallOut) {
        System.debug('appId, JSON: ' + appId  + '---' + json);
        String responseJson = '';

        try { 
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            //req.setTimeout(120*1000);
            VELOSETTINGS__C velo = VELOSETTINGS__C.getOrgDefaults();
            Integer timeOut = Integer.ValueOf(velo.ProcessAPI_Timeout__c);
            String GUID = GUIDGenerator.getGUID();
            req.setHeader(velo.Request_GUID_Name__c,GUID);
            req.setEndpoint(velo.FundingEndpoint__c+'api/process/verifyAccount');
            req.setHeader('Authorization','Bearer '+Token);
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('Accept','application/json');
            req.setTimeout(timeOut);
            //req.setTimeout(120*1000);
            req.setMethod('POST');
            
            req.setBody(json);
            HttpResponse res = h.send(req);
            responseJson = res.getBody();
            system.debug('Res is '+res.getbody());  

            DebugLogger.InsertDebugLog(appId, CallOut +', GUID: '+ GUID, 'FundingVerifyAccountOLB Request GUID');
            //DebugLogger.InsertDebugLog(appId, req.getBody(), +Callout+' Request');
            //DebugLogger.InsertDebugLog(appId, res.getBody(), +Callout+' Response'); 
        } catch (Exception e) {
            system.debug('exception'+e+' '+e.getLineNumber());
        }

        return responseJSON;
    }
}