/*********************************************
@Author : Nag
@Date : 9/24/2018
@Description : This will schedule DataPurgeingBatchJob.
**********************************************/
global class DataPurgingBatchJob_Sch implements Schedulable {
    
    global void execute(SchedulableContext sc) {

         for(Data_Retention_Settings__mdt dataRetention : [SELECT DeveloperName,Id,Is_Active__c,Label,Related_Query__c, sObject_Name__c FROM Data_Retention_Settings__mdt where Is_Active__c = true]){
            system.debug('Label: '+dataRetention.Label);
            DataPurgingBatchJob appJob = new DataPurgingBatchJob(dataRetention.Label,dataRetention);
            ID batchprocessid1 = Database.executeBatch(appJob);
        } 
   }
    
}