@isTest
private class URLGeneratorTest {

@isTest
	private static void testSuccess() {
	    
	    Document document;
        document = new Document();
        document.Body = Blob.valueOf('Some Text');
        document.ContentType = 'application/pdf';
        document.DeveloperName = 'mydocument';
        document.IsPublic = true;
        document.IsInternalUseOnly=false;
        document.Name = 'mydocument';
        document.Type ='pdf';
        document.FolderId = [select id from folder where name = 'disclosure'].id;
        insert document;
	    
        Map<String, String> resultData = new Map<String, String>();
        Map<String, String> data = new Map<String, String>();
        data.put('filename','mydocument');
        data.put('extension','pdf');
        Test.startTest(); 
        URLGenerator uRLGeneratorObj=new URLGenerator();
        resultData = uRLGeneratorObj.main(data);
        system.assertEquals('200', resultData.get('statusCode'));
        Test.stopTest();  
	}

	private static void testFailure() {
        Document document;
        document = new Document();
        document.Body = Blob.valueOf('Some Text');
        document.ContentType = 'application/pdf';
        document.DeveloperName = 'mydocument1';
        document.IsPublic = true;
        document.IsInternalUseOnly=false;
        document.Name = 'mydocument1';
        document.Type ='pdf';
        document.FolderId = [select id from folder where name = 'disclosure'].id;
        insert document;
        
        Map<String, String> resultData = new Map<String, String>();
        Map<String, String> data = new Map<String, String>();
        data.put('filename','mydocument2');
        data.put('extension','pdf');
        Test.startTest(); 
        URLGenerator uRLGeneratorObj=new URLGenerator();
        resultData = uRLGeneratorObj.main(data);
        system.assertEquals('0', resultData.get('statusCode'));
        Test.stopTest();  
	}
}