trigger ApplicationTrigger on TF4SF__Application__c (before update,after update) {
    ApplicationTriggerHandler appHandler = new ApplicationTriggerHandler();
    TriggerHandler th = new TriggerHandler();
    if(trigger.isInsert) {
      
      if(trigger.isBefore) {
        //Lead Score Functionality
        //th.LeadScoring(trigger.new);
        //th.LeadScoreQueueAssignement(trigger.new);
        //th.LeadScoreUserAssignement(trigger.new);
      }
    }

    if (Trigger.isUpdate) {
        
        if (Trigger.isBefore) {
            if(CheckRecursive.isRecursive) {
                CheckRecursive.isRecursive = false;
    
                //method sends an email to the MLO and Applicant on abandoned 
                
                System.debug('Line11');
    
                List<TF4SF__Application__c  > apList = new List<TF4SF__Application__c  >();
                //for(TF4SF__Application__c  ap : Trigger.New) {
             	//System.debug('ap---->'+ap+''+ap.Id);
                	//if(ap.TF4SF__Product__c !=null && ap.TF4SF__Application_Status__c!=null) {
                 		//System.debug('product---->'+ap.TF4SF__Product__c);
                    	//if(ap.TF4SF__Product__c.contains('Home Loan')) {                           
                       		//if(ap.TF4SF__Application_Status__c== 'Submitted') {
                        		appHandler.sendSubmissionEmail(Trigger.New); 
                       		//}
                        	//if(ap.TF4SF__Application_Status__c== 'Abandoned') {
                        		appHandler.sendAbandonedEmail(Trigger.New); 
                            //}
                        //}  
                    	//else if(ap.TF4SF__Product__c.contains('All') || ap.TF4SF__Product__c.contains('Checking') || ap.TF4SF__Product__c.contains('Savings'))
                    	//{
                            //Send abandon email
                            //if(ap.TF4SF__Application_Status__c== 'Abandoned') 
                            //{                            
                                appHandler.sendAbandonedEmailForDeposit(Trigger.new); 
                            //}
                            //else if(ap.TF4SF__Primary_Product_Status__c== 'Approved')
                            //{
                            //    //Send confir]mation email
                            //    appHandler.sendConfirmationEmailForDeposit(Trigger.new);
                            //}
                            //  else if(ap.TF4SF__Primary_Product_Status__c== 'Declined')
                            //{
                            //    //Send confirmation email
                            //    appHandler.sendDeclinedEmailForDeposit(Trigger.new);
                            //}
                        //}                       
                	//}
                //}

                //Lead Score Functionality
                //th.LeadScoring(trigger.new);
                //th.LeadScoreQueueAssignement(trigger.new);
                //th.LeadScoreUserAssignement(trigger.new);

            }  

           /*List<TF4SF__Application__c  > apList = new List<TF4SF__Application__c  >();
           for(TF4SF__Application__c  ap : Trigger.New) {
           if(ap.TF4SF__Product__c !=null && ap.TF4SF__Application_Status__c!=null) {
               if(ap.TF4SF__Product__c.contains('Home Loan') && ap.TF4SF__Application_Status__c=='Open') {
               ap.Open_To_Abandoned_DateTime__c=null;
               ap.Status_Open_DateTime__c=dateTime.valueOf(System.Now());
               apList.add(ap);
               }
               }
           } */
            
        }
        
        if (Trigger.isAfter) {
            //method sends the email to MLO and th applicant on submission of the application
           // appHandler.confirmationEmailMLO(Trigger.New);

            //On change of owner send an email
            List<TF4SF__Application__c> userAppList = new List<TF4SF__Application__c>();
            Map<string,string> newMap = new Map<string,string>();
            Set<id> userSet = new Set<id>();
            Set<id> appIdSet= new Set<id>();
            for(TF4SF__Application__c app : Trigger.new){
            if(Trigger.oldMap.get(app.id).ownerId!=null && app.ownerId!=null) {
            //System.debug('Trigger.oldMap.get(app.id).ownerId ' + Trigger.oldMap.get(app.id).ownerId );
                //  System.debug('app.ownerId'+app.owner.id);
                  if (String.isNotBlank(app.TF4SF__Product__c)) {  
                      if( app.TF4SF__Product__c.contains('Home Loan')   && Trigger.oldMap.get(app.id).ownerId != app.ownerId &&CheckRecursive.isRecursiveSendMail){
                          System.debug('Trigger.oldMap.get(app.id)app.ownerId ' + Trigger.oldMap.get(app.id).ownerId );
                          System.debug('app.ownerId'+app.owner.Email);
                          CheckRecursive.isRecursiveSendMail=false;
                          userAppList.add(app);
                          userSet.add(app.ownerId);
                          appIdSet.add(app.Id);
        
                      }
                  }
              }

            }
            List<User> userList = [select id,email from User where id in : userSet];
            for(User u: userList){
              newMap.put(String.valueOf(u.id),u.email);
            }
            
            if(userAppList.size() > 0) {
              //appHandler.changeUserNotification(userAppList,newMap);
            }
            
            
             
        }
    }
}